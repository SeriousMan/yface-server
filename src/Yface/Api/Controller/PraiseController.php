<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Exception\PraiseException;
use Yface\Library\Localization\Lang;
use Yface\Library\Serializer;
use Yface\Service\Praise\PraiseService;

class PraiseController
{
    public function index(Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));

        $praises = PraiseService::getUserPraises($user_idx, $app_type);

        $json_string = (new Serializer())->serializeWithJson($praises);

        return new Response($json_string, 200, ['Content-Type' => 'application/json']);
    }

    public function save(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $weeks = intval(trim($request->request->get('weeks', 0)));
        $goal_score = intval(trim($request->request->get('goalScore')));
        $present = trim($request->request->get('present'));
        $promises = $request->request->get('promises');
        $app_type = trim($request->headers->get('app'));

        try {
            PraiseService::saveUserPraise(
                $user_idx,
                $weeks,
                $goal_score,
                $present,
                $promises,
                $app_type
            );

            return new Response('', 200);
        } catch (PraiseException $pe) {
            return $app->json(['message' => Lang::get($pe->getMessage())], 400);
        }
    }
}
