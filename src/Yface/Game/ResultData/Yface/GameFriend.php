<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [5] - FRIEND_반갑다 친구야
 */
class GameFriend extends ResultData
{
    protected $example_count;
    protected $ox_count;
    protected $xo_count;
    protected $origin_answer_face;
    protected $user_answer_face;
    protected $accuracy_by_move_level_list;

    public function __construct($results)
    {
        parent::__construct(GameCode::FRIEND, $results);

        /** 게임 정보 저장 */
        $this->example_count = $results['exampleCount'];
        $this->ox_count = $results['oxCount'];
        $this->xo_count = $results['xoCount'];
        $this->accuracy_by_move_level_list = implode('|', $results['accuracyByMoveLevel']);
        $this->origin_answer_face = implode('|', $results['originAnswerFaceList']);
        $this->user_answer_face = implode('|', $results['userAnswerFaceList']);
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'example_count' => $this->example_count,
            'ox_count' => $this->ox_count,
            'xo_count' => $this->xo_count,
            'accuracy_by_move_level_list' => $this->accuracy_by_move_level_list,
            'origin_answer_face' => $this->origin_answer_face,
            'user_answer_face' => $this->user_answer_face
        ]);
    }
}
