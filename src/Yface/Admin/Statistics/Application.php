<?php
namespace Yface\Admin\Statistics;

use Yface\Game\GameCode;
use Yface\Library\Database\ConnectionProvider;

class Application
{
    /**
     * 통계에 필요한 데이터들을 조합하여 리턴
     * ===
     * @param $app_type
     * @return array
     */
    public static function buildStat($app_type, $research_idx)
    {
        if(empty($research_idx)) {
            $sql = 'SELECT count(idx) FROM yf_user WHERE is_disabled = 0';
            $db = ConnectionProvider::getConnection();
            $total_user_count = $db->fetchColumn($sql);
        }
        // FIXME: SQL 인젝션 공격에 취약하다.
        else {
            $sql = 'SELECT count(u.idx) FROM yf_user u JOIN yf_user_research_status urs ON u.idx = urs.u_idx WHERE u.is_disabled = 0 AND urs.r_idx = ' . $research_idx;
            $db = ConnectionProvider::getConnection();
            $total_user_count = $db->fetchColumn($sql);
        }

        if(empty($research_idx)) {
            $sql = 'SELECT count(idx) FROM yf_user WHERE is_disabled = 0 AND ' . strtolower($app_type) . '_survey_completed_date IS NOT NULL';
            $survey_completed_count = $db->fetchColumn($sql);
        }
        else {
            $sql = 'SELECT count(u.idx) FROM yf_user u JOIN yf_user_research_status urs ON u.idx = urs.u_idx WHERE u.is_disabled = 0 AND ' . strtolower($app_type) . '_survey_completed_date IS NOT NULL' . ' AND urs.r_idx = ' . $research_idx;
            $survey_completed_count = $db->fetchColumn($sql);
        }

        if(empty($research_idx)) {
            $sql = 'SELECT count(idx) FROM yf_user WHERE is_disabled = 0 AND ' . strtolower($app_type) . '_praise_start_date IS NOT NULL';
            $praise_start_count = $db->fetchColumn($sql);
        }
        else {
            $sql = 'SELECT count(u.idx) FROM yf_user u JOIN yf_user_research_status urs ON u.idx = urs.u_idx WHERE is_disabled = 0 AND ' . strtolower($app_type) . '_praise_start_date IS NOT NULL' . ' AND urs.r_idx = ' . $research_idx;
            $praise_start_count = $db->fetchColumn($sql);
        }

        // 테마별 유저 선택 정보
        if(empty($research_idx)) {
            $sql = 'SELECT count(idx) FROM yf_user_theme WHERE app_type = ? AND theme_code = ?';
        }
        else {
            $sql = 'SELECT count(ut.idx) FROM yf_user_theme ut JOIN yf_user_research_status urs ON ut.u_idx = urs.u_idx WHERE ut.app_type = ? AND ut.theme_code = ?' . ' AND urs.r_idx = ' . $research_idx;
        }

        $user_themes = [];
        for ($i = 1; $i <= 4; $i++) {
            $user_themes[] = $db->fetchColumn($sql, [$app_type, $i]);
        }

        // 앱 타입별 게임 평균 소요 시간 관련
        $play_time = 0;
        $game_codes = GameCode::getAllGameCode();
        foreach ($game_codes as $game_code) {
            if (strtolower(GameCode::getGameAppType($game_code)) != strtolower($app_type)) {
                continue;
            }

            if(empty($research_idx)) {
                $sql = 'SELECT AVG((UNIX_TIMESTAMP(end_time) - UNIX_TIMESTAMP(start_time))) AS avg_time
                        FROM ' . GameCode::getGameTableName($game_code) . ' WHERE end_time is not null';
            }
            else {
                $sql = 'SELECT AVG((UNIX_TIMESTAMP(end_time) - UNIX_TIMESTAMP(start_time))) AS avg_time
                        FROM ' . GameCode::getGameTableName($game_code) . ' g JOIN yf_user_research_status urs ON g.u_idx = urs.u_idx WHERE end_time is not null AND urs.r_idx = ' . $research_idx;
            }

            $play_time += $db->fetchColumn($sql);
        }

        $avg_play_time = intval($play_time / count($game_codes));

        // 10일 이상 사용하지 않은 유저 추출
        if(empty($research_idx)) {
            $sql = 'SELECT u3.idx
                    FROM yf_user u3
                    JOIN (
                        SELECT
                            u.idx,
                            IFNULL(DATEDIFF(NOW(), u.yface_survey_completed_date) -
                            (
                                SELECT count(r.u_idx)
                                FROM (
                                    SELECT DATE(pl.reg_date), pl.u_idx, count(DISTINCT pl.u_idx) as total_count
                                    FROM yf_user_game_play_log pl
                                    WHERE pl.game_code >= ?
                                      AND pl.game_code <= ?
                                    GROUP BY DATE(pl.reg_date)
                                ) r
                                JOIN yf_user u2 ON r.u_idx = u2.idx
                                WHERE u2.idx = u.idx
                                GROUP BY r.u_idx
                            ), 0) as criteria
                        FROM yf_user u
                        WHERE u.' . strtolower($app_type) . '_survey_completed_date IS NOT NULL
                            AND u.is_' . strtolower($app_type) . '_accept = 1
                    ) r2 ON u3.idx = r2.idx
                    WHERE r2.criteria < 10';
        }
        else {
            $sql = 'SELECT u3.idx
                    FROM yf_user u3
                    JOIN (
                        SELECT
                            u.idx,
                            IFNULL(DATEDIFF(NOW(), u.yface_survey_completed_date) -
                            (
                                SELECT count(r.u_idx)
                                FROM (
                                    SELECT DATE(pl.reg_date), pl.u_idx, count(DISTINCT pl.u_idx) as total_count
                                    FROM yf_user_game_play_log pl
                                    WHERE pl.game_code >= ?
                                      AND pl.game_code <= ?
                                    GROUP BY DATE(pl.reg_date)
                                ) r
                                JOIN yf_user u2 ON r.u_idx = u2.idx
                                WHERE u2.idx = u.idx
                                GROUP BY r.u_idx
                            ), 0) as criteria
                        FROM yf_user u
                        WHERE u.' . strtolower($app_type) . '_survey_completed_date IS NOT NULL
                            AND u.is_' . strtolower($app_type) . '_accept = 1
                    ) r2 ON u3.idx = r2.idx
                    JOIN yf_user_research_status urs ON u3.idx = urs.u_idx
                    WHERE r2.criteria < 10 AND urs.r_idx = ' . $research_idx;
        }

        $params = [GameCode::BADUM_TISH, GameCode::WHATS_THE_NUMBER];
        if (strtolower($app_type) == 'yface') {
            $params = [GameCode::CARD, GameCode::TRE];
        }

        $filtered_users = [];
        $users = $db->fetchAll($sql, $params);
        foreach ($users as $user) {
            $filtered_users[] = $user['idx'];
        }

        $avg_game_play_per_day = 0;
        $avg_consecutive_game_play_day = 0;
        if (count($filtered_users) > 0) {
            // 하루 평균 게임 실행 횟수
            $sql = 'SELECT round(avg(r.total_count), 0)
                    FROM (
                        SELECT DATE(pl.reg_date), count(DISTINCT pl.u_idx) as total_count
                        FROM yf_user_game_play_log pl
                        WHERE pl.game_code >= ?
                            AND pl.game_code <= ?
                            AND pl.u_idx IN (' . implode($filtered_users, ',') . ')
                        GROUP BY DATE(pl.reg_date)
                    ) r';
            $avg_game_play_per_day = $db->fetchColumn($sql, $params);

            // 평균 지속 일수
            $sql = 'SELECT DATE(reg_date) as play_date
                    FROM yf_user_game_play_log
                    WHERE game_code >= ?
                        AND game_code <= ?
                        AND u_idx = ?
                    GROUP BY DATE(reg_date)
                    ORDER BY reg_date';
            $consecutive_game_play_day_per_user = [];

            foreach ($filtered_users as $user) {
                /**
                 * @var $criteria_date \DateTime
                 */
                $criteria_date = null;
                $consecutive_game_play_day = 0;
                $max_consecutive_game_play_day = 0;

                $dates = $db->fetchAll($sql, array_merge($params, [$user]));
                foreach ($dates as $date) {
                    if (empty($criteria_date)) {
                        $consecutive_game_play_day++;

                        $criteria_date = new \DateTime($date['play_date']);
                        continue;
                    }

                    $play_date = new \DateTime($date['play_date']);
                    $diff = $criteria_date->diff($play_date);

                    if ($diff->days < 2) {
                        $consecutive_game_play_day++;

                        continue;
                    }

                    if ($max_consecutive_game_play_day < $consecutive_game_play_day) {
                        $max_consecutive_game_play_day = $consecutive_game_play_day;
                    }
                }

                if ($max_consecutive_game_play_day == 0) {
                    continue;
                }

                $consecutive_game_play_day_per_user[] = $max_consecutive_game_play_day;
            }

            if(count($consecutive_game_play_day_per_user) == 0) {
                $avg_consecutive_game_play_day = '-';
            }
            else {
                $avg_consecutive_game_play_day = floor(array_sum($consecutive_game_play_day_per_user) / count($consecutive_game_play_day_per_user));
            }
        }

        $avg_user_titles = [
            round(static::calculateDayToUserTitle($app_type, 0, 50, $research_idx), 1),
            round(static::calculateDayToUserTitle($app_type, 50, 100, $research_idx), 1),
            round(static::calculateDayToUserTitle($app_type, 100, 200, $research_idx), 1),
            round(static::calculateDayToUserTitle($app_type, 200, 300, $research_idx), 1)
        ];

        return compact(
            'total_user_count',
            'survey_completed_count',
            'praise_start_count',
            'user_themes',
            'avg_play_time',
            'avg_game_play_per_day',
            'avg_consecutive_game_play_day',
            'avg_user_titles'
        );
    }

    /**
     * 각 칭호를 획득한 평균 소요 일수 계산
     * ===
     * @param $app_type
     * @param $from
     * @param $to
     *
     * @return float|int
     */
    private static function calculateDayToUserTitle($app_type, $from, $to, $research_idx)
    {
        $db = ConnectionProvider::getConnection();

        if ($from == 0) {
            if(empty($research_idx)) {
                $sql = 'SELECT u_idx
                        FROM yf_user_star
                        WHERE app_type = ?
                        GROUP BY u_idx
                        HAVING count(idx) > 50';
            }
            else {
                $sql = 'SELECT us.u_idx
                        FROM yf_user_star us
                        JOIN yf_user_research_status urs
                        ON us.u_idx = urs.u_idx
                        WHERE app_type = ?
                        AND urs.r_idx = ' . $research_idx . '
                        GROUP BY us.u_idx
                        HAVING count(us.idx) > 50';
            }

            $users = $db->fetchAll($sql, [strtoupper($app_type)]);
            if (count($users) <= 0) {
                return 0;
            }

            $days = [];
            foreach ($users as $user) {
                $sql = "SELECT DATE(reg_date) as date FROM yf_user_star WHERE u_idx = ? AND app_type = ?
                        LIMIT 49, 1";
                $date = $db->fetchColumn($sql, [$user['u_idx'], strtoupper($app_type)]);

                $column = strtolower($app_type) . '_survey_completed_date';
                $sql = "SELECT DATE({$column}) as date FROM yf_user WHERE idx = ?";
                $criteria = $db->fetchColumn($sql, [$user['u_idx']]);

                $date = new \DateTime($date);
                $criteria = new \DateTime($criteria);

                $diff = $date->diff($criteria);

                $days[] = $diff->days;
            }

            return array_sum($days) / count($days);
        }

        if(empty($research_idx)) {
            $sql = "SELECT u_idx
                    FROM yf_user_star
                    WHERE app_type = ?
                    GROUP BY u_idx
                    HAVING count(idx) > {$to}";
        }
        else {
            $sql = "SELECT us.u_idx
                    FROM yf_user_star us
                    JOIN yf_user_research_status urs
                    ON us.u_idx = urs.u_idx
                    WHERE app_type = ?
                    AND urs.r_idx = ' . $research_idx . '
                    GROUP BY us.u_idx
                    HAVING count(us.idx) > {$to}";
        }

        $users = $db->fetchAll($sql, [strtoupper($app_type)]);
        if (count($users) <= 0) {
            return 0;
        }

        $days = [];
        foreach ($users as $user) {
            $to = $to - 1;
            $from = $from - 1;

            $sql = "SELECT DATE(reg_date) as date FROM yf_user_star WHERE u_idx = ? AND app_type = ? LIMIT {$to}, 1";
            $date = $db->fetchColumn($sql, [$user['u_idx'], strtoupper($app_type)]);

            $sql = "SELECT DATE(reg_date) as date FROM yf_user_star WHERE u_idx = ? AND app_type = ? LIMIT {$from}, 1";
            $criteria = $db->fetchColumn($sql, [$user['u_idx'], strtoupper($app_type)]);

            $date = new \DateTime($date);
            $criteria = new \DateTime($criteria);

            $diff = $date->diff($criteria);

            $days[] = $diff->days;
        }

        return array_sum($days) / count($days);
    }
}
