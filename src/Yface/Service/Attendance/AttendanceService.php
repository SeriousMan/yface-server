<?php
namespace Yface\Service\Attendance;

use Doctrine\ORM\EntityManager;
use Yface\DataStore\Attendance\UserAttendanceRepository;
use Yface\DataStore\Game\UserGamePlayRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Library\DateHelper;
use Yface\Model\Attendance\UserAttendance;
use Yface\Service\Game\TodayGameService;
use Yface\Service\Point\PointService;

class AttendanceService
{
    /**
     * 유저의 총 출석도장 갯수 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return int
     */
    public static function getUserTotalAttendance($user_idx, $app_type)
    {
        return UserAttendanceRepository::getRepository()->getUserTotalAttendance($user_idx, $app_type);
    }

    /**
     * 유저에게 출석 도장 부여 (조건에 맞으면 부여함)
     * ===
     * @param $user_idx
     * @param $app_type
     */
    public static function assignAttendanceToUser($user_idx, $app_type)
    {
        if (static::checkIfUserAlreadyObtained($user_idx, $app_type)) {
            return;
        }

        if (!static::checkIfUserObtainAttendanceToday($user_idx, $app_type)) {
            return;
        }

        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            return;
        }

        $user_attendance = new UserAttendance();
        $user_attendance->setUserIdx($user_idx);
        $user_attendance->setDays($user->getDaysFromSurveyCompleted($app_type));
        $user_attendance->setAppType($app_type);
        $user_attendance->setRegDate(new \DateTime('now'));

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user_attendance, $user_idx, $app_type) {
                $em->persist($user_attendance);
                $em->flush($user_attendance);

                $total_point = PointService::getUserTotalPoint($user_idx, $app_type);
                PointService::savePoint(
                    $user_idx,
                    '출석도장 획득 포인트',
                    POINT_PER_ATTENDANCE,
                    $total_point + POINT_PER_ATTENDANCE,
                    $app_type
                );
            }
        );
    }

    /**
     * 이미 출석 도장을 받았는지 여부 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return bool
     */
    public static function checkIfUserAlreadyObtained($user_idx, $app_type)
    {
        $user_attendances = UserAttendanceRepository::getRepository()->findTodayByUserIdx(
            $user_idx,
            DateHelper::getStartTimeOfToday(),
            DateHelper::getEndTimeOfToday(),
            $app_type
        );

        return !empty($user_attendances) || count($user_attendances) > 0;
    }

    /**
     * 오늘 출석 도장을 받을 수 있는지 여부 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return bool
     */
    public static function checkIfUserObtainAttendanceToday($user_idx, $app_type)
    {
        $games = TodayGameService::getTodayGames($app_type);

        return static::checkIfUserAllPlayedOnToday($user_idx, $games);
    }

    /**
     * 유저에게 배정된 오늘의 게임 갯수와 실제 플레이한 게임의 갯수가 같은지 여부 리턴
     * ===
     * @param $user_idx
     * @param $games
     * @return bool
     */
    private static function checkIfUserAllPlayedOnToday($user_idx, $games)
    {
        $today_game_counts = UserGamePlayRepository::getRepository()->getCountByUserIdxAndGameCodes(
            $user_idx,
            $games,
            DateHelper::getStartTimeOfToday(),
            DateHelper::getEndTimeOfToday()
        );

        return ($today_game_counts === count($games));
    }
}
