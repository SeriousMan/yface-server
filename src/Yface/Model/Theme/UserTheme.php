<?php
namespace Yface\Model\Theme;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Yface\Model\User\User;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Theme\UserThemeRepository")
 * @ORM\Table(name="yf_user_theme")
 * @JMS\ExclusionPolicy("none")
 */
class UserTheme
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private $days;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YCOG', 'YFACE')")
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     */
    private $reg_date;

    /**
     * @ORM\ManyToOne(targetEntity="Yface\Model\User\User", inversedBy="user_themes")
     * @ORM\JoinColumn(name="u_idx", referencedColumnName="idx")
     * @JMS\Type("Yface\Model\User\User")
     * @JMS\Exclude
     *
     * @var User
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="Theme")
     * @ORM\JoinColumn(name="theme_code", referencedColumnName="code")
     * @JMS\Type("Yface\Model\Theme\Theme")
     * @JMS\Exclude
     *
     * @var Theme
     */
    private $theme;

    /**
     * @return integer
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * @return integer
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * @param integer $days
     */
    public function setDays($days)
    {
        $this->days = $days;
    }

    /**
     * @return string
     */
    public function getAppType()
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType($app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return Theme
     */
    public function getTheme(): Theme
    {
        return $this->theme;
    }

    /**
     * @param Theme $theme
     */
    public function setTheme(Theme $theme)
    {
        $this->theme = $theme;
    }
}
