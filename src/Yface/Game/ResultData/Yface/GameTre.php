<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [12] - TRE_보물을 찾아라
 */
class GameTre extends ResultData
{
    public function __construct($results)
    {
        parent::__construct(GameCode::TRE, $results);
    }

    /**
     * @return array
     */
    public function export()
    {
        return parent::export();
    }
}
