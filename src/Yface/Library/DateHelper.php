<?php
namespace Yface\Library;

class DateHelper
{
    /**
     * 현재 기준으로 몇일이 지났는지 리턴
     * ===
     * @param \DateTime $datetime
     * @return int
     */
    public static function getDaysFromNow(\DateTime $datetime)
    {
        if (empty($datetime)) {
            return 0;
        }

        $now = time();
        $date_diff = $now - $datetime->getTimestamp();
        $days = (int)floor($date_diff / (60 * 60 * 24));

        return $days + 1;
    }

    /**
     * 자정의 기준이 변경된 오늘의 0시 리턴
     * ===
     * @return \DateTime
     */
    public static function getStartTimeOfToday() {
        $now = new \DateTime('now');

        if (static::isExceedOverDayCriteria($now)) {
            $now->setTime(4, 0, 0);

            return $now;
        }

        $now->setTime(4, 0, 0);
        $now->modify('-1 days');

        return $now;
    }

    /**
     * 자정의 기준이 변경된 오늘의 11시 59분 59초 리턴
     * ===
     * @return \DateTime
     */
    public static function getEndTimeOfToday() {
        $now = new \DateTime('now');

        if (static::isExceedOverDayCriteria($now)) {
            $now->setTime(3, 59, 59);
            $now->modify('+1 days');

            return $now;
        }

        $now->setTime(3, 59, 59);

        return $now;
    }

    /**
     * 자정의 기준이 변경된 상태에서 하루를 넘었는지 여부 판단
     * ===
     * @param \DateTime $datetime
     * @return bool
     */
    private static function isExceedOverDayCriteria(\DateTime $datetime) {
        $criteria = clone $datetime;
        $criteria->setTime(4, 0, 0);

        return $datetime > $criteria;
    }

    /**
     * 유닉스 타임스탬프를 DateTime 객체로 변환하여 리턴
     * ===
     * @param $timestamp
     * @return \DateTime
     */
    public static function convertTimestampToDateTime($timestamp) {
        $datetime = new \DateTime();
        $datetime->setTimestamp($timestamp);

        return $datetime;
    }
}
