<?php

/** 앱 타입 별 상수 */
define('APP_TYPE_YFACE', 'YFACE');
define('APP_TYPE_YCOG', 'YCOG');

/** 서버 정기 점검 관련 상수 처리 */
define('GAME_TEMPORARILY_PAUSE_TIME_START_HOUR', 4);
define('GAME_TEMPORARILY_PAUSE_TIME_START_MINUTE', 0);
define('GAME_TEMPORARILY_PAUSE_TIME_START_SECOND', 0);

define('GAME_TEMPORARILY_PAUSE_TIME_END_HOUR', 4);
define('GAME_TEMPORARILY_PAUSE_TIME_END_MINUTE', 20);
define('GAME_TEMPORARILY_PAUSE_TIME_END_SECOND', 0);

/** 게임 레발 관련 상수 */
define('GAME_LEVEL_UP_PER_STAR', 4); // 레벨 1당 별 개를 획득해야 함
define('GAME_LEVEL_UP_UPPER_BOUND', 15);

/** 포인트 관련 */
define('POINT_PER_STAR', 100);
define('POINT_PER_ATTENDANCE', 100);
