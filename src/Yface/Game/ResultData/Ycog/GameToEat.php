<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [22] - TO_EAT_먹을까 말까
 */
class GameToEat extends ResultData
{
    protected $fresh_count;
    protected $fresh_correct_count;
    protected $fresh_wrong_count;
    protected $fresh_accuracy;
    protected $fresh_no_response_count;
    protected $fresh_average_response_time;

    protected $throw_count;
    protected $throw_correct_count;
    protected $throw_wrong_count;
    protected $throw_accuracy;
    protected $throw_no_response_count;
    protected $throw_average_response_time;

    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::TO_EAT, $results);

        /** 게임 정보 저장 */
        $this->fresh_count = $results['freshCount'];
        $this->fresh_correct_count = $results['freshCorrectCount'];
        $this->fresh_wrong_count = $results['freshWrongCount'];
        $this->fresh_accuracy = $results['freshAccuracy'];
        $this->fresh_no_response_count = $results['freshNoResponseCount'];
        $this->fresh_average_response_time = $results['freshResponseTime'];

        $this->throw_count = $results['throwCount'];
        $this->throw_correct_count = $results['throwCorrectCount'];
        $this->throw_wrong_count = $results['throwWrongCount'];
        $this->throw_accuracy = $results['throwAccuracy'];
        $this->throw_no_response_count = $results['throwNoResponseCount'];
        $this->throw_average_response_time = $results['throwResponseTime'];

        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'fresh_count' => $this->fresh_count,
            'fresh_correct_count' => $this->fresh_correct_count,
            'fresh_wrong_count' => $this->fresh_wrong_count,
            'fresh_accuracy' => $this->fresh_accuracy,
            'fresh_no_response_count' => $this->fresh_no_response_count,
            'fresh_average_response_time' => $this->fresh_average_response_time,

            'throw_count' => $this->throw_count,
            'throw_correct_count' => $this->throw_correct_count,
            'throw_wrong_count' => $this->throw_wrong_count,
            'throw_accuracy' => $this->throw_accuracy,
            'throw_no_response_count' => $this->throw_no_response_count,
            'throw_average_response_time' => $this->throw_average_response_time,

            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
