<?php
namespace Yface\Service\Star;

use Doctrine\ORM\EntityManager;
use Yface\DataStore\Star\UserStarRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Game\GameCode;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Star\UserStar;
use Yface\Service\Game\GameService;
use Yface\Service\Point\PointService;

class StarService
{
    /**
     * 유저의 총 별 갯수 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return int
     */
    public static function getUserTotalStars($user_idx, $app_type)
    {
        return UserStarRepository::getRepository()->getCountByAppType($user_idx, $app_type);
    }

    /**
     * 각 게임별 유저의 총 별 갯수 리턴
     * ===
     * @param $user_idx
     * @param $game_code
     * @return int
     */
    public static function getUserStarsByGame($user_idx, $game_code)
    {
        return UserStarRepository::getRepository()->getCountByGameCode($user_idx, $game_code);
    }

    /**
     * 게임 보상으로 별 부여
     * ===
     * @param $user_idx
     * @param $game_code
     */
    public static function assignStarToUser($user_idx, $game_code)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            return;
        }

        if (static::checkIfUserAlreadyObtained($user_idx, $game_code)) {
            return;
        }

        $user_star = new UserStar();
        $user_star->setUserIdx($user_idx);
        $user_star->setGameCode($game_code);
        $user_star->setDays($user->getDaysFromSurveyCompleted(GameCode::getGameAppType($game_code)));
        $user_star->setAppType(GameCode::getGameAppType($game_code));
        $user_star->setRegDate(new \DateTime('now'));

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user_star, $user_idx, $game_code) {
                $em->persist($user_star);
                $em->flush($user_star);

                PointService::savePoint(
                    $user_idx,
                    GameCode::getGameName($game_code) . ' 별 획득 포인트',
                    POINT_PER_STAR,
                    PointService::getUserTotalPoint($user_idx, GameCode::getGameAppType($game_code)) + POINT_PER_STAR,
                    GameCode::getGameAppType($game_code)
                );
            }
        );
    }

    /**
     * 유저가 해당 게임에서 이미 별을 받았는지 여부 리턴
     * ===
     * @param $user_idx
     * @param $game_code
     * @return bool
     */
    public static function checkIfUserAlreadyObtained($user_idx, $game_code)
    {
        return GameService::isUserTodayGotStarInGame($user_idx, $game_code);
    }

    /**
     * (통계용) 유저의 일별 누적 별 갯수 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getTotalStarsByDays($user_idx, $app_type)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            return [];
        }

        $days = $user->getDaysFromSurveyCompleted($app_type);
        $total_stars = [];
        for ($i = 1; $i <= $days; $i++) {
            $total_stars[] = [
                'days' => $i,
                'stars' => UserStarRepository::getRepository()->getAccumulateCountByUserIdxAndDays(
                    $user_idx,
                    $i,
                    $app_type
                )
            ];
        }

        return $total_stars;
    }
}
