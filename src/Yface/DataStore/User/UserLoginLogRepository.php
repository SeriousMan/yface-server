<?php
namespace Yface\DataStore\User;

use Yface\Library\Database\ConnectionProvider;

class UserLoginLogRepository {
    /**
     * @var $db \Doctrine\DBAL\Connection
     */
    private $db;

    public function __construct()
    {
        $this->db = ConnectionProvider::getConnection();
    }

    /**
     * @param $user_idx
     * @param $days
     * @param $login_ip
     * @param $app_type
     * @param $device_os
     * @param $device_model
     * @return int
     */
    public function insertUserLoginLog($user_idx, $days, $login_ip, $app_type, $device_os, $device_model) {
        $params = array(
            'u_idx' => $user_idx,
            'days' => $days,
            'login_ip' => ip2long($login_ip),
            'app_type' => $app_type,
            'reg_date' => date('Y-m-d H:i:s')
        );

        // 2017-04-25 유대열 추가. (로그인시 기기 로그 남기도록)
        if (!empty($device_os)) {
            $params['device_os'] = $device_os;
        }
        if (!empty($device_model)) {
            $params['device_model'] = $device_model;
        }

        return $this->db->insert('yf_user_login_log', $params);
    }

    /**
     * @return UserLoginLogRepository
     */
    public static function getRepository() {
        return new static();
    }
}
