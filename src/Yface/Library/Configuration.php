<?php
namespace Yface\Library;

class Configuration
{
    /**
     * 설정값 변수 (override)
     * @var array
     */
    public static $values = array();

    /**
     * @param string $filename
     * @throws \Exception
     */
    public static function load($filename)
    {
        $file = file_get_contents($filename);
        $values = json_decode($file, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \Exception('Json Parse Failed.');
        }

        static::$values = array_replace_recursive(static::$values, $values);
    }

    /**
     * @param $name
     * @return array
     */
    public static function getDbParams($name)
    {
        $default_params = array(
            'driver' => 'pdo_mysql',
            'driverOptions' => array(1002 => 'SET NAMES utf8'),

            'charset' => 'utf8'
        );

        $params = static::$values['db'][$name];

        return array_merge($params, $default_params);
    }
}
