<?php
namespace Yface\DataStore\Statistics;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Statistics\StarStatistics;

class StarStatisticsRepository extends EntityRepository
{
    /**
     * @param $app_type
     * @return StarStatistics[]
     */
    public function findByAppType($app_type)
    {
        return $this->findBy(array('app_type' => $app_type), array('idx' => 'ASC'));
    }

    /**
     * @return StarStatisticsRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Statistics\StarStatistics');
    }
}
