<?php
namespace Yface\Model\Feed;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Feed\FeedActionRepository")
 * @ORM\Table(name="yf_feed_action")
 * @JMS\ExclusionPolicy("all")
 */
class FeedAction
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="f_idx", options={"unsigned":true})
     */
    private $feed_idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     */
    private $user_idx;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('LIKE', 'COOL', 'PRETTY', 'ENVIOUS')")
     */
    private $action;
    /**
     * @ORM\Column(type="boolean")
     */
    private $is_active;
    /**
     * @ORM\Column(type="datetime")
     */
    private $reg_date;

    /**
     * @ORM\ManyToOne(targetEntity="Yface\Model\Feed\Feed", inversedBy="feed_actions")
     * @ORM\JoinColumn(name="f_idx", referencedColumnName="idx")
     * @JMS\Type("Yface\Model\Feed\Feed")
     * @JMS\Exclude
     *
     * @var $feed Feed
     */
    private $feed;

    /**
     * @return integer
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * @return integer
     */
    public function getFeedIdx()
    {
        return $this->feed_idx;
    }

    /**
     * @param integer $feed_idx
     */
    public function setFeedIdx($feed_idx)
    {
        $this->feed_idx = $feed_idx;
    }

    /**
     * @return integer
     */
    public function getUserIdx()
    {
        return $this->user_idx;
    }

    /**
     * @param integer $user_idx
     */
    public function setUserIdx($user_idx)
    {
        $this->user_idx = $user_idx;
    }

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @param boolean $is_active
     */
    public function setIsActive($is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return Feed
     */
    public function getFeed(): Feed
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed(Feed $feed)
    {
        $this->feed = $feed;
    }
}
