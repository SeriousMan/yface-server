<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [21] - TAP_TAP_TAP_순서대로 콕콕콕
 */
class GameTapTapTap extends ResultData
{
    protected $from_low_count;
    protected $from_low_correct_count;
    protected $from_low_no_response_count;
    protected $from_high_count;
    protected $from_high_correct_count;
    protected $from_high_no_response_count;

    protected $answer_list;
    protected $user_answer_list;
    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::TAP_TAP_TAP, $results);

        /** 게임 정보 저장 */
        $this->from_low_count = $results['fromLowCount'];
        $this->from_low_correct_count = $results['fromLowCorrectCount'];
        $this->from_low_no_response_count = $results['fromLowNoResponseCount'];
        $this->from_high_count = $results['fromHighCount'];
        $this->from_high_correct_count = $results['fromHighCorrectCount'];
        $this->from_high_no_response_count = $results['fromHighNoResponseCount'];

        $this->answer_list = implode(
            '|',
            array_map(
                function ($value) {
                    return implode(',', $value);
                },
                $results['answerList']
            )
        );
        $this->user_answer_list = implode(
            '|',
            array_map(
                function ($value) {
                    return implode(',', $value);
                },
                $results['userAnswerList']
            )
        );
        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'from_low_count' => $this->from_low_count,
            'from_low_correct_count' => $this->from_low_correct_count,
            'from_low_no_response_count' => $this->from_low_no_response_count,
            'from_high_count' => $this->from_high_count,
            'from_high_correct_count' => $this->from_high_correct_count,
            'from_high_no_response_count' => $this->from_high_no_response_count,

            'answer_list' => $this->answer_list,
            'user_answer_list' => $this->user_answer_list,
            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
