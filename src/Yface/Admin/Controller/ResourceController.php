<?php
namespace Yface\Admin\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Request;
use Yface\Admin\Library\UrlHelper;
use Yface\Game\GameCode;
use Yface\Library\Configuration;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\File\ImageUploader;
use Yface\Library\Template\TwigRenderer;
use Yface\Service\Resource\ResourceService;
use Yface\Library\Database\ConnectionProvider;

/**
 * 로직) 리소스 업데이트
 * 1. 파일명 그대로 사용하도록
 * 2. 같은 파일명을 가지고 있는 파일에 그대로 덮어쓰기
 * 3. 같은 파일 확장자로만 업로드 가능
 * 4. 파일 덮어쓴 후 latest zip achieve -> latest_zip 갱신
 */
class ResourceController
{
    /**
     * 컨트롤러) YFACE 리소스 관리할 게임 목록
     * ===
     * @return string
     */
    public function index()
    {
        $sql = 'SELECT * FROM yf_game WHERE app_type = ? ORDER BY code ASC';
        $db = ConnectionProvider::getConnection();
        $games = $db->fetchAll($sql, ['YFACE']);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'games' => $games
        ]);

        return $twig_renderer->render('admin/resource/index');
    }

    /**
     * 컨트롤러) 게임 별 리소스 상세 목록
     * ===
     * @param Request $request
     * @return string
     */
    public function detail(Request $request)
    {
        $game_code = intval(trim($request->get('gameCode')));
        if (empty($game_code)) {
            return UrlHelper::returnWithMessage('리소스를 관리할 게임 정보가 없습니다.');
        }

        $sql = 'SELECT * FROM yf_resource_file_info WHERE game_code = ?';
        $db = ConnectionProvider::getConnection();
        $resources = $db->fetchAll($sql, [$game_code]);

        $sql = 'SELECT * FROM yf_game WHERE code = ?';
        $game = $db->fetchAssoc($sql, [$game_code]);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'resources' => $resources,
            'game' => $game
        ]);

        return $twig_renderer->render('admin/resource/detail');
    }

    /**
     * 컨트롤러) 리소스 갱신 처리
     * ===
     * @param Request $request
     * @return string
     */
    public function save(Request $request)
    {
        $resource_idx = intval(trim($request->get('resourceIdx')));
        $game_code = intval(trim($request->get('gameCode')));
        $file_name = trim($request->get('fileName'));
        $file = $request->files->get('file');

        if (empty($file) || empty($resource_idx) || empty($game_code) || empty($file_name)) {
            return UrlHelper::returnWithMessage('모든 입력란을 입력해주세요,');
        }

        $file_name_arr = explode('.', $file_name);
        array_pop($file_name_arr);
        $file_name_without_ext = implode('.', $file_name_arr);

        $db = ConnectionProvider::getConnection();
        $db->transactional(
            function (Connection $conn) use ($resource_idx, $game_code, $file_name, $file_name_without_ext, $file) {
                $file_path = Configuration::$values['path']['resources'] . '/games/' . $game_code;
                ImageUploader::upload($file, $file_path, $file_name_without_ext);

                // 업로드 내역 시간 수정
                $conn->update(
                    'yf_resource_file_info',
                    ['mod_date' => date('Y-m-d H:i:s')],
                    ['idx' => $resource_idx, 'game_code' => $game_code]
                );

                // 업데이트 내역 관련 시간 수정
                $conn->update(
                    'yf_resource_update_info',
                    ['latest_mod_date' => date('Y-m-d H:i:s')],
                    ['app_type' => GameCode::getGameAppType($game_code)]
                );

                // 업로드 내역 로깅
                $conn->insert(
                    'yf_resource_file_log',
                    [
                        'f_idx' => $resource_idx,
                        'description' => $file_name . ' 파일 갱신',
                        'reg_date' => date('Y-m-d H:i:s')
                    ]
                );

                ResourceService::zipAllFilesForFirstUpdateUser();
            }
        );

        return UrlHelper::redirect('/admin/resources/detail?gameCode=' . $game_code, '파일이 갱신되었습니다.');
    }
}
