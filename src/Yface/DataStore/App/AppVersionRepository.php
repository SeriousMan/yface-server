<?php
namespace Yface\DataStore\App;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\App\AppVersion;

class AppVersionRepository extends EntityRepository
{
    /**
     * @param $app_type
     * @return null|object|AppVersion
     */
    public function findOneByAppType($app_type)
    {
        return $this->findOneBy(array('app_type' => $app_type));
    }

    /**
     * @return AppVersionRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\App\AppVersion');
    }
}
