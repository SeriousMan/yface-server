<?php
namespace Yface\Model\Feed;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Yface\Model\User\User;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Feed\FeedRepository")
 * @ORM\Table(name="yf_feed")
 * @JMS\ExclusionPolicy("none")
 */
class Feed
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     * @JMS\SerializedName("uIdx")
     */
    private $user_idx;
    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Exclude
     */
    private $img_url;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("likeCount")
     */
    private $like_count;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("coolCount")
     */
    private $cool_count;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("prettyCount")
     */
    private $pretty_count;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("enviousCount")
     */
    private $envious_count;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\Exclude
     */
    private $is_disabled;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YCOG', 'YFACE')")
     * @JMS\Exclude
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\SerializedName("regDate")
     */
    private $reg_date;

    /**
     * 해당 유저가 해당 피드의 액션을 취했는지 알기 위한 정보
     * @JMS\Exclude
     * @var integer
     */
    private $action_user_idx;

    /**
     * @ORM\OneToOne(targetEntity="Yface\Model\User\User")
     * @ORM\JoinColumn(name="u_idx", referencedColumnName="idx")
     * @JMS\Type("Yface\Model\User\User")
     * @JMS\Exclude
     *
     * @var $user User
     */
    private $user;
    /**
     * @ORM\OneToMany(targetEntity="Yface\Model\Feed\FeedAction", mappedBy="feed")
     * @JMS\Type("ArrayCollection<Yface\Model\Feed\FeedAction>")
     * @JMS\Exclude
     * @var $feed_actions ArrayCollection
     */
    private $feed_actions;

    public function __construct()
    {
        $this->feed_actions = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * @return integer
     */
    public function getUserIdx()
    {
        return $this->user_idx;
    }

    /**
     * @param integer $user_idx
     */
    public function setUserIdx($user_idx)
    {
        $this->user_idx = $user_idx;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("nickname")
     * @JMS\Type("string")
     */
    public function getUserNickname()
    {
        return $this->user->getNickname();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("imgUrl")
     * @JMS\Type("string")
     *
     * @return string
     */
    public function getImgUrl()
    {
        return empty($this->img_url) ? null : 'http://' . $this->img_url;
    }

    /**
     * @param string $img_url
     */
    public function setImgUrl($img_url)
    {
        $this->img_url = $img_url;
    }

    /**
     * @return integer
     */
    public function getLikeCount()
    {
        return $this->like_count;
    }

    /**
     * @param integer $like_count
     */
    public function setLikeCount($like_count)
    {
        $this->like_count = $like_count;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isLikeActive")
     * @JMS\Type("boolean")
     */
    public function getIsLikeActive()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('user_idx', $this->action_user_idx))
            ->andWhere(Criteria::expr()->eq('action', 'LIKE'));

        $feed_actions = $this->feed_actions->matching($criteria);
        if ($feed_actions->count() === 0) {
            return false;
        }

        /** @var $feed_action FeedAction */
        $feed_action = $feed_actions->first();

        return $feed_action->getIsActive();
    }

    /**
     * @return integer
     */
    public function getCoolCount()
    {
        return $this->cool_count;
    }

    /**
     * @param integer $cool_count
     */
    public function setCoolCount($cool_count)
    {
        $this->cool_count = $cool_count;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isCoolActive")
     * @JMS\Type("boolean")
     */
    public function getIsCoolActive()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('user_idx', $this->action_user_idx))
            ->andWhere(Criteria::expr()->eq('action', 'COOL'));

        $feed_actions = $this->feed_actions->matching($criteria);
        if ($feed_actions->count() === 0) {
            return false;
        }

        /** @var $feed_action FeedAction */
        $feed_action = $feed_actions->first();

        return $feed_action->getIsActive();
    }

    /**
     * @return integer
     */
    public function getPrettyCount()
    {
        return $this->pretty_count;
    }

    /**
     * @param integer $pretty_count
     */
    public function setPrettyCount($pretty_count)
    {
        $this->pretty_count = $pretty_count;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isPrettyActive")
     * @JMS\Type("boolean")
     */
    public function getIsPrettyActive()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('user_idx', $this->action_user_idx))
            ->andWhere(Criteria::expr()->eq('action', 'PRETTY'));

        $feed_actions = $this->feed_actions->matching($criteria);
        if ($feed_actions->count() === 0) {
            return false;
        }

        /** @var $feed_action FeedAction */
        $feed_action = $feed_actions->first();

        return $feed_action->getIsActive();
    }

    /**
     * @return integer
     */
    public function getEnviousCount()
    {
        return $this->envious_count;
    }

    /**
     * @param integer $envious_count
     */
    public function setEnviousCount($envious_count)
    {
        $this->envious_count = $envious_count;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isEnviousActive")
     * @JMS\Type("boolean")
     */
    public function getIsEnviousActive()
    {
        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('user_idx', $this->action_user_idx))
            ->andWhere(Criteria::expr()->eq('action', 'ENVIOUS'));

        $feed_actions = $this->feed_actions->matching($criteria);
        if ($feed_actions->count() === 0) {
            return false;
        }

        /** @var $feed_action FeedAction */
        $feed_action = $feed_actions->first();

        return $feed_action->getIsActive();
    }

    /**
     * @return boolean
     */
    public function getIsDisabled()
    {
        return $this->is_disabled;
    }

    /**
     * @param boolean $is_disabled
     */
    public function setIsDisabled($is_disabled)
    {
        $this->is_disabled = $is_disabled;
    }

    /**
     * @return string
     */
    public function getAppType()
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType($app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @param integer $action_user_idx
     */
    public function setActionUserIdx($action_user_idx)
    {
        $this->action_user_idx = $action_user_idx;
    }

    /**
     * @return User
     */
    public function getUser() : User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return ArrayCollection
     */
    public function getFeedActions(): ArrayCollection
    {
        return $this->feed_actions;
    }

    /**
     * @param ArrayCollection $feed_actions
     */
    public function setFeedActions(ArrayCollection $feed_actions)
    {
        $this->feed_actions = $feed_actions;
    }

    /**
     * 액션 카운트 비정규화 필드 반영
     * ===
     * @param $action
     * @param $is_active
     */
    public function applyActionCount($action, $is_active)
    {
        switch ($action) {
            case 'LIKE':
                $this->like_count += ($is_active) ? 1 : -1;
                break;
            case 'COOL':
                $this->cool_count += ($is_active) ? 1 : -1;
                break;
            case 'PRETTY':
                $this->pretty_count += ($is_active) ? 1 : -1;
                break;
            case 'ENVIOUS':
                $this->envious_count += ($is_active) ? 1 : -1;
                break;
        }
    }
}
