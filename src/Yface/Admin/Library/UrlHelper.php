<?php
namespace Yface\Admin\Library;

class UrlHelper
{
    /**
     * 리다이렉트 (with ALERT MSG)
     * ===
     * @param $url
     * @param $msg null|string
     * @return string
     */
    public static function redirect($url, $msg = null)
    {
        $html = '<!doctype html><html><head><meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"></head><body><script>';
        if (!empty($msg)) {
            $html .= "alert(" . json_encode($msg) . ");";
        }
        $html .= "location.href=" . json_encode($url) . ";";
        $html .= "</script></body></html>\n";

        return $html;
    }

    /**
     * 이전 페이지로 (with ALERT MSG)
     * ===
     * @param string $msg
     * @return string
     */
    public static function returnWithMessage($msg)
    {
        $html = '<!doctype html><html><head><meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1"></head><body><script>';
        if (!empty($msg)) {
            $html .= "alert(" . json_encode($msg) . ");";
        }
        $html .= "history.go(-" . 1 . ")";
        $html .= "</script></body></html>\n";

        return $html;
    }
}
