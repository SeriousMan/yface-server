<?php
namespace Yface\Admin\Middleware\Controller;

use Symfony\Component\HttpFoundation\Response;
use Yface\Admin\Library\UrlHelper;
use Yface\Admin\User\UserSession;

class PreAuthenticator {
    /**
     * 관리자 로그인 검증 (로그인 안되어 있으면 로그인 페이지로)
     * ===
     * @return \Closure
     */
    public static function authenticate() {
        return function () {
            if (!UserSession::isLoggedIn()) {
                return new Response(UrlHelper::redirect('/admin/sign-in'));
            }

            return null;
        };
    }

    /**
     * 로그인이 이미 되어있으면 메인으로 이동하도록 유도
     * ===
     * @return \Closure
     */
    public static function checkIfLoggedInAndGoToMain() {
        return function () {
            if (UserSession::isLoggedIn()) {
                return new Response(UrlHelper::redirect('/admin/users'));
            }

            return null;
        };
    }

    private static function makeQueryString($arr) {
        $query_string = '?';

        foreach ($arr as $key => $value) {
            $query_string .= $key . '=' . $value . '&';
        }
        return trim($query_string, '&');
    }

    public static function authorize($app) {
        return function ($app) {
            $research_idx = $app->get('research');
            $query_array = $app->query->all();

            if(UserSession::getLoginRole() == 'admin') {
                if(!empty($research_idx)) {
                    UserSession::setCurRIdx($research_idx);
                }
                else {
                    // admin 계정은 전체사용자를 볼 수 있어야 해서 특별히 취급한다.
                    if(strpos($app->server->get('REQUEST_URI'), '/admin/users') === 0) {
                        UserSession::unsetCurRIdx();
                        return null;
                    }
                    if(!empty(UserSession::getCurRIdx())) {
                        $query_array['research'] = UserSession::getCurRIdx();
                        return new Response(UrlHelper::redirect(self::makeQueryString($query_array)));
                    }
                }
            }
            else if(UserSession::getLoginRole() == 'researcher') {
                $research_idxes = UserSession::getLoginRIdxes();

                if(!empty($research_idx)) {
                    if(!in_array($research_idx, $research_idxes)) {
                        UserSession::setCurRIdx($research_idxes[0]);
                        $query_array['research'] = $research_idxes[0];
                        return new Response(UrlHelper::redirect(self::makeQueryString($query_array)));
                    }
                    else {
                        UserSession::setCurRIdx($research_idx);
                    }
                }
                else {
                    if(!empty(UserSession::getCurRIdx())) {
                        $query_array['research'] = UserSession::getCurRIdx();
                        return new Response(UrlHelper::redirect(self::makeQueryString($query_array)));
                    }
                    else {
                        UserSession::setCurRIdx($research_idxes[0]);
                        $query_array['research'] = $research_idxes[0];
                        return new Response(UrlHelper::redirect(self::makeQueryString($query_array)));
                    }
                }
            }

            return null;
        };
    }
}
