<?php
namespace Yface\DataStore\Resource;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Resource\ResourceFileInfo;

class ResourceFileInfoRepository extends EntityRepository {
    /**
     * @param $app_type
     * @param $datetime
     * @param $files
     * @return ResourceFileInfo[]
     */
    public function findByAppTypeAndDateTime($app_type, $datetime, $files) {
        $qb = $this->createQueryBuilder('rf');
        $qb->where('rf.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere('rf.mod_date > :datetime')
            ->setParameter('datetime', $datetime, Type::DATETIME);

        if (count($files) > 0) {
            $qb->orWhere($qb->expr()->in('rf.idx', $files));
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ResourceFileInfoRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Resource\ResourceFileInfo');
    }
}
