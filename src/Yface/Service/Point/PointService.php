<?php
namespace Yface\Service\Point;

use Doctrine\ORM\EntityManager;
use Yface\DataStore\Point\UserPointDailyRepository;
use Yface\DataStore\Point\UserPointRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\PointException;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Point\UserPoint;
use Yface\Model\Point\UserPointDaily;

class PointService
{
    /**
     * 유저의 현재 포인트 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return int
     */
    public static function getUserTotalPoint($user_idx, $app_type)
    {
        return UserPointRepository::getRepository()->getUserTotalPoint($user_idx, $app_type);
    }

    /**
     * 포인트 정보 저장
     * ===
     * @param $user_idx
     * @param $desc
     * @param $amount
     * @param $total_amount
     * @param $app_type
     * @throws PointException
     */
    public static function savePoint($user_idx, $desc, $amount, $total_amount, $app_type)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            throw new PointException('point.user.incorrect');
        }

        /** 출석 도장 365개가 넘어가면 포인트 지급 X */
        if ($user->getUserAttendances() > 365) {
            return;
        }

        $user_point = new UserPoint();
        $user_point->setUserIdx($user_idx);
        $user_point->setDesc($desc);
        $user_point->setAmount($amount);
        $user_point->setTotalAmount($total_amount);
        $user_point->setAppType($app_type);

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user, $user_point, $app_type) {
                $em->persist($user_point);
                $em->flush($user_point);

                $amount = $user_point->getAmount();
                $obtain_point = ($amount > 0) ? $amount : 0;
                $use_point = ($amount > 0) ? 0 : abs($amount);

                static::logDailyUserPoint(
                    $user->getIdx(),
                    $user->getDaysFromSurveyCompleted($app_type),
                    $obtain_point,
                    $use_point,
                    $user_point->getAppType()
                );
            }
        );
    }

    /**
     * 하루 동안의 포인트 사용 내용 로깅
     * ===
     * @param $user_idx
     * @param $days
     * @param $obtain_point
     * @param $use_point
     * @param $app_type
     */
    public static function logDailyUserPoint($user_idx, $days, $obtain_point, $use_point, $app_type)
    {
        $user_point_daily = UserPointDailyRepository::getRepository()->findOneByUserIdxAndDays($user_idx, $days, $app_type);
        if (empty($user_point_daily)) {
            $user_point_daily = new UserPointDaily();
            $user_point_daily->setUserIdx($user_idx);
            $user_point_daily->setDays($days);
            $user_point_daily->setObtainPoint($obtain_point);
            $user_point_daily->setUsePoint($use_point);
            $user_point_daily->setTotalPoint($obtain_point - $use_point);
            $user_point_daily->setAppType($app_type);
        } else {
            $obtain_point = $user_point_daily->getObtainPoint() + $obtain_point;
            $use_point = $user_point_daily->getUsePoint() + $use_point;

            $user_point_daily->setObtainPoint($obtain_point);
            $user_point_daily->setUsePoint($use_point);
            $user_point_daily->setTotalPoint($obtain_point - $use_point);
        }

        $em = EntityManagerProvider::getEntityManager();
        $em->persist($user_point_daily);
        $em->flush($user_point_daily);
    }

    /**
     * 유저의 일별 포인트 기록 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return \Yface\Model\Point\UserPointDaily[]
     */
    public static function getDailyUserPoint($user_idx, $app_type)
    {
        $logs = UserPointDailyRepository::getRepository()->findByUserIdx($user_idx, $app_type);
        if (empty($logs)) {
            return array();
        }

        return $logs;
    }

    /**
     * 유저의 일별 누적 포인트 기록 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getTotalPointsByDays($user_idx, $app_type)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            return array();
        }

        $days = $user->getDaysFromSurveyCompleted($app_type);
        $total_points = array();
        for ($i = 1; $i <= $days; $i++) {
            $total_points[] = array(
                'days' => $i,
                'points' => UserPointDailyRepository::getRepository()->getAccumulatePointByUserIdxAndDays(
                    $user_idx,
                    $i,
                    $app_type
                )
            );
        }

        return $total_points;
    }
}
