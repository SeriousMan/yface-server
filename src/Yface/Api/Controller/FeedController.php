<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Exception\FeedException;
use Yface\Library\Localization\Lang;
use Yface\Library\Serializer;
use Yface\Service\Feed\FeedService;

class FeedController
{
    public function create(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));
        $image_file = $request->files->get('image');

        try {
            $feed = FeedService::createFeed($user_idx, $app_type, $image_file);

            $json_string = (new Serializer())->serializeWithJson($feed);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (FeedException $fe) {
            return $app->json(['message' => Lang::get($fe->getMessage())], 400);
        }
    }

    public function remove(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $feed_idx = intval(trim($request->request->get('fIdx')));

        try {
            FeedService::removeFeed($user_idx, $feed_idx);

            return new Response('', 200);
        } catch (FeedException $fe) {
            return $app->json(['message' => Lang::get($fe->getMessage())], 400);
        }
    }

    public function doAction(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $feed_idx = trim($request->request->get('fIdx'));
        $action = trim($request->request->get('action'));

        try {
            $feed = FeedService::saveAction($feed_idx, $user_idx, $action);

            $json_string = (new Serializer())->serializeWithJson($feed);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (FeedException $fe) {
            return $app->json(['message' => Lang::get($fe->getMessage())], 400);
        }
    }

    public function getUserFeeds(Request $request)
    {
        $user_idx = intval(trim($request->request->get('uIdx')));
        $app_type = trim($request->headers->get('app'));

        $feeds = FeedService::getUserFeeds($user_idx, $app_type);

        $json_string = (new Serializer())->serializeWithJson($feeds);

        return new Response($json_string, 200, ['Content-Type' => 'application/json']);
    }

    public function getFriendsFeeds(Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));

        $feeds = FeedService::getFriendsFeeds($user_idx, $app_type);

        $json_string = (new Serializer())->serializeWithJson($feeds);

        return new Response($json_string, 200, ['Content-Type' => 'application/json']);
    }
}
