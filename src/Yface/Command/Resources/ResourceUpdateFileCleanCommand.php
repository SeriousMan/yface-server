<?php
namespace Yface\Command\Resources;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Library\Configuration;

class ResourceUpdateFileCleanCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('resources:update:clean')
            ->setDescription('매일 새벽 리소스 업데이트 파일 제거');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('RESOURCES UPDATE CLEAN LOG - 매일 새벽 리소스 업데이트 파일 제거 CRON 시작');

        $files = glob(Configuration::$values['path']['resources'] . '/updates/' . '1*.zip');
        foreach ($files as $file) {
            unlink($file);
        }

        static::report('RESOURCES UPDATE CLEAN LOG - 매일 새벽 리소스 업데이트 파일 제거 CRON 종료');
    }
}
