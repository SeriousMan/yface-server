<?php
namespace Yface\Admin\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Request;
use Yface\Admin\Library\UrlHelper;
use Yface\Game\GameCode;
use Yface\Game\GameLevel;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\Template\TwigRenderer;
use Yface\Service\Game\TodayGameService;

class GameController
{
    /**
     * 컨트롤러) 앱 별 게임 목록
     * ===
     * @param Request $request
     * @return string
     */
    public function index(Request $request)
    {
        $app_type = trim($request->get('app'));
        if (empty($app_type)) {
            return UrlHelper::returnWithMessage('잘못된 경로입니다.');
        }

        $sql = 'SELECT * FROM yf_game WHERE app_type = ? ORDER BY code ASC';

        $db = ConnectionProvider::getConnection();
        $games = $db->fetchAll($sql, array($app_type));

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'app_type' => $app_type,
            'games' => $games,
            'today_games' => TodayGameService::getTodayGames($app_type)
        ]);

        return $twig_renderer->render('admin/game/index');
    }

    /**
     * 컨트롤러) 게임 활성화 처리
     * ===
     * @param Request $request
     * @return string
     */
    public function activate(Request $request)
    {
        $ip = $request->getClientIp();
        $game_code = intval(trim($request->get('gameCode')));
        if (empty($game_code)) {
            return UrlHelper::returnWithMessage('활성화 할 수 있는 게임이 없습니다.');
        }

        $db = ConnectionProvider::getConnection();
        $db->transactional(
            function (Connection $conn) use ($game_code, $ip) {
                /** 게임 활성화 */
                $conn->update(
                    'yf_game',
                    array('is_disabled' => 0, 'is_selected' => 1),
                    array('code' => $game_code)
                );

                /** 게임 활성화 로깅 */
                $conn->insert(
                    'yf_game_log',
                    array(
                        'game_code' => $game_code,
                        'description' => '[' . $ip . '] ' . GameCode::getGameName($game_code) . ' 활성화',
                        'is_disabled' => 0,
                        'reg_date' => date('Y-m-d H:i:s')
                    )
                );
            }
        );

        return UrlHelper::redirect(
            '/admin/games?app=' . GameCode::getGameAppType($game_code),
            '게임이 활성화되었습니다.'
        );
    }

    /**
     * 컨트롤러) 게임 비활성화 처리
     * ===
     * @param Request $request
     * @return string
     */
    public function deactivate(Request $request)
    {
        $ip = $request->getClientIp();
        $game_code = intval(trim($request->get('gameCode')));
        if (empty($game_code)) {
            return UrlHelper::returnWithMessage('비활성화 할 수 있는 게임이 없습니다.');
        }

        $sql = 'SELECT count(code) FROM yf_game WHERE app_type = ? AND is_disabled = 0';
        $db = ConnectionProvider::getConnection();
        $active_game_count = $db->fetchColumn($sql, array(GameCode::getGameAppType($game_code)));

        if ($active_game_count <= 7) {
            return UrlHelper::returnWithMessage('비활성화 게임 갯수는 5개를 넘길 수 없습니다.');
        }

        $db->transactional(
            function (Connection $conn) use ($game_code, $ip) {
                /** 게임 비활성화 */
                $conn->update(
                    'yf_game',
                    array('is_disabled' => 1, 'is_selected' => 1),
                    array('code' => $game_code)
                );

                /** 게임 비활성화 로깅 */
                $conn->insert(
                    'yf_game_log',
                    array(
                        'game_code' => $game_code,
                        'description' => '[' . $ip . '] ' . GameCode::getGameName($game_code) . ' 비활성화',
                        'is_disabled' => 1,
                        'reg_date' => date('Y-m-d H:i:s')
                    )
                );
            }
        );

        return UrlHelper::redirect(
            '/admin/games?app=' . GameCode::getGameAppType($game_code),
            '게임이 비활성화되었습니다.'
        );
    }

    /**
     * 컨트롤러) 게임 별 난이도 정보
     * ===
     * @param Request $request
     * @return string
     */
    public function level(Request $request)
    {
        $app_type = trim($request->get('app'));
        if (empty($app_type)) {
            return UrlHelper::returnWithMessage('잘못된 경로입니다.');
        }

        $sql = 'SELECT * FROM yf_game WHERE app_type = ? ORDER BY code ASC';

        $db = ConnectionProvider::getConnection();
        $games = $db->fetchAll($sql, array($app_type));

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes(array(
            'app_type' => $app_type,
            'games' => $games
        ));

        return $twig_renderer->render('/admin/game/level/index');
    }

    /**
     * 컨트롤러) 난이도 상세 정보
     * ===
     * @param Request $request
     * @return string
     */
    public function levelDetail(Request $request)
    {
        $game_code = intval(trim($request->get('gameCode')));
        if (empty($game_code)) {
            return UrlHelper::returnWithMessage('난이도를 조정할 수 있는 게임이 없습니다.');
        }

        $sql = 'SELECT * FROM yf_game_level_info WHERE game_code = ? ORDER BY level ASC';

        $db = ConnectionProvider::getConnection();
        $game_levels = $db->fetchAll($sql, array($game_code));

        $sql = 'SELECT * FROM yf_game WHERE code = ?';
        $game = $db->fetchAssoc($sql, array($game_code));

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes(array(
            'levels' => $game_levels,
            'level_title_map' => GameLevel::getKoreanTitleMap($game_code),
            'game' => $game,
            'game_code' => $game_code,
            'app_type' => strtolower(GameCode::getGameAppType($game_code))
        ));

        return $twig_renderer->render('/admin/game/level/detail');
    }

    /**
     * 컨트롤러) 난이도 정보 변경 내용 저장
     * ===
     * @param Request $request
     * @return string
     */
    public function saveLevel(Request $request)
    {
        $game_code = intval(trim($request->get('gameCode')));
        if (empty($game_code)) {
            return UrlHelper::returnWithMessage('난이도를 조정할 수 있는 게임이 없습니다.');
        }

        $db = ConnectionProvider::getConnection();
        $db->transactional(
            function (Connection $conn) use ($request, $game_code) {
                /** 난이도 레벨 정보 저장 */
                for ($i = 1; $i <= 15; $i++) {
                    $level_info = $request->get('level_' . $i);

                    $conn->update(
                        'yf_game_level_info',
                        array('level_info' => $level_info),
                        array(
                            'level' => $i,
                            'game_code' => $game_code
                        )
                    );
                }

                /** 난이도 레벨 정보 저장 로깅 */
                $conn->insert(
                    'yf_game_level_info_log',
                    array(
                        'game_code' => $game_code,
                        'description' => GameCode::getGameName($game_code) . ' 난이도 정보 수정',
                        'reg_date' => date('Y-m-d H:i:s')
                    )
                );
            }
        );

        return UrlHelper::redirect(
            '/admin/games/level/detail?gameCode=' . $game_code,
            '난이도 정보를 저장했습니다.'
        );
    }
}
