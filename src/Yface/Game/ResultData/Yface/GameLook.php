<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [6] - LOOK_내 눈을 바라봐
 */
class GameLook extends ResultData
{
    protected $origin_answer_eye_name_list;
    protected $user_answer_eye_name_list;

    public function __construct($results)
    {
        parent::__construct(GameCode::LOOK, $results);

        /** 게임 정보 저장 */
        $this->origin_answer_eye_name_list = implode('|', $results['originAnswerEyeNameList']);
        $this->user_answer_eye_name_list = implode('|', $results['userAnswerEyeNameList']);
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'origin_answer_eye_name_list' => $this->origin_answer_eye_name_list,
            'user_answer_eye_name_list' => $this->user_answer_eye_name_list
        ]);
    }
}
