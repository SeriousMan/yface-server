<?php
namespace Yface\DataStore\Feed;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Feed\Feed;

class FeedRepository extends EntityRepository
{
    /**
     * @param $feed_idx
     * @return null|Object|Feed
     */
    public function findOneByFeedIdx($feed_idx)
    {
        return $this->findOneBy(array('idx' => $feed_idx));
    }

    /**
     * @param $user_idx
     * @param $feed_idx
     * @return null|object|Feed
     */
    public function findOneByUserIdxAndFeedIdx($user_idx, $feed_idx)
    {
        return $this->findOneBy(['idx' => $feed_idx, 'user_idx' => $user_idx]);
    }

    /**
     * @param $user_idx
     * @param $app_type
     * @return Feed[]
     */
    public function findByUserIdx($user_idx, $app_type)
    {
        $qb = $this->createQueryBuilder('f');
        $qb->where('f.user_idx = :user_idx')
            ->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->andWhere('f.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere('f.is_disabled = :is_disabled')
            ->setParameter('is_disabled', false, Type::BOOLEAN)
            ->orderBy('f.idx', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $app_type
     * @return Feed[]
     */
    public function findAllFeedsByAppType($app_type)
    {
        $qb = $this->createQueryBuilder('f');
        $qb->where('f.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere('f.is_disabled = :is_disabled')
            ->setParameter('is_disabled', false, Type::BOOLEAN)
            ->orderBy('f.idx', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return FeedRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Feed\Feed');
    }
}
