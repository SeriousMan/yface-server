<?php
namespace Yface\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\DataStore\User\UserRepository;
use Yface\Game\GameCode;
use Yface\Library\Database\ConnectionProvider;

class DaysModifyCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('days:modify');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('ONCE - 날짜 보정 시작');

        self::user_game_play_modify();
        self::user_star_modify();
        self::user_point_daily_modify();
        self::user_theme_modify();
        self::user_attendance_modify();

        static::report('ONCE - 날짜 보정 종료');
    }

    protected function user_game_play_modify()
    {
        $db = ConnectionProvider::getConnection();
        $results = $db->fetchAll('SELECT * FROM yf_user_game_play_log');

        foreach ($results as $result) {
            var_dump($result['idx']);

            $user = UserRepository::getRepository()->findOneByUserIdx($result['u_idx']);
            $user->setUserLoginAppType(GameCode::getGameAppType($result['game_code']));

            $date = $user->getSurveyCompletedDate();

            $time = strtotime($result['reg_date']);

            $date_diff = $time - $date->getTimestamp();
            $days = (int)(floor($date_diff / (60 * 60 * 24)));

            $db = ConnectionProvider::getConnection();
            $db->update(
                'yf_user_game_play_log',
                [
                    'days' => $days + 1
                ],
                [
                    'idx' => $result['idx']
                ]
            );
        }
    }

    protected function user_star_modify()
    {
        $db = ConnectionProvider::getConnection();
        $results = $db->fetchAll('SELECT * FROM yf_user_star');

        foreach ($results as $result) {
            var_dump($result['idx']);

            $user = UserRepository::getRepository()->findOneByUserIdx($result['u_idx']);
            $user->setUserLoginAppType($result['app_type']);

            $date = $user->getSurveyCompletedDate();

            $time = strtotime($result['reg_date']);

            $date_diff = $time - $date->getTimestamp();
            $days = (int)(floor($date_diff / (60 * 60 * 24)));

            $db = ConnectionProvider::getConnection();
            $db->update(
                'yf_user_star',
                [
                    'days' => $days + 1
                ],
                [
                    'idx' => $result['idx']
                ]
            );
        }
    }

    protected function user_theme_modify()
    {
        $db = ConnectionProvider::getConnection();
        $results = $db->fetchAll('SELECT * FROM yf_user_theme');

        foreach ($results as $result) {
            var_dump($result['idx']);

            $user = UserRepository::getRepository()->findOneByUserIdx($result['u_idx']);
            $user->setUserLoginAppType($result['app_type']);

            $date = $user->getSurveyCompletedDate();

            $time = strtotime($result['reg_date']);

            $date_diff = $time - $date->getTimestamp();
            $days = (int)(floor($date_diff / (60 * 60 * 24)));

            $db = ConnectionProvider::getConnection();
            $db->update(
                'yf_user_theme',
                [
                    'days' => $days + 1
                ],
                [
                    'idx' => $result['idx']
                ]
            );
        }
    }

    protected function user_point_daily_modify()
    {
        $db = ConnectionProvider::getConnection();
        $results = $db->fetchAll('SELECT * FROM yf_user_point_daily_log');

        foreach ($results as $result) {
            var_dump($result['idx']);

            $user = UserRepository::getRepository()->findOneByUserIdx($result['u_idx']);
            $user->setUserLoginAppType($result['app_type']);

            $date = $user->getSurveyCompletedDate();

            $time = strtotime($result['reg_date']);

            $date_diff = $time - $date->getTimestamp();
            $days = (int)(floor($date_diff / (60 * 60 * 24)));

            $db = ConnectionProvider::getConnection();
            $db->update(
                'yf_user_point_daily_log',
                [
                    'days' => $days + 1
                ],
                [
                    'idx' => $result['idx']
                ]
            );
        }
    }

    protected function user_attendance_modify()
    {
        $db = ConnectionProvider::getConnection();
        $results = $db->fetchAll('SELECT * FROM yf_user_attendance');

        foreach ($results as $result) {
            var_dump($result['idx']);

            $user = UserRepository::getRepository()->findOneByUserIdx($result['u_idx']);
            $user->setUserLoginAppType($result['app_type']);

            $date = $user->getSurveyCompletedDate();

            $time = strtotime($result['reg_date']);

            $date_diff = $time - $date->getTimestamp();
            $days = (int)(floor($date_diff / (60 * 60 * 24)));

            $db = ConnectionProvider::getConnection();
            $db->update(
                'yf_user_attendance',
                [
                    'days' => $days + 1
                ],
                [
                    'idx' => $result['idx']
                ]
            );
        }
    }
}
