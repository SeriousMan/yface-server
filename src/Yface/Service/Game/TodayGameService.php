<?php
namespace Yface\Service\Game;

use Doctrine\ORM\EntityManager;
use Yface\DataStore\Game\GameRepository;
use Yface\DataStore\Game\TodayGameRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Library\DateHelper;
use Yface\Model\Game\Game;
use Yface\Model\Game\TodayGame;
use Yface\Model\Game\UserTodayGame;
use Yface\Service\Star\StarService;

/**
 * 과정
 * : 매일 앱 별로 6개의 게임을 (중단된 게임이 있으면 2번째 로테이션에서는 6 - 중단된 게임 갯수 만큼 선정한다.
 *
 * 규칙
 * 1. 처음 선정에서 6개의 게임이 선정되면 나머지 6개의 게임이 그 이후에 선정되어야 하고
 * 2. 게임이 모두 선정되었다면 다시 1번의 과정을 거친다.
 */
class TodayGameService
{
    /**
     * 유저의 오늘의 게임 정보 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getUserTodayGames($user_idx, $app_type)
    {
        $games = static::getTodayGames($app_type);

        $user_today_games = array();
        foreach ($games as $game) {
            $user_today_game = new UserTodayGame();
            $user_today_game->setGameCode($game);
            $user_today_game->setIsTodayGamePlayed(GameService::isUserTodayGamePlayed($user_idx, $game, $app_type));
            $user_today_game->setIsTodayGotStar(GameService::isUserTodayGotStarInGame($user_idx, $game));
            $user_today_game->setUserStars(StarService::getUserStarsByGame($user_idx, $game));

            $user_today_games[] = $user_today_game;
        }

        return $user_today_games;
    }

    /**
     * 오늘의 게임 정보 리턴
     * ===
     * @param $app_type
     * @return array
     */
    public static function getTodayGames($app_type)
    {
        $today_game = TodayGameRepository::getRepository()->findOneByDateAndAppType(
            DateHelper::getStartTimeOfToday(),
            $app_type
        );

        return explode('|', $today_game->getGameCodes());
    }

    /**
     * 오늘의 게임 선정
     * ===
     * @param $app_type
     */
    public static function selectTodayGame($app_type)
    {
        if (static::checkIfAlreadySelectedToday($app_type)) {
            return;
        }

        if (static::checkIfAllGameSelected($app_type)) {
            static::resetGameSelectedAttributes($app_type);
        }

        $target_games = array();
        $games = GameRepository::getRepository()->findNonSelectedByAppType($app_type);
        /** @var $game Game */
        foreach ($games as $game) {
            $target_games[] = $game->getCode();
        }

        /** 오늘의 게임 선정 */
        $selected_games = static::selectGamesRandomly($target_games);
        sort($selected_games);

        $today_game = new TodayGame();
        $today_game->setGameCodes(implode($selected_games, '|'));
        $today_game->setAppType($app_type);
        $today_game->setRegDate(DateHelper::getStartTimeOfToday());

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($today_game, $games, $selected_games) {
                foreach ($games as $game) {
                    $code = $game->getCode();
                    if (!in_array($code, $selected_games)) {
                        continue;
                    }

                    $game->setIsSelected(true);

                    $em->persist($game);
                    $em->flush($game);
                }

                $em->persist($today_game);
                $em->flush($today_game);
            }
        );
    }

    /**
     * 오늘의 게임 랜덤 추출
     * ===
     * @param $games
     * @return array
     */
    private static function selectGamesRandomly($games)
    {
        /** 랜덤 추출 대상 게임 개수가 6개 초과가 안되면 그냥 모두 선택 */
        if (count($games) <= 6) {
            return $games;
        }

        $selected_games = array();
        while (count($selected_games) !== 6) {
            $range = count($games) - 1;
            $random_offset = rand(0, $range);

            $selected_games[] = $games[$random_offset];

            unset($games[$random_offset]);
            $games = array_values($games);
        }

        return $selected_games;
    }

    /**
     * 모든 게임이 오늘의 게임으로 선택되었는지 여부 리턴
     * ===
     * @param $app_type
     * @return bool
     */
    private static function checkIfAllGameSelected($app_type)
    {
        $games = GameRepository::getRepository()->findActiveByAppType($app_type);
        foreach ($games as $game) {
            if ($game->getIsSelected() === true) {
                continue;
            }

            return false;
        }

        return true;
    }

    /**
     * 오늘의 게임 선택 여부 정보 초기화
     * ===
     * @param $app_type
     */
    private static function resetGameSelectedAttributes($app_type)
    {
        $games = GameRepository::getRepository()->findActiveByAppType($app_type);
        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($games) {
                foreach ($games as $game) {
                    $game->setIsSelected(false);

                    $em->persist($game);
                    $em->flush($game);
                }
            }
        );
    }

    /**
     * 이미 오늘 선정된 내역이 있는지 확인
     * ===
     * @param $app_type
     * @return bool
     */
    private static function checkIfAlreadySelectedToday($app_type)
    {
        $today_game = TodayGameRepository::getRepository()->findOneByDateAndAppType(
            DateHelper::getStartTimeOfToday(),
            $app_type
        );

        return !empty($today_game);
    }
}
