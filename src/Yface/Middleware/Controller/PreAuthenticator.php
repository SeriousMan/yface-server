<?php
namespace Yface\Middleware\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Yface\Service\User\UserService;

class PreAuthenticator {
    /**
     * 유저의 인덱스 정보 유효 인증
     * ===
     * @return \Closure
     */
    public static function authenticate() {
        return function (Request $request, Application $app) {
            $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
            if (empty($user_idx)) {
                return $app->json(array('message' => '유저 인증 정보가 올바르지 않습니다.'), 400);
            }

            $user = UserService::getUserWithUserIdx($user_idx);
            if (empty($user)) {
                return $app->json(array('message' => '유저 인증 정보가 올바르지 않습니다.'), 400);
            }

            return null;
        };
    }
}
