<?php
namespace Yface\DataStore\Survey;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Survey\UserSurveyTodo;

class UserSurveyTodoRepository extends EntityRepository
{
    /**
     * @param $user_idx
     * @param $time
     * @param $app_type
     * @return UserSurveyTodo[]
     */
    public function findByUserIdxAndTime($user_idx, $time, $app_type)
    {
        $qb = $this->createQueryBuilder('st');
        $qb->select('st')
            ->innerJoin('Yface\Model\Survey\Survey', 's', Expr\Join::WITH, 'st.survey_idx = s.idx')
            ->where('st.user_idx = :user_idx')
            ->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->andWhere('st.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere('s.is_active = :is_active')
            ->setParameter('is_active', true, Type::BOOLEAN)
            ->orderBy('st.survey_idx', 'ASC');

        if (!empty($time)) {
            $qb->andWhere('st.time = :time')
                ->setParameter('time', $time, Type::STRING);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $user_idx
     * @param $survey_idx
     * @param $time
     * @param $app_type
     * @return null|object|UserSurveyTodo
     */
    public function findOneByUserIdxAndSurveyIdx($user_idx, $survey_idx, $time, $app_type)
    {
        return $this->findOneBy(array(
            'user_idx' => $user_idx,
            'survey_idx' => $survey_idx,
            'time' => $time,
            'app_type' => $app_type
        ));
    }

    /**
     * @param $user_idx
     * @param $time
     * @param $app_type
     * @return mixed
     */
    public function getSurveyDoneCountByUserIdxAndTime($user_idx, $time, $app_type)
    {
        $qb = $this->createQueryBuilder('st');
        $qb->select('count(st)')
            ->innerJoin('Yface\Model\Survey\Survey', 's', Expr\Join::WITH, 'st.survey_idx = s.idx')
            ->where('st.user_idx = :user_idx')
            ->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->andWhere('st.time = :time')
            ->setParameter('time', $time, Type::STRING)
            ->andWhere('st.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere('st.is_done = :done')
            ->setParameter('done', true, Type::BOOLEAN)
            ->andWhere('s.is_active = :is_active')
            ->setParameter('is_active', true, Type::BOOLEAN);

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return UserSurveyTodoRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Survey\UserSurveyTodo');
    }
}
