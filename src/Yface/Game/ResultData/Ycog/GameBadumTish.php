<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [13] - BADUM_TISH_쿵짝쿵짝
 */
class GameBadumTish extends ResultData
{
    protected $answer_list;
    protected $user_answer_list;
    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::BADUM_TISH, $results);

        /** 게임 정보 저장 */
        $this->answer_list = implode('|', $results['answerList']);
        $this->user_answer_list = implode('|', $results['userAnswerList']);
        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'answer_list' => $this->answer_list,
            'user_answer_list' => $this->user_answer_list,
            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
