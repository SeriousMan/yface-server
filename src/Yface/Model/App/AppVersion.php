<?php
namespace Yface\Model\App;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\App\AppVersionRepository")
 * @ORM\Table(name="yf_app_version")
 * @JMS\ExclusionPolicy("none")
 */
class AppVersion
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", columnDefinition="ENUM('YFACE', 'YCOG')")
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="string")
     *
     * @var $version string
     */
    private $version;
    /**
     * @ORM\Column(type="boolean")
     *
     * @var $is_force_update bool
     */
    private $is_force_update;

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @return boolean
     */
    public function getIsForceUpdate(): bool
    {
        return $this->is_force_update;
    }
}
