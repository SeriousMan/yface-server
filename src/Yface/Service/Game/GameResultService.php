<?php
namespace Yface\Service\Game;

use Doctrine\DBAL\Connection;
use Yface\Game\GameCode;
use Yface\Game\ResultDataBuilder;
use Yface\Library\Database\ConnectionProvider;

class GameResultService
{
    /**
     * 결과 저장
     * ===
     * @param $user_idx
     * @param $game_code
     * @param $results
     * @param $uploaded_files
     */
    public static function saveResult($user_idx, $game_code, $results, $uploaded_files)
    {
        $conn = ConnectionProvider::getConnection();
        $conn->transactional(
            function (Connection $conn) use ($user_idx, $game_code, $results, $uploaded_files) {
                $sql = 'SELECT idx FROM ' . GameCode::getGameTableName($game_code) .
                    ' WHERE u_idx = ? ORDER BY idx DESC LIMIT 1';
                $game_idx = $conn->fetchColumn($sql, [$user_idx]);

                $conn->update(
                    GameCode::getGameTableName($game_code),
                    (new ResultDataBuilder($game_code))->build($results, $uploaded_files),
                    ['idx' => $game_idx]
                );

                $sql = 'UPDATE yf_user_game_play_log SET is_completed = 1
                        WHERE u_idx = ? AND game_code = ?
                        ORDER BY idx DESC LIMIT 1';
                $conn->executeQuery($sql, [$user_idx, $game_code]);
            }
        );
    }

    /**
     * 해당 결과가 별 획득 가능한지 여부 리턴
     * ===
     * @param $results
     * @return bool
     */
    public static function isStarObtainable($results)
    {
        return $results['obtainStar'];
    }
}
