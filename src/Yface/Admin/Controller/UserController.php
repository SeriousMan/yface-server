<?php
namespace Yface\Admin\Controller;

use Kilte\Pagination\Pagination;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Yface\Admin\Library\UrlHelper;
use Yface\Admin\Statistics\User;
use Yface\Game\GameCode;
use Yface\Game\GameResult;
use Yface\Library\Configuration;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\DateHelper;
use Yface\Library\Template\TwigRenderer;

class UserController
{
    const USER_COUNT_PER_PAGE = 20;

    /**
     * 컨트롤러) 유저 목록
     * ===
     * @param $request Request
     * @return string
     */
    public function index(Request $request)
    {
        $search_params = [
            'gender' => $request->get('gender'),
            'user_type' => $request->get('user_type'),
            'app_type' => $request->get('app_type'),
            'name' => $request->get('name'),
            'diagnosis' => $request->get('diagnosis'),
            'research' => $request->get('research')
        ];

        $page = intval(trim($request->get('page', 1)));
        $offset = ($page - 1) * static::USER_COUNT_PER_PAGE;

        $db = ConnectionProvider::getConnection();
        $qb = $db->createQueryBuilder();

        if (empty($search_params['research'])) {
            $qb->select('*')
            ->from('yf_user u')
            ->orderBy('idx', 'DESC')
            ->setMaxResults(static::USER_COUNT_PER_PAGE)
            ->setFirstResult($offset);
        }
        else {
            $qb->select('*')
            ->from('yf_user_research_status', 'urs')
            ->orderBy('urs.idx', 'DESC')
            ->innerJoin('urs', 'yf_user', 'u', 'u.idx = urs.u_idx')
            ->andWhere('r_idx = :r_idx')
            ->setParameter('r_idx', $search_params['research'])
            ->setMaxResults(static::USER_COUNT_PER_PAGE)
            ->setFirstResult($offset);
        }

        if (!empty($search_params['gender'])) {
            $qb->andWhere('gender = :gender')
                ->setParameter('gender', $search_params['gender']);
        }

        if (!empty($search_params['user_type'])) {
            $qb->andWhere('is_experiment = :is_exp')
                ->setParameter('is_exp', $search_params['user_type'] === 'exp');
        }

        if (!empty($search_params['app_type'])) {
            $qb->andWhere(strtolower($search_params['app_type']) . '_survey_completed_date IS NOT NULL');
        }

        if (!empty($search_params['name'])) {
            $qb->andWhere($qb->expr()->like('name', $qb->expr()->literal('%' . $search_params['name'] . '%')));
        }

        if (!empty($search_params['diagnosis'])) {
            $qb->andWhere($qb->expr()->like('diagnosis', $qb->expr()->literal('%' . $search_params['diagnosis'] . '%')));
        }

        $users = $qb->execute()->fetchAll();

        $qb->select('count(u.idx)')
            ->setFirstResult(null)
            ->setMaxResults(null);

        $user_count = $qb->execute()->fetchColumn();

        $pagination = new Pagination($user_count, $page, static::USER_COUNT_PER_PAGE);

        $qb2 = $db->createQueryBuilder();
        $qb2->select('*')
        ->from('yf_research')
        ->andWhere('idx = :r_idx')
        ->setParameter('r_idx', $search_params['research']);
        $research = $qb2->execute()->fetch();

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'users' => $users,
            'user_count' => $user_count,
            'page' => $page,
            'pagination' => $pagination->build(),
            'search_params' => $search_params,
            'research' => $research,
        ]);

        return $twig_renderer->render('admin/user/index');
    }

    /**
     * 컨트롤러) 유저 상세
     * ===
     * @param $user_idx
     * @return string
     */
    public function detail($user_idx)
    {
        $sql = 'SELECT * FROM yf_survey WHERE app_type = ? OR app_type = ?';
        $db = ConnectionProvider::getConnection();
        $yface_surveys = $db->fetchAll($sql, ['BOTH', APP_TYPE_YFACE]);
        $ycog_surveys = $db->fetchAll($sql, ['BOTH', APP_TYPE_YCOG]);

        $yface_survey_results = static::buildUserSurveySummaryResult($user_idx, APP_TYPE_YFACE, $yface_surveys);
        $ycog_survey_results = static::buildUserSurveySummaryResult($user_idx, APP_TYPE_YCOG, $ycog_surveys);

        $stats = User::buildUser($user_idx);
        $stats = array_merge($stats, [
            'games' => GameCode::ENG_NAMES,
            'yface_surveys' => $yface_surveys,
            'ycog_surveys' => $ycog_surveys,
            'yface_survey_results' => $yface_survey_results,
            'ycog_survey_results' => $ycog_survey_results
        ]);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes($stats);

        return $twig_renderer->render('admin/user/detail');
    }

    /**
     * @param $user_idx
     * @param $app_type
     * @param $surveys
     *
     * @return array
     */
    private static function buildUserSurveySummaryResult($user_idx, $app_type, $surveys)
    {
        $db = ConnectionProvider::getConnection();

        $survey_results = [];
        foreach ($surveys as $survey) {
            $sql = "SELECT * FROM yf_user_survey_todo ut WHERE ut.is_done = 1 AND ut.u_idx = ? AND ut.s_idx = ? AND ut.app_type = ? AND ut.time = ?";
            $survey_todo = $db->fetchAssoc($sql, [$user_idx, $survey['idx'], strtoupper($app_type), 'BEGIN']);

            if (empty($survey_todo)) {
                $yface_survey_results[$survey['idx']] = [];
                continue;
            }

            $temp_results = SurveyController::buildSurveyResult($survey_todo['idx']);

            $sql = "SELECT mod_date FROM yf_user_survey_todo ut WHERE ut.is_done = 1 AND ut.u_idx = ? AND ut.s_idx = ? AND ut.app_type = ? AND ut.time = ?";
            $begin_survey_completed_date = $db->fetchColumn($sql, [$user_idx, $survey['idx'], $app_type, 'BEGIN']);

            $end_survey_completed_date = $db->fetchColumn($sql, [$user_idx, $survey['idx'], $app_type, 'END']);

            $survey_results[$survey['idx']] = [
                'begin_survey_point' => $temp_results['begin_survey_point'],
                'begin_survey_completed_date' => $begin_survey_completed_date,
                'end_survey_point' => $temp_results['end_survey_point'],
                'end_survey_completed_date' => $end_survey_completed_date
            ];
        }

        return $survey_results;
    }

    /**
     * 컨트롤러) 유저 정보 수정
     * @param Request $request
     * @param $user_idx
     *
     * @return string
     */
    public function edit(Request $request, $user_idx)
    {
        $name = trim($request->get('name'));
        $birth_date = trim($request->get('birthdate'));
        $gender = trim($request->get('gender'));
        $diagnosis = trim($request->get('diagnosis'));

        $db = ConnectionProvider::getConnection();
        $db->update(
            'yf_user',
            [
                'name' => $name,
                'birth_date' => $birth_date,
                'gender' => $gender,
                'diagnosis' => $diagnosis
            ],
            ['idx' => $user_idx]
        );

        return UrlHelper::redirect('/admin/users/' . $user_idx, '사용자 기본 정보가 수정되었습니다.');
    }

    /**
     * 컨트롤러) 유저 상세 데이터 엑셀 추출
     * ===
     * @param $request Request
     * @param $user_idx
     *
     * @return RedirectResponse
     */
    public function detailToExcel(Request $request, $user_idx)
    {
        $limit_days = intval(trim($request->get('days', 66)));

        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle($user_idx . ' User Statistics Sheet');

        $php_excel = User::exportUserToExcel($php_excel, $user_idx, $limit_days);

        $file_name = $user_idx . '_stat_' . date('Y-m-d');
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        header('Content-type: application/x-msexcel;charset=utf-8');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($php_excel, 'Excel5');
        $objWriter->save('php://output');

        return new RedirectResponse("/admin/users/{$user_idx}");
    }

    /**
     * 컨트롤러) 유저의 상세 게임 데이터
     * ===
     * @param $user_idx
     * @param $game_code
     * @return string
     */
    public function detailGames($user_idx, $game_code)
    {
        $table_name = GameCode::getGameTableName($game_code);
        $stats = User::buildUserGame($user_idx, $game_code, $table_name);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes(array_merge([
            'games' => GameCode::ENG_NAMES,
            'game_title' => GameResult::getTitles($game_code),
            'additional_titles' => GameResult::getAdditionalTitles($game_code),
            'game_code' => $game_code
        ], $stats));

        return $twig_renderer->render('admin/user/game');
    }

    /**
     * 컨트롤러) 유저의 상세 게임 데이터 엑셀 추출
     * ===
     * @param $request Request
     * @param $user_idx
     * @param $game_code
     *
     * @return RedirectResponse
     */
    public function detailGamesToExcel(Request $request, $user_idx, $game_code)
    {
        $limit_days = intval(trim($request->get('days', 66)));
        if (empty($limit_days)) {
            $limit_days = 66;
        }

        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle(GameCode::getGameName($game_code) . ' Game Statistics Sheet');

        $sql = "SELECT * FROM yf_user WHERE idx = ?";
        $db = ConnectionProvider::getConnection();
        $user = $db->fetchAssoc($sql, [$user_idx]);
        $app_type = strtolower(GameCode::getGameAppType($game_code));
        $start_date = new \DateTime($user["{$app_type}_survey_completed_date"]);

        $row_offset = 1;
        $php_excel->getActiveSheet()
            ->fromArray([
                '아이디',
                '이름',
                '생년월일',
                '성별',
                '진단명',
                '훈련시작일',
            ], null, 'A' . $row_offset++);

        $php_excel->getActiveSheet()
            ->fromArray([
                $user['id'],
                $user['name'],
                $user['birth_date'],
                $user['gender'],
                $user['diagnosis'],
                $start_date->format('Y-m-d')
            ], null, 'A' . $row_offset);

        $php_excel = User::exportUserGameToExcel($php_excel, $user_idx, $game_code, 0, $limit_days);

        $file_name = $user_idx . '_' . GameCode::getGameName($game_code) . '_stat_' . date('Y-m-d');
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        header('Content-type: application/x-msexcel;charset=utf-8');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($php_excel, 'Excel5');
        $objWriter->save('php://output');

        return new RedirectResponse("/admin/users/{$user_idx}/games/{$game_code}");
    }

    /**
     * @param $user_idx
     * @param $app_type
     *
     * @return RedirectResponse
     */
    public function detailAppGamesToExcel($user_idx, $app_type)
    {
        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle($app_type . ' Games Statistics Sheet');

        $sql = "SELECT * FROM yf_user WHERE idx = ?";
        $db = ConnectionProvider::getConnection();
        $user = $db->fetchAssoc($sql, [$user_idx]);
        $start_date = new \DateTime($user["{$app_type}_survey_completed_date"]);

        $row_offset = 1;
        $php_excel->getActiveSheet()
            ->fromArray([
                '아이디',
                '이름',
                '생년월일',
                '성별',
                '진단명',
                '훈련시작일',
            ], null, 'A' . $row_offset++);

        $php_excel->getActiveSheet()
            ->fromArray([
                $user['id'],
                $user['name'],
                $user['birth_date'],
                $user['gender'],
                $user['diagnosis'],
                $start_date->format('Y-m-d')
            ], null, 'A' . $row_offset);

        if (strtoupper($app_type) == APP_TYPE_YFACE) {
            $start = GameCode::CARD;
            $end = GameCode::TRE;
        } else {
            $start = GameCode::BADUM_TISH;
            $end = GameCode::WHATS_THE_NUMBER;
        }

        for ($i = $start; $i <= $end; $i++) {
            $php_excel = User::exportUserGameToExcel($php_excel, $user_idx, $i, 0, 1000);
        }

        $php_excel->getActiveSheet()->setTitle('전체');

        $file_name = $user_idx . '_' . $app_type . '_games_stat_' . date('Y-m-d');
        $file_path = Configuration::$values['path']['resources'] . "/{$file_name}.xlsx";

        $objWriter = new \PHPExcel_Writer_Excel2007($php_excel);
        $objWriter->setOffice2003Compatibility(true);
        $objWriter->save($file_path);

        $file_size = filesize($file_path);
        $path_parts = pathinfo($file_path);
        $filename = $path_parts['basename'];

        header("Pragma: public");
        header("Expires: 0");
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: $file_size");

        ob_clean();
        flush();
        readfile($file_path);

        unlink($file_path);

        return new RedirectResponse("/admin/users/{$user_idx}");
    }

    /**
     * 컨트롤러) 유저의 게임 데이터 삭제
     * ===
     * @param Request $request
     * @param $user_idx
     * @param $game_code
     * @param $game_idx
     * @return RedirectResponse
     */
    public function removeGameData(Request $request, $user_idx, $game_code, $game_idx)
    {
        $page = intval(trim($request->get('page', 1)));

        $db = ConnectionProvider::getConnection();
        $db->update(
            GameCode::getGameTableName($game_code),
            [
                'is_disabled' => 1
            ],
            [
                'idx' => $game_idx
            ]
        );

        return new RedirectResponse("/admin/users/{$user_idx}/games/{$game_code}?page={$page}");
    }

    /**
     * 컨트롤러) 유저 활성화 처리
     * ===
     * @param $user_idx
     * @return string
     */
    public function activate($user_idx)
    {
        if (empty($user_idx)) {
            return UrlHelper::returnWithMessage('활성화할 유저 정보가 없습니다.');
        }

        $db = ConnectionProvider::getConnection();
        $db->update('yf_user', ['is_disabled' => 0], ['idx' => $user_idx]);

        return UrlHelper::redirect('/admin/users', '유저가 활성화되었습니다.');
    }

    /**
     * 컨트롤러) 유저 비활성화 처리
     * ===
     * @param $user_idx
     * @return string
     */
    public function deactivate($user_idx)
    {
        if (empty($user_idx)) {
            return UrlHelper::returnWithMessage('비 활성화할 유저 정보가 없습니다.');
        }

        $db = ConnectionProvider::getConnection();
        $db->update('yf_user', ['is_disabled' => 1], ['idx' => $user_idx]);

        return UrlHelper::redirect('/admin/users', '유저가 비활성화되었습니다.');
    }

    /**
     * 컨트롤러) 유저 승인 처리
     * ===
     * @param Request $request
     * @param $user_idx
     * @return string
     */
    public function accept(Request $request, $user_idx)
    {
        $app_type = trim($request->get('app'));
        if (empty($user_idx)) {
            return UrlHelper::returnWithMessage('승인할 유저 정보가 없습니다.');
        }

        $db = ConnectionProvider::getConnection();
        $db->update(
            'yf_user',
            [
                'is_' . $app_type . '_accept' => 1,
                $app_type . '_survey_completed_date' => DateHelper::getStartTimeOfToday()->format('Y-m-d H:i:s')
            ],
            ['idx' => $user_idx]
        );

        return UrlHelper::redirect('/admin/users', '유저가 승인되었습니다.');
    }

    public function listPoints(Request $request, $user_idx)
    {
        $app_type = trim($request->get('app', APP_TYPE_YFACE));
        $page = intval(trim($request->get('page', 1)));
        $offset = ($page - 1) * static::USER_COUNT_PER_PAGE;

        $sql = 'SELECT * FROM yf_user WHERE idx = ?';
        $db = ConnectionProvider::getConnection();
        $user = $db->fetchAssoc($sql, [$user_idx]);

        $sql = "SELECT * FROM yf_user_point WHERE u_idx = ? AND app_type = ? ORDER BY idx DESC LIMIT {$offset}, " . static::USER_COUNT_PER_PAGE;
        $points = $db->fetchAll($sql, [$user_idx, strtoupper($app_type)]);

        $sql = 'SELECT count(idx) FROM yf_user_point WHERE u_idx = ? AND app_type = ?';
        $point_count = $db->fetchColumn($sql, [$user_idx, strtoupper($app_type)]);

        $pagination = new Pagination($point_count, $page, static::USER_COUNT_PER_PAGE);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'user' => $user,
            'app_type' => strtolower($app_type),
            'points' => $points,
            'pagination' => $pagination->build(),
            'page' => $page
        ]);

        return $twig_renderer->render('admin/user/point');
    }

    public function listAttendance(Request $request, $user_idx)
    {
        $app_type = trim($request->get('app', APP_TYPE_YFACE));

        $sql = 'SELECT * FROM yf_user WHERE idx = ?';
        $db = ConnectionProvider::getConnection();
        $user = $db->fetchAssoc($sql, [$user_idx]);

        $sql = "SELECT * FROM yf_user_attendance WHERE u_idx = ? AND app_type = ? ORDER BY idx DESC";
        $results = $db->fetchAll($sql, [$user_idx, strtoupper($app_type)]);

        $attendance_list = [];
        foreach ($results as $row) {
            $date = date('Y-m-d', strtotime($row['reg_date']));
            $attendance_list[$date] = 1;
        }

        $sql = "SELECT distinct DATE(reg_date) as date FROM yf_user_login_log WHERE u_idx = ? AND app_type = ? GROUP BY DATE(reg_date)";
        $results = $db->fetchAll($sql, [$user_idx, strtoupper($app_type)]);

        $login_logs = [];
        foreach ($results as $row) {
            $date = date('Y-m-d', strtotime($row['date']));
            $login_logs[$date] = 1;
        }

        $start_date = new \DateTime($user[strtolower($app_type) . "_survey_completed_date"]);
        $start_date->setTime(0, 0, 0);

        $end_date = new \DateTime('now');
        $end_date->setTime(0, 0, 0);

        $diff = $end_date->diff($start_date);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'user' => $user,
            'app_type' => strtolower($app_type),
            'attendance_list' => $attendance_list,
            'start_date' => new \DateTime($user[strtolower($app_type) . "_survey_completed_date"]),
            'login_logs' => $login_logs,
            'diff_days' => $diff->days,
        ]);

        return $twig_renderer->render('admin/user/attendance');
    }

    public function changeExperiment(Request $request, $user_idx)
    {
        $page = $request->get('page', 1);

        $db = ConnectionProvider::getConnection();
        $db->update(
            'yf_user',
            ['is_experiment' => 1],
            ['idx' => $user_idx]
        );

        return UrlHelper::redirect('/admin/users?page=' . $page, '피실험자로 전환되었습니다.');
    }

    public function changeNormal(Request $request, $user_idx)
    {
        $page = $request->get('page', 1);

        $db = ConnectionProvider::getConnection();
        $db->update(
            'yf_user',
            ['is_experiment' => 0],
            ['idx' => $user_idx]
        );

        return UrlHelper::redirect('/admin/users?page=' . $page, '일반 사용자로 전환되었습니다.');
    }

    public function viewGraphs($user_idx, $app_type)
    {
        $app_type = strtoupper($app_type);

        $sql = 'SELECT * FROM yf_user WHERE idx = ?';
        $db = ConnectionProvider::getConnection();
        $user = $db->fetchAssoc($sql, [$user_idx]);

        $results = [];

        if (strtolower($app_type) == 'yface') {
            $start = GameCode::CARD;
            $end = GameCode::TRE;
        } else {
            $start = GameCode::BADUM_TISH;
            $end = GameCode::WHATS_THE_NUMBER;
        }

        for ($i = $start; $i <= $end; $i++) {
            $table_name = GameCode::getGameTableName($i);

            $db = ConnectionProvider::getConnection();
            $sql = "SELECT * FROM {$table_name} WHERE u_idx = ? AND is_disabled = 0 AND accuracy IS NOT NULL ORDER BY idx";
            $results[$i] = $db->fetchAll($sql, [$user_idx]);
        }

        $max_days = 0;
        foreach ($results as $key => $value) {
            if ($max_days < count($value)) {
                $max_days = count($value);

                continue;
            }
        }

        $day_labels = [];
        for ($i = 1; $i <= $max_days; $i++) {
            $day_labels[] = "{$i}일차";
        }

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'user' => $user,
            'app_type' => strtolower($app_type),
            'results' => $results,
            'games' => GameCode::getAllGameCode(),
            'game_names' => GameCode::ENG_NAMES,
            'game_acronyms' => GameCode::ACRONYM,
            'game_colors' => GameCode::COLORS,
            'day_labels' => $day_labels,
        ]);

        return $twig_renderer->render('admin/user/graph');
    }
}
