<?php
namespace Yface\Admin;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Yface\Admin\Middleware\Controller\PreAuthenticator;
use Yface\Admin\User\UserSession;

class WebControllerProvider implements ControllerProviderInterface
{
    /**
     * 컨트롤러 네임스페이스 공통 사용
     * ===
     * @var string
     */
    private static $CONTROLLER_NAMESPACE = 'Yface\Admin\Controller\\';

    /**
     * @param Application $app
     * @return ControllerCollection
     */
    public function connect(Application $app)
    {
        /** @var ControllerCollection $routes */
        $routes = $app['controllers_factory'];

        /** 로그인 */
        $routes->get('/sign-in', static::$CONTROLLER_NAMESPACE . 'SignInController::index')
            ->before(PreAuthenticator::checkIfLoggedInAndGoToMain());
        $routes->post('/sign-in', static::$CONTROLLER_NAMESPACE . 'SignInController::login')
            ->before(PreAuthenticator::checkIfLoggedInAndGoToMain());

        /** 로그아웃 */
        $routes->get('/sign-out', static::$CONTROLLER_NAMESPACE . 'SignInController::logout');

        /** 홈 (Not Controller Just Redirect) */
        $routes->get('/', function () {
            return RedirectResponse::create('users');
        })
            ->before(PreAuthenticator::authenticate());

        /** 동의 관련 */
        $routes->get('/privacy', static::$CONTROLLER_NAMESPACE . 'PrivacyController::index')
            ->before(PreAuthenticator::authenticate());
        $routes->post('/privacy', static::$CONTROLLER_NAMESPACE . 'PrivacyController::save')
            ->before(PreAuthenticator::authenticate());

        /** 유저 */
        $routes->get('/users', static::$CONTROLLER_NAMESPACE . 'UserController::index')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/users/{user_idx}', static::$CONTROLLER_NAMESPACE . 'UserController::detail')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/edit', static::$CONTROLLER_NAMESPACE . 'UserController::edit')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/export', static::$CONTROLLER_NAMESPACE . 'UserController::detailToExcel')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/games/{game_code}', static::$CONTROLLER_NAMESPACE . 'UserController::detailGames')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/games/{game_code}/export', static::$CONTROLLER_NAMESPACE . 'UserController::detailGamesToExcel')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/{app_type}/games/export', static:: $CONTROLLER_NAMESPACE . 'UserController::detailAppGamesToExcel')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/games/{game_code}/remove/{game_idx}', static::$CONTROLLER_NAMESPACE . 'UserController::removeGameData')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/activate', static::$CONTROLLER_NAMESPACE . 'UserController::activate')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/deactivate', static::$CONTROLLER_NAMESPACE . 'UserController::deactivate')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/accept', static::$CONTROLLER_NAMESPACE . 'UserController::accept')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/points', static::$CONTROLLER_NAMESPACE . 'UserController::listPoints')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/attendance', static::$CONTROLLER_NAMESPACE . 'UserController::listAttendance')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/change/experiment', static::$CONTROLLER_NAMESPACE . 'UserController::changeExperiment')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/users/{user_idx}/change/normal', static::$CONTROLLER_NAMESPACE . 'UserController::changeNormal')
            ->before(PreAuthenticator::authenticate());

        $routes->get('/users/{user_idx}/{app_type}/graph', static::$CONTROLLER_NAMESPACE . 'UserController::viewGraphs')
            ->before(PreAuthenticator::authenticate());

        /** 연구 */
        $routes->get('/research', static::$CONTROLLER_NAMESPACE . 'ResearchController::index')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/research/manage', static::$CONTROLLER_NAMESPACE . 'ResearchController::manage')
            ->before(PreAuthenticator::authenticate());

        /** 게임 */
        $routes->get('/games', static::$CONTROLLER_NAMESPACE . 'GameController::index')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/games/activate', static::$CONTROLLER_NAMESPACE . 'GameController::activate')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/games/deactivate', static::$CONTROLLER_NAMESPACE . 'GameController::deactivate')
            ->before(PreAuthenticator::authenticate());

        /** 게임 레벨 */
        $routes->get('/games/level', static::$CONTROLLER_NAMESPACE . 'GameController::level')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/games/level/detail', static::$CONTROLLER_NAMESPACE . 'GameController::levelDetail')
            ->before(PreAuthenticator::authenticate());
        $routes->post('/games/level', static::$CONTROLLER_NAMESPACE . 'GameController::saveLevel')
            ->before(PreAuthenticator::authenticate());

        /** 설문 */
        $routes->get('/surveys', static::$CONTROLLER_NAMESPACE . 'SurveyController::index')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->post('/surveys', static::$CONTROLLER_NAMESPACE . 'SurveyController::save')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/surveys/activate', static::$CONTROLLER_NAMESPACE . 'SurveyController::activate')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/surveys/deactivate', static::$CONTROLLER_NAMESPACE . 'SurveyController::deactivate')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/surveys/detail', static::$CONTROLLER_NAMESPACE . 'SurveyController::detail')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/surveys/results', static::$CONTROLLER_NAMESPACE . 'SurveyController::listResult')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/surveys/{app_type}/export', static::$CONTROLLER_NAMESPACE . 'SurveyController::exportAllSurveyResultToExcel')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/surveys/{survey_id}/{app_type}/graph', static::$CONTROLLER_NAMESPACE . 'SurveyController::viewGraph')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/surveys/{survey_id}/{app_type}/export', static::$CONTROLLER_NAMESPACE . 'SurveyController::exportSurveyResultToExcel')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));

        /** 리소스 관리 */
        $routes->get('/resources', static::$CONTROLLER_NAMESPACE . 'ResourceController::index')
            ->before(PreAuthenticator::authenticate());
        $routes->get('/resources/detail', static::$CONTROLLER_NAMESPACE . 'ResourceController::detail')
            ->before(PreAuthenticator::authenticate());
        $routes->post('/resources', static::$CONTROLLER_NAMESPACE . 'ResourceController::save')
            ->before(PreAuthenticator::authenticate());

        /** 통계 */
        $routes->get('/statistics/app', static::$CONTROLLER_NAMESPACE . 'StatisticsController::app')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/app/excel', static::$CONTROLLER_NAMESPACE . 'StatisticsController::appToExcel')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/games', static::$CONTROLLER_NAMESPACE . 'StatisticsController::game')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/games/{app_type}/export', static::$CONTROLLER_NAMESPACE . 'StatisticsController::detailGamesToExcel')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/games/{game_code}', static::$CONTROLLER_NAMESPACE . 'StatisticsController::detailGame')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/praises', static::$CONTROLLER_NAMESPACE . 'StatisticsController::listUsersUsingPraise')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/praises/{app_type}/export', static::$CONTROLLER_NAMESPACE . 'StatisticsController::exportPraiseSummary')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/praises/{app_type}/{user_idx}', static::$CONTROLLER_NAMESPACE . 'StatisticsController::detailPraise')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/praises/{app_type}/{user_idx}/graph', static::$CONTROLLER_NAMESPACE . 'StatisticsController::viewPraiseGraph')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));
        $routes->get('/statistics/praises/{app_type}/{user_idx}/export', static::$CONTROLLER_NAMESPACE . 'StatisticsController::exportPraise')
            ->before(PreAuthenticator::authenticate())
            ->before(PreAuthenticator::authorize($app));

        return $routes;
    }
}
