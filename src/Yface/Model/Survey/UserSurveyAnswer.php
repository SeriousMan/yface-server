<?php
namespace Yface\Model\Survey;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="yf_user_survey_answer")
 * @JMS\ExclusionPolicy("none")
 */
class UserSurveyAnswer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     *
     * @var $user_idx integer
     */
    private $user_idx;
    /**
     * @ORM\Column(type="integer", name="sq_idx", options={"unsigned":true})
     *
     * @var $survey_question_idx integer
     */
    private $survey_question_idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $time integer
     */
    private $survey_todo_idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $answer integer
     */
    private $answer;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;

    /**
     * @ORM\ManyToOne(targetEntity="Yface\Model\Survey\UserSurveyTodo", inversedBy="answers")
     * @ORM\JoinColumn(name="survey_todo_idx", referencedColumnName="idx")
     * @JMS\Exclude
     *
     * @var $survey_todo UserSurveyTodo
     */
    private $survey_todo;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getUserIdx(): int
    {
        return $this->user_idx;
    }

    /**
     * @param int $user_idx
     */
    public function setUserIdx(int $user_idx)
    {
        $this->user_idx = $user_idx;
    }

    /**
     * @return int
     */
    public function getSurveyQuestionIdx(): int
    {
        return $this->survey_question_idx;
    }

    /**
     * @param int $survey_question_idx
     */
    public function setSurveyQuestionIdx(int $survey_question_idx)
    {
        $this->survey_question_idx = $survey_question_idx;
    }

    /**
     * @return int
     */
    public function getSurveyTodoIdx(): int
    {
        return $this->survey_todo_idx;
    }

    /**
     * @param int $survey_todo_idx
     */
    public function setSurveyTodoIdx(int $survey_todo_idx)
    {
        $this->survey_todo_idx = $survey_todo_idx;
    }

    /**
     * @return int
     */
    public function getAnswer(): int
    {
        return $this->answer;
    }

    /**
     * @param int $answer
     */
    public function setAnswer(int $answer)
    {
        $this->answer = $answer;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return UserSurveyTodo
     */
    public function getSurveyTodo(): UserSurveyTodo
    {
        return $this->survey_todo;
    }

    /**
     * @param UserSurveyTodo $survey_todo
     */
    public function setSurveyTodo(UserSurveyTodo $survey_todo)
    {
        $this->survey_todo = $survey_todo;
    }
}
