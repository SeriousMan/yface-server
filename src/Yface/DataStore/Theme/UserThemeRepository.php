<?php
namespace Yface\DataStore\Theme;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Theme\UserTheme;

class UserThemeRepository extends EntityRepository
{
    /**
     * @param $user_idx
     * @return null|object|UserTheme
     */
    public function findOneByUserIdx($user_idx) {
        return $this->findOneBy(array('user_idx' => $user_idx));
    }

    /**
     * @return UserThemeRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Theme\UserTheme');
    }
}
