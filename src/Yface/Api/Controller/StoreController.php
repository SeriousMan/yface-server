<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Exception\StoreException;
use Yface\Library\Localization\Lang;
use Yface\Library\Serializer;
use Yface\Service\Store\StoreService;

class StoreController
{
    public function index(Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $theme_code = intval(trim($request->request->get('theme')));
        $app_type = trim($request->headers->get('app'));

        $products = StoreService::getProductList($user_idx, $theme_code, $app_type);

        $json_string = (new Serializer())->serializeWithJson($products);

        return new Response($json_string, 200, ['Content-Type' => 'application/json']);
    }

    public function order(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $product_idx = intval(trim($request->request->get('pIdx')));
        $app_type = trim($request->headers->get('app'));

        try {
            StoreService::orderProduct($user_idx, $product_idx, $app_type);

            return new Response('', 200);
        } catch (StoreException $se) {
            return $app->json(['message' => Lang::get($se->getMessage())], 400);
        }
    }

    public function getUserProducts(Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $theme_code = intval(trim($request->request->get('theme')));
        $app_type = trim($request->headers->get('app'));

        $user_products = StoreService::getUserProductList($user_idx, $theme_code, $app_type);

        $json_string = (new Serializer())->serializeWithJson(
            $user_products,
            [],
            ['isUserPurchased']
        );

        return new Response($json_string, 200, ['Content-Type' => 'application/json']);
    }

    public function modifyUserProducts(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $products = $request->request->get('products');

        try {
            StoreService::modifyUserProductsPosition($user_idx, $products);

            return new Response('', 200);
        } catch (StoreException $se) {
            return $app->json(['message' => Lang::get($se->getMessage())], 400);
        }
    }
}
