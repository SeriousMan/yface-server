<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [18] - LETS_GO_FISHING_고기 잡이
 */
class GameLetsGoFishing extends ResultData
{
    protected $answer_direction_list;
    protected $user_direction_list;
    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::LETS_GO_FISHING, $results);

        /** 게임 정보 저장 */
        $this->answer_direction_list = implode('|', $results['answerDirectionList']);
        $this->user_direction_list = implode('|', $results['userDirectionList']);
        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'answer_direction_list' => $this->answer_direction_list,
            'user_direction_list' => $this->user_direction_list,
            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
