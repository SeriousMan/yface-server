<?php
namespace Yface\Game;

use Yface\Exception\GameException;
use Yface\Game\ResultData\Ycog\GameBadumTish;
use Yface\Game\ResultData\Ycog\GameFindTheMatch;
use Yface\Game\ResultData\Ycog\GameHitTheRoad;
use Yface\Game\ResultData\Ycog\GameIdenticalPicture;
use Yface\Game\ResultData\Ycog\GameIsItThere;
use Yface\Game\ResultData\Ycog\GameLetsGoFishing;
use Yface\Game\ResultData\Ycog\GameNavigateMe;
use Yface\Game\ResultData\Ycog\GameStackTheBricks;
use Yface\Game\ResultData\Ycog\GameTapTapTap;
use Yface\Game\ResultData\Ycog\GameToEat;
use Yface\Game\ResultData\Ycog\GameUpDown;
use Yface\Game\ResultData\Ycog\GameWhatsTheNumber;
use Yface\Game\ResultData\Yface\GameCard;
use Yface\Game\ResultData\Yface\GameEye;
use Yface\Game\ResultData\Yface\GameEyeFight;
use Yface\Game\ResultData\Yface\GameFace;
use Yface\Game\ResultData\Yface\GameFriend;
use Yface\Game\ResultData\Yface\GameLook;
use Yface\Game\ResultData\Yface\GameMatch;
use Yface\Game\ResultData\Yface\GameModel;
use Yface\Game\ResultData\Yface\GameOrder;
use Yface\Game\ResultData\Yface\GameTalk;
use Yface\Game\ResultData\Yface\GameThief;
use Yface\Game\ResultData\Yface\GameTre;

class ResultDataBuilder
{
    private $game_code;

    /**
     * 생성자
     * ===
     * @param $game_code
     */
    public function __construct($game_code)
    {
        $this->game_code = $game_code;
    }

    /**
     * 받아온 결과와 게임 고유 번호를 토대로 DB에 저장할 필드를 추출하여 리턴
     * ===
     * @param $results
     * @param array $uploaded_files
     * @return array
     * @throws GameException
     */
    public function build($results, $uploaded_files = [])
    {
        switch ($this->game_code) {
            // YFACE
            case GameCode::CARD:
                return (new GameCard($results))->export();
            case GameCode::EYE:
                return (new GameEye($results))->export();
            case GameCode::EYE_FIGHT:
                return (new GameEyeFight($results))->export();
            case GameCode::FACE:
                return (new GameFace($results, $uploaded_files))->export();
            case GameCode::FRIEND:
                return (new GameFriend($results))->export();
            case GameCode::LOOK:
                return (new GameLook($results))->export();
            case GameCode::MATCH:
                return (new GameMatch($results))->export();
            case GameCode::MODEL:
                return (new GameModel($results, $uploaded_files))->export();
            case GameCode::ORDER:
                return (new GameOrder($results))->export();
            case GameCode::TALK:
                return (new GameTalk($results, $uploaded_files))->export();
            case GameCode::THIEF:
                return (new GameThief($results))->export();
            case GameCode::TRE:
                return (new GameTre($results))->export();

            // YCOG
            case GameCode::BADUM_TISH:
                return (new GameBadumTish($results))->export();
            case GameCode::FIND_THE_MATCH:
                return (new GameFindTheMatch($results))->export();
            case GameCode::HIT_THE_ROAD:
                return (new GameHitTheRoad($results))->export();
            case GameCode::IDENTICAL_PICTURE:
                return (new GameIdenticalPicture($results))->export();
            case GameCode::IS_IT_THERE:
                return (new GameIsItThere($results))->export();
            case GameCode::LETS_GO_FISHING:
                return (new GameLetsGoFishing($results))->export();
            case GameCode::NAVIGATE_ME:
                return (new GameNavigateMe($results))->export();
            case GameCode::STACK_THE_BRICKS:
                return (new GameStackTheBricks($results))->export();
            case GameCode::TAP_TAP_TAP:
                return (new GameTapTapTap($results))->export();
            case GameCode::TO_EAT:
                return (new GameToEat($results))->export();
            case GameCode::UP_DOWN:
                return (new GameUpDown($results))->export();
            case GameCode::WHATS_THE_NUMBER:
                return (new GameWhatsTheNumber($results))->export();

            default:
                throw new GameException('game.game.incorrect');
        }
    }
}
