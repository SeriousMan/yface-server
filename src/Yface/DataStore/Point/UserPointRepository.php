<?php
namespace Yface\DataStore\Point;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;

class UserPointRepository extends EntityRepository
{
    /**
     * @param $user_idx
     * @param $app_type
     * @return int
     */
    public function getUserTotalPoint($user_idx, $app_type)
    {
        $dql = 'SELECT sum(p.amount)
                FROM Yface\Model\Point\UserPoint p
                WHERE p.user_idx = :user_idx
                  AND p.app_type = :app_type';

        $query = $this->_em->createQuery($dql);
        $query->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->setParameter('app_type', $app_type, Type::STRING);

        return (int)$query->getSingleScalarResult();
    }

    /**
     * @return UserPointRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Point\UserPoint');
    }
}
