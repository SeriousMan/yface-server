<?php
namespace Yface\DataStore\Statistics;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Statistics\ExecutionRateStatistics;

class ExecutionRateStatisticsRepository extends EntityRepository
{
    /**
     * @param $game_code
     * @return ExecutionRateStatistics[]
     */
    public function findByGameCode($game_code)
    {
        return $this->findBy(array('game_code' => $game_code), array('idx' => 'ASC'));
    }

    /**
     * @return ExecutionRateStatisticsRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Statistics\ExecutionRateStatistics');
    }
}
