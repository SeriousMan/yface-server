<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [19] - NAVIGATE_ME_요리 조리
 */
class GameNavigateMe extends ResultData
{
    protected $green_count;
    protected $green_answer_count;
    protected $green_wrong_count;
    protected $green_accuracy;
    protected $green_no_response_count;
    protected $green_average_response_time;

    protected $yellow_count;
    protected $yellow_answer_count;
    protected $yellow_wrong_count;
    protected $yellow_accuracy;
    protected $yellow_no_response_count;
    protected $yellow_average_response_time;

    protected $red_count;
    protected $red_answer_count;
    protected $red_accuracy;
    protected $red_wrong_count;

    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::NAVIGATE_ME, $results);

        /** 게임 정보 저장 */
        $this->green_count = $results['greenCount'];
        $this->green_answer_count = $results['greenAnswerCount'];
        $this->green_wrong_count = $results['greenWrongCount'];
        $this->green_accuracy = $results['greenAccuracy'];
        $this->green_no_response_count = $results['greenNoResponseCount'];
        $this->green_average_response_time = $results['greenAverageResponseTime'];

        $this->yellow_count = $results['yellowCount'];
        $this->yellow_answer_count = $results['yellowAnswerCount'];
        $this->yellow_wrong_count = $results['yellowWrongCount'];
        $this->yellow_accuracy = $results['yellowAccuracy'];
        $this->yellow_no_response_count = $results['yellowNoResponseCount'];
        $this->yellow_average_response_time = $results['yellowAverageResponseTime'];

        $this->red_count = $results['redCount'];
        $this->red_answer_count = $results['redAnswerCount'];
        $this->red_accuracy = $results['redAccuracy'];
        $this->red_wrong_count = $results['redWrongCount'];

        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'green_count' => $this->green_count,
            'green_answer_count' => $this->green_answer_count,
            'green_wrong_count' => $this->green_wrong_count,
            'green_accuracy' => $this->green_accuracy,
            'green_no_response_count' => $this->green_no_response_count,
            'green_average_response_time' => $this->green_average_response_time,

            'yellow_count' => $this->yellow_count,
            'yellow_answer_count' => $this->yellow_answer_count,
            'yellow_wrong_count' => $this->yellow_wrong_count,
            'yellow_accuracy' => $this->yellow_accuracy,
            'yellow_no_response_count' => $this->yellow_no_response_count,
            'yellow_average_response_time' => $this->yellow_average_response_time,

            'red_count' => $this->red_count,
            'red_answer_count' => $this->red_answer_count,
            'red_accuracy' => $this->red_accuracy,
            'red_wrong_count' => $this->red_wrong_count,

            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
