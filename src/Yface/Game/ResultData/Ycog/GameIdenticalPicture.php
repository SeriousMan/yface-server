<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\IdenticalPicture;
use Yface\Game\ResultData;

/**
 * CODE [16] - IDENTICAL_PICTURE_무무똑
 */
class GameIdenticalPicture extends ResultData
{
    protected $answer_count;
    protected $user_answer_count_list;

    protected $answer_set_list;
    protected $answer_set_list_3d;
    protected $user_set_list;
    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::IDENTICAL_PICTURE, $results);

        /** 게임 정보 저장 */
        $this->answer_count = $results['answerCount'];
        $this->user_answer_count_list = implode('|', $results['userAnswerCountList']);
        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];

        $this->answer_set_list = implode(
            '|',
            array_map(
                function (array $value) {
                    return implode('|', $value);
                },
                $results['answerSetList']
            )
        );

        $level_info = json_decode($this->level_info, true);

        // 2D -> 3D 변환
        $this->answer_set_list_3d = IdenticalPicture::convertAnswerSetListTo3d($level_info['level'], $this->answer_set_list);

        /** SET LIST 3-Dimensional Array */
        $this->user_set_list = implode(
            '|',
            array_map(
                function (array $value) {
                    return implode(';', array_map(function (array $value) {
                        return implode(',', $value);
                    }, $value));
                },
                $results['userSetList']
            )
        );
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'answer_count' => $this->answer_count,
            'user_answer_count_list' => $this->user_answer_count_list,

            'answer_set_list' => $this->answer_set_list,
            'answer_set_list_3d' => $this->answer_set_list_3d,
            'user_set_list' => $this->user_set_list,
            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
