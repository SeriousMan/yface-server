<?php
namespace Yface\Admin\Controller;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\Request;
use Yface\Admin\Library\UrlHelper;
use Yface\Game\GameCode;
use Yface\Game\GameLevel;
use Yface\Library\Configuration;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\File\FileHelper;
use Yface\Library\File\ImageUploader;
use Yface\Library\Template\TwigRenderer;
use Yface\Service\Game\TodayGameService;

class PrivacyController
{
    /**
     * 컨트롤러) 동의 관련 이미지 수정
     * ===
     *
     * @return string
     */
    public function index()
    {
        $db = ConnectionProvider::getConnection();

        $sql = "SELECT * FROM yf_acceptance_image ORDER BY idx ASC";
        $resources = $db->fetchAll($sql);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'resources' => $resources
        ]);

        return $twig_renderer->render('admin/privacy/index');
    }

    public function save(Request $request)
    {
        $resource_idx = intval(trim($request->get('resourceIdx')));
        $lang = trim($request->get('lang'));
        $app_type = trim($request->get('appType'));
        $file = $request->files->get('file');

        if (empty($file) || empty($resource_idx) || empty($app_type) || empty($lang)) {
            return UrlHelper::returnWithMessage('모든 입력란을 입력해주세요.');
        }

        $db = ConnectionProvider::getConnection();
        $db->transactional(
            function (Connection $conn) use ($resource_idx, $lang, $file, $app_type) {
                $file_name = FileHelper::generateRandomFileName() . "_{$lang}";
                $file_path = Configuration::$values['path']['resources'] . '/accept';

                $file_name = ImageUploader::upload($file, $file_path, $file_name);
                $image_url = Configuration::$values['url']['resources'] . '/accept/' . $file_name;

                $conn->update(
                    'yf_acceptance_image',
                    [
                        'img_url' => $image_url
                    ],
                    [
                        'idx' => $resource_idx
                    ]
                );
            }
        );

        return UrlHelper::redirect('/admin/privacy', '파일이 갱신되었습니다.');
    }
}
