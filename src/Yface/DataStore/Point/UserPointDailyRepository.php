<?php
namespace Yface\DataStore\Point;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Point\UserPointDaily;

class UserPointDailyRepository extends EntityRepository
{
    /**
     * @param $user_idx
     * @param $days
     * @param $app_type
     * @return null|object|UserPointDaily
     */
    public function findOneByUserIdxAndDays($user_idx, $days, $app_type)
    {
        return $this->findOneBy(array('user_idx' => $user_idx, 'days' => $days, 'app_type' => $app_type));
    }

    /**
     * @param $user_idx
     * @param $app_type
     * @return UserPointDaily[]
     */
    public function findByUserIdx($user_idx, $app_type)
    {
        return $this->findBy(array('user_idx' => $user_idx, 'app_type' => $app_type));
    }

    /**
     * @param $user_idx
     * @param $days
     * @param $app_type
     * @return int
     */
    public function getAccumulatePointByUserIdxAndDays($user_idx, $days, $app_type)
    {
        $dql = 'SELECT sum(us.obtain_point)
                FROM Yface\Model\Point\UserPointDaily us
                WHERE us.user_idx = :user_idx
                  AND us.app_type = :app_type
                  AND us.days <= :days';

        $query = $this->_em->createQuery($dql);
        $query->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->setParameter('app_type', $app_type, Type::STRING)
            ->setParameter('days', $days, Type::INTEGER);

        return (int)$query->getSingleScalarResult();
    }

    /**
     * @return UserPointDailyRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Point\UserPointDaily');
    }
}
