<?php
namespace Yface\Library;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Yface\Library\ExclusionPolicy\PropertyExclusionPolicy;

class Serializer
{
    private $serializer;
    private $attributes;

    public function __construct()
    {
        $this->serializer = SerializerBuilder::create()->build();
        $this->attributes = $this->buildAttributes();
    }

    private function buildAttributes()
    {
        $context = SerializationContext::create();
        $context->setSerializeNull(true)->enableMaxDepthChecks();

        return $context;
    }

    /**
     * @param $any_object
     * @param array $groups
     * @param array $exclude_fields
     * @return mixed|string
     */
    public function serializeWithJson($any_object, $groups = array(), $exclude_fields = array())
    {
        if (!empty($groups)) {
            $this->attributes->setGroups($groups);
        }

        if (!empty($exclude_fields)) {
            $this->attributes->addExclusionStrategy(
                new PropertyExclusionPolicy($exclude_fields)
            );
        }

        return $this->serializer->serialize($any_object, 'json', $this->attributes);
    }
}