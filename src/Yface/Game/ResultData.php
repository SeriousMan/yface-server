<?php
namespace Yface\Game;

abstract class ResultData
{
    protected $game_code;
    protected $correct_count;
    protected $wrong_count;
    protected $play_count;
    protected $accuracy;
    protected $response_time;
    protected $avg_response_time;
    protected $end_time;
    protected $is_obtain_star;
    protected $level_info;
    protected $uploaded_files;

    /**
     * (생성자)
     * 앱에서 받은 결과로 테이블에 들어갈 컬럼 데이터 맵핑
     * ===
     * @param $results
     * @param $uploaded_files array
     * @param $game_code
     */
    public function __construct($game_code, $results, $uploaded_files = [])
    {
        $this->game_code = $game_code;
        $this->correct_count = $results['correctCount'];
        $this->wrong_count = $results['wrongCount'];
        $this->play_count = $results['playCount'];
        $this->accuracy = $results['accuracy'];
        $this->response_time = implode(',', $results['responseTimeList']);
        $this->avg_response_time = $results['averageResponseTime'];
        $this->end_time = date("Y-m-d H:i:s");
        $this->is_obtain_star = $results['obtainStar'];
        $this->level_info = json_encode($results['levelInfo']);

        if (!empty($uploaded_files)) {
            $this->uploaded_files = implode('|', $uploaded_files);
        }
    }

    /** @return array */
    protected function export()
    {
        $results = [
            'correct_count' => $this->correct_count,
            'wrong_count' => $this->wrong_count,
            'play_count' => $this->play_count,
            'accuracy' => $this->accuracy,
            'response_time' => $this->response_time,
            'avg_response_time' => $this->avg_response_time,
            'end_time' => $this->end_time,
            'is_obtain_star' => $this->is_obtain_star,
            'level_info' => $this->level_info
        ];

        if (!empty($this->uploaded_files)) {
            $results = array_merge($results, ['uploaded_files' => $this->uploaded_files]);
        }

        return $results;
    }
}
