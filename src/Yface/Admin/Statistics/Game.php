<?php
namespace Yface\Admin\Statistics;

use Yface\Game\GameCode;
use Yface\Library\Database\ConnectionProvider;

class Game
{
    /**
     * 게임별 통계용 데이터 리턴
     * ===
     * @param $game_code
     * @return array
     */
    public static function buildGame($game_code, $research_idx)
    {
        $table_name = GameCode::getGameTableName($game_code);

        $db = ConnectionProvider::getConnection();
        if(empty($research_idx)) {
            $sql = "SELECT count(idx) FROM {$table_name} WHERE is_obtain_star = 1";
        }
        else {
            $sql = "SELECT count(g.idx) FROM {$table_name} g JOIN yf_user_research_status urs ON g.u_idx = urs.u_idx WHERE is_obtain_star = 1 AND urs.r_idx = " . $research_idx;
        }

        $total_star = $db->fetchColumn($sql);
        $total_point = $total_star * 100;

        if(empty($research_idx)) {
            $sql = "SELECT count(idx) FROM {$table_name} WHERE accuracy IS NOT NULL AND is_disabled = 0";
        }
        else {
            $sql = "SELECT count(g.idx) FROM {$table_name} g JOIN yf_user_research_status urs ON g.u_idx = urs.u_idx WHERE accuracy IS NOT NULL AND is_disabled = 0 AND urs.r_idx = " . $research_idx;
        }

        $total_play_count = $db->fetchColumn($sql);

        if(empty($research_idx)) {
            $sql = "SELECT current_level, avg(accuracy) as avg_accuracy
                    FROM {$table_name}
                    WHERE accuracy IS NOT NULL
                      AND is_disabled = 0
                    GROUP BY current_level";
        }
        else {
            $sql = "SELECT current_level, avg(accuracy) as avg_accuracy
                    FROM {$table_name} g
                    JOIN yf_user_research_status urs
                    ON g.u_idx = urs.u_idx
                    WHERE accuracy IS NOT NULL
                      AND is_disabled = 0
                      AND urs.r_idx = " . $research_idx . "
                    GROUP BY current_level";
        }

        $avg_accuracy_by_level = $db->fetchAll($sql);

        if(empty($research_idx)) {
            $sql = "SELECT current_level, avg(avg_response_time) as avg_rt
                    FROM {$table_name}
                    WHERE avg_response_time IS NOT NULL
                      AND is_disabled = 0
                    GROUP BY current_level";
        }
        else {
            $sql = "SELECT current_level, avg(avg_response_time) as avg_rt
                    FROM {$table_name} g
                    JOIN yf_user_research_status urs
                    ON g.u_idx = urs.u_idx
                    WHERE avg_response_time IS NOT NULL
                      AND is_disabled = 0
                      AND urs.r_idx = " . $research_idx . "
                    GROUP BY current_level";
        }

        $avg_response_time_by_level = $db->fetchAll($sql);

        /** 아래의 게임은 레벨이 상, 중, 하로 나뉘어져 있다. */
        if (static::isThreePartLevelGame($game_code)) {
            $avg_accuracy_by_level = static::divideThreePartLevel($avg_accuracy_by_level, 'avg_accuracy');
            $avg_response_time_by_level = static::divideThreePartLevel($avg_response_time_by_level, 'avg_rt');
        }

        if(empty($research_idx)) {
            $sql = "SELECT u_idx, current_level, DATE(reg_date) as date
                    FROM {$table_name}
                    WHERE is_disabled = 0
                    GROUP BY u_idx, current_level
                    ORDER BY u_idx ASC, reg_date ASC";
        }
        else {
            $sql = "SELECT g.u_idx, current_level, DATE(g.reg_date) as date
                    FROM {$table_name} g
                    JOIN yf_user_research_status urs
                    ON g.u_idx = urs.u_idx
                    WHERE is_disabled = 0
                    AND urs.r_idx = {$research_idx}
                    GROUP BY g.u_idx, current_level
                    ORDER BY g.u_idx ASC, g.reg_date ASC";
        }

        $level_up_dates = $db->fetchAll($sql);

        /** 레벨 업 평균 소요 일수 계산 (유저별) -> 추가 계산 (게임별) */
        $user_idx = null;
        $user_level_up_date = [];
        $user_level_up_days = [];
        $avg_level_up_days =[];
        foreach ($level_up_dates as $date) {
            /** 유저별로 계산해야 하기 때문에 유저 정보가 다르면 평균 소요일수 계산 로직 실행 */
            if ($date['u_idx'] != $user_idx) {
                /** 처음 loop은 초기화만 */
                if (empty($user_idx)) {
                    $user_idx = $date['u_idx'];
                    $user_level_up_date = [];
                    $user_level_up_date[] = $date['date'];

                    continue;
                }

                $user_idx = $date['u_idx'];

                /** 아직 1레벨이면 무시 */
                if (count($user_level_up_date) <= 1) {
                    $user_level_up_date = [];
                    $user_level_up_date[] = $date['date'];

                    continue;
                }

                /** 레벨업 소요일수를 차례로 계산하여 임시 저장 */
                for ($i = 0; $i < count($user_level_up_date) - 1; $i++) {
                    $level_up_date = new \DateTime($user_level_up_date[$i + 1]);
                    $criteria = new \DateTime($user_level_up_date[$i]);
                    $diff = $level_up_date->diff($criteria);

                    $user_level_up_days[] = $diff->days;
                }

                /** 유저의 평균 레벨업 소요일수 계산하여 임시 저장 */
                $avg_level_up_days[] = array_sum($user_level_up_days) / count($user_level_up_days);
                $user_level_up_date = [];
                $user_level_up_date[] = $date['date'];

                continue;
            }

            $user_level_up_date[] = $date['date'];
        }

        /** 해당 게임의 평균 소요일수 계산 */
        $avg_level_up_day = 0;
        if (count($avg_level_up_days) > 0) {
            $avg_level_up_day = array_sum($avg_level_up_days) / count($avg_level_up_days);
        }

        return compact(
            'total_star',
            'total_point',
            'total_play_count',
            'avg_level_up_day',
            'avg_accuracy_by_level',
            'avg_response_time_by_level'
        );
    }

    /**
     * 레벨이 상중하로 나뉘어져있는 게임인지 여부 리턴
     * ===
     * @param $game_code
     * @return bool
     */
    private static function isThreePartLevelGame($game_code)
    {
        return in_array($game_code, [GameCode::MODEL, GameCode::TAP_TAP_TAP, GameCode::FACE]);
    }

    /**
     * 레벨을 상중하 형태로 재편
     * ===
     * @param $value
     * @param $target_string
     * @return array
     */
    private static function divideThreePartLevel($value, $target_string)
    {
        $temp_arr = [];
        $temp_result = 0;

        for ($i = 1; $i <= 15; $i++) {
            $temp_result = floatval($temp_result) + floatval($value[$i - 1][$target_string]);

            if ($i % 5 == 0) {
                $temp_arr[] = [$target_string => floatval($temp_result / 5)];
                $temp_result = 0;
            }
        }

        return $temp_arr;
    }
}
