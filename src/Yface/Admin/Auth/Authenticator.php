<?php
namespace Yface\Admin\Auth;

use Yface\Library\Database\ConnectionProvider;

class Authenticator
{
    /**
     * 관리자 인증 성공 여부 리턴
     * ===
     * @param $id
     * @param $password
     * @return bool
     */
    public static function authenticate($id, $password)
    {
        $db = ConnectionProvider::getConnection();
        $encrypt_password = $db->fetchColumn('SELECT password FROM yf_admin WHERE id = ?', array($id));

        return password_verify($password, $encrypt_password);
    }
}
