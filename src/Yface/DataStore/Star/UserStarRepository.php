<?php
namespace Yface\DataStore\Star;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Star\UserStar;

class UserStarRepository extends EntityRepository
{
    /**
     * @param $user_idx
     * @param $game_code
     * @return int
     */
    public function getCountByGameCode($user_idx, $game_code)
    {
        $dql = 'SELECT count(us)
                FROM Yface\Model\Star\UserStar us
                WHERE us.user_idx = :user_idx
                  AND us.game_code = :game_code';

        $query = $this->_em->createQuery($dql);
        $query->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->setParameter('game_code', $game_code, Type::INTEGER);

        return (int)$query->getSingleScalarResult();
    }

    /**
     * @param $user_idx
     * @param $app_type
     * @return int
     */
    public function getCountByAppType($user_idx, $app_type)
    {
        $dql = 'SELECT count(us)
                FROM Yface\Model\Star\UserStar us
                WHERE us.user_idx = :user_idx
                  AND us.app_type = :app_type';

        $query = $this->_em->createQuery($dql);
        $query->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->setParameter('app_type', $app_type, Type::STRING);

        return (int)$query->getSingleScalarResult();
    }

    /**
     * @param $user_idx
     * @param $days
     * @param $app_type
     * @return int
     */
    public function getAccumulateCountByUserIdxAndDays($user_idx, $days, $app_type)
    {
        $dql = 'SELECT count(us)
                FROM Yface\Model\Star\UserStar us
                WHERE us.user_idx = :user_idx
                  AND us.app_type = :app_type
                  AND us.days <= :days';

        $query = $this->_em->createQuery($dql);
        $query->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->setParameter('app_type', $app_type, Type::STRING)
            ->setParameter('days', $days, Type::INTEGER);

        return (int)$query->getSingleScalarResult();
    }

    /**
     * @param $user_idx
     * @param $game_code
     * @param $from
     * @param $to
     * @return UserStar[]
     */
    public function findByUserIdxAndGameCode($user_idx, $game_code, $from, $to)
    {
        $dql = 'SELECT us
                FROM Yface\Model\Star\UserStar us
                WHERE us.user_idx = :user_idx
                  AND us.game_code = :game_code
                  AND us.reg_date >= :from
                  AND us.reg_date <= :to';

        $query = $this->_em->createQuery($dql);
        $query->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->setParameter('game_code', $game_code, Type::INTEGER)
            ->setParameter('from', $from, Type::DATETIME)
            ->setParameter('to', $to, Type::DATETIME);

        return $query->getResult();
    }

    /**
     * @return UserStarRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Star\UserStar');
    }
}
