<?php
namespace Yface\DataStore\Game;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Game\Game;

class GameRepository extends EntityRepository
{
    /**
     * @param $app_type
     * @return Game[]
     */
    public function findActiveByAppType($app_type)
    {
        return $this->findBy(array('app_type' => $app_type, 'is_disabled' => 0));
    }

    /**
     * @param $app_type
     * @return Game[]
     */
    public function findNonSelectedByAppType($app_type)
    {
        return $this->findBy(array('app_type' => $app_type, 'is_disabled' => 0, 'is_selected' => 0));
    }

    /**
     * @return GameRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Game\Game');
    }
}
