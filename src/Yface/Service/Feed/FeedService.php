<?php
namespace Yface\Service\Feed;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Yface\DataStore\Feed\FeedActionRepository;
use Yface\DataStore\Feed\FeedRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\FeedException;
use Yface\Library\Configuration;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Library\File\FileHelper;
use Yface\Library\File\ImageUploader;
use Yface\Model\Feed\Feed;
use Yface\Model\Feed\FeedAction;

class FeedService
{
    /**
     * 피드 생성
     * ===
     * @param $user_idx
     * @param $app_type
     * @param UploadedFile $image_file
     * @return Feed
     * @throws FeedException
     */
    public static function createFeed($user_idx, $app_type, UploadedFile $image_file)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            throw new FeedException('sns.user.incorrect');
        }

        $feed = new Feed();
        $feed->setUser($user);
        $feed->setLikeCount(0);
        $feed->setCoolCount(0);
        $feed->setPrettyCount(0);
        $feed->setEnviousCount(0);
        $feed->setIsDisabled(false);
        $feed->setAppType($app_type);
        $feed->setRegDate(new \DateTime('now'));

        $em = EntityManagerProvider::getEntityManager();
        $em->beginTransaction();

        try {
            $file_path = Configuration::$values['path']['resources'] . '/feeds';
            $file_name = FileHelper::generateRandomFileName();

            $file_name = ImageUploader::upload($image_file, $file_path, $file_name);
            $image_url = Configuration::$values['url']['resources'] . '/feeds/' . $file_name;

            $feed->setImgUrl($image_url);

            $em->persist($feed);
            $em->flush($feed);
            $em->refresh($feed);
            $em->commit();

            return $feed;
        } catch (\Exception $e) {
            $em->rollback();
            $em->close();

            throw new FeedException('sns.error.occured');
        }
    }

    /**
     * 피드 삭제
     * ===
     * @param $user_idx
     * @param $feed_idx
     * @throws FeedException
     */
    public static function removeFeed($user_idx, $feed_idx)
    {
        $feed = FeedRepository::getRepository()->findOneByUserIdxAndFeedIdx($user_idx, $feed_idx);
        if (empty($feed)) {
            throw new FeedException('sns.no.feed.cancel');
        }

        $feed->setIsDisabled(true);

        $em = EntityManagerProvider::getEntityManager();
        $em->persist($feed);
        $em->flush($feed);
    }

    /**
     * 피드 액션
     * ===
     * @param $feed_idx
     * @param $user_idx
     * @param $action
     * @throws FeedException
     * @return null|Object|Feed
     */
    public static function saveAction($feed_idx, $user_idx, $action)
    {
        $feed = FeedRepository::getRepository()->findOneByFeedIdx($feed_idx);
        if (empty($feed)) {
            throw new FeedException('sns.feed.incorrect');
        }

        $feed_action = FeedActionRepository::getRepository()->findOneByFeedIdxAndUserIdx($feed_idx, $user_idx, $action);
        if (empty($feed_action)) {
            $feed_action = new FeedAction();
            $feed_action->setFeedIdx($feed_idx);
            $feed_action->setFeed($feed);
            $feed_action->setUserIdx($user_idx);
            $feed_action->setAction($action);
            $feed_action->setIsActive(true);
            $feed_action->setRegDate(new \DateTime('now'));
        } else {
            $feed_action->setIsActive(!$feed_action->getIsActive());
        }

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($feed_action, $feed) {
                $em->persist($feed_action);
                $em->flush($feed_action);

                $feed->applyActionCount($feed_action->getAction(), $feed_action->getIsActive());

                $em->persist($feed);
                $em->flush($feed);
            }
        );

        $feed->setActionUserIdx($user_idx);

        return $feed;
    }

    /**
     * 유저 피드 목록
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getUserFeeds($user_idx, $app_type)
    {
        $user_feeds = FeedRepository::getRepository()->findByUserIdx($user_idx, $app_type);

        $feeds = array();
        /** @var $feed Feed */
        foreach ($user_feeds as $feed) {
            $feed->setActionUserIdx($user_idx);
            $feeds[] = $feed;
        }

        return $feeds;
    }

    /**
     * 나의 친구의 피드 목록
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getFriendsFeeds($user_idx, $app_type)
    {
        $user_feeds = FeedRepository::getRepository()->findAllFeedsByAppType($app_type);

        $feeds = array();
        /** @var $feed Feed */
        foreach ($user_feeds as $feed) {
            $feed->setActionUserIdx($user_idx);
            $feeds[] = $feed;
        }

        return $feeds;
    }
}
