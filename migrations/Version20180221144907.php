<?php declare(strict_types = 1);

namespace YFACE\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180221144907 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('
          CREATE TABLE `yf_researcher` (
          `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `a_idx` int(10) unsigned NOT NULL,
          `r_idx` int(10) unsigned NOT NULL,
          `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`idx`),
          `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
        ');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('
          DROP TABLE `yf_researcher`;
        ');
    }
}
