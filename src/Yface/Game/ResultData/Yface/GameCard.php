<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [1] - CARD_쌍둥이를 찾아라
 */
class GameCard extends ResultData
{
    protected $pair_count;
    protected $available_fail_count;
    protected $total_used_fail_count;
    protected $timeout_fail_count;
    protected $no_remain_fail_count;
    protected $used_fail_count_list;
    protected $card_visible_time;

    public function __construct($results)
    {
        parent::__construct(GameCode::CARD, $results);

        /** 난이도 정보 저장 */
        $this->pair_count = $results['levelInfo']['pairCount'];
        $this->available_fail_count = $results['levelInfo']['availableFailCount'];
        $this->card_visible_time = $results['levelInfo']['cardVisibleTime'];

        /** 게임 정보 저장 */
        $this->total_used_fail_count = $results['totalUsedFailCount'];
        $this->timeout_fail_count = $results['timeoutFailCount'];
        $this->no_remain_fail_count = $results['noRemainFailCount'];
        $this->used_fail_count_list = implode('|', $results['usedFailCountList']);
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'pair_count' => $this->pair_count,
            'available_fail_count' => $this->available_fail_count,
            'total_used_fail_count' => $this->total_used_fail_count,
            'timeout_fail_count' => $this->timeout_fail_count,
            'no_remain_fail_count' => $this->no_remain_fail_count,
            'used_fail_count_list' => $this->used_fail_count_list,
            'card_visible_time' => $this->card_visible_time
        ]);
    }
}
