<?php
namespace Yface\Model\Game;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Game\TodayGameRepository")
 * @ORM\Table(name="yf_today_game")
 * @JMS\ExclusionPolicy("none")
 */
class TodayGame
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var $game_codes string
     */
    private $game_codes;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YFACE', 'YCOG')")
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return string
     */
    public function getGameCodes(): string
    {
        return $this->game_codes;
    }

    /**
     * @param string $game_codes
     */
    public function setGameCodes(string $game_codes)
    {
        $this->game_codes = $game_codes;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }
}
