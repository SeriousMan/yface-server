<?php
namespace Yface\Command;

use Symfony\Component\Console\Command\Command;
use Yface\Library\Configuration;

class BaseCommand extends Command
{
    public static function report($message)
    {
        if (!isset(Configuration::$values['rollbar']['accessToken'])) {
            return;
        }

        \Rollbar::report_message('[Command] ' . $message, \Level::INFO);
    }
}
