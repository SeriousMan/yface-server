<?php
namespace Yface\Model\Game;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Game\GameRepository")
 * @ORM\Table(name="yf_game")
 * @JMS\ExclusionPolicy("none")
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="NONE")
     *
     * @var $code integer
     */
    private $code;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YFACE', 'YCOG')")
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="string")
     *
     * @var $title_kr string
     */
    private $title_kr;
    /**
     * @ORM\Column(type="string")
     *
     * @var $title_en string
     */
    private $title_en;
    /**
     * @ORM\Column(type="boolean")
     *
     * @var $is_disabled boolean
     */
    private $is_disabled;
    /**
     * @ORM\Column(type="boolean")
     *
     * @var $is_selected boolean
     */
    private $is_selected;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var $mod_date \DateTime
     */
    private $mod_date;

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return string
     */
    public function getTitleKr(): string
    {
        return $this->title_kr;
    }

    /**
     * @param string $title_kr
     */
    public function setTitleKr(string $title_kr)
    {
        $this->title_kr = $title_kr;
    }

    /**
     * @return string
     */
    public function getTitleEn(): string
    {
        return $this->title_en;
    }

    /**
     * @param string $title_en
     */
    public function setTitleEn(string $title_en)
    {
        $this->title_en = $title_en;
    }

    /**
     * @return boolean
     */
    public function getIsDisabled(): bool
    {
        return $this->is_disabled;
    }

    /**
     * @param boolean $is_disabled
     */
    public function setIsDisabled(bool $is_disabled)
    {
        $this->is_disabled = $is_disabled;
    }

    /**
     * @return boolean
     */
    public function getIsSelected(): bool
    {
        return $this->is_selected;
    }

    /**
     * @param boolean $is_selected
     */
    public function setIsSelected(bool $is_selected)
    {
        $this->is_selected = $is_selected;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return \DateTime
     */
    public function getModDate(): \DateTime
    {
        return $this->mod_date;
    }

    /**
     * @param \DateTime $mod_date
     */
    public function setModDate(\DateTime $mod_date)
    {
        $this->mod_date = $mod_date;
    }
}
