<?php
namespace Yface\Command\Game;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Service\Game\TodayGameService;

class TodayGameSelectCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('game:today:select')
            ->setDescription('오늘의 게임 선정');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('TODAY GAME SELECT - 오늘의 게임 선정 CRON 시작');

        self::select(APP_TYPE_YFACE);
        self::select(APP_TYPE_YCOG);

        static::report('TODAY GAME SELECT - 오늘의 게임 선정 CRON 종료');
    }

    protected function select($app_type)
    {
        TodayGameService::selectTodayGame($app_type);
    }
}
