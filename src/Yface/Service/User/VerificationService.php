<?php
namespace Yface\Service\User;

use Yface\DataStore\User\UserRepository;
use Yface\Exception\RegistrationException;
use Yface\Exception\UserException;

class VerificationService
{
    /**
     * 아이디 중복 확인 및 유효성 검사
     * ===
     * @param $id
     * @throws \Exception
     */
    public static function verifyId($id)
    {
        try {
            RegistrationValidationService::validateId($id);
        } catch (RegistrationException $re) {
            throw new UserException('user.sign.up.id.already.in.use');
        }
    }

    /**
     * @param $nickname
     * @throws \Exception
     */
    public static function verifyNickName($nickname)
    {
        try {
            RegistrationValidationService::validateNickname($nickname);
        } catch (RegistrationException $re) {
            throw new UserException('user.sign.up.nickname.already.in.use');
        }
    }
}
