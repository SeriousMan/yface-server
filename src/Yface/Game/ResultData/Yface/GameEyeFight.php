<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [3] - EYE_FIGHT_눈싸움 한판
 */
class GameEyeFight extends ResultData
{
    protected $active_accuracy;
    protected $both_count;
    protected $both_correct_count;
    protected $both_correct_accuracy;
    protected $both_wrong_count;
    protected $both_timeout_count;
    protected $both_average_response_time;
    protected $one_side_count;
    protected $one_side_correct_count;
    protected $one_side_correct_accuracy;
    protected $one_side_wrong_count;
    protected $one_side_timeout_count;
    protected $one_side_average_response_time;

    public function __construct($results)
    {
        parent::__construct(GameCode::EYE_FIGHT, $results);

        /** 게임 정보 저장 */
        $this->active_accuracy = $results['activeAccuracy'];
        $this->both_count = $results['bothCount'];
        $this->both_correct_count = $results['bothCorrectCount'];
        $this->both_correct_accuracy = $results['bothCorrectAccuracy'];
        $this->both_wrong_count = $results['bothWrongCount'];
        $this->both_timeout_count = $results['bothTimeoutCount'];
        $this->both_average_response_time = $results['bothAverageResponseTime'];
        $this->one_side_count = $results['oneSideCount'];
        $this->one_side_correct_count = $results['oneSideCorrectCount'];
        $this->one_side_correct_accuracy = $results['oneSideCorrectAccuracy'];
        $this->one_side_wrong_count = $results['oneSideWrongCount'];
        $this->one_side_timeout_count = $results['oneSideTimeoutCount'];
        $this->one_side_average_response_time = $results['oneSideAverageResponseTime'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'active_accuracy' => $this->active_accuracy,
            'both_count' => $this->both_count,
            'both_correct_count' => $this->both_correct_count,
            'both_correct_accuracy' => $this->both_correct_accuracy,
            'both_wrong_count' => $this->both_wrong_count,
            'both_timeout_count' => $this->both_timeout_count,
            'both_average_response_time' => $this->both_average_response_time,
            'one_side_count' => $this->one_side_count,
            'one_side_correct_count' => $this->one_side_correct_count,
            'one_side_correct_accuracy' => $this->one_side_correct_accuracy,
            'one_side_wrong_count' => $this->one_side_wrong_count,
            'one_side_timeout_count' => $this->one_side_timeout_count,
            'one_side_average_response_time' => $this->one_side_average_response_time
        ]);
    }
}
