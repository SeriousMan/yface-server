<?php
namespace Yface\Command\Praise;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\DataStore\Praise\PraiseRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Praise\Praise;
use Yface\Model\User\User;
use Yface\Service\User\UserService;

class UserPraiseWeeklyCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('user:praise:weekly')
            ->setDescription('주차별 칭찬스티커 생성');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('USER PRAISE WEEKLY - 주차별 칭찬스티커 생성 CRON 시작');

        self::create(APP_TYPE_YFACE);
        self::create(APP_TYPE_YCOG);

        static::report('USER PRAISE WEEKLY - 주차별 칭찬스티커 생성 CRON 종료');
    }

    private function create($app_type)
    {
        $users = UserService::getAllPraiseStartedUser($app_type);
        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($users, $app_type) {
                /** @var $user User */
                foreach ($users as $user) {
                    $praise = PraiseRepository::getRepository()->findOneByUserIdxAndWeeks(
                        $user->getIdx(),
                        $user->getWeeksFromPraiseStarted($app_type),
                        $app_type
                    );

                    if (!empty($praise)) {
                        continue;
                    }

                    $praise = new Praise();
                    $praise->setUser($user);
                    $praise->setWeeks($user->getWeeksFromPraiseStarted($app_type));
                    $praise->setAppType($app_type);
                    $praise->setRegDate(new \DateTime('now'));
                    $praise->setModDate(new \DateTime('now'));

                    $em->persist($praise);
                    $em->flush($praise);
                }
            }
        );
    }
}
