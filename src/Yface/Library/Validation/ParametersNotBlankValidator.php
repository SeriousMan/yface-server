<?php
namespace Yface\Library\Validation;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * bunch of parameter check if null or empty
 * ===
 * Class ParameterNotBlankValidator
 * @package Yface\Library\Validation
 */
class ParametersNotBlankValidator {
    /**
     * @param array $parameters
     * @return bool
     */
    public static function validate(array $parameters = array()) {
        $validator = Validation::createValidator();

        foreach ($parameters as $parameter) {
            $invalidation_list = $validator->validate($parameter, new Assert\NotBlank());
            if (count($invalidation_list) !== 0) {
                return false;
            }
        }

        return true;
    }
}