<?php
namespace Yface\Command\Point;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\User\User;
use Yface\Service\Point\PointService;
use Yface\Service\User\UserService;

class PointDailyLogCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('user:point:daily')
            ->setDescription('매일 설문 조사를 완료한 유저의 하루 포인트 내역을 생성');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('POINT DAILY LOG - 설문 조사를 완료한 유저의 하루 간의 포인트 내역 생성 CRON 시작');

        self::create(APP_TYPE_YFACE);
        self::create(APP_TYPE_YCOG);

        static::report('POINT DAILY LOG - 설문 조사를 완료한 유저의 하루 간의 포인트 내역 생성 CRON 종료');
    }

    private function create($app_type)
    {
        $users = UserService::getAllSurveyCompletedUser($app_type);

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function () use ($users, $app_type) {
                /** @var $user User */
                foreach ($users as $user) {
                    PointService::logDailyUserPoint(
                        $user->getIdx(),
                        $user->getDaysFromSurveyCompleted($app_type),
                        0,
                        0,
                        $app_type
                    );
                }
            }
        );
    }
}
