<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [8] - MODEL_나는야 모델
 */
class GameModel extends ResultData
{
    protected $origin_answer_gesture_list;
    protected $user_answer_gesture_list;
    protected $im_good_count;
    protected $your_good_count;

    public function __construct($results, $uploaded_files)
    {
        parent::__construct(GameCode::MODEL, $results, $uploaded_files);

        $this->origin_answer_gesture_list = implode('|', $results['originAnswerGestureList']);
        $this->user_answer_gesture_list = implode('|', $results['userAnswerGestureList']);
        $this->im_good_count = $results['imGoodCount'];
        $this->your_good_count = $results['yourGoodCount'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'origin_answer_gesture_list' => $this->origin_answer_gesture_list,
            'user_answer_gesture_list' => $this->user_answer_gesture_list,
            'im_good_count' => $this->im_good_count,
            'your_good_count' => $this->your_good_count
        ]);
    }
}
