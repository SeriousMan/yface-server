<?php
namespace Yface\Admin\Controller;

use Doctrine\DBAL\Connection;
use Kilte\Pagination\Pagination;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Yface\Admin\Library\UrlHelper;
use Yface\DataStore\Survey\UserSurveyTodoRepository;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\Template\TwigRenderer;
use Yface\Model\Survey\UserSurveyTodo;
use Yface\Service\Survey\SurveyService;

class SurveyController
{
    const USER_COUNT_PER_PAGE = 20;

    /**
     * 컨트롤러) 설문 목록
     * ===
     * @return string
     */
    public function index()
    {
        $db = ConnectionProvider::getConnection();
        $surveys = $db->fetchAll('SELECT * FROM yf_survey ORDER BY idx ASC');

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'surveys' => $surveys
        ]);

        return $twig_renderer->render('admin/survey/index');
    }

    /**
     * 컨트롤러) 설문 활성화 처리
     * ===
     *
     * @param Request $request
     *
     * @return string
     */
    public function activate(Request $request)
    {
        $survey_idx = intval(trim($request->get('surveyIdx')));
        if (empty($survey_idx)) {
            return UrlHelper::returnWithMessage('활성화할 설문 정보가 없습니다.');
        }

        $db = ConnectionProvider::getConnection();
        $db->transactional(
            function (Connection $conn) use ($survey_idx) {
                /** 설문 활성화 */
                $conn->update(
                    'yf_survey',
                    ['is_active' => 1],
                    ['idx' => $survey_idx]
                );

                /** 설문 활성화 로깅 */
                $conn->insert(
                    'yf_survey_log',
                    [
                        'survey_idx' => $survey_idx,
                        'description' => '설문 활성화 적용',
                        'reg_date' => date('Y-m-d H:i:s')
                    ]
                );
            }
        );

        return UrlHelper::redirect('/admin/surveys', '설문이 활성화되었습니다.');
    }

    /**
     * 컨트롤러) 설문 비활성화 처리
     * ===
     *
     * @param Request $request
     *
     * @return string
     */
    public function deactivate(Request $request)
    {
        $survey_idx = intval(trim($request->get('surveyIdx')));
        if (empty($survey_idx)) {
            return UrlHelper::returnWithMessage('비활성화할 설문 정보가 없습니다.');
        }

        $db = ConnectionProvider::getConnection();
        $db->transactional(
            function (Connection $conn) use ($survey_idx) {
                /** 설문 비 활성화 */
                $conn->update(
                    'yf_survey',
                    ['is_active' => 0],
                    ['idx' => $survey_idx]
                );

                /** 설문 비활성화 로깅 */
                $conn->insert(
                    'yf_survey_log',
                    [
                        'survey_idx' => $survey_idx,
                        'description' => '설문 비활성화 적용',
                        'reg_date' => date('Y-m-d H:i:s')
                    ]
                );
            }
        );

        return UrlHelper::redirect('/admin/surveys', '설문이 비활성화되었습니다.');
    }

    /**
     * 컨트롤러) 설문 항목 정보
     * ===
     *
     * @param Request $request
     *
     * @return string
     */
    public function detail(Request $request)
    {
        $survey_idx = intval(trim($request->get('surveyIdx')));
        if (empty($survey_idx)) {
            return UrlHelper::returnWithMessage('항목을 수정할 설문 정보가 없습니다.');
        }

        $sql = 'SELECT * FROM yf_survey_question WHERE s_idx = ? ORDER BY idx ASC';

        $db = ConnectionProvider::getConnection();
        $questions = $db->fetchAll($sql, [$survey_idx]);

        $sql = 'SELECT * FROM yf_survey WHERE idx = ?';
        $survey = $db->fetchAssoc($sql, [$survey_idx]);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'survey' => $survey,
            'questions' => $questions
        ]);

        return $twig_renderer->render('admin/survey/detail');
    }

    /**
     * 컨트롤러) 설문 항목 변경 내용 저장
     * ===
     *
     * @param Request $request
     *
     * @return string
     */
    public function save(Request $request)
    {
        $survey_idx = intval(trim($request->get('surveyIdx')));
        if (empty($survey_idx)) {
            return UrlHelper::returnWithMessage('항목을 수정할 설문 정보가 없습니다.');
        }

        $survey_question_ids_list = $request->get('surveyQuestionIdx');
        $survey_question_ko_list = $request->get('surveyQuestionKo');
        $survey_question_en_list = $request->get('surveyQuestionEn');

        $db = ConnectionProvider::getConnection();
        $db->transactional(
            function (Connection $conn) use ($survey_question_ids_list, $survey_question_ko_list, $survey_question_en_list, $survey_idx) {
                foreach ($survey_question_ids_list as $key => $value) {
                    $conn->update(
                        'yf_survey_question',
                        [
                            'question_ko' => $survey_question_ko_list[$key],
                            'question_en' => $survey_question_en_list[$key]
                        ],
                        ['idx' => $value]
                    );
                }
            }
        );

        return UrlHelper::redirect(
            '/admin/surveys/detail?surveyIdx=' . $survey_idx,
            '설문 항목을 수정했습니다.'
        );
    }

    /**
     * 컨트롤러) 설문 목록
     * ===
     *
     * @param Request $request
     *
     * @return string
     */
    public function listResult(Request $request)
    {
        $app_type = strtolower(trim($request->get('type', APP_TYPE_YFACE)));
        $research_idx = trim($request->get('research'));

        $sql = 'SELECT * FROM yf_survey WHERE app_type = ? OR app_type = ? ORDER BY idx ASC';
        $db = ConnectionProvider::getConnection();
        $surveys = $db->fetchAll($sql, ['BOTH', strtoupper($app_type)]);

        $qb = $db->createQueryBuilder();
        $qb->select('*')
        ->from('yf_research')
        ->andWhere('idx = :r_idx')
        ->setParameter('r_idx', $research_idx);
        $research = $qb->execute()->fetch();
        if(empty($research)) {
            $research['title'] = '전체';
        }

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'surveys' => $surveys,
            'app_type' => $app_type,
            'research' => $research
        ]);

        return $twig_renderer->render('admin/survey/list');
    }

    /**
     * @param $survey_id
     * @param $app_type
     *
     * @return string
     */
    public function viewGraph(Request $request)
    {
        $survey_id = trim($request->get('survey_id'));
        $app_type = strtolower(trim($request->get('type', APP_TYPE_YFACE)));
        $research_idx = trim($request->get('research'));

        // 사전
        if(empty($research_idx)) {
            $sql = "SELECT count(idx) FROM yf_user_survey_todo ut WHERE ut.is_done = 1 AND ut.s_idx = ? AND ut.app_type = ? AND ut.time = ?";
        }
        else {
            $sql = "SELECT count(ut.idx) FROM yf_user_survey_todo ut JOIN yf_user_research_status urs ON ut.u_idx = urs.u_idx WHERE ut.is_done = 1 AND ut.s_idx = ? AND ut.app_type = ? AND ut.time = ? AND urs.r_idx = " . $research_idx;
        }

        $db = ConnectionProvider::getConnection();
        $survey_begin_count = $db->fetchColumn($sql, [$survey_id, $app_type, 'BEGIN']);
        $survey_end_count = $db->fetchColumn($sql, [$survey_id, $app_type, 'END']);

        if(empty($research_idx)) {
            $sql = "SELECT * FROM yf_user_survey_todo ut WHERE ut.is_done = 1 AND ut.s_idx = ? AND ut.app_type = ? AND ut.time = ?";
        }
        else {
            $sql = "SELECT ut.idx idx FROM yf_user_survey_todo ut JOIN yf_user_research_status urs ON ut.u_idx = urs.u_idx WHERE ut.is_done = 1 AND ut.s_idx = ? AND ut.app_type = ? AND ut.time = ? AND urs.r_idx = " . $research_idx;
        }

        $survey_begins = $db->fetchAll($sql, [$survey_id, $app_type, 'BEGIN']);
        $survey_ends = $db->fetchAll($sql, [$survey_id, $app_type, 'END']);

        $survey_begin_total_point = 0;
        foreach ($survey_begins as $survey) {
            /** @var $user_survey_todo_begin UserSurveyTodo */
            $user_survey_todo_begin = UserSurveyTodoRepository::getRepository()->findOneBy(['idx' => $survey['idx']]);
            $survey_begin_total_point += SurveyService::calculateUserSurveyResult($user_survey_todo_begin);
        }

        $survey_end_total_point = 0;
        foreach ($survey_ends as $survey) {
            /** @var $user_survey_todo_begin UserSurveyTodo */
            $user_survey_todo_begin = UserSurveyTodoRepository::getRepository()->findOneBy(['idx' => $survey['idx']]);
            $survey_begin_total_point += SurveyService::calculateUserSurveyResult($user_survey_todo_begin);
        }

        $avg_begin_survey_point = 0;
        if (!empty($survey_begin_count)) {
            $avg_begin_survey_point = ($survey_begin_total_point / $survey_begin_count);
        }

        $avg_end_survey_point = 0;
        if (!empty($survey_end_count)) {
            $avg_end_survey_point = ($survey_end_total_point / $survey_end_count);
        }

        $sql = "SELECT * FROM yf_survey WHERE idx = ?";
        $survey = $db->fetchAssoc($sql, [$survey_id]);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'app_type' => $app_type,
            'survey' => $survey,
            'avg_begin_survey_point' => $avg_begin_survey_point,
            'avg_end_survey_point' => $avg_end_survey_point
        ]);

        return $twig_renderer->render('admin/survey/graph');
    }

    /**
     * @param $app_type
     *
     * @return RedirectResponse
     */
    public function exportAllSurveyResultToExcel(Request $request)
    {
        $app_type = strtolower(trim($request->get('type', APP_TYPE_YFACE)));
        $research_idx = trim($request->get('research'));

        $sql = "SELECT * FROM yf_survey WHERE app_type = ? OR app_type = ?";
        $db = ConnectionProvider::getConnection();
        $surveys = $db->fetchAll($sql, ['BOTH', $app_type]);

        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle("All Survey Statistics Sheet");

        $php_excel->removeSheetByIndex(0);

        $sheet_index = 0;
        foreach ($surveys as $survey) {
            $php_excel->createSheet($sheet_index);
            $php_excel->setActiveSheetIndex($sheet_index++);

            $php_excel = static::exportExcel($php_excel, $survey['idx'], $app_type, $research_idx);
        }

        $file_name = "{$app_type}_survey_" . date('Y-m-d');
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        header('Content-type: application/x-msexcel;charset=utf-8');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($php_excel, 'Excel5');
        $objWriter->save('php://output');

        return new RedirectResponse('/admin/surveys/results?type=' . $app_type);
    }

    /**
     * @param $survey_id
     * @param $app_type
     *
     * @return RedirectResponse
     */
    public function exportSurveyResultToExcel(Request $request)
    {
        $survey_id = trim($request->get('survey_id'));
        $app_type = strtolower(trim($request->get('type', APP_TYPE_YFACE)));
        $research_idx = trim($request->get('research'));

        $sql = "SELECT * FROM yf_survey WHERE idx = ?";
        $db = ConnectionProvider::getConnection();
        $survey = $db->fetchAssoc($sql, [$survey_id]);

        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle("{$survey['acronym']} Survey Statistics Sheet");

        $php_excel = static::exportExcel($php_excel, $survey_id, $app_type, $research_idx);

        $file_name = $survey['acronym'] . '_survey_' . date('Y-m-d');
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        header('Content-type: application/x-msexcel;charset=utf-8');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($php_excel, 'Excel5');
        $objWriter->save('php://output');

        return new RedirectResponse('/admin/surveys/results?type=' . $app_type);
    }

    /**
     * @param \PHPExcel $php_excel
     * @param $survey_id
     * @param $app_type
     *
     * @return \PHPExcel
     */
    public static function exportExcel(\PHPExcel $php_excel, $survey_id, $app_type, $research_idx)
    {
        if(empty($research_idx)) {
            $sql = "SELECT * FROM yf_user_survey_todo ut WHERE ut.is_done = 1 AND ut.s_idx = ? AND ut.app_type = ? AND ut.time = ?";
        }
        else {
            $sql = "SELECT ut.idx idx FROM yf_user_survey_todo ut JOIN yf_user_research_status urs ON ut.u_idx = urs.u_idx WHERE ut.is_done = 1 AND ut.s_idx = ? AND ut.app_type = ? AND ut.time = ? AND urs.r_idx = " . $research_idx;
        }

        $db = ConnectionProvider::getConnection();
        $survey_todos = $db->fetchAll($sql, [$survey_id, $app_type, 'BEGIN']);

        $sql = "SELECT * FROM yf_survey WHERE idx = ?";
        $survey = $db->fetchAssoc($sql, [$survey_id]);

        $php_excel->getActiveSheet()->setTitle($survey['acronym']);

        $sql = "SELECT count(*) FROM yf_survey_question WHERE s_idx = ?";
        $question_count = $db->fetchColumn($sql, [$survey_id]);

        $titles = [];
        for ($i = 1; $i <= $question_count; $i++) {
            $titles[] = "{$survey['acronym']}_{$i}";
        }

        $titles[] = "{$survey['acronym']}_총점_사전";

        for ($i = 1; $i <= $question_count; $i++) {
            $titles[] = "{$survey['acronym']}_{$i}";
        }

        $titles[] = "{$survey['acronym']}_총점_사후";

        $row_offset = 1;
        $php_excel->getActiveSheet()
            ->fromArray(array_merge([
                '아이디',
                '이름',
                '생년월일',
                '성별',
                '진단명',
                '훈련시작일',
            ], $titles), null, 'A' . $row_offset++);

        foreach ($survey_todos as $survey_todo) {
            $data = static::buildSurveyResult($survey_todo['idx']);

            $start_date = new \DateTime($data['user']["{$app_type}_survey_completed_date"]);

            $result_values = [];
            foreach ($data['survey_answers_begin'] as $survey_answer) {
                $result_values[] = $survey_answer['answer'];
            }
            $result_values[] = $data['begin_survey_point'];

            foreach ($data['survey_answers_end'] as $survey_answer) {
                $result_values[] = $survey_answer['answer'];
            }
            $result_values[] = $data['end_survey_point'];

            $php_excel->getActiveSheet()
                ->fromArray(array_merge([
                    $data['user']['id'],
                    $data['user']['name'],
                    $data['user']['birth_date'],
                    $data['user']['gender'],
                    $data['user']['diagnosis'],
                    $start_date->format('Y-m-d'),
                ], $result_values), null, 'A' . $row_offset++);
        }

        return $php_excel;
    }

    /**
     * 설문 조사 결과 리턴
     * ===
     *
     * @param $todo_idx
     *
     * @return array
     */
    public static function buildSurveyResult($todo_idx)
    {
        $sql = "SELECT * FROM yf_user_survey_todo WHERE idx = ?";
        $db = ConnectionProvider::getConnection();
        $survey_todo = $db->fetchAssoc($sql, [$todo_idx]);

        $sql = "SELECT * FROM yf_survey WHERE idx = ?";
        $survey = $db->fetchAssoc($sql, [$survey_todo['s_idx']]);

        $sql = "SELECT * FROM yf_user_survey_answer WHERE survey_todo_idx = ? ORDER BY sq_idx ASC";
        $survey_answers_begin = $db->fetchAll($sql, [$todo_idx]);

        $sql = "SELECT * FROM yf_user WHERE idx = ?";
        $user = $db->fetchAssoc($sql, [$survey_todo['u_idx']]);

        /** @var $user_survey_todo_begin UserSurveyTodo */
        $user_survey_todo_begin = UserSurveyTodoRepository::getRepository()->findOneBy(['idx' => $todo_idx]);
        $begin_survey_point = SurveyService::calculateUserSurveyResult($user_survey_todo_begin);

        $sql = "SELECT * FROM yf_user_survey_todo WHERE is_done = 1 AND s_idx = ? AND app_type = ? AND time = ?";
        $survey_todo_after = $db->fetchAssoc($sql, [$survey['idx'], $survey_todo['app_type'], 'END']);

        $survey_answers_end = [];
        $end_survey_point = null;
        if (!empty($survey_todo_after)) {
            $sql = "SELECT * FROM yf_user_survey_answer WHERE survey_todo_idx = ? ORDER BY sq_idx ASC";
            $survey_answers_end = $db->fetchAll($sql, [$survey_todo_after['idx']]);

            /** @var $user_survey_todo_end UserSurveyTodo */
            $user_survey_todo_end = UserSurveyTodoRepository::getRepository()->findOneBy(['idx' => $survey_todo_after['idx']]);
            $end_survey_point = SurveyService::calculateUserSurveyResult($user_survey_todo_end);
        }

        return compact(
            'user',
            'survey_answers_begin',
            'survey_answers_end',
            'begin_survey_point',
            'end_survey_point'
        );
    }
}
