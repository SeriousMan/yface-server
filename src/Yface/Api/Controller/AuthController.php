<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Exception\AuthException;
use Yface\Library\Localization\Lang;
use Yface\Library\Serializer;
use Yface\Service\Auth\AuthenticationService;

class AuthController
{
    public function authenticate(Application $app, Request $request)
    {
        $id = trim($request->request->get('id'));
        $password = trim($request->request->get('password'));
        $app_type = trim($request->headers->get('app'));
        $device_os = trim($request->request->get('deviceOs', ''));
        $device_model = trim($request->request->get('deviceModel', ''));
        $login_ip = trim($request->getClientIp());

        try {
            $user = AuthenticationService::authenticate($id, $password, $login_ip, $app_type, $device_os, $device_model);

            $json_string = (new Serializer())->serializeWithJson($user);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (AuthException $ae) {
            return $app->json(['message' => Lang::get($ae->getMessage())], 400);
        }
    }
}
