<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [4] - FACE_얼굴로 말해요
 */
class GameFace extends ResultData
{
    protected $origin_answer_emotion;
    protected $user_answer_emotion;
    protected $im_good_count;
    protected $your_good_count;

    public function __construct($results, $uploaded_files)
    {
        parent::__construct(GameCode::FACE, $results, $uploaded_files);

        $this->origin_answer_emotion = implode('|', $results['originAnswerEmotionList']);
        $this->user_answer_emotion = implode('|', $results['userAnswerEmotionList']);
        $this->im_good_count = $results['imGoodCount'];
        $this->your_good_count = $results['yourGoodCount'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'origin_answer_emotion' => $this->origin_answer_emotion,
            'user_answer_emotion' => $this->user_answer_emotion,
            'im_good_count' => $this->im_good_count,
            'your_good_count' => $this->your_good_count
        ]);
    }
}
