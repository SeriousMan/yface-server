<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Yface\Library\Configuration;
use Yface\Library\Database\ConnectionProvider;
use Yface\Service\App\VersionService;

class AppController
{
    public function checkAppUpdate(Application $app, Request $request)
    {
        $app_type = trim($request->headers->get('app'));
        $version = trim($request->get('v'));

        $update_info = VersionService::getAppUpdateInfo($version, $app_type);

        return $app->json($update_info);
    }

    public function getAcceptanceImageUrl(Application $app)
    {
        $db = ConnectionProvider::getConnection();
        $files = $db->fetchAll('SELECT * FROM yf_acceptance_image ORDER BY idx asc');

        $images = [];
        foreach ($files as $file) {
            $images[strtolower($file['app_type'])][strtolower($file['lang'])] = 'http://' . $file['img_url'];
        }

        return $app->json($images);
    }
}
