<?php
namespace Yface\Model\Survey;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Yface\Library\Localization\Lang;

/**
 * @ORM\Entity
 * @ORM\Table(name="yf_survey_question")
 * @JMS\ExclusionPolicy("none")
 */
class SurveyQuestion
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="string")
     * @JMS\Exclude
     *
     * @var $question_ko
     */
    private $question_ko;
    /**
     * @ORM\Column(type="string")
     * @JMS\Exclude
     *
     * @var $question_en
     */
    private $question_en;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\SerializedName("useNumberTitle")
     *
     * @var $use_number_title bool
     */
    private $use_number_title;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;
    /**
     * @ORM\ManyToOne(targetEntity="Yface\Model\Survey\Survey", inversedBy="questions")
     * @ORM\JoinColumn(name="s_idx", referencedColumnName="idx")
     * @JMS\Type("Yface\Model\Survey\Survey")
     * @JMS\Exclude
     *
     * @var $survey Survey
     */
    private $survey;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("question")
     * @JMS\Type("string")
     *
     * @return string
     */
    public function getQuestion(): string
    {
        if (Lang::getLocale() == 'ko') {
            return $this->question_ko;
        } else {
            return $this->question_en;
        }
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @return boolean
     */
    public function isUseNumberTitle(): bool
    {
        return $this->use_number_title;
    }

    /**
     * @param boolean $use_number_title
     */
    public function setUseNumberTitle(bool $use_number_title)
    {
        $this->use_number_title = $use_number_title;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return Survey
     */
    public function getSurvey(): Survey
    {
        return $this->survey;
    }

    /**
     * @param Survey $survey
     */
    public function setSurvey(Survey $survey)
    {
        $this->survey = $survey;
    }
}
