<?php
namespace Yface\DataStore\Store;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Store\UserProduct;

class UserProductRepository extends EntityRepository
{
    /**
     * @param $idx
     * @param $user_idx
     * @return null|object|UserProduct
     */
    public function findOneByIdxAndUserIdx($idx, $user_idx) {
        return $this->findOneBy(array('idx' => $idx, 'user_idx' => $user_idx));
    }

    /**
     * @param $user_idx
     * @param $product_idx
     * @param $app_type
     * @return null|object|UserProduct
     */
    public function findOneByUserIdxAndProductIdx($user_idx, $product_idx, $app_type)
    {
        return $this->findOneBy(array('user_idx' => $user_idx, 'product_idx' => $product_idx, 'app_type' => $app_type));
    }

    /**
     * @param $user_idx
     * @param $theme_code
     * @param $app_type
     * @return UserProduct[]
     */
    public function findByUserIdxAndThemeCode($user_idx, $theme_code, $app_type)
    {
        $qb = $this->createQueryBuilder('up');
        $qb->innerJoin('Yface\Model\Store\Product', 'p', Expr\Join::WITH, 'up.product_idx = p.idx')
            ->where('up.user_idx = :user_idx')
            ->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->andWhere('up.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere('p.theme_code = :theme_code')
            ->setParameter('theme_code', $theme_code, Type::INTEGER)
            ->orderBy('up.idx', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $user_idx
     * @param array $product_ids
     * @param $app_type
     * @return UserProduct[]
     */
    public function findByUserIdxAndProductIds($user_idx, array $product_ids = array(), $app_type)
    {
        $qb = $this->createQueryBuilder('up');
        $qb->where('up.user_idx = :user_idx')
            ->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->andWhere('up.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere($qb->expr()->in('up.product_idx', $product_ids));

        return $qb->getQuery()->getResult();
    }

    /**
     * @return UserProductRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Store\UserProduct');
    }
}
