<?php
namespace Yface\DataStore\Game;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Game\UserGamePlay;

class UserGamePlayRepository extends EntityRepository
{
    /**
     * @param $user_idx
     * @param $game_code
     * @param $days
     * @return null|object|UserGamePlay
     */
    public function findOneByUserIdxAndGameCode($user_idx, $game_code, $days)
    {
        return $this->findOneBy(array(
            'user_idx' => $user_idx,
            'game_code' => $game_code,
            'days' => $days
        ));
    }

    /**
     * @param $user_idx
     * @param $game_codes
     * @param $from
     * @param $to
     * @return int
     */
    public function getCountByUserIdxAndGameCodes($user_idx, $game_codes, $from, $to)
    {
        $qb = $this->createQueryBuilder('ug');
        $qb->select('count(ug)')
            ->where('ug.is_completed = ' . $qb->expr()->literal(1))
            ->andWhere('ug.user_idx = :user_idx')
            ->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->andWhere($qb->expr()->in('ug.game_code', $game_codes))
            ->andWhere('ug.reg_date >= :from')
            ->setParameter('from', $from, Type::DATETIME)
            ->andWhere('ug.reg_date <= :to')
            ->setParameter('to', $to, Type::DATETIME);

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param $user_idx
     * @param $days
     * @param $app_type
     * @return mixed
     */
    public function getCountInDaysByUserIdx($user_idx, $days, $app_type)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->select('count(u)')
            ->where('u.user_idx = :user_idx')
            ->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->andWhere('u.days = :days')
            ->setParameter('days', $days, Type::INTEGER)
            ->andWhere('u.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere('u.is_completed = 1');

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @return UserGamePlayRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Game\UserGamePlay');
    }
}
