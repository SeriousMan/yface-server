<?php
namespace Yface\DataStore\Survey;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Survey\Survey;

class SurveyRepository extends EntityRepository
{
    /**
     * @param $age
     * @param $app_type
     * @return Survey[]
     */
    public function getSurveyTodo($age, $app_type)
    {
        $qb = $this->createQueryBuilder('s');

        $orX = $qb->expr()->orX();
        $orX->add($qb->expr()->eq('s.app_type', $qb->expr()->literal($app_type)));
        $orX->add($qb->expr()->eq('s.app_type', $qb->expr()->literal('BOTH')));

        $andX = $qb->expr()->andX();
        $andX->add($qb->expr()->lte('s.min_age', $qb->expr()->literal($age)));
        $andX->add($qb->expr()->gte('s.max_age', $qb->expr()->literal($age)));

        $qb->where($orX)
            ->andWhere('s.is_active = :is_active')
            ->setParameter('is_active', true, Type::BOOLEAN)
            ->andWhere($andX);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return SurveyRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Survey\Survey');
    }
}
