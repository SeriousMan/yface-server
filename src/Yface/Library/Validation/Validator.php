<?php
namespace Yface\Library\Validation;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validation;

class Validator
{
    /**
     * 유효 검증 조건을 받아 검증한 후 유효한지 여부 리턴
     * ===
     * @param $parameter
     * @param Constraint $constraint
     * @return bool
     */
    public static function validate($parameter, Constraint $constraint)
    {
        $invalidate_list = Validation::createValidator()->validate($parameter, $constraint);

        return count($invalidate_list) === 0;
    }
}
