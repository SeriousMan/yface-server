<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Exception\RegistrationException;
use Yface\Exception\UserException;
use Yface\Library\Localization\Lang;
use Yface\Library\Serializer;
use Yface\Service\User\UserService;
use Yface\Service\User\VerificationService;

class UserController
{
    public function index(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));

        try {
            $user = UserService::getUserWithUserIdx($user_idx, $app_type);

            $json_string = (new Serializer())->serializeWithJson($user);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (UserException $ue) {
            return $app->json(['message' => Lang::get($ue->getMessage())], 400);
        }
    }

    public function create(Application $app, Request $request)
    {
        $id = trim($request->request->get('id'));
        $password = trim($request->request->get('password'));
        $email = trim($request->request->get('email'));
        $name = trim($request->request->get('name'));
        $nickname = trim($request->request->get('nickname'));
        $phone = trim($request->request->get('phone'));
        $gender = trim($request->request->get('gender'));
        $birth_date = trim($request->request->get('birthDate'));
        $diagnosis = trim($request->request->get('diagnosis'));
        $guardian_password = trim($request->request->get('guardianPassword'));
        $device_os = trim($request->request->get('deviceOs'));
        $device_model = trim($request->request->get('deviceModel'));
        $app_type = trim($request->headers->get('app'));

        try {
            $user = UserService::registerUser(
                $id,
                $password,
                $email,
                $name,
                $nickname,
                $phone,
                $gender,
                $birth_date,
                $diagnosis,
                $guardian_password,
                $device_os,
                $device_model,
                $app_type
            );

            $json_string = (new Serializer())->serializeWithJson($user);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (RegistrationException $re) {
            return $app->json(['message' => Lang::get($re->getMessage())], 400);
        }
    }

    public function verify(Application $app, Request $request)
    {
        $id = trim($request->request->get('id'));
        $nickname = trim($request->request->get('nickname'));

        try {
            if (empty($id) && empty($nickname)) {
                throw new UserException('user.please.enter.correctly');
            }

            if (!empty($id)) {
                VerificationService::verifyId($id);
            }

            if (!empty($nickname)) {
                VerificationService::verifyNickName($nickname);
            }

            return $app->json(['isValid' => true]);
        } catch (UserException $ue) {
            return $app->json(['isValid' => false, 'message' => Lang::get($ue->getMessage())]);
        }
    }

    public function modify(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $nickname = trim($request->request->get('nickname'));
        $password = trim($request->request->get('password'));
        $email = trim($request->request->get('email'));
        $guardian_relation = trim($request->request->get('guardianRelation'));
        $guardian_password = trim($request->request->get('guardianPassword'));
        $is_praise_started = $request->request->get('isPraiseStarted');
        $app_type = trim($request->headers->get('app'));

        try {
            $user = UserService::modify(
                $user_idx,
                $email,
                $password,
                $nickname,
                $guardian_relation,
                $guardian_password,
                $is_praise_started,
                $app_type
            );

            $json_string = (new Serializer())->serializeWithJson($user);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (RegistrationException $re) {
            return $app->json(['message' => Lang::get($re->getMessage())], 400);
        } catch (UserException $ue) {
            return $app->json(['message' => Lang::get($ue->getMessage())], 400);
        }
    }

    public function findId(Application $app, Request $request)
    {
        $email = trim($request->request->get('email'));
        $name = trim($request->request->get('name'));
        $birth_date = trim($request->request->get('birthDate'));

        try {
            $id = UserService::findUserId($email, $name, $birth_date);

            return $app->json(['id' => $id]);
        } catch (UserException $ue) {
            return $app->json(['message' => Lang::get($ue->getMessage())], 400);
        }
    }

    public function findPassword(Application $app, Request $request)
    {
        $id = trim($request->request->get('id'));
        $email = trim($request->request->get('email'));
        $name = trim($request->request->get('name'));
        $birth_date = trim($request->request->get('birthDate'));

        try {
            $password = UserService::findUserPassword($id, $email, $name, $birth_date);

            return $app->json(['password' => $password]);
        } catch (UserException $ue) {
            return $app->json(['message' => Lang::get($ue->getMessage())], 400);
        }
    }
}
