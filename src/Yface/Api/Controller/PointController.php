<?php
namespace Yface\Api\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Library\Serializer;
use Yface\Service\Point\PointService;

class PointController
{
    public function index(Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));

        $logs = PointService::getDailyUserPoint($user_idx, $app_type);

        $json_string = (new Serializer())->serializeWithJson($logs);

        return new Response($json_string, 200, array('Content-Type' => 'application/json'));
    }
}
