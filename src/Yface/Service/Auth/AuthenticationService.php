<?php
namespace Yface\Service\Auth;

use Yface\DataStore\User\UserLoginLogRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\AuthException;
use Yface\Model\User\User;
use Yface\Service\User\UserService;

class AuthenticationService {
    /**
     * 유저 로그인
     * ===
     * @param $id
     * @param $password
     * @param $login_ip
     * @param $app_type
     * @param $device_os
     * @param $device_model
     *
     * @return User
     * @throws AuthException
     */
    public static function authenticate($id, $password, $login_ip, $app_type, $device_os, $device_model) {
        if (empty($id) || empty($password)) {
            throw new AuthException('user.login.information.incorrect');
        }

        $user = UserRepository::getRepository()->findOneById($id);
        if (empty($user)) {
            throw new AuthException('user.login.id.incorrect');
        }

        /** 어느 앱으로 로그인 했는지 정보 저장 */
        $user->setUserLoginAppType($app_type);

        $encrypt_password = $user->getEncryptPassword();
        if (!password_verify($password, $encrypt_password)) {
            throw new AuthException('user.login.password.incorrect');
        }

        if ($user->getIsDisabled()) {
            throw new AuthException('user.login.account.prohibit');
        }

        /** 로그인 로깅 */
        UserLoginLogRepository::getRepository()->insertUserLoginLog(
            $user->getIdx(),
            $user->getDaysFromSurveyCompleted(),
            $login_ip,
            $app_type,
            $device_os,
            $device_model
        );

        return $user;
    }
}
