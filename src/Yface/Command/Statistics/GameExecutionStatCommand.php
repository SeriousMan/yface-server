<?php
namespace Yface\Command\Statistics;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Game\GameCode;
use Yface\Library\Database\ConnectionProvider;

class GameExecutionStatCommand extends BaseCommand
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $conn;

    public function __construct()
    {
        parent::__construct();

        $this->conn = ConnectionProvider::getConnection();
    }

    protected function configure()
    {
        $this->setName('stat:game:exec')
            ->setDescription('게임별 수행률 통계 산출');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('STATISTICS GAME - 게임별 수행률 통계 산출 CRON 시작');

        $this->conn->transactional(
            function (Connection $conn) {
                /** 통계 테이블 초기화 */
                $conn->executeQuery('TRUNCATE yf_stat_execution_rate');

                $codes = GameCode::getAllGameCode();
                foreach ($codes as $code) {
                    self::summarizeByGameCode($code);
                }
            }
        );

        static::report('STATISTICS GAME - 게임별 수행률 통계 산출 CRON 종료');
    }

    /**
     * 통계용 게임별 수행률 산출
     * ===
     * @param $game_code
     */
    private function summarizeByGameCode($game_code)
    {
        $this->conn->transactional(
            function (Connection $conn) use ($game_code) {
                $sql = 'SELECT count(idx) as days
                        FROM ' . GameCode::getGameTableName($game_code) . '
                        WHERE accuracy IS NOT NULL
                            AND is_disabled = 0
                        GROUP BY u_idx
                        ORDER BY days DESC
                        LIMIT 1';
                $days = $conn->fetchColumn($sql);

                for ($i = 1; $i <= $days; $i++) {
                    $sql = 'SELECT AVG(accuracy) FROM ' . GameCode::getGameTableName($game_code) . ' t
                            INNER JOIN (
                                SELECT u_idx, SUBSTRING_INDEX(SUBSTRING_INDEX(group_concat(idx ORDER BY idx ASC), \',\', ?), \',\', -1) as row_idx
                                FROM ' . GameCode::getGameTableName($game_code) . '
                                WHERE accuracy IS NOT NULL
                                    AND is_disabled = 0
                                GROUP BY u_idx
                                HAVING count(idx) >= ?
                            ) s ON t.idx = s.row_idx';
                    $avg_accuracy = $conn->fetchColumn($sql, [$i, $i]);

                    $conn->insert(
                        'yf_stat_execution_rate',
                        [
                            'days' => $i,
                            'game_code' => $game_code,
                            'avg_value' => $avg_accuracy
                        ]
                    );
                }
            }
        );
    }
}
