<?php
namespace Yface\Model\Praise;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Yface\Model\User\User;
use Yface\Service\Praise\PraiseService;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Praise\PraiseRepository")
 * @ORM\Table(name="yf_praise")
 * @JMS\ExclusionPolicy("none")
 */
class Praise
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     * @JMS\Exclude
     *
     * @var $weeks integer
     */
    private $user_idx;

    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $weeks integer
     */
    private $weeks;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true}, nullable=true)
     * @JMS\SerializedName("goalScore")
     *
     * @var $goal_score integer
     */
    private $goal_score;
    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var $present string
     */
    private $present;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YFACE', 'YCOG')")
     * @JMS\Exclude
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $mod_date \DateTime
     */
    private $mod_date;

    /**
     * @ORM\OneToOne(targetEntity="Yface\Model\User\User")
     * @ORM\JoinColumn(name="u_idx", referencedColumnName="idx")
     * @JMS\Type("Yface\Model\User\User")
     * @JMS\Exclude
     *
     * @var $user User
     */
    private $user;
    /**
     * @ORM\OneToMany(
     *     targetEntity="Yface\Model\Praise\Promise",
     *     mappedBy="praise",
     *     cascade={"persist", "remove"},
     *     orphanRemoval=true
     * )
     * @JMS\Type("ArrayCollection<Yface\Model\Praise\Promise>")
     *
     * @var $promises ArrayCollection
     */
    private $promises;

    public function __construct()
    {
        $this->promises = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getWeeks(): int
    {
        return $this->weeks;
    }

    /**
     * @param int $weeks
     */
    public function setWeeks(int $weeks)
    {
        $this->weeks = $weeks;
    }

    /**
     * @return int
     */
    public function getGoalScore(): int
    {
        return $this->goal_score;
    }

    /**
     * @param int $goal_score
     */
    public function setGoalScore(int $goal_score)
    {
        $this->goal_score = $goal_score;
    }

    /**
     * @return string
     */
    public function getPresent(): string
    {
        return $this->present;
    }

    /**
     * @param string $present
     */
    public function setPresent(string $present)
    {
        $this->present = $present;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return \DateTime
     */
    public function getModDate(): \DateTime
    {
        return $this->mod_date;
    }

    /**
     * @param \DateTime $mod_date
     */
    public function setModDate(\DateTime $mod_date)
    {
        $this->mod_date = $mod_date;
    }

    /**
     * @return ArrayCollection
     */
    public function getPromises()
    {
        return $this->promises;
    }

    /**
     * @param ArrayCollection $promises
     */
    public function setPromises($promises)
    {
        $this->promises = $promises;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("gameScores")
     * @JMS\Type("array")
     */
    public function getGameScore()
    {
        $this->user->setUserLoginAppType($this->app_type);
        $survey_completed_date = $this->user->getSurveyCompletedDate();
        $praise_start_date = clone $this->user->getPraiseStartDate();

        $praise_start_date->modify('+ ' . ($this->weeks - 1) . 'weeks');

        $date_diff = $praise_start_date->getTimestamp() - $survey_completed_date->getTimestamp();
        $date_diff = (int)(floor($date_diff / (60 * 60 * 24)));
        $days = $date_diff + 1;

        return PraiseService::getGameScores($this->user_idx, $days, $this->app_type);
    }
}
