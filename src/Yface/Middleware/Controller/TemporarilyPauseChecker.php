<?php
namespace Yface\Middleware\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class TemporarilyPauseChecker
{
    /**
     * 정기 점검 시간 확인
     * ===
     * @return \Closure
     */
    public static function check() {
        return function (Request $request, Application $app) {
            $now = new \DateTime();

            $pause_start = new \DateTime();
            $pause_start->setTime(
                GAME_TEMPORARILY_PAUSE_TIME_START_HOUR,
                GAME_TEMPORARILY_PAUSE_TIME_START_MINUTE,
                GAME_TEMPORARILY_PAUSE_TIME_START_SECOND
            );

            $pause_end = new \DateTime();
            $pause_end->setTime(
                GAME_TEMPORARILY_PAUSE_TIME_END_HOUR,
                GAME_TEMPORARILY_PAUSE_TIME_END_MINUTE,
                GAME_TEMPORARILY_PAUSE_TIME_END_SECOND
            );

            if ($now >= $pause_start && $now <= $pause_end) {
                return $app->json(array('message' => '정기 점검중입니다. 잠시후 다시 시도해주세요.'), 400);
            }

            return null;
        };
    }
}
