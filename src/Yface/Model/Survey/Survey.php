<?php
namespace Yface\Model\Survey;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Survey\SurveyRepository")
 * @ORM\Table(name="yf_survey")
 * @JMS\ExclusionPolicy("none")
 */
class Survey
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var $name string
     */
    private $name;
    /**
     * @ORM\Column(type="string")
     *
     * @var $target string
     */
    private $target;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("minAge")
     *
     * @var $min_age integer
     */
    private $min_age;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("maxAge")
     *
     * @var $max_age integer
     */
    private $max_age;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("answerCount")
     *
     * @var $answer_count integer
     */
    private $answer_count;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\Exclude
     *
     * @var $is_active bool
     */
    private $is_active;
    /**
     * @ORM\Column(type="string", columnDefinition="EUM('YFACE', 'YCOG', 'BOTH')")
     * @JMS\Exclude
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;

    /**
     * @ORM\OneToMany(targetEntity="Yface\Model\Survey\SurveyQuestion", mappedBy="survey")
     * @JMS\Type("ArrayCollection<Yface\Model\Survey\SurveyQuestion>")
     *
     * @var SurveyQuestion[]
     */
    private $questions;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTarget(): string
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget(string $target)
    {
        $this->target = $target;
    }

    /**
     * @return int
     */
    public function getMinAge(): int
    {
        return $this->min_age;
    }

    /**
     * @param int $min_age
     */
    public function setMinAge(int $min_age)
    {
        $this->min_age = $min_age;
    }

    /**
     * @return int
     */
    public function getMaxAge(): int
    {
        return $this->max_age;
    }

    /**
     * @param int $max_age
     */
    public function setMaxAge(int $max_age)
    {
        $this->max_age = $max_age;
    }

    /**
     * @return int
     */
    public function getAnswerCount(): int
    {
        return $this->answer_count;
    }

    /**
     * @param int $answer_count
     */
    public function setAnswerCount(int $answer_count)
    {
        $this->answer_count = $answer_count;
    }

    /**
     * @return boolean
     */
    public function isIsActive(): bool
    {
        return $this->is_active;
    }

    /**
     * @param boolean $is_active
     */
    public function setIsActive(bool $is_active)
    {
        $this->is_active = $is_active;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param mixed $questions
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
    }
}
