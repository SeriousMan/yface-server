# -*- mode: ruby -*-
# vi: set ft=ruby :

# Ubuntu 16.04 LTS (xenial64) DEV Server Provisioning


### Requirements
### - api.face.kr hosts add for 192.168.33.50

Vagrant.configure(2) do |config|

  config.vm.define "yface_web", primary: true do |web|
    web.vm.box = "ubuntu/xenial64"
    web.vm.network "private_network", ip: "192.168.33.50"

    # NFS mount for fix permission issue
    web.vm.synced_folder ".", "/vagrant", :nfs => true
    web.bindfs.bind_folder "/vagrant", "/vagrant", owner: "www-data", group: "www-data"

    web.vm.provider :virtualbox do |vb|
      vb.name = "yface_web"
      vb.gui = false
      vb.memory = "1024"
    end

    # Nginx + PHP7 install
    web.vm.provision "shell", inline: <<-SHELL
        sudo apt-get update
        sudo apt-get install -y python-software-properties build-essential curl unzip

        sudo apt-get install -y nginx

        sudo apt-get install -y php-cli php-fpm php-gd php-dev php-curl \
                                php-common php-mbstring php-mcrypt php-mysql \
                                php-zip php-xml php-xmlrpc
    SHELL

    # Virtual Host Configuration
    web.vm.provision "file", source: "configs/nginx/sites-available/default", destination: "/tmp/default.conf"

    # Restarting Service
    web.vm.provision "shell", inline: <<-SHELL
        sudo cp /tmp/default.conf /etc/nginx/sites-available/default
        sudo service php7.0-fpm restart
        sudo service nginx restart
    SHELL

    web.vm.post_up_message = "web server provisioning completed!"
  end

end
