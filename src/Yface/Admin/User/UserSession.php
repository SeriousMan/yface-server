<?php
namespace Yface\Admin\User;

use Yface\Library\Database\ConnectionProvider;

class UserSession
{
    const KEY_LOGIN_ID = 'session_login_id';
    const KEY_LOGIN_IDX = 'session_login_idx';
    const KEY_LOGIN_ROLE = 'session_login_role';
    const KEY_LOGIN_R_IDXES = 'session_login_r_idxes';
    const KEY_CUR_R_IDX = 'session_cur_r_idx';

    /**
     * @param string $key
     * @param string $value
     */
    private static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @return string|null
     */
    private static function get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return null;
        }
    }

    /**
     * 현재 관리자가 로그인 했는지 여부 리턴
     * ===
     * @return bool
     */
    public static function isLoggedIn()
    {
        $login_id = self::getLoginId();
        return ($login_id !== false) && ($login_id !== null) && (strlen($login_id) > 0);
    }

    /**
     * 현재 로그인 되어있는 관리자의 아이디
     * ===
     * @return null|string
     */
    public static function getLoginId()
    {
        return self::get(self::KEY_LOGIN_ID);
    }

    public static function getLoginIdx()
    {
        return self::get(self::KEY_LOGIN_IDX);
    }

    public static function getLoginRIdxes()
    {
        return self::get(self::KEY_LOGIN_R_IDXES);
    }

    public static function getLoginRole()
    {
        return self::get(self::KEY_LOGIN_ROLE);
    }

    public static function getCurRIdx() {
        return self::get(self::KEY_CUR_R_IDX);
    }
    public static function setCurRIdx($r_idx) {
        return self::set(self::KEY_CUR_R_IDX, $r_idx);
    }
    public static function unsetCurRIdx() {
        unset($_SESSION[self::KEY_CUR_R_IDX]);
    }

    /**
     * 세션 로그인 처리
     * ===
     * @param $user_id
     * @return bool
     */
    public static function login($user_id)
    {
        $db = ConnectionProvider::getConnection();
        $qb = $db->createQueryBuilder();
        $qb->select('*')
           ->from('yf_admin')
           ->andWhere('id = :id')
           ->setParameter('id', $user_id);
        $user = $qb->execute()->fetch();
        $user_research_idxes = ConnectionProvider::getConnection()->executeQuery('SELECT r_idx FROM yf_researcher WHERE a_idx = ' . $user['idx'])->fetchAll(\PDO::FETCH_COLUMN);
        static::set(static::KEY_LOGIN_ID, trim($user_id));
        static::set(static::KEY_LOGIN_IDX, trim($user['idx']));
        static::set(static::KEY_LOGIN_ROLE, trim($user['role']));
        static::set(static::KEY_LOGIN_R_IDXES, $user_research_idxes);

        return true;
    }

    /**
     * 세션 로그아웃 처리
     */
    public static function logout()
    {
        session_unset();

        // 간혹 Warning: session_destroy(): Session object destruction failed 가 발생하는 경우가 있다.
        @session_destroy();
    }
}
