<?php
namespace Yface\Service\Game;

use Doctrine\DBAL\Connection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Yface\DataStore\Game\GameLevelInfoRepository;
use Yface\DataStore\Game\UserGamePlayRepository;
use Yface\DataStore\Star\UserStarRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\GameException;
use Yface\Game\GameCode;
use Yface\Library\Configuration as Config;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Library\DateHelper;
use Yface\Library\File\FileHelper;
use Yface\Library\File\FileUploader;
use Yface\Service\Attendance\AttendanceService;
use Yface\Service\Star\StarService;

class GameService
{
    /**
     * @param $user_idx
     * @param $game_code
     * @return mixed
     */
    public static function getLevelInfo($user_idx, $game_code)
    {
        $level_info = GameLevelInfoRepository::getRepository()->findOneByGameCodeAndLevel($game_code, static::getUserCurrentLevel($user_idx, $game_code));

        return json_decode($level_info->getLevelInfo(), true);
    }

    /**
     * 게임 시작
     * ===
     * @param $user_idx
     * @param $game_code
     * @throws GameException
     */
    public static function startGame($user_idx, $game_code)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            throw new GameException('game.user.incorrect');
        }

        $conn = ConnectionProvider::getConnection();
        $conn->transactional(
            function (Connection $conn) use ($user, $user_idx, $game_code) {
                $conn->insert(
                    'yf_user_game_play_log',
                    [
                        'u_idx' => $user_idx,
                        'game_code' => $game_code,
                        'days' => $user->getDaysFromSurveyCompleted(GameCode::getGameAppType($game_code)),
                        'description' => GameCode::getGameName($game_code) . ' 시작',
                        'is_completed' => 0,
                        'app_type' => GameCode::getGameAppType($game_code),
                        'reg_date' => date('Y-m-d H:i:s')
                    ]
                );

                $conn->insert(
                    GameCode::getGameTableName($game_code),
                    [
                        'u_idx' => $user_idx,
                        'current_level' => static::getUserCurrentLevel($user_idx, $game_code),
                        'start_time' => date('Y-m-d H:i:s'),
                        'is_disabled' => 0,
                        'reg_date' => date('Y-m-d H:i:s')
                    ]
                );
            }
        );
    }

    /**
     * 게임 종료
     * ===
     * @param $user_idx
     * @param $game_code
     * @param $results
     * @param UploadedFile[] $files
     */
    public static function endGame($user_idx, $game_code, $results, $files)
    {
        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function () use ($user_idx, $game_code, $results, $files) {
                $uploaded_files = [];
                if (!empty($files)) {
                    $file_path = GameCode::getGameResultFilePath($game_code);

                    foreach ($files as $file) {
                        $file_name = FileHelper::generateRandomFileName();
                        $uploaded_files[] = FileUploader::upload($file, $file_path, $file_name);
                    }
                }

                GameResultService::saveResult($user_idx, $game_code, $results, $uploaded_files);
                AttendanceService::assignAttendanceToUser($user_idx, GameCode::getGameAppType($game_code));

                if (GameResultService::isStarObtainable($results)) {
                    StarService::assignStarToUser($user_idx, $game_code);
                }
            }
        );
    }

    /**
     * 유저의 현재 레벨 리턴
     * (별과 출석 도장을 무제한으로 받을 수 있어도 레벨은 최대 15까지)
     * ===
     * @param $user_idx
     * @param $game_code
     * @return int
     */
    private static function getUserCurrentLevel($user_idx, $game_code)
    {
        $stars = UserStarRepository::getRepository()->getCountByGameCode($user_idx, $game_code);
        $level = intval(floor($stars / GAME_LEVEL_UP_PER_STAR)) + 1;

        return min($level, GAME_LEVEL_UP_UPPER_BOUND);
    }

    /**
     * 유저가 해당 게임을 당일 했는지 여부 리턴
     * ===
     * @param $user_idx
     * @param $game_code
     * @param $app_type
     * @return bool
     */
    public static function isUserTodayGamePlayed($user_idx, $game_code, $app_type)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            return false;
        }

        $user_game_play = UserGamePlayRepository::getRepository()->findOneByUserIdxAndGameCode(
            $user_idx,
            $game_code,
            $user->getDaysFromSurveyCompleted($app_type)
        );

        return !empty($user_game_play);
    }

    /**
     * 오늘 해당 게임에서 별을 얻었는지 여부 리턴
     * ===
     * @param $user_idx
     * @param $game_code
     * @return bool
     */
    public static function isUserTodayGotStarInGame($user_idx, $game_code)
    {
        $user_stars = UserStarRepository::getRepository()->findByUserIdxAndGameCode(
            $user_idx,
            $game_code,
            DateHelper::getStartTimeOfToday(),
            DateHelper::getEndTimeOfToday()
        );

        return !empty($user_stars) || count($user_stars) > 0;
    }
}
