<?php

namespace Yface\Game;

class IdenticalPicture
{
    const colors = [
        "blue", "green", "purple", "red", "yellow"
    ];

    const categories = [
        ["bed", "bookshelf", "chair", "couch", "desk", "drawer", "dressing_table", "hanger", "wardrobe"],
        ["air_conditioner", "blender", "fan", "fridge", "iron", "microwave_oven", "pc", "tv", "vacuum", "washer"],
        ["apple", "banana", "cherry", "grape", "kiwi", "melon", "orange", "strawberry", "watermelon"],
        ["chicken", "crocodile", "dog", "elephant", "giraffe", "hippo", "lion", "monkey", "pig", "rabbit", "rhino", "tiger"],
        ["crayon", "eraser", "glue", "marker", "note", "pen", "pencil", "ruler", "sissor", "sketch_book", "sticky_tape"],
        ["bowl", "chopstick", "fork", "fripan", "knife", "ladle", "pot", "rice_scoop", "spoon", "turner"],
        ["accordion", "drum", "guitar", "harp", "piano", "recorder", "saxophone", "tambourine", "triangle", "trumpet", "violin"],
        ["bath", "shampoo", "shower_ball", "shower", "sink", "soap", "tooth_brush", "toothpaste"],
        ["bread", "cake", "candy", "chocolate", "cookie", "hotdog", "ice_cream", "pudding", "snack"],
        ["dress", "hanbok", "hat", "highheel", "jacket", "long_skirts", "pants", "rain_boots", "shirt", "short_pants", "sneakers", "socks"],
        ["airplane", "bicycle", "bus", "car", "helicopter", "scooter", "ship", "skateboard", "train", "truck"],
    ];

    /**
     * @param $current_level
     *
     * @return array
     */
    public static function getLevelStats($current_level)
    {
        switch ($current_level) {
            case 1:
            case 2:
            case 3:
            case 4:
                return ['number_per_set' => 3, 'answer_count_by_set' => 2];
                break;
            case 5:
            case 6:
            case 7:
            case 8:
                return ['number_per_set' => 4, 'answer_count_by_set' => 2];
                break;
            case 9:
            case 10:
            case 11:
            case 12:
                return ['number_per_set' => 5, 'answer_count_by_set' => 3];
                break;
            case 13:
            case 14:
            case 15:
                return ['number_per_set' => 6, 'answer_count_by_set' => 4];
                break;
            default:
                return [];
                break;
        }
    }

    /**
     * @param $current_level
     * @param $answer_set_list_2d
     *
     * @return string
     */
    public static function convertAnswerSetListTo3d($current_level, $answer_set_list_2d)
    {
        $stats = IdenticalPicture::getLevelStats($current_level);
        $tempArr = explode('|', $answer_set_list_2d);

        $tempSetArr = [];
        for ($i = 0; $i <= 9; $i++) {
            $setArr = [];

            for ($j = 0; $j < $stats['number_per_set']; $j++) {
                $index = ($i * $stats['number_per_set']) + $j;
                $temp = explode('_', $tempArr[$index]);

                if (count($temp) > 2) {
                    $setArr[] = ['category' => $temp[0] . '_' . $temp[1], 'color' => $temp[2]];
                } else {
                    $setArr[] = ['category' => $temp[0], 'color' => $temp[1]];
                }
            }

            $tempSetArr[] = $setArr;
        }

        $resultSetArr = [];

        foreach ($tempSetArr as $tempSet) {
            $length = count($tempSet);

            $result = [];
            for ($i = 0; $i < $length - 1; $i++) {
                $category = $tempSet[$i]['category'];
                $color = $tempSet[$i]['color'];

                for ($j = $i + 1; $j < $length; $j++) {
                    if ($tempSet[$j]['color'] == $color) {
                        $result[] = $category . '_' . $color . ',' . $tempSet[$j]['category'] . '_' . $tempSet[$j]['color'];

                        continue;
                    }

                    foreach (IdenticalPicture::categories as $categories) {
                        if (in_array($category, $categories) && in_array($tempSet[$j]['category'], $categories)) {
                            $result[] = $category . '_' . $color . ',' . $tempSet[$j]['category'] . '_' . $tempSet[$j]['color'];

                            continue;
                        }
                    }
                }
            }

            $resultSetArr[] = implode(';', $result);
        }

        $resultStr = implode('|', $resultSetArr);

        return $resultStr;
    }
}
