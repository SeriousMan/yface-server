<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Service\Research\ResearchService;
use Yface\Library\Serializer;
use Yface\DataStore\Research\ResearchRepository;

class ResearchController
{
    public function create(Application $app, Request $request)
    {
        $title = $request->get('title');
        $research = ResearchService::create($title);
        $json_string = (new Serializer())->serializeWithJson($research);

        return new Response($json_string, 200, ['Content-Type' => 'application/json']);
    }

    public function update(Application $app, Request $request)
    {
        $research_idx = intval(trim($request->get('research_idx')));
        $research_title = $request->get('research_title');

        try {
            $research = ResearchService::update($research_idx, $research_title);
            $json_string = (new Serializer())->serializeWithJson($research);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (RegistrationException $re) {
            return $app->json(['message' => Lang::get($re->getMessage())], 400);
        } catch (UserException $ue) {
            return $app->json(['message' => Lang::get($ue->getMessage())], 400);
        }
    }

    public function delete(Application $app, Request $request)
    {
        $research_idx = intval(trim($request->get('research_idx')));

        try {
            $research = ResearchService::delete($research_idx);
            $json_string = (new Serializer())->serializeWithJson($research);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (RegistrationException $re) {
            return $app->json(['message' => Lang::get($re->getMessage())], 400);
        } catch (UserException $ue) {
            return $app->json(['message' => Lang::get($ue->getMessage())], 400);
        }
    }
    public function updateUsers(Application $app, Request $request)
    {
        $body = array(
            'research_idx' => $request->get('research_idx'),
            'user_idxes_added' => $request->get('user_idxes_added'),
            'user_idxes_removed' => $request->get('user_idxes_removed')
        );
        $result = ResearchService::updateUsers($body);
        $json_string = (new Serializer())->serializeWithJson($result);

        return new Response($json_string, 200, ['Content-Type' => 'application/json']);
    }
}
