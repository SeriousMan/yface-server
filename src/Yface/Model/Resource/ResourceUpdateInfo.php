<?php
namespace Yface\Model\Resource;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Resource\ResourceUpdateInfoRepository")
 * @ORM\Table(name="yf_resource_update_info")
 */
class ResourceUpdateInfo {
    /**
     * @ORM\Id
     * @ORM\Column(type="string", columnDefinition="ENUM('YFACE', 'YCOG')")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     */
    private $latest_mod_date;

    /**
     * @return string
     */
    public function getAppType()
    {
        return $this->app_type;
    }

    /**
     * @return \DateTime
     */
    public function getLatestModDate()
    {
        return $this->latest_mod_date;
    }
}