<?php
use Silex\Application;
use Silex\Provider\SessionServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Yface\Library\Configuration;
use Yface\Library\Localization\Lang;

require_once __DIR__ . "/bootstrap.php";

$app = new Application();

/**
 * SESSION_ID가 empty string인 경우가 생기는데,
 * 이 경우 "session_start(): Key cannot be empty" 에러가 발생한다.
 */
$session_name = session_name();
if (isset($_COOKIE[$session_name]) && empty($_COOKIE[$session_name])) {
    unset($_COOKIE[$session_name]);
}

/**
 * Bug for workaround
 * (https://stackoverflow.com/questions/3185779/the-session-id-is-too-long-or-contains-illegal-characters-valid-characters-are)
 */
$ok = @session_start();
if(!$ok){
    session_regenerate_id(true); // replace the Session ID
    session_start();
}

/**
 * $_SESSION과 호환이 되도록 한다
 */
$app->register(new SessionServiceProvider(), array(
    'session.storage' => new PhpBridgeSessionStorage()
));

/**
 * Application Controller Lazy Mount And Session Optional Start
 */
$app->on(KernelEvents::REQUEST, function (GetResponseEvent $event) use ($app) {
    $controller_route_map = array_map(function ($path) {
        return '\Yface\\' . $path;
    }, array(
        '/api' => 'Api\ApiControllerProvider',
        '/admin' => 'Admin\WebControllerProvider'
    ));

    foreach ($controller_route_map as $route => $controller_path) {
        if (strpos($event->getRequest()->getRequestUri(), $route, 0) === 0) {
            if (!class_exists($controller_path)) {
                $app->abort(Response::HTTP_NOT_FOUND, 'Controller Not Found');
            }

            $app->mount($route, new $controller_path);
            $app->flush();

            break;
        }
    }
}, Application::EARLY_EVENT);

/**
 * Controller Early Event (for Application Default Options Setting)
 */
$app->on(KernelEvents::CONTROLLER, function (FilterControllerEvent $event) use ($app) {
    /**
     * User Locale Default Setting - English
     */
    $locale = $event->getRequest()->getPreferredLanguage(Configuration::$values['locales']);
    if (empty($locale)) {
        // Locale Fallback
        $locale = 'en';
    }

    Lang::setLocale($locale);
}, Application::EARLY_EVENT);

/**
 * Parsing Request Body Built JSON Format
 */
$app->before(function (Request $request) {
    if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
        $data = json_decode($request->getContent(), true);
        $request->request->replace(is_array($data) ? $data : array());
    }
});

// Error Handling for case that "Routes Not Found"
$app->error(
    function (\Exception $e, $code) use ($app) {
        if ($e instanceof HttpException && $code == 404) {
            return $app->json(array('message' => 'route not found'), $code);
        }

        if (isset(Configuration::$values['rollbar']['accessToken'])) {
            Rollbar::report_exception($e);
            Rollbar::flush();
        }

        throw $e;
    }
);

$app->run();
