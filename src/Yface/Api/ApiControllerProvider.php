<?php
namespace Yface\Api;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Yface\Library\Localization\Lang;
use Yface\Library\Template\TwigRenderer;
use Yface\Middleware\Controller\PreAuthenticator;
use Yface\Middleware\Controller\TemporarilyPauseChecker;

class ApiControllerProvider implements ControllerProviderInterface
{
    private static $CONTROLLER_NAMESPACE = 'Yface\Api\Controller\\';

    public function connect(Application $app)
    {
        /** @var ControllerCollection $routes */
        $routes = $app['controllers_factory'];

        /**
         * 앱 업데이트 확인 API
         */
        $routes->post('/app/updates', static::$CONTROLLER_NAMESPACE . 'AppController::checkAppUpdate')
            ->before(TemporarilyPauseChecker::check());
        $routes->post('/app/acceptance', static::$CONTROLLER_NAMESPACE . 'AppController::getAcceptanceImageUrl');

        /**
         * 리소스 업데이트 API
         */
        $routes->post('/resources/updates', static::$CONTROLLER_NAMESPACE . 'ResourceController::getUpdate')
            ->before(TemporarilyPauseChecker::check());

        /**
         * 인증 API
         */
        $routes->post('/auth/user', static::$CONTROLLER_NAMESPACE . 'AuthController::authenticate')
            ->before(TemporarilyPauseChecker::check());

        /**
         * 유저 API
         */
        $routes->post('/users', static::$CONTROLLER_NAMESPACE . 'UserController::create')
            ->before(TemporarilyPauseChecker::check());
        $routes->post('/users/get', static::$CONTROLLER_NAMESPACE . 'UserController::index')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/users/save', static::$CONTROLLER_NAMESPACE . 'UserController::modify')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        $routes->post('/users/verification', static::$CONTROLLER_NAMESPACE . 'UserController::verify')
            ->before(TemporarilyPauseChecker::check());

        $routes->post('/users/inquiry/id', static::$CONTROLLER_NAMESPACE . 'UserController::findId')
            ->before(TemporarilyPauseChecker::check());
        $routes->post('/users/inquiry/password', static::$CONTROLLER_NAMESPACE . 'UserController::findPassword')
            ->before(TemporarilyPauseChecker::check());

        /**
         * 테마 API
         */
        $routes->post('/me/themes', static::$CONTROLLER_NAMESPACE . 'MeController::registerTheme')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 프로필 저장 API
         */
        $routes->post('/me/profiles/images', static::$CONTROLLER_NAMESPACE . 'MeController::registerProfileImage')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 피드 API
         */
        $routes->post('/feeds/uploads', static::$CONTROLLER_NAMESPACE . 'FeedController::create')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/feeds/action', static::$CONTROLLER_NAMESPACE . 'FeedController::doAction')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/feeds/remove', static::$CONTROLLER_NAMESPACE . 'FeedController::remove')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/feeds', static::$CONTROLLER_NAMESPACE . 'FeedController::getUserFeeds')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/friends/feeds', static::$CONTROLLER_NAMESPACE . 'FeedController::getFriendsFeeds')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 상점 API
         */
        $routes->post('/store/products', static::$CONTROLLER_NAMESPACE . 'StoreController::index')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/store/products/order', static::$CONTROLLER_NAMESPACE . 'StoreController::order')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 마이월드 API
         */
        $routes->post('/me/products', static::$CONTROLLER_NAMESPACE . 'StoreController::getUserProducts')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/me/products/save', static::$CONTROLLER_NAMESPACE . 'StoreController::modifyUserProducts')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 포인트 API
         */
        $routes->post('/me/points', static::$CONTROLLER_NAMESPACE . 'PointController::index')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 설문 API
         */
        $routes->post('/me/survey', static::$CONTROLLER_NAMESPACE . 'SurveyController::index')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/me/survey/save', static::$CONTROLLER_NAMESPACE . 'SurveyController::save')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/me/survey/results', static::$CONTROLLER_NAMESPACE . 'SurveyController::getResults')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 칭찬 API
         */
        $routes->post('/me/praises', static::$CONTROLLER_NAMESPACE . 'PraiseController::index')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/me/praises/save', static::$CONTROLLER_NAMESPACE . 'PraiseController::save')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 인 게임 API
         */
        $routes->post('/games/start', static::$CONTROLLER_NAMESPACE . 'GameController::start')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/games/end', static::$CONTROLLER_NAMESPACE . 'GameController::end')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 통계 API
         */
        $routes->post('/statistics/stars', static::$CONTROLLER_NAMESPACE . 'StatisticsController::getStars')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/statistics/points', static::$CONTROLLER_NAMESPACE . 'StatisticsController::getPoints')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());
        $routes->post('/statistics/execution-rates', static::$CONTROLLER_NAMESPACE . 'StatisticsController::getExecutionRate')
            ->before(TemporarilyPauseChecker::check())
            ->before(PreAuthenticator::authenticate());

        /**
         * 연구 그룹 API
         */
        $routes->post('/research', static::$CONTROLLER_NAMESPACE .
        'ResearchController::create'); // TODO: 인증
        $routes->post('/research/{research_idx}/update', static::$CONTROLLER_NAMESPACE .
        'ResearchController::update'); // TODO: 인증
        $routes->post('/research/{research_idx}/delete', static::$CONTROLLER_NAMESPACE .
        'ResearchController::delete'); // TODO: 인증
        $routes->post('/research/{research_idx}/updateUsers', static::$CONTROLLER_NAMESPACE .
        'ResearchController::updateUsers'); // TODO: 인증

        /**
         * 웹뷰) 저작권
         */
        $routes->get('/copyright', function (Request $request) {
            $app_type = trim($request->get('app', APP_TYPE_YFACE));

            $twig_renderer = TwigRenderer::getRenderer();
            $twig_renderer->setAttribute('app_type', strtolower($app_type));

            $locale = Lang::getLocale();

            return $twig_renderer->render("copyright/index.{$locale}");
        });

        $routes->get('/privacy', function (Request $request) {
            $app_type = trim($request->get('app', APP_TYPE_YFACE));

            $twig_renderer = TwigRenderer::getRenderer();
            $twig_renderer->setAttribute('app_type', strtolower($app_type));

            $locale = Lang::getLocale();

            return $twig_renderer->render("privacy.{$locale}");
        });

        return $routes;
    }
}
