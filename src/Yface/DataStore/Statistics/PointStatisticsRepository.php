<?php
namespace Yface\DataStore\Statistics;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Statistics\PointStatistics;

class PointStatisticsRepository extends EntityRepository
{
    /**
     * @param $app_type
     * @return PointStatistics[]
     */
    public function findByAppType($app_type)
    {
        return $this->findBy(array('app_type' => $app_type), array('idx' => 'ASC'));
    }

    /**
     * @return PointStatisticsRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Statistics\PointStatistics');
    }
}
