<?php
namespace Yface\Library\Localization;

use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\Translator;
use Yface\Library\Configuration;

/**
 * 싱글턴 인스턴스로 Translator 리턴하는 프로바이더 클래스
 * 참고: http://modernpug.github.io/php-the-right-way/pages/Design-Patterns.html
 */
class TranslatorProvider
{
    /**
     * @var $translator Translator
     */
    private static $translator;

    /**
     * @param null $locale
     * @return Translator
     */
    public static function getTranslator($locale = null)
    {
        if (null === static::$translator) {
            static::$translator = static::createTranslator($locale);
        }

        return static::$translator;
    }

    /**
     * @param $locale
     * @return Translator
     */
    private static function createTranslator($locale)
    {
        $translator = new Translator(
            $locale,
            new MessageSelector(),
            sys_get_temp_dir() . '/translator/cache/v3',
            Configuration::$values['debug']
        );

        $translator->setFallbackLocales(array('ko'));
        $translator->addLoader('yaml', new YamlFileLoader());

        $translator->addResource(
            'yaml',
            Configuration::$values['path']['lang'] . '/en.yaml',
            'en'
        );
        $translator->addResource(
            'yaml',
            Configuration::$values['path']['lang'] . '/ko.yaml',
            'ko'
        );

        return $translator;
    }
}
