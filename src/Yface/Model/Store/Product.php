<?php
namespace Yface\Model\Store;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Store\ProductRepository")
 * @ORM\Table(name="yf_product")
 * @JMS\ExclusionPolicy("none")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\Exclude
     *
     * @var $theme_code integer
     */
    private $theme_code;
    /**
     * @ORM\Column(type="string", length=45)
     *
     * @var $name string
     */
    private $name;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $price integer
     */
    private $price;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;

    /**
     * @JMS\SerializedName("isUserPurchased")
     *
     * @var $is_user_purchased boolean
     */
    private $is_user_purchased;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getThemeCode(): int
    {
        return $this->theme_code;
    }

    /**
     * @param int $theme_code
     */
    public function setThemeCode(int $theme_code)
    {
        $this->theme_code = $theme_code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price)
    {
        $this->price = $price;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return boolean
     */
    public function getIsUserPurchased()
    {
        return $this->is_user_purchased;
    }

    /**
     * @param boolean $is_user_purchased
     */
    public function setIsUserPurchased($is_user_purchased)
    {
        $this->is_user_purchased = $is_user_purchased;
    }
}
