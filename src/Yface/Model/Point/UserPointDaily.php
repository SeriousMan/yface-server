<?php
namespace Yface\Model\Point;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Point\UserPointDailyRepository")
 * @ORM\Table(name="yf_user_point_daily_log")
 * @JMS\ExclusionPolicy("none")
 */
class UserPointDaily
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     * @JMS\Exclude
     *
     * @var $user_idx integer
     */
    private $user_idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $days integer
     */
    private $days;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $obtain_point integer
     */
    private $obtain_point;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $use_point integer
     */
    private $use_point;
    /**
     * @ORM\Column(type="integer")
     *
     * @var $total_point integer
     */
    private $total_point;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YFACE', 'YCOG')")
     * @JMS\Exclude
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getUserIdx(): int
    {
        return $this->user_idx;
    }

    /**
     * @param int $user_idx
     */
    public function setUserIdx(int $user_idx)
    {
        $this->user_idx = $user_idx;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

    /**
     * @param int $days
     */
    public function setDays(int $days)
    {
        $this->days = $days;
    }

    /**
     * @return int
     */
    public function getObtainPoint(): int
    {
        return $this->obtain_point;
    }

    /**
     * @param int $obtain_point
     */
    public function setObtainPoint(int $obtain_point)
    {
        $this->obtain_point = $obtain_point;
    }

    /**
     * @return int
     */
    public function getUsePoint(): int
    {
        return $this->use_point;
    }

    /**
     * @param int $use_point
     */
    public function setUsePoint(int $use_point)
    {
        $this->use_point = $use_point;
    }

    /**
     * @return int
     */
    public function getTotalPoint(): int
    {
        return $this->total_point;
    }

    /**
     * @param int $total_point
     */
    public function setTotalPoint(int $total_point)
    {
        $this->total_point = $total_point;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }
}
