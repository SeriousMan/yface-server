<?php
namespace Yface\Model\Survey;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Survey\UserSurveyTodoRepository")
 * @ORM\Table(name="yf_user_survey_todo")
 * @JMS\ExclusionPolicy("none")
 */
class UserSurveyTodo
{
    const TIME_BEGIN = 'BEGIN';
    const TIME_END = 'END';

    const TIME_END_PERIOD = 66;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     * @JMS\Exclude
     *
     * @var $user_idx integer
     */
    private $user_idx;
    /**
     * @ORM\Column(type="integer", name="s_idx", options={"unsigned":true})
     * @JMS\Exclude
     *
     * @var $survey_idx integer
     */
    private $survey_idx;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('BEGIN', 'END')")
     *
     * @var $time string
     */
    private $time;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\SerializedName("isDone")
     *
     * @var $is_done bool
     */
    private $is_done;
    /**
     * @ORM\Column(type="string", columnDefinition="EUM('YFACE', 'YCOG')")
     * @JMS\Exclude
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $mod_date \DateTime
     */
    private $mod_date;

    /**
     * @ORM\OneToOne(targetEntity="Survey")
     * @ORM\JoinColumn(name="s_idx", referencedColumnName="idx")
     *
     * @var $survey Survey
     */
    private $survey;

    /**
     * @ORM\OneToMany(targetEntity="Yface\Model\Survey\UserSurveyAnswer", mappedBy="survey_todo")
     * @JMS\Exclude
     *
     * @var $answers ArrayCollection
     */
    private $answers;

    public function __clone()
    {
        $this->idx = null;
        $this->answers = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getUserIdx(): int
    {
        return $this->user_idx;
    }

    /**
     * @param int $user_idx
     */
    public function setUserIdx(int $user_idx)
    {
        $this->user_idx = $user_idx;
    }

    /**
     * @return integer
     */
    public function getSurveyIdx(): int
    {
        return $this->survey_idx;
    }

    /**
     * @param integer $survey_idx
     */
    public function setSurveyIdx(int $survey_idx)
    {
        $this->survey_idx = $survey_idx;
    }

    /**
     * @return string
     */
    public function getTime(): string
    {
        return $this->time;
    }

    /**
     * @param string $time
     */
    public function setTime(string $time)
    {
        $this->time = $time;
    }

    /**
     * @return boolean
     */
    public function getIsDone(): bool
    {
        return $this->is_done;
    }

    /**
     * @param boolean $is_done
     */
    public function setIsDone(bool $is_done)
    {
        $this->is_done = $is_done;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return \DateTime
     */
    public function getModDate(): \DateTime
    {
        return $this->mod_date;
    }

    /**
     * @param \DateTime $mod_date
     */
    public function setModDate(\DateTime $mod_date)
    {
        $this->mod_date = $mod_date;
    }

    /**
     * @return Survey
     */
    public function getSurvey(): Survey
    {
        return $this->survey;
    }

    /**
     * @param Survey $survey
     */
    public function setSurvey(Survey $survey)
    {
        $this->survey = $survey;
    }

    /**
     * @return ArrayCollection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * @param ArrayCollection $answers
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
    }
}
