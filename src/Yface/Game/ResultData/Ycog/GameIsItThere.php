<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [17] - IS_IT_THERE_있니 없니
 */
class GameIsItThere extends ResultData
{
    protected $total_answer_count;

    protected $target_count;
    protected $target_answer_count;
    protected $target_wrong_count;
    protected $target_accuracy;
    protected $target_no_response_count;
    protected $target_average_response_time;

    protected $non_target_count;
    protected $non_target_answer_count;
    protected $non_target_wrong_count;
    protected $non_target_accuracy;
    protected $non_target_no_response_count;
    protected $non_target_average_response_time;

    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::IS_IT_THERE, $results);

        /** 게임 정보 저장 */
        $this->total_answer_count = $results['totalAnswerCount'];

        $this->target_count = $results['targetCount'];
        $this->target_answer_count = $results['targetAnswerCount'];
        $this->target_wrong_count = $results['targetWrongCount'];
        $this->target_accuracy = $results['targetAccuracy'];
        $this->target_no_response_count = $results['targetNoResponseCount'];
        $this->target_average_response_time = $results['targetAverageResponseTime'];

        $this->non_target_count = $results['nonTargetCount'];
        $this->non_target_answer_count = $results['nonTargetAnswerCount'];
        $this->non_target_wrong_count = $results['nonTargetWrongCount'];
        $this->non_target_accuracy = $results['nonTargetAccuracy'];
        $this->non_target_no_response_count = $results['nonTargetNoResponseCount'];
        $this->non_target_average_response_time = $results['nonTargetAverageResponseTime'];

        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'total_answer_count' => $this->total_answer_count,

            'target_count' => $this->target_count,
            'target_answer_count' => $this->target_answer_count,
            'target_wrong_count' => $this->target_wrong_count,
            'target_accuracy' => $this->target_accuracy,
            'target_no_response_count' => $this->target_no_response_count,
            'target_average_response_time' => $this->target_average_response_time,

            'non_target_count' => $this->non_target_count,
            'non_target_answer_count' => $this->non_target_answer_count,
            'non_target_wrong_count' => $this->non_target_wrong_count,
            'non_target_accuracy' => $this->non_target_accuracy,
            'non_target_no_response_count' => $this->non_target_no_response_count,
            'non_target_average_response_time' => $this->non_target_average_response_time,

            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
