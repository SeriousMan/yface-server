<?php
namespace Yface\DataStore\Research;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Research\Research;

class ResearchRepository extends EntityRepository
{
    /**
     * @param $title
     * @return null|object|Research
     */
    public function findOneByTitle($title) {
        return $this->findOneBy(array('title' => $title));
    }

    /**
     * @param $research_idx
     * @return null|object|Research
     */
    public function findOneByResearchIdx($research_idx) {
        return $this->findOneBy(array('idx' => $research_idx));
    }

    /**
     * @return UserRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Research\Research');
    }
}
