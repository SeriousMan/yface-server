<?php
namespace Yface\Admin\Statistics;

use Yface\Library\Database\ConnectionProvider;
use Yface\Service\Praise\PraiseService;

class Praise
{
    /**
     * 전체 유저 칭찬스티커 통계 리턴
     * ===
     * @param $app_type
     *
     * @return array
     */
    public static function buildPraiseSummary($app_type, $research_idx)
    {
        if(empty($research_idx)) {
            $sql = "SELECT * FROM yf_user WHERE {$app_type}_praise_start_date IS NOT NULL ORDER BY idx DESC";
        }
        else {
            $sql = "SELECT u.idx idx, u.id id, u.name name, u.birth_date birth_date, u.gender gender, u.diagnosis diagnosis, u.yface_survey_completed_date yface_survey_completed_date, u.ycog_survey_completed_date ycog_survey_completed_date, u.yface_praise_start_date yface_praise_start_date, u.ycog_praise_start_date ycog_praise_start_date FROM yf_user u JOIN yf_user_research_status urs ON u.idx = urs.u_idx WHERE {$app_type}_praise_start_date IS NOT NULL AND urs.r_idx = " . $research_idx . " ORDER BY u.idx DESC";
        }

        $db = ConnectionProvider::getConnection();
        $users = $db->fetchAll($sql);

        if(empty($research_idx)) {
            $sql = "SELECT MAX(weeks) FROM yf_praise WHERE app_type = ?";
        }
        else {
            $sql = "SELECT MAX(weeks) FROM yf_praise p JOIN yf_user_research_status urs ON p.u_idx = urs.u_idx WHERE app_type = ? AND urs.r_idx = " . $research_idx;
        }

        $maximum_weeks = $db->fetchColumn($sql, [$app_type]);

        $praise_summary_by_users = [];
        foreach ($users as $user) {
            $sql = "SELECT *
                FROM yf_praise
                WHERE u_idx = ? AND app_type = ?
                ORDER BY weeks ASC";
            $praises = $db->fetchAll($sql, [$user['idx'], strtoupper($app_type)]);

            $sql = 'SELECT * FROM yf_praise_promise
                WHERE praise_idx = ?
                ORDER BY idx ASC';

            $game_scores_by_week = [];
            $total_scores_by_week = [];
            $goal_scores_by_week = [];

            foreach ($praises as $praise) {
                $praise_promises = $db->fetchAll($sql, [$praise['idx']]);

                $week_start_days = (($praise['weeks'] - 1) * 7) + 1;
                $game_scores = PraiseService::getGameScores($user['idx'], $week_start_days, strtoupper($app_type));
                $promise_score = 0;

                foreach ($praise_promises as $praise_promise) {
                    for ($i = 1; $i <= 7; $i++) {
                        $promise_score += $praise_promise['day_' . $i];
                    }
                }

                /** 게임 스코어와 총점 점수 추출 (주별) */
                $game_scores_by_week[$praise['weeks']] = $game_scores;
                $total_scores_by_week[$praise['weeks']] = array_sum($game_scores) + $promise_score;
                $goal_scores_by_week[$praise['weeks']] = $praise['goal_score'];
            }

            $achieved_count = 0;
            foreach ($goal_scores_by_week as $key => $value) {
                if (empty($value)) {
                    continue;
                }

                if ($value <= $total_scores_by_week[$key]) {
                    $achieved_count++;
                }
            }
            
            $rate_of_achieved = ($achieved_count / count($total_scores_by_week)) * 100;

            $praise_summary_by_users[$user['idx']] = compact(
                'total_scores_by_week',
                'goal_scores_by_week',
                'rate_of_achieved'
            );
        }

        return compact(
            'users',
            'praise_summary_by_users',
            'maximum_weeks'
        );
    }

    /**
     * 유저의 칭찬스티커 통계를 위한 데이터 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function buildUserPraise($user_idx, $app_type)
    {
        $sql = 'SELECT * FROM yf_user WHERE idx = ?';
        $db = ConnectionProvider::getConnection();
        $user = $db->fetchAssoc($sql, [$user_idx]);

        $sql = "SELECT *
                FROM yf_praise
                WHERE u_idx = ? AND app_type = ?
                ORDER BY weeks ASC";
        $praises = $db->fetchAll($sql, [$user_idx, strtoupper($app_type)]);

        $sql = 'SELECT * FROM yf_praise_promise
                WHERE praise_idx = ?
                ORDER BY idx ASC';

        $promises = [];
        $game_scores_by_week = [];
        $total_scores_by_week = [];
        $goal_scores_by_week = [];

        foreach ($praises as $praise) {
            $praise_promises = $db->fetchAll($sql, [$praise['idx']]);
            $promises[$praise['weeks']] = $praise_promises;

            $week_start_days = (($praise['weeks'] - 1) * 7) + 1;
            $game_scores = PraiseService::getGameScores($user_idx, $week_start_days, strtoupper($app_type));
            $promise_score = 0;

            /** 약속 타이틀만 추출 */
            foreach ($praise_promises as $praise_promise) {
                for ($i = 1; $i <= 7; $i++) {
                    $promise_score += $praise_promise['day_' . $i];
                }
            }

            /** 게임 스코어와 총점 점수 추출 (주별) */
            $game_scores_by_week[$praise['weeks']] = $game_scores;
            $total_scores_by_week[$praise['weeks']] = array_sum($game_scores) + $promise_score;
            $goal_scores_by_week[$praise['weeks']] = $praise['goal_score'];
        }

        $achieved_count = 0;
        foreach ($goal_scores_by_week as $key => $value) {
            if (empty($value)) {
                continue;
            }

            if ($value <= $total_scores_by_week[$key]) {
                $achieved_count++;
            }
        }

        $rate_of_achieved = ($achieved_count / count($total_scores_by_week)) * 100;

        return compact(
            'user',
            'praises',
            'promises',
            'game_scores_by_week',
            'total_scores_by_week',
            'rate_of_achieved'
        );
    }
}
