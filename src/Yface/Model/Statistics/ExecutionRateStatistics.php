<?php
namespace Yface\Model\Statistics;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Statistics\ExecutionRateStatisticsRepository")
 * @ORM\Table(name="yf_stat_execution_rate")
 * @JMS\ExclusionPolicy("none")
 */
class ExecutionRateStatistics
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", length=3, options={"unsigned":true})
     *
     * @var $days integer
     */
    private $days;
    /**
     * @ORM\Column(type="float")
     * @JMS\SerializedName("avgValue")
     * @JMS\Accessor(getter="getAvgValue", setter="setAvgValue")
     *
     * @var $avg_value float
     */
    private $avg_value;
    /**
     * @ORM\Column(type="integer")
     * @JMS\Exclude
     *
     * @var $game_code integer
     */
    private $game_code;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

    /**
     * @param int $days
     */
    public function setDays(int $days)
    {
        $this->days = $days;
    }

    /**
     * @return float
     */
    public function getAvgValue(): float
    {
        return ($this->avg_value * 100);
    }

    /**
     * @param float $avg_value
     */
    public function setAvgValue(float $avg_value)
    {
        $this->avg_value = $avg_value;
    }

    /**
     * @return int
     */
    public function getGameCode(): int
    {
        return $this->game_code;
    }

    /**
     * @param int $game_code
     */
    public function setGameCode(int $game_code)
    {
        $this->game_code = $game_code;
    }
}
