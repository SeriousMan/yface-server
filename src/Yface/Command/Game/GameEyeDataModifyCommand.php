<?php
namespace Yface\Command\Game;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Library\Database\ConnectionProvider;
use Yface\Service\Game\TodayGameService;

class GameEyeDataModifyCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('game:ey:modify');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('ONCE - 어디보고 있게 데이터 보정 시작');

        self::modify();

        static::report('ONCE - 어디보고 있게 데이터 보정 종료');
    }

    protected function modify()
    {
        $db = ConnectionProvider::getConnection();
        $results = $db->fetchAll('SELECT * FROM yf_game_eye WHERE avg_response_time >= 8000');

        foreach ($results as $result) {
            var_dump($result['idx']);

            $temp_response_time_arr = explode(',', $result['response_time']);

            $response_time_arr = [];

            $length = count($temp_response_time_arr);
            for ($i = $length - 1; $i >= 1; $i--) {
                $response_time_arr[$i] = $temp_response_time_arr[$i] - $temp_response_time_arr[$i - 1] - 2000;
            }

            $response_time_arr[0] = $temp_response_time_arr[0];
            ksort($response_time_arr);
            $response_time_str = implode(',', $response_time_arr);

            $avg_response_time = array_sum($response_time_arr) / count($response_time_arr);

            $db->update(
                'yf_game_eye',
                [
                    'response_time' => $response_time_str,
                    'avg_response_time' => $avg_response_time
                ],
                [
                    'idx' => $result['idx']
                ]
            );
        }
    }
}
