<?php
namespace Yface\Service\Store;

use Doctrine\ORM\EntityManager;
use Yface\DataStore\Store\ProductRepository;
use Yface\DataStore\Store\UserProductRepository;
use Yface\Exception\StoreException;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Store\UserProduct;
use Yface\Service\Point\PointService;

class StoreService
{
    /**
     * 상품 목록 리턴
     * ===
     * @param $user_idx
     * @param $theme_code
     * @param $app_type
     * @return \Yface\Model\Store\Product[]
     */
    public static function getProductList($user_idx, $theme_code, $app_type)
    {
        $products = ProductRepository::getRepository()->findByThemeCode($theme_code);
        if (empty($products)) {
            return array();
        }

        /** 상품 고유 번호 추출 */
        $product_ids = array();
        foreach ($products as $product) {
            $product_ids[] = $product->getIdx();
        }

        $user_products = UserProductRepository::getRepository()->findByUserIdxAndProductIds(
            $user_idx,
            $product_ids,
            $app_type
        );

        /** 유저가 구입한 상품 고유 번호 추출 */
        $user_product_ids = array();
        foreach ($user_products as $user_product) {
            $user_product_ids[] = $user_product->getProductIdx();
        }

        /** 기존 상품 목록 정보에 유저 구입 여부 정보 추가 */
        /** @var \Yface\Model\Store\Product $product */
        foreach ($products as $product) {
            $is_user_purchased = in_array($product->getIdx(), $user_product_ids);
            $product->setIsUserPurchased($is_user_purchased);
        }

        return $products;
    }

    /**
     * 유저가 구입한 상품들 위치 수정
     * ===
     * @param $user_idx
     * @param $products
     */
    public static function modifyUserProductsPosition($user_idx, $products)
    {
        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user_idx, $products) {
                foreach ($products as $product) {
                    $user_product = UserProductRepository::getRepository()->findOneByIdxAndUserIdx(
                        $product['idx'],
                        $user_idx
                    );

                    if (empty($user_product)) {
                        throw new StoreException('store.information.not.exist');
                    }

                    $user_product->setCoordinateX($product['coordinateX']);
                    $user_product->setCoordinateY($product['coordinateY']);

                    $em->persist($user_product);
                    $em->flush($user_product);
                }
            }
        );
    }

    /**
     * 유저가 구입한 상품 목록 리턴
     * ===
     * @param $user_idx
     * @param $theme_code
     * @param $app_type
     * @return \Yface\Model\Store\UserProduct[]
     */
    public static function getUserProductList($user_idx, $theme_code, $app_type)
    {
        $user_products = UserProductRepository::getRepository()->findByUserIdxAndThemeCode(
            $user_idx,
            $theme_code,
            $app_type
        );

        return $user_products;
    }

    /**
     * 상품 구입
     * ===
     * @param $user_idx
     * @param $product_idx
     * @param $app_type
     * @throws StoreException
     */
    public static function orderProduct($user_idx, $product_idx, $app_type)
    {
        static::assertPurchaseAvailable($user_idx, $product_idx, $app_type);

        $product = ProductRepository::getRepository()->findOneByProductIdx($product_idx);
        if (empty($product)) {
            throw new StoreException('store.no.item.information');
        }

        $user_product = new UserProduct();
        $user_product->setUserIdx($user_idx);
        $user_product->setProduct($product);
        /** 좌표 기본값 200으로 설정 */
        $user_product->setCoordinateX(200);
        $user_product->setCoordinateY(200);
        $user_product->setAppType($app_type);
        $user_product->setRegDate(new \DateTime('now'));

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user_idx, $product, $user_product, $app_type) {
                $em->persist($user_product);
                $em->flush($user_product);

                $price = $product->getPrice();
                $total_amount = PointService::getUserTotalPoint($user_idx, $app_type);

                PointService::savePoint(
                    $user_idx,
                    $product->getName() . ' 상품 구입',
                    0 - $price,
                    $total_amount - $price,
                    $app_type
                );
            }
        );
    }

    /**
     * 유저가 상품을 구매할 수 있는지 검사
     * ===
     * @param $user_idx
     * @param $product_idx
     * @param $app_type
     * @throws StoreException
     */
    private static function assertPurchaseAvailable($user_idx, $product_idx, $app_type)
    {
        if (static::isAlreadyPurchased($user_idx, $product_idx, $app_type)) {
            throw new StoreException('store.already.got');
        }

        if (!static::hasUserPointPurchaseAvailable($user_idx, $product_idx, $app_type)) {
            throw new StoreException('store.point.not.enough');
        }
    }

    /**
     * 유저가 이미 구입했는지 여부 리턴
     * ===
     * @param $user_idx
     * @param $product_idx
     * @param $app_type
     * @return bool
     */
    private static function isAlreadyPurchased($user_idx, $product_idx, $app_type)
    {
        $user_product = UserProductRepository::getRepository()->findOneByUserIdxAndProductIdx($user_idx, $product_idx, $app_type);

        return !empty($user_product);
    }

    /**
     * 유저가 구입 가능한 포인트가 있는지 여부
     * ===
     * @param $user_idx
     * @param $product_idx
     * @param $app_type
     * @return bool
     * @throws StoreException
     */
    private static function hasUserPointPurchaseAvailable($user_idx, $product_idx, $app_type)
    {
        $product = ProductRepository::getRepository()->findOneByProductIdx($product_idx);
        if (empty($product)) {
            throw new StoreException('store.no.available');
        }

        $price = $product->getPrice();
        $user_point = PointService::getUserTotalPoint($user_idx, $app_type);

        return ($price <= $user_point);
    }
}
