<?php
namespace Yface\Service\App;

use Version\Operator;
use Version\Version;
use Yface\DataStore\App\AppVersionRepository;

class VersionService
{
    /**
     * 유저의 앱에 대한 업데이트 진행 여부 정보 리턴
     * ===
     * @param $version
     * @param $app_type
     * @return array
     */
    public static function getAppUpdateInfo($version, $app_type)
    {
        $app_version = AppVersionRepository::getRepository()->findOneByAppType($app_type);
        $is_update_needed = static::isAppUpdateNeeded($version, $app_version->getVersion());

        return array(
            'isUpdateNeeded' => $is_update_needed,
            'isForceUpdate' => ($is_update_needed) ? $app_version->getIsForceUpdate() : false
        );
    }

    /**
     * 유저의 앱이 업데이트가 필요한지 여부 리턴
     * ===
     * @param $user_version
     * @param $latest_version
     * @return bool
     */
    private static function isAppUpdateNeeded($user_version, $latest_version)
    {
        return (new Operator(">"))
            ->compare(Version::parse($latest_version), Version::parse($user_version));
    }
}
