<?php
namespace Yface\Model\Theme;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Theme\ThemeRepository")
 * @ORM\Table(name="yf_theme")
 * @JMS\ExclusionPolicy("none")
 */
class Theme {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $code;
    /**
     * @ORM\Column(type="string", length=45)
     * @JMS\SerializedName("titleKr")
     */
    private $title_kr;
    /**
     * @ORM\Column(type="string", length=45)
     * @JMS\SerializedName("titleEn")
     */
    private $title_en;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     */
    private $reg_date;

    /**
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getTitleKr()
    {
        return $this->title_kr;
    }

    /**
     * @return string
     */
    public function getTitleEn()
    {
        return $this->title_en;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }
}