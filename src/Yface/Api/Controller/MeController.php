<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Exception\UserException;
use Yface\Library\Localization\Lang;
use Yface\Library\Serializer;
use Yface\Service\Theme\UserThemeService;
use Yface\Service\User\UserService;

class MeController
{
    public function registerTheme(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $theme_code = intval(trim($request->request->get('theme')));
        $app_type = trim($request->headers->get('app'));

        try {
            UserThemeService::selectTodayTheme($user_idx, $theme_code, $app_type);

            return new Response('', 200, ['Content-Type' => 'plain/text']);
        } catch (UserException $ue) {
            return $app->json(['message' => Lang::get($ue->getMessage())], 400);
        }
    }

    public function registerProfileImage(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $file = $request->files->get('profileImg');
        $app_type = trim($request->headers->get('app'));

        try {
            $user = UserService::registerUserProfileImage($user_idx, $file, $app_type);

            $json_string = (new Serializer())->serializeWithJson($user);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (UserException $ue) {
            return $app->json(['message' => Lang::get($ue->getMessage())], 400);
        }
    }
}
