<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Exception\GameException;
use Yface\Game\GameCode;
use Yface\Library\Localization\Lang;
use Yface\Library\Serializer;
use Yface\Service\Game\GameService;
use Yface\Service\User\UserService;

class GameController
{
    /**
     * 해당 게임을 시작할 때 호출하는 시작 API
     * (게임을 시작할 때 - 유저의 레벨에 따른 게임 난이도 정보를 리턴)
     * ===
     *
     * @param Application $app
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function start(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $game_code = intval(trim($request->get('gameCode')));

        try {
            GameService::startGame($user_idx, $game_code);
            $level_info = GameService::getLevelInfo($user_idx, $game_code);

            return $app->json($level_info);
        } catch (GameException $ge) {
            return $app->json(['message' => Lang::get($ge->getMessage())], 400);
        }
    }

    /**
     * 해당 게임을 정상적으로 완수하고 결과를 저장하는 종료 API
     * (게임이 완료된 후 - 출석 도장, 별, 포인트 정보가 변경되는걸 앱에서 갱신하기 위해 유저 정보 리턴)
     * ===
     *
     * @param Application $app
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function end(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $game_code = intval(trim($request->request->get('gameCode')));
        $results = json_decode(trim($request->request->get('results')), true);
        $files = $request->files->get('files');

        try {
            GameService::endGame($user_idx, $game_code, $results, $files);

            $user = UserService::getUserWithUserIdx($user_idx, GameCode::getGameAppType($game_code));
            $json_string = (new Serializer())->serializeWithJson($user);

            return new Response($json_string, 200, ['Content-Type' => 'application/json']);
        } catch (GameException $ge) {
            return $app->json(['message' => Lang::get($ge->getMessage())], 400);
        }
    }
}
