<?php
namespace Yface\Library\File;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Yface\Exception\YFFileException;

class FileUploader
{
    /**
     * @param UploadedFile $file
     * @param string $dest_dir
     * @param string $dest_filename 파일명 (확장자 제외)
     * @return string 파일명.실제 업로드한 파일의 확장자
     * @throws YFFileException
     */
    public static function upload(UploadedFile $file, $dest_dir, $dest_filename)
    {
        if (!$file instanceof UploadedFile) {
            throw new YFFileException('file.no.image');
        }

        if ($file->getError() > 0) {
            throw new YFFileException('file.error.occurred');
        }

        if ($file->getSize() == 0) {
            throw new YFFileException('file.not.valid');
        }

        $dest_filename_with_ext = $dest_filename . '.' . $file->getClientOriginalExtension();

        try {
            $file->move($dest_dir, $dest_filename_with_ext);
        } catch (FileException $e) {
            throw new YFFileException('file.error.occurred');
        }

        return $dest_filename_with_ext;
    }
}
