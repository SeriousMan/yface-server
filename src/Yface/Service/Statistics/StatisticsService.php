<?php
namespace Yface\Service\Statistics;

use Yface\DataStore\Game\GameExecutionRepository;
use Yface\DataStore\Statistics\ExecutionRateStatisticsRepository;
use Yface\DataStore\Statistics\PointStatisticsRepository;
use Yface\DataStore\Statistics\StarStatisticsRepository;
use Yface\Library\Serializer;
use Yface\Service\Point\PointService;
use Yface\Service\Praise\PraiseService;
use Yface\Service\Star\StarService;

class StatisticsService
{
    /**
     * 유저의 통계용 일별 누적 별 갯수 정보 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getUserTotalStarStats($user_idx, $app_type)
    {
        $friends = StarStatisticsRepository::getRepository()->findByAppType($app_type);
        $json_string = (new Serializer())->serializeWithJson($friends);

        $me = StarService::getTotalStarsByDays($user_idx, $app_type);

        $my_days = count($me);
        $friends = array_slice(json_decode($json_string), 0, $my_days);

        return array(
            'me' => $me,
            'friends' => $friends
        );
    }

    /**
     * 유저의 통계용 일별 누적 포인트 정보 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getUserTotalPointDailyStats($user_idx, $app_type)
    {
        $friends = PointStatisticsRepository::getRepository()->findByAppType($app_type);
        $json_string = (new Serializer())->serializeWithJson($friends);

        $me = PointService::getTotalPointsByDays($user_idx, $app_type);
        $my_days = count($me);
        $friends = array_slice(json_decode($json_string), 0, $my_days);

        return array(
            'me' => $me,
            'friends' => $friends
        );
    }

    /**
     * 유저의 통계용 주별 누적 포인트 정보 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getUserTotalPointWeeklyStats($user_idx, $app_type)
    {
        $me = PraiseService::getPointByWeeks($user_idx, $app_type);

        return array('me' => $me);
    }

    /**
     * 유저의 통계용 일별 게임 수행률 정보 리턴
     * ===
     * @param $user_idx
     * @param $game_code
     * @return array
     */
    public static function getUserGameExecutionRateStats($user_idx, $game_code)
    {
        $friends = ExecutionRateStatisticsRepository::getRepository()->findByGameCode($game_code);
        $json_string = (new Serializer())->serializeWithJson($friends);

        $me = static::getUserGameExecutionRates($user_idx, $game_code);
        $my_days = count($me);
        $friends = array_slice(json_decode($json_string), 0, $my_days);

        return array(
            'me' => $me,
            'friends' => $friends
        );
    }

    /**
     * 유저의 게임 수행률 정보 리턴
     * ===
     * @param $user_idx
     * @param $game_code
     * @return array
     */
    private static function getUserGameExecutionRates($user_idx, $game_code)
    {
        $results = GameExecutionRepository::getRepository()->getGameExecutionRatesByGameCode($user_idx, $game_code);

        $rates = array();
        foreach ($results as $key => $row) {
            $rates[] = array(
                'days' => $key + 1,
                'rates' => ($row['rate'] * 100),
                'isObtainStar' => (bool)$row['is_obtain_star']
            );
        }

        return $rates;
    }
}
