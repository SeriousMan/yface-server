<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [7] - MATCH_이리보고 저리보고
 */
class GameMatch extends ResultData
{
    protected $origin_answer_face_list;
    protected $user_answer_face_list;

    public function __construct($results)
    {
        parent::__construct(GameCode::MATCH, $results);

        /** 게임 정보 저장 */
        $this->origin_answer_face_list = implode('|', $results['originAnswerFaceList']);
        $this->user_answer_face_list = implode('|', $results['userAnswerFaceList']);
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'origin_answer_face_list' => $this->origin_answer_face_list,
            'user_answer_face_list' => $this->user_answer_face_list
        ]);
    }
}
