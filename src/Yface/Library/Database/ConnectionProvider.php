<?php
namespace Yface\Library\Database;

use Yface\Library\Configuration as Config;
use /** @noinspection PhpInternalEntityUsedInspection */
    Doctrine\DBAL\Configuration;
use Doctrine\DBAL\DriverManager;

/**
 * 싱글턴 인스턴스로 커넥션 리턴하는 프로바이더 클래스
 * 참고: http://modernpug.github.io/php-the-right-way/pages/Design-Patterns.html
 */
class ConnectionProvider
{
    /**
     * 커넥션 풀 싱글턴 인스턴스를 위한 변수
     * @var array
     */
    private static $connection_pool = array();

    /**
     * @param string $connection_name
     * @return \Doctrine\DBAL\Connection
     */
    public static function getConnection($connection_name = 'default')
    {
        if (!isset(static::$connection_pool[$connection_name])) {
            static::$connection_pool[$connection_name] = static::createConnection($connection_name);
        }

        return static::$connection_pool[$connection_name];
    }

    /**
     * @param string $connection_name
     * @return \Doctrine\DBAL\Connection
     * @throws \Doctrine\DBAL\DBALException
     */
    private static function createConnection($connection_name)
    {
        /** @noinspection PhpInternalEntityUsedInspection */
        $config = new Configuration();
        $config->setResultCacheImpl(DatabaseCacheHelper::getAdaptableCache());

        $connection = DriverManager::getConnection(Config::getDbParams($connection_name), $config);

        return $connection;
    }

    /**
     * 싱글턴 인스턴스 외부 생성 불가
     */
    protected function __construct()
    {
    }

    /**
     * 싱글턴 인스턴스 복제 불가
     * @return void
     */
    private function __clone()
    {
    }
}
