<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Yface\Library\Configuration;
use Yface\Library\Localization\Lang;
use Yface\Service\Resource\ResourceService;

class ResourceController
{
    public function getUpdate(Application $app, Request $request)
    {
        $files = $request->request->get('files');
        $timestamp = intval(trim($request->request->get('t')));
        $app_type = trim($request->headers->get('app'));

        try {
            if (!empty($timestamp)) {
                $update_url = ResourceService::getResourceUpdateUrl($files, $app_type, $timestamp);
                $update_url = empty($update_url) ? null : 'http://' . $update_url;
            } else {
                if (!ResourceService::isResourceFileExists('/updates/latest_files.zip')) {
                    throw new \Exception('file.error.occurred');
                }

                $update_url = 'http://' . Configuration::$values['url']['resources'] . '/updates/latest_files.zip';
            }

            return $app->json([
                'updateUrl' => $update_url,
                'updatedDate' => date('Y-m-d H:i:s')
            ]);
        } catch (\Exception $e) {
            return $app->json(['message' => Lang::get($e->getMessage())], 400);
        }
    }
}
