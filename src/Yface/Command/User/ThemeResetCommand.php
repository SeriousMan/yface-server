<?php
namespace Yface\Command\User;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Service\User\UserService;

class ThemeResetCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('user:theme:reset')
            ->setDescription('매일 새벽 4시 이후 유저의 테마 설정 정보를 초기화');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('USER THEME RESET - 유저의 테마 설정 정보 초기화 CRON 시작');
        self::reset();
        static::report('USER THEME RESET - 유저의 테마 설정 정보 초기화 CRON 종료');
    }

    private function reset()
    {
        $users = UserService::getAllUser();

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($users) {
                foreach ($users as $user) {
                    $user->setIsThemeSelected(false, APP_TYPE_YFACE);
                    $user->setIsThemeSelected(false, APP_TYPE_YCOG);

                    $em->persist($user);
                    $em->flush($user);
                }
            }
        );
    }
}
