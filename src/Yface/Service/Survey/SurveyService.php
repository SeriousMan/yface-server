<?php
namespace Yface\Service\Survey;

use Doctrine\ORM\EntityManager;
use Yface\DataStore\Survey\SurveyRepository;
use Yface\DataStore\Survey\UserSurveyTodoRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\SurveyException;
use Yface\Exception\UserException;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Library\DateHelper;
use Yface\Model\Survey\UserSurveyAnswer;
use Yface\Model\Survey\UserSurveyTodo;
use Yface\Model\User\User;
use Yface\Service\Point\PointService;

class SurveyService
{
    /**
     * 유저의 설문 목록
     * ===
     *
     * @param $user_idx
     * @param $app_type
     *
     * @return \Yface\Model\Survey\UserSurveyTodo[]
     */
    public static function getUserSurveyList($user_idx, $app_type)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            return [];
        }

        $days = $user->getDaysFromSurveyCompleted($app_type);
        $time = ($days < UserSurveyTodo::TIME_END_PERIOD) ? UserSurveyTodo::TIME_BEGIN : UserSurveyTodo::TIME_END;

        return UserSurveyTodoRepository::getRepository()->findByUserIdxAndTime(
            $user_idx,
            $time,
            $app_type
        );
    }

    /**
     * 유저가 해야 할 설문 선택하여 추가
     * ===
     *
     * @param User $user
     * @param $app_type
     *
     * @throws UserException
     */
    public static function choiceUserSurveyTodo(User $user, $app_type)
    {
        $surveys = SurveyRepository::getRepository()->getSurveyTodo($user->getAge(), $app_type);
        /** 해야 할 설문이 없는 경우 완료한 것으로 간주 */
        if (count($surveys) === 0) {
            static::checkCompleteAllSurvey($user->getIdx(), $app_type);

            return;
        }

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user, $surveys, $app_type) {
                /** 가입 후, 66일 후 설문 할 수 있도록 추가 */
                foreach ($surveys as $survey) {
                    $user_survey_todo = new UserSurveyTodo();
                    $user_survey_todo->setUserIdx($user->getIdx());
                    $user_survey_todo->setSurvey($survey);
                    $user_survey_todo->setIsDone(false);
                    $user_survey_todo->setAppType($app_type);
                    $user_survey_todo->setTime(UserSurveyTodo::TIME_BEGIN);
                    $user_survey_todo->setRegDate(new \DateTime('now'));
                    $user_survey_todo->setModDate(new \DateTime('now'));

                    $em->persist($user_survey_todo);
                    $em->flush($user_survey_todo);

                    $other_survey_todo = clone $user_survey_todo;
                    $other_survey_todo->setTime(UserSurveyTodo::TIME_END);

                    $em->persist($other_survey_todo);
                    $em->flush($other_survey_todo);
                }

                $user->setIsSurveyTarget(true);

                $em->persist($user);
                $em->flush($user);
            }
        );
    }

    /**
     * 유저 문답 저장
     * ===
     *
     * @param $user_idx
     * @param $survey_idx
     * @param $answers
     * @param $app_type
     */
    public static function saveSurveyAnswers($user_idx, $survey_idx, $answers, $app_type)
    {
        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user_idx, $survey_idx, $answers, $app_type) {
                $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
                $days = $user->getDaysFromSurveyCompleted($app_type);
                $time = ($days < UserSurveyTodo::TIME_END_PERIOD) ? UserSurveyTodo::TIME_BEGIN : UserSurveyTodo::TIME_END;

                $survey_todo = UserSurveyTodoRepository::getRepository()->findOneByUserIdxAndSurveyIdx(
                    $user_idx,
                    $survey_idx,
                    $time,
                    $app_type
                );
                if (empty($survey_todo)) {
                    throw new SurveyException('survey.not.subject');
                }

                foreach ($answers as $answer) {
                    $user_survey_answer = new UserSurveyAnswer();
                    $user_survey_answer->setUserIdx($user_idx);
                    $user_survey_answer->setSurveyTodo($survey_todo);
                    $user_survey_answer->setSurveyQuestionIdx($answer['sqIdx']);
                    $user_survey_answer->setSurveyTodoIdx($survey_todo->getIdx());
                    $user_survey_answer->setAnswer($answer['answer']);
                    $user_survey_answer->setRegDate(new \DateTime('now'));

                    $em->persist($user_survey_answer);
                    $em->flush($user_survey_answer);
                }

                static::checkCompleteSurvey($user_idx, $survey_idx, $time, $app_type);
                static::checkCompleteAllSurvey($user_idx, $app_type);
            }
        );
    }

    /**
     * 설문 완료 표시
     * ===
     *
     * @param $user_idx
     * @param $survey_idx
     * @param $app_type
     * @param $time
     */
    private static function checkCompleteSurvey($user_idx, $survey_idx, $time, $app_type)
    {
        $user_survey_todo = UserSurveyTodoRepository::getRepository()->findOneByUserIdxAndSurveyIdx(
            $user_idx,
            $survey_idx,
            $time,
            $app_type
        );
        $user_survey_todo->setIsDone(true);

        $em = EntityManagerProvider::getEntityManager();
        $em->persist($user_survey_todo);
        $em->flush($user_survey_todo);
    }

    /**
     * 해당 앱의 모든 설문을 완료한 경우
     * ===
     *
     * @param $user_idx
     * @param $app_type
     */
    public static function checkCompleteAllSurvey($user_idx, $app_type)
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            return;
        }

        $days = $user->getDaysFromSurveyCompleted($app_type);
        $time = ($days < UserSurveyTodo::TIME_END_PERIOD) ? UserSurveyTodo::TIME_BEGIN : UserSurveyTodo::TIME_END;

        /** 66일 후 설문조사는 완료일자 갱신 처리 안함 */
        if ($time === UserSurveyTodo::TIME_END) {
            return;
        }

        if (!static::checkIfSurveyAllCompleted($user_idx, $time, $app_type)) {
            return;
        }

        $user->setSurveyCompletedDate(DateHelper::getStartTimeOfToday(), $app_type);

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user, $user_idx, $app_type) {
                $em->persist($user);
                $em->flush($user);

                PointService::logDailyUserPoint($user_idx, 1, 0, 0, $app_type);
            }
        );
    }

    /**
     * 모든 설문이 완료되었는지 확인
     * ===
     *
     * @param $user_idx
     * @param $time
     * @param $app_type
     *
     * @return bool
     */
    public static function checkIfSurveyAllCompleted($user_idx, $time, $app_type)
    {
        $user_survey_todo_list = UserSurveyTodoRepository::getRepository()->findByUserIdxAndTime(
            $user_idx,
            $time,
            $app_type
        );

        $is_completed = true;
        foreach ($user_survey_todo_list as $user_survey_todo) {
            if ($user_survey_todo->getIsDone()) {
                continue;
            }

            $is_completed = false;
        }

        return $is_completed;
    }

    /**
     * 유저 설문 결과 리턴
     * ===
     *
     * @param $user_idx
     * @param $app_type
     *
     * @throws SurveyException
     * @return array
     */
    public static function getUserSurveyResults($user_idx, $app_type)
    {
        if (!static::checkIfUserCanViewSurveyResult($user_idx, $app_type)) {
            throw new SurveyException('survey.cant.check.result');
        }

        $user_survey_todo_list = UserSurveyTodoRepository::getRepository()->findByUserIdxAndTime(
            $user_idx,
            null,
            $app_type
        );

        $survey_results = [];
        foreach ($user_survey_todo_list as $user_survey_todo) {
            $survey = $user_survey_todo->getSurvey();
            $survey_name = $survey->getIdx();
            $survey_time = $user_survey_todo->getTime();

            $survey_results[$survey_name][$survey_time] = static::calculateUserSurveyResult($user_survey_todo);
        }

        $results = [];
        foreach ($survey_results as $key => $value) {
            $results[] = [
                'idx' => $key,
                'preScore' => $value[UserSurveyTodo::TIME_BEGIN],
                'postScore' => $value[UserSurveyTodo::TIME_END]
            ];
        }

        return $results;
    }

    /**
     * 유저가 설문 결과를 볼 수 있는지 여부 리턴
     * ===
     *
     * @param $user_idx
     * @param $app_type
     *
     * @return bool
     */
    private static function checkIfUserCanViewSurveyResult($user_idx, $app_type)
    {
        $begin_done_count = UserSurveyTodoRepository::getRepository()->getSurveyDoneCountByUserIdxAndTime(
            $user_idx,
            UserSurveyTodo::TIME_BEGIN,
            $app_type
        );
        $end_done_count = UserSurveyTodoRepository::getRepository()->getSurveyDoneCountByUserIdxAndTime(
            $user_idx,
            UserSurveyTodo::TIME_END,
            $app_type
        );

        return $begin_done_count === $end_done_count;
    }

    /**
     * TODO: 리팩터링 필요
     *
     * 유저 설문 결과 산출 값 리턴
     * ===
     *
     * @param UserSurveyTodo $user_survey_todo
     *
     * @return float|int
     * @throws SurveyException
     */
    public static function calculateUserSurveyResult(UserSurveyTodo $user_survey_todo)
    {
        $sum = 0;
        $survey = $user_survey_todo->getSurvey();
        $answers = $user_survey_todo->getAnswers();

        if ($survey->getName() == 'PedsQL') {
            if ($survey->getMinAge() == 5 && $survey->getTarget() == 'GUARDIAN') {
                /** @var $answer UserSurveyAnswer */
                foreach ($answers as $answer) {
                    $temp = $answer->getAnswer();

                    switch ($temp) {
                        case 1:
                            $sum += 100;
                            break;
                        case 2:
                            $sum += 50;
                            break;
                        case 3:
                            $sum += 0;
                            break;
                    }
                }
            } else {
                /** @var $answer UserSurveyAnswer */
                foreach ($answers as $answer) {
                    $temp = $answer->getAnswer();

                    switch ($temp) {
                        case 1:
                            $sum += 100;
                            break;
                        case 2:
                            $sum += 75;
                            break;
                        case 3:
                            $sum += 50;
                            break;
                        case 4:
                            $sum += 25;
                            break;
                        case 5:
                            $sum += 0;
                    }
                }
            }

            return $sum / $answers->count();
        } else if ($survey->getName() == 'SRS') {
            /** @var $answer UserSurveyAnswer */
            foreach ($answers as $key => $answer) {
                /** 해당 문항만 역코딩 */
                if (in_array($key + 1, [3, 7, 11, 12, 15, 17, 21, 22, 26, 32, 38, 40, 43, 45, 48, 52, 55])) {
                    $sum += abs($answer->getAnswer() - 3);
                } else {
                    $sum += $answer->getAnswer();
                }
            }

            return $sum;
        } else if ($survey->getName() == 'SSIS') {
            /** @var $answer UserSurveyAnswer */
            foreach ($answers as $answer) {
                $sum += $answer->getAnswer() - 1;
            }

            return $sum;
        } else if ($survey->getName() == '집행기능') {
            /** @var $answer UserSurveyAnswer */
            foreach ($answers as $answer) {
                $sum += $answer->getAnswer();
            }

            return $sum;
        } else if ($survey->getName() == 'PSI-SF') {
            /** @var $answer UserSurveyAnswer */
            foreach ($answers as $answer) {
                $sum += $answer->getAnswer();
            }

            $db = ConnectionProvider::getConnection();
            $value = $db->fetchColumn(
                'SELECT value FROM yf_survey_psi_sf_unit WHERE point = ?',
                [$sum]
            );

            return $value;
        }

        throw new SurveyException('survey.no.type');
    }
}
