<?php
namespace Yface\Model\Attendance;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Attendance\UserAttendanceRepository")
 * @ORM\Table(name="yf_user_attendance")
 * @JMS\ExclusionPolicy("none")
 */
class UserAttendance
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     *
     * @var $user_idx integer
     */
    private $user_idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $days integer
     */
    private $days;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YFACE', 'YCOG')")
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getUserIdx(): int
    {
        return $this->user_idx;
    }

    /**
     * @param int $user_idx
     */
    public function setUserIdx(int $user_idx)
    {
        $this->user_idx = $user_idx;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

    /**
     * @param int $days
     */
    public function setDays(int $days)
    {
        $this->days = $days;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }
}
