<?php
namespace Yface\DataStore\Game;

use Yface\Game\GameCode;
use Yface\Library\Database\ConnectionProvider;

class GameExecutionRepository
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $db;

    public function __construct()
    {
        $this->db = ConnectionProvider::getConnection();
    }

    /**
     * @param $user_idx
     * @param $game_code
     * @return array
     */
    public function getGameExecutionRatesByGameCode($user_idx, $game_code)
    {
        $table_name = GameCode::getGameTableName($game_code);
        if (empty($table_name)) {
            return [];
        }

        $sql = 'SELECT
                  accuracy as rate,
                  is_obtain_star
                FROM ' . $table_name . '
                WHERE u_idx = ?
                    AND accuracy IS NOT NULL
                ORDER BY idx ASC';

        return $this->db->fetchAll($sql, [$user_idx]);
    }

    /**
     * @return GameExecutionRepository
     */
    public static function getRepository()
    {
        return new static();
    }
}
