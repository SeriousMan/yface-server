<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [9] - ORDER_차례대로 톡톡톡
 */
class GameOrder extends ResultData
{
    protected $answer_order;
    protected $user_order_list;

    public function __construct($results)
    {
        parent::__construct(GameCode::ORDER, $results);

        /** 게임 정보 저장 */
        $answer_order = '';
        foreach ($results['userOrderList'] as $key => $value) {
            $answer_order .= strval($key + 1);
        }

        $this->answer_order = $answer_order;
        $this->user_order_list = implode('|', $results['userOrderList']);
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'answer_order' => $this->answer_order,
            'user_order_list' => $this->user_order_list
        ]);
    }
}
