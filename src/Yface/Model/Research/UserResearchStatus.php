<?php
namespace Yface\Model\Research;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Research\UserResearchStatusRepository")
 * @ORM\Table(name="yf_user_research_status")
 * @JMS\ExclusionPolicy("none")
 */
class UserResearchStatus
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idx;
    /**
     * @ORM\Column(type="integer")
     */
    private $u_idx;
    /**
     * @ORM\Column(type="integer")
     */
    private $r_idx;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     */
    private $reg_date;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     */
    private $mod_date;

    /**
     * @ORM\ManyToOne(targetEntity="Yface\Model\Research\Research", inversedBy="user_research_status")
     * @ORM\JoinColumn(name="r_idx", referencedColumnName="idx")
     * @JMS\Type("Yface\Model\Research\Research")
     */
    // TODO: null 값으로 반환된다.
    private $research;

    public function __construct()
    {
        //
    }

    /**
     * @return integer
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * @return integer
     */
    public function getUidx()
    {
        return $this->u_idx;
    }

    /**
     * @param integer $u_idx
     */
    public function setUidx($u_idx)
    {
        $this->u_idx = $u_idx;
    }

    /**
     * @return integer
     */
    public function getRidx()
    {
        return $this->r_idx;
    }

    /**
     * @param integer $r_idx
     */
    public function setRidx($r_idx)
    {
        $this->r_idx = $r_idx;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return \DateTime
     */
    public function getModDate()
    {
        return $this->mod_date;
    }

    /**
     * @param \DateTime $mod_date
     */
    public function setModDate($mod_date)
    {
        $this->mod_date = $mod_date;
    }

    public function getResearch()
    {
        return $this->research;
    }
    public function setResearch($research)
    {
        $this->research = $research;
    }
}
