<?php
namespace Yface\Command\Game;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Library\Database\ConnectionProvider;
use Yface\Service\Game\TodayGameService;

class GameTalkModifyCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('game:ta:modify');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('ONCE - 대화가 필요해 데이터 보정 시작');

        self::modify();

        static::report('ONCE - 대화가 필요해 데이터 보정 종료');
    }

    protected function modify()
    {
        $db = ConnectionProvider::getConnection();
        $db->executeQuery('UPDATE yf_game_talk SET is_recorded = ? WHERE accuracy IS NOT NULL', ["O"]);
    }
}
