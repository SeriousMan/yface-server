<?php
namespace Yface\Library\Database;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Proxy\ProxyFactory;
use Doctrine\ORM\Tools\Setup;

/**
 * 싱글턴 인스턴스로 엔티티 매니저 리턴하는 프로바이더 클래스
 * 참고: http://modernpug.github.io/php-the-right-way/pages/Design-Patterns.html
 */
class EntityManagerProvider
{
    const METADATA_CACHE_NAMESPACE = "YF_NAMESPACE_4";

    /**
     * 엔티티 매니저 풀 싱글턴 인스턴스를 위한 변수
     * @var $entity_manager_pool EntityManager[]
     */
    private static $entity_manager_pool = array();

    /**
     * @param $connection_pool_name
     * @return EntityManager
     */
    public static function getEntityManager($connection_pool_name = 'default')
    {
        if (!isset(static::$entity_manager_pool[$connection_pool_name])) {
            static::$entity_manager_pool[$connection_pool_name] = static::createEntityManager($connection_pool_name);
        }

        return static::$entity_manager_pool[$connection_pool_name];
    }

    /**
     * @param $connection_pool_name
     * @return EntityManager
     * @throws \Doctrine\ORM\ORMException
     */
    private static function createEntityManager($connection_pool_name)
    {
        $paths = array(__DIR__ . '/../..');
        $config = Setup::createAnnotationMetadataConfiguration(array(__DIR__ . "/../.."), false);
        /**
         * PROD 레벨에서는 메타데이터를 파싱한것을 캐싱해서 저장해놓기 때문에 해당 메서드 필요
         * setNamespace 메서드가 있는 곳은 CacheProvider 쪽에 존재
         */
        $config->getMetadataCacheImpl()->setNamespace(self::METADATA_CACHE_NAMESPACE);

        /**
         * 캐싱 적용
         */
        $config->setQueryCacheImpl(DatabaseCacheHelper::getAdaptableCache());

        /**
         * Simple Metadata Driver 로는 ORM Annotation 이 인식이 안되서 드라이버 변경으로 해결
         * http://stackoverflow.com/questions/15099060/doctrine2-class-is-not-a-valid-entity-or-mapped-super-class
         */
        $driver = new AnnotationDriver(new AnnotationReader(), $paths);
        $config->setMetadataDriverImpl($driver);

        /**
         * Doctrine2 ORM, JMS Serializer 동시 사용 이슈
         * (JMS Serializer 를 위해 AnnotationReader 로 Metadata Driver 를 변경했는데 이 드라이버의 Auto-Loader 가 PSR-0 을 따르지 않아 발생)
         * http://stackoverflow.com/questions/14629137/jmsserializer-stand-alone-annotation-does-not-exist-or-cannot-be-auto-loaded
         */
        AnnotationRegistry::registerLoader('class_exists');

        /**
         * 실제 운영할 때는 Proxy Directory 설정이 필요함
         * http://docs.doctrine-project.org/en/latest/reference/advanced-configuration.html#generating-proxy-classes
         */
        $config->setAutoGenerateProxyClasses(ProxyFactory::AUTOGENERATE_FILE_NOT_EXISTS);
        $config->setProxyDir(sys_get_temp_dir() . '/doctrine_proxy');

        $connection = ConnectionProvider::getConnection($connection_pool_name);

        /**
         * Entity Custom Data Type 설정
         */
        $em = EntityManager::create($connection, $config);

        return $em;
    }

    /**
     * 싱글턴 인스턴스 외부 생성 불가
     */
    protected function __construct()
    {
    }

    /**
     * 싱글턴 인스턴스 복제 불가
     * @return void
     */
    private function __clone()
    {
    }
}
