<?php
namespace Yface\Model\Store;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Store\UserProductRepository")
 * @ORM\Table(name="yf_user_product")
 * @JMS\ExclusionPolicy("none")
 */
class UserProduct
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     * @JMS\Exclude
     *
     * @var $user_idx integer
     */
    private $user_idx;
    /**
     * @ORM\Column(type="integer", name="p_idx", options={"unsigned":true})
     * @JMS\Exclude
     *
     * @var $product_idx integer
     */
    private $product_idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true}, nullable=true)
     * @JMS\Exclude
     *
     * @var $point_idx integer
     */
    private $point_idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("coordinateX")
     *
     * @var $coordinate_x integer
     */
    private $coordinate_x;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @JMS\SerializedName("coordinateY")
     *
     * @var $coordinate_y integer
     */
    private $coordinate_y;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YCOG', 'YFACE')")
     * @JMS\Exclude
     *
     * @var $app_type string
     */
    private $app_type;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;

    /**
     * @ORM\OneToOne(targetEntity="Yface\Model\Store\Product")
     * @ORM\JoinColumn(name="p_idx", referencedColumnName="idx")
     * @JMS\Type("Yface\Model\Store\Product")
     *
     * @var $product Product
     */
    private $product;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getUserIdx(): int
    {
        return $this->user_idx;
    }

    /**
     * @param int $user_idx
     */
    public function setUserIdx(int $user_idx)
    {
        $this->user_idx = $user_idx;
    }

    /**
     * @return int
     */
    public function getProductIdx(): int
    {
        return $this->product_idx;
    }

    /**
     * @return int
     */
    public function getPointIdx(): int
    {
        return $this->point_idx;
    }

    /**
     * @param int $point_idx
     */
    public function setPointIdx(int $point_idx)
    {
        $this->point_idx = $point_idx;
    }

    /**
     * @return int
     */
    public function getCoordinateX(): int
    {
        return $this->coordinate_x;
    }

    /**
     * @param int $coordinate_x
     */
    public function setCoordinateX(int $coordinate_x)
    {
        $this->coordinate_x = $coordinate_x;
    }

    /**
     * @return int
     */
    public function getCoordinateY(): int
    {
        return $this->coordinate_y;
    }

    /**
     * @param int $coordinate_y
     */
    public function setCoordinateY(int $coordinate_y)
    {
        $this->coordinate_y = $coordinate_y;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return Product
     */
    public function getProduct() : Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;
    }
}
