<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [2] - EYE_쌍둥이를 찾아라
 */
class GameEye extends ResultData
{
    protected $origin_answer_list;
    protected $user_answer_list;
    protected $total_consonants_count;
    protected $total_vowels_count;
    protected $correct_consonants_count;
    protected $correct_vowels_count;
    protected $consonants_accuracy;
    protected $vowels_accuracy;
    protected $direction_count_answer_accuracy_list;

    public function __construct($results)
    {
        parent::__construct(GameCode::EYE, $results);

        /** 게임 정보 저장 */
        $this->origin_answer_list = implode('|', $results['originAnswerList']);
        $this->user_answer_list = implode('|', $results['userAnswerList']);
        $this->total_consonants_count = $results['totalConsonantsCount'];
        $this->total_vowels_count = $results['totalVowelsCount'];
        $this->correct_consonants_count = $results['correctConsonantsCount'];
        $this->correct_vowels_count = $results['correctVowelsCount'];
        $this->consonants_accuracy = $results['consonantsAccuracy'];
        $this->vowels_accuracy = $results['vowelsAccuracy'];
        $this->direction_count_answer_accuracy_list = implode(
            '|',
            array_map(
                function ($value, $key) {
                    return $key . '=' . strval($value);
                },
                $results['directionCountAnswerAccuracyMap'],
                array_keys($results['directionCountAnswerAccuracyMap'])
            )
        );
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'origin_answer_list' => $this->origin_answer_list,
            'user_answer_list' => $this->user_answer_list,
            'total_consonants_count' => $this->total_consonants_count,
            'total_vowels_count' => $this->total_vowels_count,
            'correct_consonants_count' => $this->correct_consonants_count,
            'correct_vowels_count' => $this->correct_vowels_count,
            'consonants_accuracy' => $this->consonants_accuracy,
            'vowels_accuracy' => $this->vowels_accuracy,
            'direction_count_answer_accuracy_list' => $this->direction_count_answer_accuracy_list
        ]);
    }
}
