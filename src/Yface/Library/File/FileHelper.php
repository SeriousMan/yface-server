<?php
namespace Yface\Library\File;

class FileHelper
{
    /**
     * MD5 Hashed 이름 리턴
     * ===
     * @return string
     */
    public static function generateRandomFileName()
    {
        $raw_string = strval(time()) . rand(1, 1000);

        return hash('md5', $raw_string);
    }
}
