<?php
namespace Yface\Model\Praise;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="yf_praise_promise")
 * @JMS\ExclusionPolicy("none")
 */
class Promise
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", name="u_idx", options={"unsigned":true})
     * @JMS\Exclude
     *
     * @var $user_idx integer
     */
    private $user_idx;
    /**
     * @ORM\Column(type="string")
     *
     * @var $title string
     */
    private $title;
    /**
     * @ORM\Column(type="integer", length=1, options={"unsigned":true}, nullable=true)
     * @JMS\Exclude
     *
     * @var $day_1 integer
     */
    private $day_1;
    /**
     * @ORM\Column(type="integer", length=1, options={"unsigned":true}, nullable=true)
     * @JMS\Exclude
     *
     * @var $day_2 integer
     */
    private $day_2;
    /**
     * @ORM\Column(type="integer", length=1, options={"unsigned":true}, nullable=true)
     * @JMS\Exclude
     *
     * @var $day_3 integer
     */
    private $day_3;
    /**
     * @ORM\Column(type="integer", length=1, options={"unsigned":true}, nullable=true)
     * @JMS\Exclude
     *
     * @var $day_4 integer
     */
    private $day_4;
    /**
     * @ORM\Column(type="integer", length=1, options={"unsigned":true}, nullable=true)
     * @JMS\Exclude
     *
     * @var $day_5 integer
     */
    private $day_5;
    /**
     * @ORM\Column(type="integer", length=1, options={"unsigned":true}, nullable=true)
     * @JMS\Exclude
     *
     * @var $day_6 integer
     */
    private $day_6;
    /**
     * @ORM\Column(type="integer", length=1, options={"unsigned":true}, nullable=true)
     * @JMS\Exclude
     *
     * @var $day_7 integer
     */
    private $day_7;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;

    /**
     * @ORM\ManyToOne(targetEntity="Yface\Model\Praise\Praise", inversedBy="promises")
     * @ORM\JoinColumn(name="praise_idx", referencedColumnName="idx")
     * @JMS\Type("Yface\Model\Praise\Praise")
     * @JMS\Exclude
     *
     * @var $praise Praise
     */
    private $praise;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @param int $idx
     */
    public function setIdx(int $idx)
    {
        $this->idx = $idx;
    }

    /**
     * @return int
     */
    public function getUserIdx(): int
    {
        return $this->user_idx;
    }

    /**
     * @param int $user_idx
     */
    public function setUserIdx(int $user_idx)
    {
        $this->user_idx = $user_idx;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getDay1(): int
    {
        return $this->day_1;
    }

    /**
     * @param int $day_1
     */
    public function setDay1(int $day_1)
    {
        $this->day_1 = $day_1;
    }

    /**
     * @return int
     */
    public function getDay2(): int
    {
        return $this->day_2;
    }

    /**
     * @param int $day_2
     */
    public function setDay2(int $day_2)
    {
        $this->day_2 = $day_2;
    }

    /**
     * @return int
     */
    public function getDay3(): int
    {
        return $this->day_3;
    }

    /**
     * @param int $day_3
     */
    public function setDay3(int $day_3)
    {
        $this->day_3 = $day_3;
    }

    /**
     * @return int
     */
    public function getDay4(): int
    {
        return $this->day_4;
    }

    /**
     * @param int $day_4
     */
    public function setDay4(int $day_4)
    {
        $this->day_4 = $day_4;
    }

    /**
     * @return int
     */
    public function getDay5(): int
    {
        return $this->day_5;
    }

    /**
     * @param int $day_5
     */
    public function setDay5(int $day_5)
    {
        $this->day_5 = $day_5;
    }

    /**
     * @return int
     */
    public function getDay6(): int
    {
        return $this->day_6;
    }

    /**
     * @param int $day_6
     */
    public function setDay6(int $day_6)
    {
        $this->day_6 = $day_6;
    }

    /**
     * @return int
     */
    public function getDay7(): int
    {
        return $this->day_7;
    }

    /**
     * @param int $day_7
     */
    public function setDay7(int $day_7)
    {
        $this->day_7 = $day_7;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return Praise
     */
    public function getPraise(): Praise
    {
        return $this->praise;
    }

    /**
     * @param Praise $praise
     */
    public function setPraise(Praise $praise)
    {
        $this->praise = $praise;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("scores")
     * @JMS\Type("array")
     *
     * @return array
     */
    public function getScore()
    {
        return array(
            $this->day_1,
            $this->day_2,
            $this->day_3,
            $this->day_4,
            $this->day_5,
            $this->day_6,
            $this->day_7
        );
    }
}
