<?php
namespace Yface\DataStore\Feed;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Feed\FeedAction;

class FeedActionRepository extends EntityRepository
{
    /**
     * @param $feed_idx
     * @param $user_idx
     * @param $action
     * @return null|FeedAction
     */
    public function findOneByFeedIdxAndUserIdx($feed_idx, $user_idx, $action) {
        $qb = $this->createQueryBuilder('fa');
        $qb->where('fa.feed_idx = :feed_idx')
            ->setParameter('feed_idx', $feed_idx, Type::INTEGER)
            ->andWhere('fa.user_idx = :user_idx')
            ->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->andWhere('fa.action = :action')
            ->setParameter('action', $action, Type::STRING);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return FeedActionRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Feed\FeedAction');
    }
}
