<?php
namespace Yface\Model\Resource;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Resource\ResourceFileInfoRepository")
 * @ORM\Table(name="yf_resource_file_info")
 */
class ResourceFileInfo {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private $game_code;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('YFACE', 'YCOG')")
     */
    private $app_type;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $file_name;
    /**
     * @ORM\Column(type="datetime")
     */
    private $reg_date;
    /**
     * @ORM\Column(type="datetime")
     */
    private $mod_date;

    /**
     * @return integer
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * @return integer
     */
    public function getGameCode()
    {
        return $this->game_code;
    }

    /**
     * @param integer $game_code
     */
    public function setGameCode($game_code)
    {
        $this->game_code = $game_code;
    }

    /**
     * @return string
     */
    public function getAppType()
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType($app_type)
    {
        $this->app_type = $app_type;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->file_name;
    }

    /**
     * @param string $file_name
     */
    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return \DateTime
     */
    public function getModDate()
    {
        return $this->mod_date;
    }

    /**
     * @param \DateTime $mod_date
     */
    public function setModDate($mod_date)
    {
        $this->mod_date = $mod_date;
    }
}