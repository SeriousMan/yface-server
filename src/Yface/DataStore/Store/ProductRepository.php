<?php
namespace Yface\DataStore\Store;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Store\Product;

class ProductRepository extends EntityRepository
{
    /**
     * @param $product_idx
     * @return null|object|Product
     */
    public function findOneByProductIdx($product_idx)
    {
        return $this->findOneBy(array('idx' => $product_idx));
    }

    /**
     * @param $theme_code
     * @return Product[]
     */
    public function findByThemeCode($theme_code) {
        return $this->findBy(array('theme_code' => $theme_code));
    }

    /**
     * @return ProductRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Store\Product');
    }
}
