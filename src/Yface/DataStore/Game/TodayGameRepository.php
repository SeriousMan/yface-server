<?php
namespace Yface\DataStore\Game;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Game\TodayGame;

class TodayGameRepository extends EntityRepository
{
    /**
     * @param $date
     * @param $app_type
     * @return null|object|TodayGame
     */
    public function findOneByDateAndAppType($date, $app_type)
    {
        return $this->findOneBy(array('reg_date' => $date, 'app_type' => $app_type));
    }

    /**
     * @return TodayGameRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Game\TodayGame');
    }
}
