<?php
namespace Yface\Admin\Statistics;

use Yface\Game\GameCode;
use Yface\Game\GameResult;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\MathHelper;

class User
{
    /**
     * 유저 요약 통계 리턴
     * ===
     * @param $user_idx
     *
     * @return array
     */
    public static function buildUser($user_idx)
    {
        $db = ConnectionProvider::getConnection();
        $sql = 'SELECT * FROM yf_user WHERE idx = ?';
        $user = $db->fetchAssoc($sql, [$user_idx]);

        $sql = 'SELECT * FROM yf_user_summary_info where u_idx = ?';
        $user_summary = $db->fetchAssoc($sql, [$user_idx]);

        $sql = 'SELECT reg_date
                FROM yf_user_login_log
                WHERE u_idx = ?
                    AND DATE(reg_date) = DATE(NOW())';
        $user_today_login_date = $db->fetchColumn($sql, [$user_idx]);

        $sql = 'SELECT reg_date
                FROM yf_user_login_log
                WHERE u_idx = ?
                GROUP BY DATE(reg_date)
                ORDER BY reg_date DESC
                LIMIT 1';
        $user_last_login_date = $db->fetchColumn($sql, [$user_idx]);

        $sql = 'SELECT count(r.date)
                FROM (
                    SELECT distinct DATE(reg_date) as date
                    FROM yf_user_login_log
                    WHERE u_idx = ?
                    GROUP BY DATE(reg_date)
                ) r';
        $user_login_count = $db->fetchColumn($sql, [$user_idx]);

        $sql = 'SELECT count(idx) FROM yf_user_attendance WHERE u_idx = ?';
        $user_attendance_count = $db->fetchColumn($sql, [$user_idx]);

        $sql = 'SELECT count(idx) FROM yf_user_star WHERE u_idx = ?';
        $user_star_count = $db->fetchColumn($sql, [$user_idx]);

        $sql = 'SELECT sum(amount) FROM yf_user_point WHERE u_idx = ? AND amount > 0';
        $user_point = $db->fetchColumn($sql, [$user_idx]);

        // 테마별 유저 선택 정보
        $sql = 'SELECT count(idx) FROM yf_user_theme WHERE u_idx = ? AND theme_code = ?';

        $user_themes = [];
        for ($i = 1; $i <= 4; $i++) {
            $user_themes[] = $db->fetchColumn($sql, [$user_idx, $i]);
        }

        $sql = 'SELECT * FROM yf_user_point WHERE u_idx = ? AND app_type = ?';
        $user_yface_points = $db->fetchAll($sql, [$user_idx, APP_TYPE_YFACE]);
        $user_ycog_points = $db->fetchAll($sql, [$user_idx, APP_TYPE_YCOG]);

        /**
         * 초보자 : 시작 시
         * 베테랑 : 50개
         * 전문가 : 100개
         * 마스터 : 200개
         * 그랜드 마스터 : 300개
         */
        $user_title = '초보자';
        if ($user_star_count >= 300) {
            $user_title = '그랜드 마스터';
        } else if ($user_star_count >= 200) {
            $user_title = '마스터';
        } else if ($user_star_count >= 100) {
            $user_title = '전문가';
        } else if ($user_star_count >= 50) {
            $user_title = '베테랑';
        }

        $is_yface_active = false;
        for ($i = GameCode::CARD; $i < GameCode::BADUM_TISH; $i++) {
            $table_name = GameCode::getGameTableName($i);
            $sql = "SELECT count(*) FROM {$table_name} WHERE u_idx = ? AND is_disabled = 0 AND accuracy IS NOT NULL";

            $result = $db->fetchColumn($sql, [$user_idx]);
            if ($result > 0) {
                $is_yface_active = true;
                break;
            }
        }

        $is_ycog_active = false;
        for ($i = GameCode::BADUM_TISH; $i <= GameCode::WHATS_THE_NUMBER; $i++) {
            $table_name = GameCode::getGameTableName($i);
            $sql = "SELECT count(*) FROM {$table_name} WHERE u_idx = ? AND is_disabled = 0 AND accuracy IS NOT NULL";
            $result = $db->fetchColumn($sql, [$user_idx]);
            if ($result > 0) {
                $is_ycog_active = true;
                break;
            }
        }

        $game_levels = [];
        $game_accuracies = [];
        $avg_level_ups = [];
        for ($i = GameCode::CARD; $i <= GameCode::WHATS_THE_NUMBER; $i++) {
            $table_name = GameCode::getGameTableName($i);
            $sql = "SELECT current_level FROM {$table_name} WHERE u_idx = ? AND is_disabled = 0 AND accuracy IS NOT NULL ORDER BY idx DESC LIMIT 1";

            $current_level = $db->fetchColumn($sql, [$user_idx]);

            $sql = "SELECT avg(accuracy) FROM {$table_name} WHERE u_idx = ? AND current_level = ? AND accuracy IS NOT NULL";
            $accuracy = $db->fetchColumn($sql, [$user_idx, $current_level]);

            $game_levels[] = $current_level;
            $game_accuracies[] = $accuracy;

            /** 유저의 게임별 평균 레벨업 소요 일수 계산 */
            $sql = "SELECT current_level, DATE(reg_date) as date
                    FROM {$table_name}
                    WHERE is_disabled = 0
                      AND u_idx = ?
                      AND accuracy IS NOT NULL
                    GROUP BY current_level
                    ORDER BY reg_date ASC";
            $level_up_dates = $db->fetchAll($sql, [$user_idx]);

            /** 아직 1레벨이면 무시 */
            if (count($level_up_dates) <= 1) {
                continue;
            }

            $user_level_up_days = [];
            /** 레벨업 소요일수를 차례로 계산하여 임시 저장 */
            for ($j = 0; $j < count($level_up_dates) - 1; $j++) {
                $level_up_date = new \DateTime($level_up_dates[$j + 1]['date']);
                $criteria = new \DateTime($level_up_dates[$j]['date']);
                $diff = $level_up_date->diff($criteria);

                $user_level_up_days[] = $diff->days;
            }

            /** 해당 게임의 평균 소요일수 계산 */
            if (count($user_level_up_days) > 0) {
                $avg_level_ups[] = array_sum($user_level_up_days) / count($user_level_up_days);
            }
        }

        /** 전체 평균 레벨업 소요 일수 계산 */
        $avg_level_up_day = 0;
        if (count($avg_level_ups) > 0) {
            $avg_level_up_day = array_sum($avg_level_ups) / count($avg_level_ups);
        }

        return compact(
            'user',
            'user_summary',
            'user_today_login_date',
            'user_last_login_date',
            'user_login_count',
            'user_attendance_count',
            'user_star_count',
            'user_point',
            'user_themes',
            'user_title',
            'user_yface_points',
            'user_ycog_points',
            'game_levels',
            'game_accuracies',
            'avg_level_up_day',
            'is_yface_active',
            'is_ycog_active'
        );
    }

    /**
     * 유저 요약 (+ 게임 데이터) 엑셀 리턴
     * ===
     * @param \PHPExcel $excel
     * @param $user_idx
     * @param $limit_days
     *
     * @return \PHPExcel
     */
    public static function exportUserToExcel(\PHPExcel $excel, $user_idx, $limit_days = 66)
    {
        $stats = User::buildUser($user_idx);

        $row_offset = 1;
        $excel->setActiveSheetIndex(0)
            ->setTitle('유저 요약')
            ->fromArray([
                '아이디',
                '이름',
                '생년월일',
                '성별',
                '진단명',
                '피실험자 여부',
                '훈련 시작 일시 (YFACE)',
                '훈련 시작 일시 (YCOG)',
                '사용한 핸드폰 기종',
                '칭찬스티커 사용 여부 (YFACE)',
                '칭찬스티커 사용 일시 (YFACE)',
                '칭찬스티커 사용 여부 (YCOG)',
                '칭찬스티커 사용 일시 (YCOG)',
                '마지막 접속 일시',
                '총 접속 일수',
                '총 출석 일수',
                '전체 평균 레벨업 소요 일수',
                '전체 누적 별 개수',
                '전체 누적 포인트',
                '칭호',
                '공룡시대 선택 횟수',
                '루루공주 선택 횟수',
                '세계여행 선택 횟수',
                '로봇세상 선택 횟수'
            ], null, 'A' . $row_offset++);

        $excel->getActiveSheet()
            ->fromArray([
                $stats['user']['id'],
                $stats['user']['name'],
                $stats['user']['birth_date'],
                $stats['user']['gender'],
                $stats['user']['diagnosis'],
                ($stats['user']['is_experiment']) ? 'T' : 'F',
                $stats['user']['yface_survey_completed_date'],
                $stats['user']['ycog_survey_completed_date'],
                $stats['user_summary']['device_model'],
                $stats['user']['is_yface_praise_started'],
                $stats['user']['yface_praise_start_date'],
                $stats['user']['is_ycog_praise_started'],
                $stats['user']['ycog_praise_start_date'],
                $stats['user_last_login_date'],
                $stats['user_login_count'],
                $stats['user_attendance_count'],
                $stats['avg_level_up_day'],
                $stats['user_star_count'],
                $stats['user_point'],
                $stats['user_title'],
                $stats['user_themes'][0],
                $stats['user_themes'][1],
                $stats['user_themes'][2],
                $stats['user_themes'][3]
            ], null, 'A' . $row_offset);

        $row_offset = 1;
        $excel->createSheet()->setTitle('YFACE 포인트 사용 내역');
        $excel->setActiveSheetIndex(1)
            ->fromArray([
                '사용 일자',
                '획득 또는 사용 포인트',
                '현재 포인트',
                '사용 설명'
            ], null, 'A' . $row_offset++);

        foreach ($stats['user_yface_points'] as $point) {
            $excel->getActiveSheet()
                ->fromArray([
                    $point['reg_date'],
                    $point['amount'],
                    $point['total_amount'],
                    $point['description']
                ], null, 'A' . $row_offset++);
        }

        $row_offset = 1;
        $excel->createSheet()->setTitle('YCOG 포인트 사용 내역');
        $excel->setActiveSheetIndex(2)
            ->fromArray([
                '사용 일자',
                '획득 또는 사용 포인트',
                '현재 포인트',
                '사용 설명'
            ], null, 'A' . $row_offset++);

        foreach ($stats['user_ycog_points'] as $point) {
            $excel->getActiveSheet()
                ->fromArray([
                    $point['reg_date'],
                    $point['amount'],
                    $point['total_amount'],
                    $point['description']
                ], null, 'A' . $row_offset++);
        }

        return $excel;
    }

    /**
     * 유저의 게임 데이터 통계 리턴
     * ===
     * @param $user_idx
     * @param $game_code
     * @param $table_name
     * @param null $offset
     * @param null $limit
     * @param $for_excel
     *
     * @return array
     */
    public static function buildUserGame($user_idx, $game_code, $table_name, $offset = null, $limit = null, $for_excel = false)
    {
        $db = ConnectionProvider::getConnection();
        $sql = 'SELECT * FROM yf_user WHERE idx = ?';
        $user = $db->fetchAssoc($sql, [$user_idx]);

        $limit_clause = '';
        if ($for_excel || $offset !== null) {
            $limit_clause = "LIMIT {$offset}, {$limit}";
        }

        /**
         * 엑셀 추출용이 아니면 JOIN으로 자를 필요가 없음
         */
        $sql = "SELECT g.* FROM {$table_name} g
                JOIN (
                  SELECT idx FROM {$table_name}
                  WHERE u_idx = ? AND is_disabled = 0 AND accuracy IS NOT NULL
                  ORDER BY idx ASC
                ";
        if ($for_excel) {
            $sql .= $limit_clause;
        }

        $sql .=  ") r ON g.idx = r.idx
                ORDER BY g.idx DESC";
        if (!$for_excel) {
            $sql = $sql . ' ' . $limit_clause;
        }

        $rows = $db->fetchAll($sql, [$user_idx]);

        // 반응 편향, 민감도 추가
        if ($game_code == GameCode::TO_EAT || $game_code == GameCode::UP_DOWN || $game_code == GameCode::NAVIGATE_ME ||
            $game_code == GameCode::IS_IT_THERE || $game_code == GameCode::EYE_FIGHT || $game_code == GameCode::FRIEND) {
            $s = 0;
            $n = 0;
            $miss_count = 0;
            $false_count = 0;

            $rows = array_reverse($rows);

            foreach ($rows as &$row) {
                // HIT, FALSE, MISS, CORRECT 시행별로 누적
                switch ($game_code) {
                    case GameCode::TO_EAT:
                        $s += $row['fresh_count'];
                        $n += $row['throw_count'];
                        $miss_count += $row['fresh_wrong_count'];
                        $false_count += $row['throw_wrong_count'];
                        break;
                    case GameCode::UP_DOWN:
                        $s += $row['up_count'] + $row['down_count'];
                        $n += $row['fake_count'];
                        $miss_count += $row['up_wrong_count'] + $row['down_wrong_count'];
                        $false_count += $row['fake_wrong_count'];
                        break;
                    case GameCode::NAVIGATE_ME:
                        $s += $row['green_count'] + $row['yellow_count'];
                        $n += $row['red_count'];
                        $miss_count += $row['green_wrong_count'] + $row['yellow_wrong_count'];
                        $false_count += $row['red_wrong_count'];
                        break;
                    case GameCode::IS_IT_THERE:
                        $s += $row['target_count'];
                        $n += $row['non_target_count'];
                        $miss_count += $row['target_wrong_count'];
                        $false_count += $row['non_target_wrong_count'];
                        break;
                    case GameCode::EYE_FIGHT:
                        $s += $row['both_count'];
                        $n += $row['one_side_count'];
                        $miss_count += $row['both_wrong_count'];
                        $false_count += $row['one_side_wrong_count'];
                        break;
                    case GameCode::FRIEND:
                        $level_info = json_decode($row['level_info'], true);
                        $temp = (intval($level_info['answerCount']) * intval($level_info['repeatCount']));
                        $s += $temp;
                        $n += (10 - $temp);

                        $miss_count += $row['xo_count'];
                        $false_count += $row['ox_count'];
                        break;
                    default:
                        break;
                }

                $false = 0;
                if ($n != 0) {
                    $false = $false_count / $n;
                }

                $miss = 0;
                if ($s != 0) {
                    $miss = $miss_count / $s;
                }

                // 반응 편향
                $response_set = 0;
                if ($false != 0) {
                    $response_set = ($miss / $false);
                }

                $row['response_set'] = $response_set;
                $hit = 1 - $miss;

                // 민감도
                $row['sensitivity'] = MathHelper::getNormSInv($hit) - MathHelper::getNormSInv($false);
            }

            $rows = array_reverse($rows);
        }

        /**
         * 이후 정보들은 excel 추출용이 아니면 limit clause 가 필요 없다
         */
        if (!$for_excel) {
            $limit_clause = "";
        }

        $sql = "SELECT g.current_level, avg(g.accuracy) as avg_accuracy
                FROM {$table_name} g
                JOIN (
                    SELECT idx FROM {$table_name}
                    WHERE u_idx = ?
                        AND accuracy IS NOT NULL
                        AND is_disabled = 0
                    ORDER BY idx ASC
                    {$limit_clause}
                ) r ON g.idx = r.idx
                GROUP BY g.current_level
                ORDER BY g.current_level DESC
                LIMIT 1";
        $level_info = $db->fetchAssoc($sql, [$user_idx]);

        $sql = "SELECT count(g.idx) as total_play_count
                FROM {$table_name} g
                JOIN (
                    SELECT idx FROM {$table_name}
                    WHERE u_idx = ?
                        AND accuracy IS NOT NULL
                        AND is_disabled = 0
                    ORDER BY idx ASC
                    {$limit_clause}
                ) r ON g.idx = r.idx";
        $play_info = $db->fetchAssoc($sql, [$user_idx]);

        $sql = "SELECT count(g.idx) FROM yf_user_star g
                JOIN (
                    SELECT idx FROM yf_user_star
                    WHERE u_idx = ?
                        AND game_code = ?
                    ORDER BY idx ASC
                    {$limit_clause}
                ) r on g.idx = r.idx";
        $total_star_count = $db->fetchColumn($sql, [$user_idx, $game_code]);

        // 전체 누적 대상 추가 정보
        $additional_info = [];
        if (count(GameResult::getAdditionalTitles($game_code)) > 0) {
            switch ($game_code) {
                case GameCode::CARD:
                case GameCode::FIND_THE_MATCH:
                    $fields = 'SUM(g.timeout_fail_count) as total_fail_count, SUM(g.no_remain_fail_count) as total_no_remain_fail_count';
                    break;
                case GameCode::FACE:
                case GameCode::MODEL:
                    $fields = 'SUM(g.im_good_count) as total_im_good_count, SUM(g.your_good_count) as total_your_good_count';
                    break;
                default:
                    $fields = '';
            }

            if (!empty($fields)) {
                $sql = "SELECT {$fields} FROM {$table_name} g
                    JOIN (
                      SELECT idx FROM {$table_name}
                      WHERE u_idx = ?
                        AND is_disabled = 0
                        AND accuracy IS NOT NULL
                      ORDER BY idx ASC
                      {$limit_clause}
                    ) r ON g.idx = r.idx";

                $additional_info = $db->fetchArray($sql, [$user_idx]);
            }

            // 난이도별 정답률 - 상, 중, 하
            if (GameCode::ORDER == $game_code || GameCode::MODEL == $game_code || GameCode::FACE == $game_code) {
                $level_steps = [
                    [1, 5],
                    [6, 10],
                    [11, 15]
                ];

                foreach ($level_steps as $step) {
                    $sql = "SELECT avg(g.accuracy) FROM {$table_name} g
                            JOIN (
                              SELECT idx FROM {$table_name}
                              WHERE u_idx = ?
                                AND is_disabled = 0
                                AND accuracy IS NOT NULL
                                AND (current_level >= {$step[0]} AND current_level <= {$step[1]})
                                  ORDER BY idx ASC
                                  {$limit_clause}
                                ) r ON g.idx = r.idx";
                    $additional_info[] = $db->fetchColumn($sql, [$user_idx]);
                }
            }

            // 배경 얼굴 자극에 따른 정답률
            if (GameCode::LOOK == $game_code) {
                $count_by_face = [];
                $answer_count_by_face = [];

                foreach ($rows as $row) {
                    $origin_answer_eye_names = explode('|', $row['origin_answer_eye_name_list']);
                    $user_answer_eye_names = explode('|', $row['user_answer_eye_name_list']);

                    foreach ($origin_answer_eye_names as $key => $origin_answer_eye_name) {
                        if (isset($count_by_face[$origin_answer_eye_name])) {
                            $count_by_face[$origin_answer_eye_name]++;
                        } else {
                            $count_by_face[$origin_answer_eye_name] = 1;
                        }

                        // 정답 확인 전에 _0 인지 먼저 확인
                        $tempArr = explode('_', $user_answer_eye_names[$key]);
                        if ($tempArr[1] != '0.png') {
                            if (!isset($answer_count_by_face[$origin_answer_eye_name])) {
                                $answer_count_by_face[$origin_answer_eye_name] = 0;
                            }

                            continue;
                        }

                        // 정답 확인 전 양쪽 포멧팅
                        $origin_sex = substr($origin_answer_eye_name, 0, strpos($origin_answer_eye_name, 'm') + 1);

                        $origin_number = substr($origin_answer_eye_name, strpos($origin_answer_eye_name, 'm') + 1, strpos($origin_answer_eye_name, '.'));
                        $origin_number = intval($origin_number);

                        $user_sex = substr($user_answer_eye_names[$key], 0, strpos($user_answer_eye_names[$key], 'm') + 1);

                        $user_number = substr($user_answer_eye_names[$key], strpos($user_answer_eye_names[$key], 'm') + 1, strpos($user_answer_eye_names[$key], '_'));
                        $user_number = intval($user_number);

                        if ($origin_sex != $user_sex || $origin_number != $user_number) {
                            if (!isset($answer_count_by_face[$origin_answer_eye_name])) {
                                $answer_count_by_face[$origin_answer_eye_name] = 0;
                            }

                            continue;
                        }

                        if (isset($answer_count_by_face[$origin_answer_eye_name])) {
                            $answer_count_by_face[$origin_answer_eye_name]++;
                        } else {
                            $answer_count_by_face[$origin_answer_eye_name] = 1;
                        }
                    }
                }

                $accuracy_by_face = [];
                foreach ($count_by_face as $file => $value) {
                    $accuracy_by_face[] = $file . ' = ' . ($answer_count_by_face[$file] / $value);
                }

                $additional_info[] = implode('|', $accuracy_by_face);
            }

            // 얼굴 자극에 따른 정답률
            if (GameCode::THIEF == $game_code) {
                $count_by_face = [];
                $answer_count_by_face = [];

                foreach ($rows as $row) {
                    $origin_answer_faces = explode('|', $row['origin_answer_face_list']);
                    $user_answer_faces = explode('|', $row['user_answer_face_list']);

                    foreach ($origin_answer_faces as $key => $origin_answer_face) {
                        if (isset($count_by_face[$origin_answer_face])) {
                            $count_by_face[$origin_answer_face]++;
                        } else {
                            $count_by_face[$origin_answer_face] = 1;
                        }

                        // 정답 확인
                        if ($origin_answer_face != $user_answer_faces[$key]) {
                            if (!isset($answer_count_by_face[$origin_answer_face])) {
                                $answer_count_by_face[$origin_answer_face] = 0;
                            }

                            continue;
                        }

                        if (isset($answer_count_by_face[$origin_answer_face])) {
                            $answer_count_by_face[$origin_answer_face]++;
                        } else {
                            $answer_count_by_face[$origin_answer_face] = 1;
                        }
                    }
                }

                $accuracy_by_face = [];
                foreach ($count_by_face as $file => $value) {
                    $accuracy_by_face[] = $file . ' = ' . ($answer_count_by_face[$file] / $value);
                }

                $additional_info[] = implode('|', $accuracy_by_face);
            }
        }

        $sql = "SELECT count(idx) FROM {$table_name} WHERE u_idx = ? AND is_disabled = 0";
        $row_count = $db->fetchColumn($sql, [$user_idx]);

        $sql = "SELECT days, avg_value FROM yf_stat_execution_rate WHERE game_code = ?";
        $avg_execution_rates = $db->fetchAll($sql, [$game_code]);

        $sql = "SELECT accuracy, reg_date FROM {$table_name} WHERE u_idx = ? AND is_disabled = 0 ORDER BY idx ASC";
        $execution_rates = $db->fetchAll($sql, [$user_idx]);

        return compact(
            'user',
            'rows',
            'level_info',
            'play_info',
            'total_star_count',
            'additional_info',
            'row_count',
            'avg_execution_rates',
            'execution_rates'
        );
    }

    /**
     * 유저 게임 데이터 엑셀 추출
     * ===
     * @param \PHPExcel $excel
     * @param $user_idx
     * @param $game_code
     * @param int $active_sheet
     * @param $limit_days
     *
     * @return \PHPExcel
     */
    public static function exportUserGameToExcel(\PHPExcel $excel, $user_idx, $game_code, $active_sheet = 0, $limit_days = 66)
    {
        $table_name = GameCode::getGameTableName($game_code);

        $stats = static::buildUserGame($user_idx, $game_code, $table_name, 0, $limit_days, true);

        /** 엑셀로 추출 */

        $excel->setActiveSheetIndex($active_sheet)
            ->setTitle(GameCode::getGameName($game_code));

        $row_offset = 1;
        $game_titles = GameResult::getTitles($game_code, true, $stats);
        $acronym = GameCode::ACRONYM[GameCode::ENG_NAMES[$game_code - 1]];

        foreach ($game_titles as &$title) {
            $title = "{$acronym}{$title}";
        }

        $column_index = $excel->getActiveSheet()->getHighestColumn(1);
        $column_index = \PHPExcel_Cell::columnIndexFromString($column_index);
        $column_index = \PHPExcel_Cell::stringFromColumnIndex($column_index);

        $excel->getActiveSheet()
            ->fromArray($game_titles, null, $column_index . $row_offset++);

        foreach ($stats['rows'] as $row) {
            $temp_arr = [];
            foreach ($game_titles as $key => $value) {
                // 반응 시간 N개
                if (strpos($key, 'response_time_') !== false) {
                    $temp_arr[] = static::extractResponseTime($row, $key);

                    continue;
                }

                $temp_arr[] = $row[$key];
            }

            $excel->getActiveSheet()
                ->fromArray($temp_arr, null, $column_index . $row_offset++);
        }

        // 평균 관련
        $temp_arr = [];
        $temp_column_index = $column_index;
        foreach ($game_titles as $key => $value) {
            if (strpos($key, 'response_time_') !== false ||
                $key == 'accuracy' ||
                strpos($key, 'avg_response_time') !== false ||
                strpos($key, 'active_accuracy') !== false ||
                strpos($key, 'accuracy_except_no_response') !== false
            ) {
                $highest_row = $excel->getActiveSheet()->getHighestRow($column_index);

                $temp_arr[] = "=AVERAGE({$temp_column_index}2:{$temp_column_index}{$highest_row})";

                $excel->getActiveSheet()->getStyle("{$temp_column_index}{$row_offset}")
                    ->getFill()
                    ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setRGB('FFFF00');
            } else {
                $temp_arr[] = NULL;
            }

            $temp_column_index = \PHPExcel_Cell::columnIndexFromString($temp_column_index);
            $temp_column_index = \PHPExcel_Cell::stringFromColumnIndex($temp_column_index);
        }

        $excel->getActiveSheet()->fromArray($temp_arr, null, $column_index . $row_offset++);

        // 사용자 게임 요약
        $row_offset += 2;
        $excel->getActiveSheet()
            ->fromArray([
                '현재 레벨',
                '현재 레벨 평균 정확도',
                '누적 별 개수',
                '총 시행 횟수'
            ], null, $column_index . $row_offset++);

        $excel->getActiveSheet()
            ->fromArray([
                $stats['level_info']['current_level'],
                $stats['level_info']['avg_accuracy'],
                $stats['total_star_count'],
                $stats['play_info']['total_play_count']
            ], null, $column_index . $row_offset++);

        $additional_titles = GameResult::getAdditionalTitles($game_code);
        if (count($additional_titles) > 0) {
            $excel->getActiveSheet()
                ->fromArray($additional_titles, null, $column_index . ++$row_offset)
                ->fromArray($stats['additional_info'], null, $column_index . ++$row_offset);
        }

        return $excel;
    }

    /**
     * @param $row
     * @param $key
     *
     * @return mixed
     */
    private static function extractResponseTime($row, $key)
    {
        $results = explode(',', $row['response_time']);

        $pos = strpos($key, 'response_time_');
        $pos += strlen('response_time_');

        $index = intval(substr($key, $pos, 1));

        $result = $results[$index - 1];

        return $result;
    }
}
