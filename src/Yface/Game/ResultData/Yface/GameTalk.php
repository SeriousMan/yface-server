<?php
namespace Yface\Game\ResultData\Yface;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [10] - TALK_대화가 필요해
 */
class GameTalk extends ResultData
{
    protected $origin_answer_string_list;
    protected $user_answer_string_list;
    protected $im_good_count;
    protected $your_good_count;

    public function __construct($results, $uploaded_files)
    {
        parent::__construct(GameCode::TALK, $results, $uploaded_files);

        $this->origin_answer_string_list = implode('|', $results['originAnswerStringList']);
        $this->user_answer_string_list = implode('|', $results['userAnswerStringList']);
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'origin_answer_string_list' => $this->origin_answer_string_list,
            'user_answer_string_list' => $this->user_answer_string_list,
            'is_recorded' => 'O'
        ]);
    }
}
