<?php
namespace Yface\Command\Statistics;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\DateHelper;

class TotalStarStatCommand extends BaseCommand
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $conn;

    public function __construct()
    {
        parent::__construct();

        $this->conn = ConnectionProvider::getConnection();
    }

    protected function configure()
    {
        $this->setName('stat:star:total')
            ->setDescription('일별 별 누적 통계 산출용');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('STATISTICS STAR - 일별 별 누적 통계 산출 CRON 시작');

        $this->conn->transactional(
            function (Connection $conn) {
                /** 통계 테이블 초기화 */
                $conn->executeQuery('TRUNCATE yf_stat_stars');

                self::summarizeByAppType(APP_TYPE_YFACE);
                self::summarizeByAppType(APP_TYPE_YCOG);
            }
        );

        static::report('STATISTICS STAR - 일별 별 누적 통계 산출 CRON 종료');
    }

    private function summarizeByAppType($app_type)
    {
        /** 몇일 까지 통계를 구해야 하는지 알기 위해 설문조사 완료일이 현재 기준 가장 오래된 유저의 설문 완료 날짜를 가져온다. */
        $sql = 'SELECT ' . strtolower($app_type) . '_survey_completed_date as s_date
                FROM yf_user
                WHERE ' . strtolower($app_type) . '_survey_completed_date IS NOT NULL
                ORDER BY ' . strtolower($app_type) . '_survey_completed_date ASC
                LIMIT 1';

        $date = $this->conn->fetchColumn($sql);
        $datetime = new \DateTime($date);
        $days = DateHelper::getDaysFromNow($datetime);

        $now = DateHelper::getStartTimeOfToday();

        $this->conn->transactional(
            function (Connection $conn) use ($days, $now, $app_type) {
                /** 몇일이 지났는지 구한 뒤 구간만큼 통계를 산출하여 저장 */
                for ($i = 1; $i <= $days; $i++) {
                    $sql = 'SELECT
                              (COUNT(st.idx) / COUNT(u.idx)) as avg_value
                            FROM yf_user u
                            LEFT OUTER JOIN yf_user_star st
                              ON st.u_idx = u.idx AND st.days <= ? AND st.app_type = ?
                            WHERE u.' . strtolower($app_type) . '_survey_completed_date IS NOT NULL
                              AND u.' . strtolower($app_type) . '_survey_completed_date <= ?';
                    $value = $conn->fetchColumn($sql, array($i, $app_type, $now->format('Y-m-d H:i:s')));

                    $conn->insert(
                        'yf_stat_stars',
                        array(
                            'days' => $i,
                            'avg_value' => (float)$value,
                            'app_type' => $app_type
                        )
                    );

                    $now->modify('-1 days');
                }
            }
        );
    }
}
