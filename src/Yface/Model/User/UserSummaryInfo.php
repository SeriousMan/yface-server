<?php
namespace Yface\Model\User;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="yf_user_summary_info")
 */
class UserSummaryInfo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     */
    private $u_idx;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('IOS', 'ANDROID')")
     */
    private $device_os;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $device_model;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $training_start_date;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $training_end_date;

    /**
     * @return integer
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * @return integer
     */
    public function getUIdx()
    {
        return $this->u_idx;
    }

    /**
     * @param integer $u_idx
     */
    public function setUIdx($u_idx)
    {
        $this->u_idx = $u_idx;
    }

    /**
     * @return string
     */
    public function getDeviceOs()
    {
        return $this->device_os;
    }

    /**
     * @param string $device_os
     */
    public function setDeviceOs($device_os)
    {
        $this->device_os = $device_os;
    }

    /**
     * @return string
     */
    public function getDeviceModel()
    {
        return $this->device_model;
    }

    /**
     * @param string $device_model
     */
    public function setDeviceModel($device_model)
    {
        $this->device_model = $device_model;
    }

    /**
     * @return \DateTime
     */
    public function getTrainingStartDate()
    {
        return $this->training_start_date;
    }

    /**
     * @param \DateTime $training_start_date
     */
    public function setTrainingStartDate($training_start_date)
    {
        $this->training_start_date = $training_start_date;
    }

    /**
     * @return \DateTime
     */
    public function getTrainingEndDate()
    {
        return $this->training_end_date;
    }

    /**
     * @param \DateTime $training_end_date
     */
    public function setTrainingEndDate($training_end_date)
    {
        $this->training_end_date = $training_end_date;
    }
}
