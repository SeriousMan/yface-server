<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [24] - WHATS_THE_NUMBER_숫자놀이123
 */
class GameWhatsTheNumber extends ResultData
{
    protected $answer_number_list;
    protected $user_number_list;
    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::WHATS_THE_NUMBER, $results);

        /** 게임 정보 저장 */
        $this->answer_number_list = implode('|', $results['answerNumberList']);
        $this->user_number_list = implode('|', $results['userNumberList']);
        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'answer_number_list' => $this->answer_number_list,
            'user_number_list' => $this->user_number_list,
            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
