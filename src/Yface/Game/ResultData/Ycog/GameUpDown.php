<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [23] - UP_DOWN_올리고 내리고
 */
class GameUpDown extends ResultData
{
    protected $up_count;
    protected $up_answer_count;
    protected $up_wrong_count;
    protected $up_accuracy;
    protected $up_no_response_count;
    protected $up_average_response_time;

    protected $down_count;
    protected $down_answer_count;
    protected $down_wrong_count;
    protected $down_accuracy;
    protected $down_no_response_count;
    protected $down_average_response_time;

    protected $fake_count;
    protected $fake_answer_count;
    protected $fake_accuracy;
    protected $fake_wrong_count;

    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::UP_DOWN, $results);

        /** 게임 정보 저장 */
        $this->up_count = $results['upCount'];
        $this->up_answer_count = $results['upAnswerCount'];
        $this->up_wrong_count = $results['upWrongCount'];
        $this->up_accuracy = $results['upAccuracy'];
        $this->up_no_response_count = $results['upNoResponseCount'];
        $this->up_average_response_time = $results['upAverageResponseTime'];

        $this->down_count = $results['downCount'];
        $this->down_answer_count = $results['downAnswerCount'];
        $this->down_wrong_count = $results['downWrongCount'];
        $this->down_accuracy = $results['downAccuracy'];
        $this->down_no_response_count = $results['downNoResponseCount'];
        $this->down_average_response_time = $results['downAverageResponseTime'];

        $this->fake_count = $results['fakeCount'];
        $this->fake_answer_count = $results['fakeAnswerCount'];
        $this->fake_accuracy = $results['fakeAccuracy'];
        $this->fake_wrong_count = $results['fakeWrongCount'];

        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'up_count' => $this->up_count,
            'up_answer_count' => $this->up_answer_count,
            'up_wrong_count' => $this->up_wrong_count,
            'up_accuracy' => $this->up_accuracy,
            'up_no_response_count' => $this->up_no_response_count,
            'up_average_response_time' => $this->up_average_response_time,

            'down_count' => $this->down_count,
            'down_answer_count' => $this->down_answer_count,
            'down_wrong_count' => $this->down_wrong_count,
            'down_accuracy' => $this->down_accuracy,
            'down_no_response_count' => $this->down_no_response_count,
            'down_average_response_time' => $this->down_average_response_time,

            'fake_count' => $this->fake_count,
            'fake_answer_count' => $this->fake_answer_count,
            'fake_accuracy' => $this->fake_accuracy,
            'fake_wrong_count' => $this->fake_wrong_count,

            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
