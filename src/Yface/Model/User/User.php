<?php
namespace Yface\Model\User;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Yface\Library\DateHelper;
use Yface\Model\Survey\UserSurveyTodo;
use Yface\Model\Theme\UserTheme;
use Yface\Service\Attendance\AttendanceService;
use Yface\Service\Game\TodayGameService;
use Yface\Service\Point\PointService;
use Yface\Service\Star\StarService;
use Yface\Service\Survey\SurveyService;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\User\UserRepository")
 * @ORM\Table(name="yf_user")
 * @JMS\ExclusionPolicy("none")
 */
class User
{
    const B_CRYPT_COST = 10;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idx;
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $id;
    /**
     * @ORM\Column(type="string", name="password", length=255)
     * @JMS\Exclude
     */
    private $encrypt_password;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=11)
     */
    private $phone;
    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('M', 'F')")
     */
    private $gender;
    /**
     * @ORM\Column(type="string", length=8)
     * @JMS\Exclude
     */
    private $birth_date;
    /**
     * @ORM\Column(type="string", length=15)
     */
    private $nickname;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Exclude
     */
    private $profile_img_url;
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $diagnosis;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @JMS\SerializedName("guardianRelation")
     */
    private $guardian_relation;
    /**
     * @ORM\Column(type="string", length=4)
     * @JMS\SerializedName("guardianPassword")
     */
    private $guardian_password;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\Exclude
     */
    private $is_yface_theme_selected;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\Exclude
     */
    private $is_ycog_theme_selected;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\Exclude
     */
    private $is_yface_praise_started;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\Exclude
     */
    private $is_ycog_praise_started;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Exclude
     */
    private $yface_praise_start_date;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Exclude
     */
    private $ycog_praise_start_date;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Exclude
     *
     * @var $yface_survey_completed_date \DateTime
     */
    private $yface_survey_completed_date;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @JMS\Exclude
     *
     * @var $ycog_survey_completed_date \DateTime
     */
    private $ycog_survey_completed_date;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\SerializedName("isSurveyTarget")
     *
     * @var $is_survey_target bool
     */
    private $is_survey_target;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     */
    private $latest_login_date;
    /**
     * @ORM\Column(type="boolean")
     * @JMS\Exclude
     */
    private $is_disabled;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     */
    private $reg_date;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     */
    private $mod_date;

    /**
     * @JMS\Exclude
     * @var string
     */
    private $app_type;

    /**
     * @ORM\OneToMany(targetEntity="Yface\Model\Theme\UserTheme", mappedBy="user")
     * @JMS\Type("ArrayCollection<Yface\Model\Theme\UserTheme>")
     * @JMS\Exclude
     *
     * @var ArrayCollection
     */
    private $user_themes;

    public function __construct()
    {
        $this->user_themes = new ArrayCollection();
        $this->app_type = 'YFACE';
    }

    /**
     * @return integer
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEncryptPassword()
    {
        return $this->encrypt_password;
    }

    /**
     * @param string $password
     */
    public function setPlainTextPassword($password)
    {
        $this->encrypt_password = (new BCryptPasswordEncoder(static::B_CRYPT_COST))->encodePassword($password, false);
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * @param string $birth_date
     */
    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("profileImgUrl")
     * @JMS\Type("string")
     *
     * @return string
     */
    public function getProfileImgUrl()
    {
        return empty($this->profile_img_url) ? null : 'http://' . $this->profile_img_url;
    }

    /**
     * @param string $profile_img_url
     */
    public function setProfileImgUrl($profile_img_url)
    {
        $this->profile_img_url = $profile_img_url;
    }

    /**
     * @return string
     */
    public function getDiagnosis()
    {
        return $this->diagnosis;
    }

    /**
     * @param string $diagnosis
     */
    public function setDiagnosis($diagnosis)
    {
        $this->diagnosis = $diagnosis;
    }

    /**
     * @return string
     */
    public function getGuardianRelation()
    {
        return $this->guardian_relation;
    }

    /**
     * @param string $guardian_relation
     */
    public function setGuardianRelation($guardian_relation)
    {
        $this->guardian_relation = $guardian_relation;
    }

    /**
     * @return string
     */
    public function getGuardianPassword()
    {
        return $this->guardian_password;
    }

    /**
     * @param string $guardian_password
     */
    public function setGuardianPassword($guardian_password)
    {
        $this->guardian_password = $guardian_password;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isThemeSelected")
     * @JMS\Type("boolean")
     *
     * @return boolean
     */
    public function getIsThemeSelected()
    {
        if ($this->app_type == 'YFACE') {
            return $this->is_yface_theme_selected;
        }

        return $this->is_ycog_theme_selected;
    }

    /**
     * @param $is_theme_selected
     * @param $app_type
     */
    public function setIsThemeSelected($is_theme_selected, $app_type)
    {
        if (strtoupper($app_type) == 'YFACE') {
            $this->is_yface_theme_selected = $is_theme_selected;
        } else {
            $this->is_ycog_theme_selected = $is_theme_selected;
        }
    }

    /**
     * @return boolean
     */
    public function getIsPraiseStarted()
    {
        if ($this->app_type === 'YFACE') {
            return $this->is_yface_praise_started;
        }

        return $this->is_ycog_praise_started;
    }

    /**
     * @param boolean $is_praise_started
     * @param $app_type
     */
    public function setIsPraiseStarted($is_praise_started, $app_type)
    {
        if (strtoupper($app_type) === 'YFACE') {
            $this->is_yface_praise_started = $is_praise_started;
        } else {
            $this->is_ycog_praise_started = $is_praise_started;
        }
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("praiseStartDate")
     * @JMS\Type("DateTime<'Y-m-d'>")
     *
     * @return \DateTime
     */
    public function getPraiseStartDate()
    {
        if ($this->app_type == 'YFACE') {
            return $this->yface_praise_start_date;
        }

        return $this->ycog_praise_start_date;
    }

    /**
     * @param \DateTime $praise_start_date
     * @param $app_type
     */
    public function setPraiseStartDate($praise_start_date, $app_type)
    {
        if (strtoupper($app_type) == 'YFACE') {
            $this->yface_praise_start_date = $praise_start_date;
        } else {
            $this->ycog_praise_start_date = $praise_start_date;
        }
    }

    /**
     * @return \DateTime
     */
    public function getLatestLoginDate()
    {
        return $this->latest_login_date;
    }

    /**
     * @param \DateTime $latest_login_date
     */
    public function setLatestLoginDate($latest_login_date)
    {
        $this->latest_login_date = $latest_login_date;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("surveyCompletedDate")
     * @JMS\Type("DateTime<'Y-m-d'>")
     *
     * @return \DateTime
     */
    public function getSurveyCompletedDate()
    {
        if ($this->app_type == 'YFACE') {
            return $this->yface_survey_completed_date;
        }

        return $this->ycog_survey_completed_date;
    }

    /**
     * @param \DateTime $survey_completed_date
     * @param $app_type
     */
    public function setSurveyCompletedDate($survey_completed_date, $app_type)
    {
        if (strtoupper($app_type) == 'YFACE') {
            $this->yface_survey_completed_date = $survey_completed_date;
        } else {
            $this->ycog_survey_completed_date = $survey_completed_date;
        }
    }

    /**
     * @return boolean
     */
    public function getIsDisabled()
    {
        return $this->is_disabled;
    }

    /**
     * @param boolean $is_disabled
     */
    public function setIsDisabled($is_disabled)
    {
        $this->is_disabled = $is_disabled;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return \DateTime
     */
    public function getModDate()
    {
        return $this->mod_date;
    }

    /**
     * @param \DateTime $mod_date
     */
    public function setModDate($mod_date)
    {
        $this->mod_date = $mod_date;
    }

    /**
     * @param string $app_type
     */
    public function setUserLoginAppType(string $app_type)
    {
        $this->app_type = strtoupper($app_type);
    }

    /**
     * @return boolean
     */
    public function isIsSurveyTarget(): bool
    {
        return $this->is_survey_target;
    }

    /**
     * @param boolean $is_survey_target
     */
    public function setIsSurveyTarget(bool $is_survey_target)
    {
        $this->is_survey_target = $is_survey_target;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isPreSurveyCompleted")
     * @JMS\Type("boolean")
     *
     * @return bool
     */
    public function getPreSurveyCompleted()
    {
        return !empty($this->getSurveyCompletedDate());
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("isPostSurveyCompleted")
     * @JMS\Type("boolean")
     *
     * @return bool
     */
    public function getPostSurveyCompleted()
    {
        return SurveyService::checkIfSurveyAllCompleted($this->idx, UserSurveyTodo::TIME_END, $this->app_type);
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("userTodayGames")
     * @JMS\Type("array")
     *
     * @return array
     */
    public function getUserTodayGames()
    {
        return TodayGameService::getUserTodayGames($this->idx, $this->app_type);
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("userPoints")
     * @JMS\Type("integer")
     *
     * @return int
     */
    public function getUserPoints()
    {
        return PointService::getUserTotalPoint($this->idx, $this->app_type);
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("userStars")
     * @JMS\Type("integer")
     *
     * @return int
     */
    public function getUserStars()
    {
        return StarService::getUserTotalStars($this->idx, $this->app_type);
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("userAttendances")
     * @JMS\Type("integer")
     *
     * @return int
     */
    public function getUserAttendances()
    {
        return AttendanceService::getUserTotalAttendance($this->idx, $this->app_type);
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("userTodayTheme")
     * @JMS\Type("integer")
     */
    public function getUserTodayTheme() {
        if (!$this->hasSelectTheme()) {
            return null;
        }

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->eq('app_type', $this->app_type))
            ->andWhere(Criteria::expr()->gte('reg_date', DateHelper::getStartTimeOfToday()))
            ->andWhere(Criteria::expr()->lte('reg_date', DateHelper::getEndTimeOfToday()));

        $user_themes = $this->user_themes->matching($criteria);
        if ($user_themes->count() === 0) {
            return null;
        }

        /** @var $user_theme UserTheme */
        $user_theme = $user_themes->first();
        $theme = $user_theme->getTheme();

        return $theme->getCode();
    }

    /**
     * 오늘 해당 앱에서 테마를 선택한 적이 있는지 여부
     * ===
     * @return bool
     */
    public function hasSelectTheme() {
        return $this->getIsThemeSelected();
    }

    /**
     * 만 나이 리턴
     * ===
     * @return int
     */
    public function getAge()
    {
        $birth_year = date("Y", strtotime($this->birth_date));
        $birth_month = date("m", strtotime($this->birth_date));
        $birth_day = date("d", strtotime($this->birth_date));

        $now_year = date("Y");
        $now_month = date("m");
        $now_day = date("d");

        if ($birth_month < $now_month) {
            $age = $now_year - $birth_year;
        } else if ($birth_month == $now_month) {
            if ($birth_day <= $now_day) {
                $age = $now_year - $birth_year;
            } else {
                $age = $now_year - $birth_year - 1;
            }
        }
        else {
            $age = $now_year - $birth_year - 1;
        }

        return $age;
    }

    /**
     * @param $app_type
     * @return int
     */
    public function getDaysFromSurveyCompleted($app_type = null)
    {
        if (!empty($app_type)) {
            $this->setUserLoginAppType($app_type);
        }

        $date = $this->getSurveyCompletedDate();
        if (empty($date)) {
            return 0;
        }

        $now = time();
        $date_diff = $now - $date->getTimestamp();
        $days = (int)(floor($date_diff / (60 * 60 * 24)));

        return $days + 1;
    }

    /**
     * @param $app_type
     * @return int
     */
    public function getDaysFromPraiseStarted($app_type = null)
    {
        if (!empty($app_type)) {
            $this->setUserLoginAppType($app_type);
        }

        $date = $this->getPraiseStartDate();
        if (empty($date)) {
            return 0;
        }

        $now = time();
        $date_diff = $now - $date->getTimestamp();
        $days = (int)(floor($date_diff / (60 * 60 * 24)));

        return $days + 1;
    }

    /**
     * @param null $app_type
     * @return int
     */
    public function getWeeksFromPraiseStarted($app_type = null)
    {
        if (!empty($app_type)) {
            $this->setUserLoginAppType($app_type);
        }

        $date = $this->getPraiseStartDate();
        if (empty($date)) {
            return 0;
        }

        $now = time();
        $date_diff = $now - $date->getTimestamp();
        $weeks = (int)floor($date_diff / (60 * 60 * 24 * 7));

        return $weeks + 1;
    }
}
