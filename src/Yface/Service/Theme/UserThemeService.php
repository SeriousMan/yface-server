<?php
namespace Yface\Service\Theme;

use Doctrine\ORM\EntityManager;
use Yface\DataStore\Theme\ThemeRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\UserException;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Theme\UserTheme;

class UserThemeService {
    /**
     * 오늘의 테마 선택
     * ===
     * @param $user_idx
     * @param $theme_code
     * @param $app_type
     * @throws UserException
     */
    public static function selectTodayTheme($user_idx, $theme_code, $app_type) {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            throw new UserException('user.please.enter.correctly');
        }

        $user->setUserLoginAppType($app_type);
        if ($user->hasSelectTheme()) {
            throw new UserException('user.theme.already.choose');
        }

        $theme = static::getThemeByCode($theme_code);

        $user_theme = new UserTheme();
        $user_theme->setUser($user);
        $user_theme->setTheme($theme);
        $user_theme->setDays($user->getDaysFromSurveyCompleted($app_type));
        $user_theme->setAppType($app_type);
        $user_theme->setRegDate(new \DateTime('now'));

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user, $user_theme, $app_type) {
                $em->persist($user_theme);
                $em->flush($user_theme);

                $user->setIsThemeSelected(true, $app_type);

                $em->persist($user);
                $em->flush($user);
            }
        );
    }

    /**
     * 테마 식별 코드로 테마 엔티티 리턴
     * ===
     * @param $theme_code
     * @return null|object|\Yface\Model\Theme\Theme
     * @throws UserException
     */
    private static function getThemeByCode($theme_code) {
        if (empty($theme_code)) {
            throw new UserException('user.theme.not.exist');
        }

        $theme = ThemeRepository::getRepository()->findOneByThemeCode($theme_code);
        if (empty($theme)) {
            throw new UserException('user.theme.not.exist');
        }

        return $theme;
    }
}
