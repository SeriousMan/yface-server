<?php
namespace Yface\Model\Research;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Doctrine\Common\Collections\ArrayCollection;
use Yface\Model\Research\UserResearchStatus;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Research\ResearchRepository")
 * @ORM\Table(name="yf_research")
 * @JMS\ExclusionPolicy("none")
 */
class Research
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idx;
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $title;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     */
    private $reg_date;
    /**
     * @ORM\Column(type="datetime")
     * @JMS\Exclude
     */
    private $mod_date;

    /**
     * @ORM\OneToMany(targetEntity="Yface\Model\Research\UserResearchStatus", mappedBy="research", orphanRemoval=true, cascade={"persist", "remove"})
     * @JMS\Type("ArrayCollection<Yface\Model\Research\UserResearchStatus>")
     *
     * @var ArrayCollection
     */
    private $user_research_status;

    public function __construct()
    {
        $this->user_research_status = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $name
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate()
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate($reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return \DateTime
     */
    public function getModDate()
    {
        return $this->mod_date;
    }

    /**
     * @param \DateTime $mod_date
     */
    public function setModDate($mod_date)
    {
        $this->mod_date = $mod_date;
    }

    /**
     * @return \ArrayCollection
     */
    public function getUserResearchStatus()
    {
        return $this->user_research_status;
    }

    /**
     * @param \ArrayCollection $user_research_status
     */
    public function setUserResearchStatus($user_research_status)
    {
        $this->user_research_status = $user_research_status;
    }

    public function removeUserResearchStatus($u_idx)
    {
        $removedUser = $this->user_research_status->filter(function ($u) use ($u_idx) { return $u->getUidx() == $u_idx; })->current();
        $this->user_research_status->removeElement($removedUser);
    }
    public function addUserResearchStatus($u_idx)
    {
        $new = new UserResearchStatus();
        $new->setUidx($u_idx);
        $new->setRidx($this->idx);
        $new->setResearch($this);
        $this->user_research_status->add($new);
        return $new;
    }
}
