<?php
namespace Yface\Admin\Controller;

use Symfony\Component\HttpFoundation\Request;
use Yface\Admin\Auth\Authenticator;
use Yface\Admin\Exception\AdminException;
use Yface\Admin\Library\UrlHelper;
use Yface\Admin\User\UserSession;
use Yface\Library\Template\TwigRenderer;

class SignInController
{
    /**
     * 컨트롤러) 로그인 화면
     * ===
     * @return string
     */
    public function index()
    {
        $twig_renderer = TwigRenderer::getRenderer();

        return $twig_renderer->render('admin/login');
    }

    /**
     * 컨트롤러) 로그인 처리
     * ===
     * @param Request $request
     * @return string
     */
    public function login(Request $request)
    {
        $id = trim($request->get('id'));
        $password = trim($request->get('password'));

        try {
            if (empty($id) || empty($password)) {
                throw new AdminException('입력란에 정보를 모두 기입해주세요.');
            }

            if (!Authenticator::authenticate($id, $password)) {
                throw new AdminException('입력한 정보를 다시 확인해주세요.');
            }

            /** 실제 로그인 처리 */
            UserSession::login($id);

            if(UserSession::getLoginRole() == 'admin') {
                return UrlHelper::redirect('/admin/users');
            }
            else if(UserSession::getLoginRole() == 'researcher') {
                return UrlHelper::redirect('/admin/users?research=' . UserSession::getLoginRIdxes()[0]);
            }
        } catch (AdminException $ae) {
            return UrlHelper::returnWithMessage($ae->getMessage());
        }
    }

    /**
     * 컨트롤러) 로그아웃 처리
     * ===
     * @return string
     */
    public function logout()
    {
        UserSession::logout();

        return UrlHelper::redirect('/admin/sign-in');
    }
}
