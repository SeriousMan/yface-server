<?php
namespace Yface\DataStore\Praise;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Praise\Praise;

class PraiseRepository extends EntityRepository
{
    /**
     * @param $user_idx
     * @param $app_type
     * @return Praise[]
     */
    public function findByUserIdx($user_idx, $app_type)
    {
        return $this->findBy(
            array('user_idx' => $user_idx, 'app_type' => $app_type),
            array('weeks' => 'ASC')
        );
    }

    /**
     * @param $user_idx
     * @param $weeks
     * @param $app_type
     * @return null|Object|Praise
     */
    public function findOneByUserIdxAndWeeks($user_idx, $weeks, $app_type)
    {
        return $this->findOneBy(array('user_idx' => $user_idx, 'weeks' => $weeks, 'app_type' => $app_type));
    }

    /**
     * @return PraiseRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Praise\Praise');
    }
}
