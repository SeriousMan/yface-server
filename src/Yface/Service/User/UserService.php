<?php
namespace Yface\Service\User;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\RegistrationException;
use Yface\Exception\UserException;
use Yface\Exception\YFFileException;
use Yface\Library\Configuration;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Library\DateHelper;
use Yface\Library\File\FileHelper;
use Yface\Library\File\ImageUploader;
use Yface\Model\User\User;
use Yface\Model\User\UserSummaryInfo;
use Yface\Service\Praise\PraiseService;
use Yface\Service\Survey\SurveyService;

class UserService {
    /**
     * 유저 등록
     * ===
     * @param $id
     * @param $password
     * @param $email
     * @param $name
     * @param $nickname
     * @param $phone
     * @param $gender
     * @param $birth_date
     * @param $diagnosis
     * @param $guardian_password
     * @param $device_os
     * @param $device_model
     * @param $app_type
     * @throws RegistrationException
     *
     * @return User
     */
    public static function registerUser(
        $id,
        $password,
        $email,
        $name,
        $nickname,
        $phone,
        $gender,
        $birth_date,
        $diagnosis,
        $guardian_password,
        $device_os,
        $device_model,
        $app_type
    ) {
        RegistrationValidationService::validate(
            $id,
            $password,
            $email,
            $name,
            $nickname,
            $phone,
            $gender,
            $birth_date,
            $diagnosis,
            $guardian_password
        );

        $user = new User();
        $user->setId($id);
        $user->setPlainTextPassword($password);
        $user->setEmail($email);
        $user->setName($name);
        $user->setNickname($nickname);
        $user->setPhone($phone);
        $user->setGender($gender);
        $user->setBirthDate($birth_date);
        $user->setDiagnosis($diagnosis);
        $user->setGuardianPassword($guardian_password);
        $user->setIsThemeSelected(false, 'YFACE');
        $user->setIsThemeSelected(false, 'YCOG');
        $user->setIsPraiseStarted(false, 'YFACE');
        $user->setIsPraiseStarted(false, 'YCOG');
        $user->setIsSurveyTarget(false);
        $user->setIsDisabled(false);
        $user->setLatestLoginDate(new \DateTime('now'));
        $user->setRegDate(new \DateTime('now'));
        $user->setModDate(new \DateTime('now'));
        $user->setUserLoginAppType($app_type);

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user, $device_os, $device_model) {
                $em->persist($user);
                $em->flush($user);

                $user_summary = new UserSummaryInfo();
                $user_summary->setUIdx($user->getIdx());
                $user_summary->setDeviceOs($device_os);
                $user_summary->setDeviceModel($device_model);

                $em->persist($user_summary);
                $em->flush($user_summary);

                SurveyService::choiceUserSurveyTodo($user, 'YFACE');
                SurveyService::choiceUserSurveyTodo($user, 'YCOG');
            }
        );

        return $user;
    }

    /**
     * 유저 고유 번호로 유저 정보 획득
     * ===
     * @param $user_idx
     * @param $app_type
     * @return null|User
     */
    public static function getUserWithUserIdx($user_idx, $app_type = null) {
        if (empty($user_idx)) {
            return null;
        }

        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (!empty($user) && !empty($app_type)) {
            $user->setUserLoginAppType($app_type);
        }

        return $user;
    }

    /**
     * 유저 정보 수정
     * ===
     * @param $user_idx
     * @param $email
     * @param $password
     * @param $nickname
     * @param $guardian_relation
     * @param $guardian_password
     * @param $is_praise_started
     * @param $app_type
     * @return User
     *
     * @throws UserException
     * @throws RegistrationException
     */
    public static function modify(
        $user_idx,
        $email,
        $password,
        $nickname,
        $guardian_relation,
        $guardian_password,
        $is_praise_started,
        $app_type
    ) {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            throw new UserException('user.please.enter.correctly');
        }

        if (!empty($email)) {
            RegistrationValidationService::validateEmail($email);

            $user->setEmail($email);
        }

        if (!empty($password)) {
            RegistrationValidationService::validatePassword($password);
            $user->setPlainTextPassword($password);
        }

        if (!empty($nickname)) {
            RegistrationValidationService::validateNickname($nickname);
            $user->setNickname($nickname);
        }

        if (!empty($guardian_relation)) {
            $user->setGuardianRelation($guardian_relation);
        }

        if (!empty($guardian_password)) {
            RegistrationValidationService::validateGuardianPassword($guardian_password);
            $user->setGuardianPassword($guardian_password);
        }

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user, $is_praise_started, $app_type, $user_idx) {
                if (!empty($is_praise_started)) {
                    $user->setIsPraiseStarted(true, $app_type);
                    $user->setPraiseStartDate(DateHelper::getStartTimeOfToday(), $app_type);

                    PraiseService::saveUserPraise($user_idx, $user->getWeeksFromPraiseStarted($app_type), 0, '', array(), $app_type);
                }

                $em->persist($user);
                $em->flush($user);
            }
        );

        $user->setUserLoginAppType($app_type);

        return $user;
    }

    /**
     * 유저 프로필 이미지 등록
     * ===
     * @param $user_idx
     * @param UploadedFile $file
     * @param $app_type
     * @return User
     * @throws UserException
     */
    public static function registerUserProfileImage($user_idx, UploadedFile $file, $app_type) {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            throw new UserException('user.please.enter.correctly');
        }

        $em = EntityManagerProvider::getEntityManager();
        $em->beginTransaction();

        try {
            $file_path = Configuration::$values['path']['resources'] . '/profiles/images';
            $file_name = FileHelper::generateRandomFileName();

            $file_name = ImageUploader::upload($file, $file_path, $file_name);
            $profile_img_url = Configuration::$values['url']['resources'] . '/profiles/images/' . $file_name;

            $user->setProfileImgUrl($profile_img_url);
            $user->setUserLoginAppType($app_type);

            $em->persist($user);
            $em->flush($user);
            $em->commit();

            return $user;
        } catch (YFFileException $fe) {
            $em->rollback();
            $em->close();

            throw new UserException($fe->getMessage());
        }
    }

    /**
     * 유저 아이디 정보 찾기
     * ===
     * @param $email
     * @param $name
     * @param $birth_date
     * @throws UserException
     * @return string
     */
    public static function findUserId($email, $name, $birth_date) {
        $user = UserRepository::getRepository()->findOneByEmailAndNameAndBirthDate(
            $email,
            $name,
            $birth_date
        );
        if (empty($user)) {
            throw new UserException('user.not.exist.searching.for');
        }

        return $user->getId();
    }

    /**
     * 유저 비밀번호 재설정 (4자)
     * ===
     * @param $id
     * @param $email
     * @param $name
     * @param $birth_date
     * @return string
     * @throws UserException
     */
    public static function findUserPassword($id, $email, $name, $birth_date) {
        $user = UserRepository::getRepository()->findOneByEmailAndNameAndBirthDate(
            $email,
            $name,
            $birth_date
        );
        if (empty($user)) {
            throw new UserException('user.not.exist.searching.for');
        }

        if ($id !== $user->getId()) {
            throw new UserException('user.not.exist.searching.for');
        }

        $password = static::generateRandomString(4);
        $user->setPlainTextPassword($password);

        $em = EntityManagerProvider::getEntityManager();
        $em->persist($user);
        $em->flush($user);

        return $password;
    }

    /**
     * @param int $length
     * @return string
     */
    private static function generateRandomString($length = 4) {
        return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)))), 1, $length);
    }

    /**
     * 모든 유저 리턴
     * ===
     * @return User[]
     */
    public static function getAllUser()
    {
        return UserRepository::getRepository()->findAll();
    }

    /**
     * 해당 앱의 설문 조사를 완료한 모든 유저 리턴
     * ===
     * @param $app_type
     * @return \Yface\Model\User\User[]
     */
    public static function getAllSurveyCompletedUser($app_type)
    {
        return UserRepository::getRepository()->findSurveyCompletedUserByAppType($app_type);
    }

    /**
     * 해당 앱의 칭찬 스티커를 시작한 모든 유저 리턴
     * ===
     * @param $app_type
     * @return \Yface\Model\User\User[]
     */
    public static function getAllPraiseStartedUser($app_type)
    {
        return UserRepository::getRepository()->findPraiseStartedUserByAppType($app_type);
    }
}
