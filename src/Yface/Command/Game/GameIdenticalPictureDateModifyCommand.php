<?php
namespace Yface\Command\Game;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Game\IdenticalPicture;
use Yface\Library\Database\ConnectionProvider;
use Yface\Service\Game\TodayGameService;

class GameIdenticalPictureDateModifyCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('game:ip:modify');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('ONCE - 무무똑 데이터 보정 시작');

        self::modify();

        static::report('ONCE - 무무똑 데이터 보정 종료');
    }

    protected function modify()
    {
        $db = ConnectionProvider::getConnection();
        // 전체 대상 다시 돌려야 함 (강제 업데이트 이후에)
        $results = $db->fetchAll('SELECT * FROM yf_game_identical_picture WHERE accuracy IS NOT NULL ORDER BY idx ASC');

        foreach ($results as $result) {
            var_dump($result['idx']);

            $resultStr = IdenticalPicture::convertAnswerSetListTo3d($result['current_level'], $result['answer_set_list']);

            $db->update(
                'yf_game_identical_picture',
                [
                    'answer_set_list_3d' => $resultStr
                ],
                [
                    'idx' => $result['idx']
                ]
            );
        }
    }
}
