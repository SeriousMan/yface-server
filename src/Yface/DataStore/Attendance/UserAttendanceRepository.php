<?php
namespace Yface\DataStore\Attendance;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Attendance\UserAttendance;

class UserAttendanceRepository extends EntityRepository
{
    /**
     * @param $user_idx
     * @param $app_type
     * @return int
     */
    public function getUserTotalAttendance($user_idx, $app_type)
    {
        $dql = 'SELECT count(ua)
                FROM Yface\Model\Attendance\UserAttendance ua
                WHERE ua.user_idx = :user_idx
                  AND ua.app_type = :app_type';

        $query = $this->_em->createQuery($dql);
        $query->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->setParameter('app_type', $app_type, Type::STRING);

        return (int)$query->getSingleScalarResult();
    }

    /**
     * @param $user_idx
     * @param $from
     * @param $to
     * @param $app_type
     * @return UserAttendance[]
     */
    public function findTodayByUserIdx($user_idx, $from, $to, $app_type)
    {
        $dql = 'SELECT ua
                FROM Yface\Model\Attendance\UserAttendance ua
                WHERE ua.user_idx = :user_idx
                  AND ua.app_type = :app_type
                  AND ua.reg_date <= :from
                  AND ua.reg_date >= :to';

        $query = $this->_em->createQuery($dql);
        $query->setParameter('user_idx', $user_idx, Type::INTEGER)
            ->setParameter('app_type', $app_type, Type::STRING)
            ->setParameter('from', $from, Type::DATETIME)
            ->setParameter('to', $to, Type::DATETIME);

        return $query->getResult();
    }

    /**
     * @return UserAttendanceRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Attendance\UserAttendance');
    }
}
