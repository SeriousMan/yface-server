<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [14] - FIND_THE_MATCH_짝꿍을 찾아라
 */
class GameFindTheMatch extends ResultData
{
    protected $total_used_fail_count;
    protected $timeout_fail_count;
    protected $accuracy_except_no_response;
    protected $no_remain_fail_count;
    protected $used_fail_count_list;

    protected $pair_count;
    protected $available_fail_count;
    protected $game_time;

    public function __construct($results)
    {
        parent::__construct(GameCode::FIND_THE_MATCH, $results);

        /** 난이도 정보 */
        $this->pair_count = $results['levelInfo']['pairCount'];
        $this->available_fail_count = $results['levelInfo']['availableFailCount'];
        $this->game_time = $results['levelInfo']['gameTime'];

        /** 게임 정보 저장 */
        $this->total_used_fail_count = $results['totalUsedFailCount'];
        $this->timeout_fail_count = $results['timeoutFailCount'];
        $this->no_remain_fail_count = $results['noRemainFailCount'];
        $this->used_fail_count_list = implode('|', $results['usedFailCountList']);
        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'pair_count' => $this->pair_count,
            'available_fail_count' => $this->available_fail_count,
            'game_time' => $this->game_time,

            'total_used_fail_count' => $this->total_used_fail_count,
            'timeout_fail_count' => $this->timeout_fail_count,
            'no_remain_fail_count' => $this->no_remain_fail_count,
            'used_fail_count_list' => $this->used_fail_count_list,
            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
