<?php
namespace Yface\Service\Praise;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Yface\DataStore\Game\UserGamePlayRepository;
use Yface\DataStore\Praise\PraiseRepository;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\PraiseException;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Praise\Praise;
use Yface\Model\Praise\Promise;

class PraiseService
{
    /**
     * 유저의 칭찬 스티커 정보 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return \Yface\Model\Praise\Praise[]
     */
    public static function getUserPraises($user_idx, $app_type)
    {
        return PraiseRepository::getRepository()->findByUserIdx($user_idx, $app_type);
    }

    /**
     * 통계용 유저의 주차별 칭찬 스티커 점수 정보 리턴
     * ===
     * @param $user_idx
     * @param $app_type
     * @return array
     */
    public static function getPointByWeeks($user_idx, $app_type)
    {
        $praises = array();

        $user_praises = PraiseRepository::getRepository()->findByUserIdx($user_idx, $app_type);
        /** @var $user_praise Praise */
        foreach ($user_praises as $key => $user_praise) {
            $sum = 0;

            $game_scores = static::getGameScores($user_idx, (($key * 7) + 1), $app_type);
            $sum += array_sum($game_scores);

            /**
             * @var $promise Promise
             */
            foreach ($user_praise->getPromises() as $promise) {
                $sum += $promise->getDay1() + $promise->getDay2() + $promise->getDay3() + $promise->getDay4() + $promise->getDay5() + $promise->getDay6() + $promise->getDay7();
            }

            $praises[] = array(
                'week' => $key + 1,
                'points' => $sum
            );
        }

        return $praises;
    }

    /**
     * 칭찬스티커 정보 저장 또는 생성
     * ===
     * @param $user_idx
     * @param $weeks
     * @param $goal_score
     * @param $present
     * @param $promises
     * @param $app_type
     * @throws PraiseException
     */
    public static function saveUserPraise(
        $user_idx,
        $weeks,
        $goal_score,
        $present,
        $promises,
        $app_type
    )
    {
        $user = UserRepository::getRepository()->findOneByUserIdx($user_idx);
        if (empty($user)) {
            throw new PraiseException('behavior.user.incorrect');
        }

        if (empty($weeks)) {
            $weeks = $user->getWeeksFromPraiseStarted($app_type);

            if (empty($weeks)) {
                throw new PraiseException('behavior.not.activated');
            }
        }

        $user_praise = PraiseRepository::getRepository()->findOneByUserIdxAndWeeks($user_idx, $weeks, $app_type);
        if (empty($user_praise)) {
            $user_praise = new Praise();
            $user_praise->setUser($user);
            $user_praise->setWeeks($weeks);
            $user_praise->setAppType($app_type);
            $user_praise->setRegDate(new \DateTime('now'));
            $user_praise->setModDate(new \DateTime('now'));
        }

        $user_praise->setGoalScore($goal_score);
        $user_praise->setPresent($present);

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($user, $user_praise, $promises) {
                $user_promises = $user_praise->getPromises();
                foreach ($user_promises as $use_promise) {
                    $em->remove($use_promise);
                }

                $user_promises = new ArrayCollection();
                foreach ($promises as $promise) {
                    $user_promise = new Promise();
                    $user_promise->setUserIdx($user->getIdx());
                    $user_promise->setPraise($user_praise);
                    $user_promise->setTitle($promise['title']);
                    $user_promise->setRegDate(new \DateTime('now'));

                    $user_promises->add($user_promise);
                }

                /** @var $promise Promise */
                foreach ($user_promises as $key => $promise) {
                    $promise->setDay1($promises[$key]['scores'][0]);
                    $promise->setDay2($promises[$key]['scores'][1]);
                    $promise->setDay3($promises[$key]['scores'][2]);
                    $promise->setDay4($promises[$key]['scores'][3]);
                    $promise->setDay5($promises[$key]['scores'][4]);
                    $promise->setDay6($promises[$key]['scores'][5]);
                    $promise->setDay7($promises[$key]['scores'][6]);
                }

                $user_praise->setPromises($user_promises);

                $em->persist($user_praise);
                $em->flush($user_praise);
            }
        );
    }

    /**
     * 정책: 칭찬 스티커의 게임 점수 = 하루에 게임을 완벽히 실행한 횟수
     *
     * 유저의 칭찬 스티커 특정 주차 게임 점수 리턴
     * ===
     * @param $user_idx
     * @param $week_start_days
     * @param $app_type
     * @return array
     */
    public static function getGameScores($user_idx, $week_start_days, $app_type)
    {
        $game_scores = array();
        for ($i = $week_start_days; $i <= $week_start_days + 6; $i++) {
            $game_score = UserGamePlayRepository::getRepository()->getCountInDaysByUserIdx(
                $user_idx,
                $i,
                $app_type
            );
            $game_scores[] = min($game_score, 6);
        }

        return $game_scores;
    }
}
