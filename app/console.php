<?php

use Symfony\Component\Console\Application;

require_once __DIR__ . "/bootstrap.php";

$application = new Application();
$application->add(new \Yface\Command\User\ThemeResetCommand());
$application->add(new \Yface\Command\Point\PointDailyLogCommand());
$application->add(new \Yface\Command\Praise\UserPraiseWeeklyCommand());
$application->add(new \Yface\Command\Game\TodayGameSelectCommand());
$application->add(new \Yface\Command\Resources\ResourceUpdateFileCleanCommand());

$application->add(new \Yface\Command\Statistics\TotalStarStatCommand());
$application->add(new \Yface\Command\Statistics\TotalPointStatCommand());
$application->add(new \Yface\Command\Statistics\GameExecutionStatCommand());

// 데이터 보정 (1회용)
$application->add(new \Yface\Command\Game\GameHitTheRoadDataModifyCommand());
$application->add(new \Yface\Command\Game\GameEyeDataModifyCommand());
$application->add(new \Yface\Command\Game\GameIdenticalPictureDateModifyCommand());
$application->add(new \Yface\Command\Game\GameTalkModifyCommand());

$application->add(new \Yface\Command\DaysModifyCommand());

$application->run();
