<?php
namespace Yface\Model\Game;

use JMS\Serializer\Annotation as JMS;

/**
 * @JMS\ExclusionPolicy("none")
 */
class UserTodayGame
{
    /**
     * @JMS\SerializedName("gameCode")
     *
     * @var $game_code integer
     */
    private $game_code;
    /**
     * - 유저가 오늘 해당 게임을 했는가
     * @JMS\SerializedName("isTodayGamePlayed")
     *
     * @var $is_today_game_played boolean
     */
    private $is_today_game_played;
    /**
     * - 오늘 별을 얻었는가
     * @JMS\SerializedName("isTodayGotStar")
     *
     * @var $is_today_got_star boolean
     */
    private $is_today_got_star;
    /**
     * @JMS\SerializedName("userStars")
     *
     * @var $user_stars integer
     */
    private $user_stars;

    /**
     * @return int
     */
    public function getGameCode(): int
    {
        return $this->game_code;
    }

    /**
     * @param int $game_code
     */
    public function setGameCode(int $game_code)
    {
        $this->game_code = $game_code;
    }

    /**
     * @return boolean
     */
    public function getIsTodayGamePlayed(): bool
    {
        return $this->is_today_game_played;
    }

    /**
     * @param boolean $is_today_game_played
     */
    public function setIsTodayGamePlayed(bool $is_today_game_played)
    {
        $this->is_today_game_played = $is_today_game_played;
    }

    /**
     * @return boolean
     */
    public function getIsTodayGotStar(): bool
    {
        return $this->is_today_got_star;
    }

    /**
     * @param boolean $is_today_got_star
     */
    public function setIsTodayGotStar(bool $is_today_got_star)
    {
        $this->is_today_got_star = $is_today_got_star;
    }

    /**
     * @return int
     */
    public function getUserStars(): int
    {
        return $this->user_stars;
    }

    /**
     * @param int $user_stars
     */
    public function setUserStars(int $user_stars)
    {
        $this->user_stars = $user_stars;
    }
}
