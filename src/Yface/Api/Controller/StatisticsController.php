<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Yface\Service\Statistics\StatisticsService;

class StatisticsController
{
    public function getStars(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));

        $statistics = StatisticsService::getUserTotalStarStats($user_idx, $app_type);

        return $app->json($statistics);
    }

    public function getPoints(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));
        $type = strtoupper(trim($request->request->get('type')));

        if ($type == 'DAILY') {
            $statistics = StatisticsService::getUserTotalPointDailyStats($user_idx, $app_type);
        } else {
            $statistics = StatisticsService::getUserTotalPointWeeklyStats($user_idx, $app_type);
        }

        return $app->json($statistics);
    }

    public function getExecutionRate(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $game_code = intval(trim($request->request->get('gameCode')));

        $statistics = StatisticsService::getUserGameExecutionRateStats($user_idx, $game_code);

        return $app->json($statistics);
    }
}
