<?php
namespace Yface\DataStore\Game;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Game\GameLevelInfo;

class GameLevelInfoRepository extends EntityRepository
{
    /**
     * @param $game_code
     * @param $level
     * @return null|object|GameLevelInfo
     */
    public function findOneByGameCodeAndLevel($game_code, $level)
    {
        return $this->findOneBy(array('game_code' => $game_code, 'level' => $level));
    }

    /**
     * @return GameLevelInfoRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Game\GameLevelInfo');
    }
}
