<?php
namespace Yface\Game;

final class GameResult
{
    const ResultTitleMap = [
        GameCode::CARD => [
            'pair_count' => '카드 개수',
            'card_visible_time' => '제한 시간',

            'available_fail_count' => '실패 가능 수',
            'total_used_fail_count' => '사용한 실패 수',
            'timeout_fail_count' => '시간 초과 실패 수',
            'no_remain_fail_count' => '실패 가능 횟수 모두 사용 실패 수',
            'used_fail_count_list' => '시행별로 사용한 실패 가능 수',
        ],
        GameCode::EYE => [
            'origin_answer_list' => '정답 단어',
            'user_answer_list' => '만들어진 단어',

            'total_consonants_count' => '맞춰야하는 자음 개수',
            'correct_consonants_count' => '맞은 자음 개수',

            'total_vowels_count' => '맞춰야하는 모음 개수',
            'correct_vowels_count' => '맞은 모음 개수',

            'consonants_accuracy' => '자음 정답률',
            'vowels_accuracy' => '모음 정답률',

            'direction_count_answer_accuracy_list' => '눈 방향별 정답률'
        ],
        GameCode::EYE_FIGHT => [
            'both_count' => '양쪽 눈 감은 사진 개수',
            'both_correct_count' => '양쪽 눈 감은 사진 양쪽 터치 횟수',
            'both_wrong_count' => '양쪽 눈 감은 사진 한쪽 터치 횟수',
            'both_timeout_count' => '양쪽 눈 감은 사진 시간 초과 횟수',
            'both_correct_accuracy' => '양쪽 눈 감은 사진 정답률',
//            'both_average_response_time' => '양쪽 눈 감은 사진 평균 반응 시간',

            'one_side_count' => '한쪽 눈 감은 사진 개수',
            'one_side_correct_count' => '한쪽 눈 사진일 때 한쪽 눈을 터치한 총 횟수',
            'one_side_wrong_count' => '한쪽 눈 감은 사진 양쪽 터치 횟수',
            'one_side_timeout_count' => '한쪽 눈 사진일 때 시간 초과 횟수',
            'one_side_correct_accuracy' => '한쪽 눈 사진 정답률',

//            'one_side_average_response_time' => '한쪽 눈 감은 사진 평균 반응 시간',

            'response_set' => '반응편향',
            'sensitivity' => '민감도',
        ],
        GameCode::FACE => [
            'origin_answer_emotion' => '정답 정서',
            'user_answer_emotion' => '선택 정서',

            'uploaded_files' => '촬영 사진들',

            'im_good_count' => '내가 더 잘했다고 대답한 횟수',
            'your_good_count' => '파트너가 더 잘했다고 대답한 횟수'
        ],
        GameCode::FRIEND => [
            'example_count' => '외워야 하는 얼굴의 개수',

            'ox_count' => '맞는데 아니라고 한 횟수',
            'xo_count' => '아닌데 맞다고 한 횟수',

            'accuracy_by_move_level_list' => '얼굴 자극에 따른 정답률',

            'origin_answer_face' => '정답 얼굴',
            'user_answer_face' => '아동이 선택한 얼굴',

            'response_set' => '반응편향',
            'sensitivity' => '민감도',
        ],
        GameCode::LOOK => [
            'origin_answer_eye_name_list' => '정답 눈',
            'user_answer_eye_name_list' => '선택한 눈'
        ],
        GameCode::MATCH => [
            'origin_answer_face_list' => '정답 얼굴',
            'user_answer_face_list' => '선택 얼굴'
        ],
        GameCode::MODEL => [
            'origin_answer_gesture_list' => '정답 제스쳐',
            'user_answer_gesture_list' => '선택한 제스쳐',

            'uploaded_files' => '촬영 사진들',

            'im_good_count' => '내가 더 잘했다고 대답한 횟수',
            'your_good_count' => '파트너가 더 잘했다고 대답한 횟수'
        ],
        GameCode::ORDER => [
            'answer_order' => '정답 순서',
            'user_order_list' => '선택 순서'
        ],
        GameCode::TALK => [
            'origin_answer_string_list' => '정답 문장',
            'user_answer_string_list' => '선택 문장',

            'uploaded_files' => '녹음한 파일들',
            'is_recorded' => '아동의 녹음 여부',
        ],
        GameCode::THIEF => [
            'origin_answer_face_list' => '정답 얼굴',
            'user_answer_face_list' => '선택 얼굴'
        ],
        GameCode::TRE => [],

        GameCode::BADUM_TISH => [
            'answer_list' => '정답 연주 순서',
            'user_answer_list' => '선택 연주 순서'
        ],
        GameCode::FIND_THE_MATCH => [
            'pair_count' => '카드 개수',
            'game_time' => '제한 시간',

            'available_fail_count' => '실패 가능 수',
            'total_used_fail_count' => '사용한 실패 수',
            'timeout_fail_count' => '시간 초과 실패 수',
            'no_remain_fail_count' => '실패 가능 횟수 모두 사용 실패 수',
            'used_fail_count_list' => '시행별로 사용한 실패 가능 수',
        ],
        GameCode::HIT_THE_ROAD => [
            'answer_list' => '정답 보기',
            'user_answer_list' => '선택 보기'
        ],
        GameCode::IDENTICAL_PICTURE => [
            'answer_count' => '정답 쌍 수',
            'answer_set_list_3d' => '정답 쌍',

            'user_set_list' => '선택 쌍',
            'user_answer_count_list' => '성공한 정답 쌍 수',
        ],
        GameCode::IS_IT_THERE => [
            'total_answer_count' => '전체 시행 정답 수',

            'target_count' => '타겟 단어 수',
            'target_answer_count' => '타겟 정답 수',
            'target_wrong_count' => '타겟 오답 수',
            'target_no_response_count' => '타겟 무반응 수',
            'target_accuracy' => '타겟 정답률',
            'target_average_response_time' => '타겟 평균 반응 시간',

            'non_target_count' => '디스트랙터 단어 수',
            'non_target_answer_count' => '디스트랙터 정답 수',
            'non_target_wrong_count' => '디스트랙터 오답 수',
            'non_target_no_response_count' => '디스트랙터 무반응 수',
            'non_target_accuracy' => '디스트랙터 정답률',
            'non_target_average_response_time' => '디스트랙터 평균 반응 시간',

            'response_set' => '반응편향',
            'sensitivity' => '민감도',
        ],
        GameCode::LETS_GO_FISHING => [
            'answer_direction_list' => '정답 방향',
            'user_direction_list' => '선택 방향'
        ],
        GameCode::NAVIGATE_ME => [
            'green_count' => '초록색 수',
            'green_answer_count' => '초록색 정답 수',
            'green_wrong_count' => '초록색 오답 수',
            'green_no_response_count' => '초록색 무반응 수',
            'green_accuracy' => '초록색 정답률',
            'green_average_response_time' => '초록색 평균 반응 시간',

            'yellow_count' => '노란색 수',
            'yellow_answer_count' => '노란색 정답 수',
            'yellow_wrong_count' => '노란색 오답 수',
            'yellow_no_response_count' => '노란색 무반응 수',
            'yellow_accuracy' => '노란색 정답률',
            'yellow_average_response_time' => '노란색 평균 반응 시간',

            'red_count' => '빨간색 수',
            'red_answer_count' => '빨간색 정답 수',
            'red_wrong_count' => '빨간색 오답 수',
            'red_accuracy' => '빨간색 정답률',

            'response_set' => '반응편향',
            'sensitivity' => '민감도',
        ],
        GameCode::STACK_THE_BRICKS => [
            'answer_move_count' => '정답 이동 횟수',
            'user_move_count_list' => '선택 이동 횟수',

            'average_move_count' => '평균 이동 횟수'
        ],
        GameCode::TAP_TAP_TAP => [
            'from_low_count' => '작은 수부터 한 횟수',
            'from_low_correct_count' => '작은 수부터 한 정답 수',
            'from_low_no_response_count' => '작은 수부터 한 무반응 수',

            'from_high_count' => '큰 수부터 한 횟수',
            'from_high_correct_count' => '큰 수부터 한 정답 수',
            'from_high_no_response_count' => '큰 수부터 한 무반응 수',

            'answer_list' => '정답 순서',
            'user_answer_list' => '선택 순서'
        ],
        GameCode::TO_EAT => [
            'fresh_count' => '신선한 초밥 개수',
            'fresh_correct_count' => '신선한 초밥 정답 횟수',
            'fresh_wrong_count' => '신선한 초밥 오답 횟수',
            'fresh_no_response_count' => '신선한 초밥 정답 무반응 횟수',
            'fresh_accuracy' => '신선한 초밥 정답률',
            'fresh_average_response_time' => '신선한 초밥 평균 반응 시간',

            'throw_count' => '상한 초밥 개수',
            'throw_correct_count' => '상한 초밥 정답 횟수',
            'throw_wrong_count' => '상한 초밥 오답 횟수',
            'throw_no_response_count' => '상한 초밥 정답 무반응 횟수',
            'throw_accuracy' => '상한 초밥 정답률',
            'throw_average_response_time' => '상한 초밥 평균 반응 시간',

            'response_set' => '반응편향',
            'sensitivity' => '민감도',
        ],
        GameCode::UP_DOWN => [
            'up_count' => '올려 개수',
            'up_answer_count' => '올려 정답 횟수',
            'up_wrong_count' => '올려 오답 횟수',
            'up_no_response_count' => '올려 정답 무반응 횟수',
            'up_accuracy' => '올려 정답률',
            'up_average_response_time' => '올려 평균 반응 시간',

            'down_count' => '내려 개수',
            'down_answer_count' => '내려 정답 횟수',
            'down_wrong_count' => '내려 오답 횟수',
            'down_no_response_count' => '내려 정답 무반응 횟수',
            'down_accuracy' => '내려 정답률',
            'down_average_response_time' => '내려 평균 반응 시간',

            'fake_count' => '올/내리지마 개수',
            'fake_answer_count' => '올/내리지마 정답 횟수',
//            'fake_wrong_count' => '올/내리지마 오답 횟수',
//            'fake_accuracy' => '올/내리자마 정답률',

            'response_set' => '반응편향',
            'sensitivity' => '민감도',
        ],
        GameCode::WHATS_THE_NUMBER => [
            'answer_number_list' => '정답 숫자',
            'user_number_list' => '선택 숫자'
        ]
    ];

    const AdditionalResultTitleMap = [
        GameCode::CARD => [
            '시간 초과로 실패한 횟수 (전체)',
            '실패 가능 횟수를 모두 사용하여 실패한 횟수 (전체)'
        ],
        GameCode::FACE => [
            '파트너가 더 잘 했다고 대답한 횟수 (전체)',
            '내가 더 잘 했다고 대답한 횟수 (전체)',
            '정답률 - 하',
            '정답률 - 중',
            '정답률 - 상',
        ],
        GameCode::MODEL => [
            '파트너가 더 잘 했다고 대답한 횟수 (전체)',
            '내가 더 잘 했다고 대답한 횟수 (전체)',
            '정답률 - 하',
            '정답률 - 중',
            '정답률 - 상',
        ],
        GameCode::ORDER => [
            '정답률 - 하',
            '정답률 - 중',
            '정답률 - 상',
        ],
        GameCode::FIND_THE_MATCH => [
            '시간 초과로 실패한 횟수 (전체)',
            '실패 가능 횟수를 모두 사용하여 실패한 횟수 (전체)'
        ],
        GameCode::LOOK => [
            '배경 얼굴 자극에 따른 정답률',
        ],
        GameCode::THIEF => [
            '얼굴 자극에 따른 정답률',
        ],
    ];

    /**
     * 게임 별 결과 제목 리턴
     * ===
     * @param $game_code
     * @param $export_excel
     * @param $stats
     *
     * @return array
     */
    public static function getTitles($game_code, $export_excel = false, $stats = [])
    {
        $common_array = [
            'reg_date' => '일자',

            'start_time' => '시작 시간',
            'end_time' => '종료 시간',

            'play_count' => '전체 시행 수',
            'correct_count' => '정답 시행 수',
            'wrong_count' => '오답 시행 수',

            'accuracy' => '정확도',
        ];

        if ($game_code == GameCode::EYE_FIGHT) {
            $common_array = array_merge($common_array, ['active_accuracy' => '무반응 제외 정답률']);
        }

        if (GameCode::getGameAppType($game_code) == APP_TYPE_YCOG) {
            $common_array = array_merge($common_array, ['accuracy_except_no_response' => '무반응 제외 정답률']);
        }

        $common_array = array_merge($common_array, ['avg_response_time' => '평균 반응 시간']);
        if (!$export_excel) {
            $common_array = array_merge($common_array, ['response_time' => '시행 별 반응 시간']);
        }

        if (!empty($stats) && $export_excel) {
            $common_array = array_merge($common_array, static::getResponseTimeTitles($stats));
        }

        return array_merge($common_array, static::ResultTitleMap[$game_code]);
    }

    /**
     * 추가 제목 리턴
     * ===
     * @param $game_code
     * @return array|mixed
     */
    public static function getAdditionalTitles($game_code)
    {
        if (!array_key_exists($game_code, static::AdditionalResultTitleMap)) {
            return [];
        }

        return static::AdditionalResultTitleMap[$game_code];
    }

    /**
     * @param $stats
     *
     * @return array
     */
    private static function getResponseTimeTitles($stats)
    {
        $max_number = static::getResponseTimeRows($stats);

        $titles = [];
        for ($i = 1; $i <= $max_number; $i++) {
            $titles["response_time_{$i}"] = "시행 별 반응 시간 {$i}";
        }

        return $titles;
    }

    /**
     * @param $stats
     *
     * @return int
     */
    public static function getResponseTimeRows($stats)
    {
        $max_number = 0;
        $rows = $stats['rows'];
        foreach ($rows as $row) {
            $results = $row['response_time'];
            $number = count(explode(',', $results));

            if ($max_number < $number) {
                $max_number = $number;

                continue;
            }
        }

        return $max_number;
    }
}
