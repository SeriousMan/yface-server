<?php
namespace Yface\DataStore\Research;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Research\UserResearchStatus;

class UserResearchStatusRepository extends EntityRepository
{
    /**
     * @return UserRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Research\Research');
    }
}
