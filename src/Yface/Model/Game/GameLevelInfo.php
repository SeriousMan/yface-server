<?php
namespace Yface\Model\Game;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Game\GameLevelInfoRepository")
 * @ORM\Table(name="yf_game_level_info")
 * @JMS\ExclusionPolicy("all")
 */
class GameLevelInfo
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $game_code integer
     */
    private $game_code;
    /**
     * @ORM\Column(type="integer", options={"unsigned":true})
     *
     * @var $level integer
     */
    private $level;
    /**
     * @ORM\Column(type="string")
     *
     * @var $level_info string
     */
    private $level_info;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var $reg_date \DateTime
     */
    private $reg_date;
    /**
     * @ORM\Column(type="datetime")
     *
     * @var $mod_date \DateTime
     */
    private $mod_date;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getGameCode(): int
    {
        return $this->game_code;
    }

    /**
     * @param int $game_code
     */
    public function setGameCode(int $game_code)
    {
        $this->game_code = $game_code;
    }

    /**
     * @return int
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     */
    public function setLevel(int $level)
    {
        $this->level = $level;
    }

    /**
     * @return string
     */
    public function getLevelInfo(): string
    {
        return $this->level_info;
    }

    /**
     * @param string $level_info
     */
    public function setLevelInfo(string $level_info)
    {
        $this->level_info = $level_info;
    }

    /**
     * @return \DateTime
     */
    public function getRegDate(): \DateTime
    {
        return $this->reg_date;
    }

    /**
     * @param \DateTime $reg_date
     */
    public function setRegDate(\DateTime $reg_date)
    {
        $this->reg_date = $reg_date;
    }

    /**
     * @return \DateTime
     */
    public function getModDate(): \DateTime
    {
        return $this->mod_date;
    }

    /**
     * @param \DateTime $mod_date
     */
    public function setModDate(\DateTime $mod_date)
    {
        $this->mod_date = $mod_date;
    }
}
