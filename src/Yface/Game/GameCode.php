<?php
namespace Yface\Game;

use Yface\Library\Configuration;

final class GameCode
{
    const ENG_NAMES = [
        'CARD',
        'EYE',
        'EYE_FIGHT',
        'FACE',
        'FRIEND',
        'LOOK',
        'MATCH',
        'MODEL',
        'ORDER',
        'TALK',
        'THIEF',
        'TRE',

        'BADUM_TISH',
        'FIND_THE_MATCH',
        'HIT_THE_ROAD',
        'IDENTICAL_PICTURE',
        'IS_IT_THERE',
        'LETS_GO_FISHING',
        'NAVIGATE_ME',
        'STACK_THE_BRICKS',
        'TAP_TAP_TAP',
        'TO_EAT',
        'UP_DOWN',
        'WHATS_THE_NUMBER'
    ];

    const ACRONYM = [
        'CARD' => 'YF_CD',
        'EYE' => 'YF_EY',
        'EYE_FIGHT' => 'YF_EF',
        'FACE' => 'YF_FA',
        'FRIEND' => 'YF_FR',
        'LOOK' => 'YF_LO',
        'MATCH' => 'YF_MA',
        'MODEL' => 'YF_MO',
        'ORDER' => 'YF_OR',
        'TALK' => 'YF_TA',
        'THIEF' => 'YF_TH',
        'TRE' => 'YF_TR',

        'BADUM_TISH' => 'YC_BT',
        'FIND_THE_MATCH' => 'YC_FM',
        'HIT_THE_ROAD' => 'YC_HT',
        'IDENTICAL_PICTURE' => 'YC_IP',
        'IS_IT_THERE' => 'YC_IT',
        'LETS_GO_FISHING' => 'YC_FI',
        'NAVIGATE_ME' => 'YC_NA',
        'STACK_THE_BRICKS' => 'YC_BR',
        'TAP_TAP_TAP' => 'YC_TA',
        'TO_EAT' => 'YC_EA',
        'UP_DOWN' => 'YC_UP',
        'WHATS_THE_NUMBER' => 'YC_NU',
    ];

    const COLORS = [
        'CARD' => '#ED7D31',
        'EYE' => '#5B9BD5',
        'EYE_FIGHT' => '#A5A5A5',
        'FACE' => '#FFC000',
        'FRIEND' => '#70AD47',
        'LOOK' => '#D83040',
        'MATCH' => '#345BAA',
        'MODEL' => '#9E480E',
        'ORDER' => '#636363',
        'TALK' => '#997300',
        'THIEF' => '#532B89',
        'TRE' => '#43682B',

        'BADUM_TISH' => '#9DC3E6',
        'FIND_THE_MATCH' => '#C55A11',
        'HIT_THE_ROAD' => '#44546A',
        'IDENTICAL_PICTURE' => '#F3E80D',
        'IS_IT_THERE' => '#378731',
        'LETS_GO_FISHING' => '#D51992',
        'NAVIGATE_ME' => '#1D3B99',
        'STACK_THE_BRICKS' => '#523434',
        'TAP_TAP_TAP' => '#B8A7D1',
        'TO_EAT' => '#30696A',
        'UP_DOWN' => '#9C063C',
        'WHATS_THE_NUMBER' => '#EE963E',
    ];

    /** YFACE */
    const CARD = 1;
    const EYE = 2;
    const EYE_FIGHT = 3;
    const FACE = 4;
    const FRIEND = 5;
    const LOOK = 6;
    const MATCH = 7;
    const MODEL = 8;
    const ORDER = 9;
    const TALK = 10;
    const THIEF = 11;
    const TRE = 12;

    /** YCOG */
    const BADUM_TISH = 13;
    const FIND_THE_MATCH = 14;
    const HIT_THE_ROAD = 15;
    const IDENTICAL_PICTURE = 16;
    const IS_IT_THERE = 17;
    const LETS_GO_FISHING = 18;
    const NAVIGATE_ME = 19;
    const STACK_THE_BRICKS = 20;
    const TAP_TAP_TAP = 21;
    const TO_EAT = 22;
    const UP_DOWN = 23;
    const WHATS_THE_NUMBER = 24;

    public static function getAllGameCode()
    {
        return [
            static::CARD,
            static::EYE,
            static::EYE_FIGHT,
            static::FACE,
            static::FRIEND,
            static::LOOK,
            static::MATCH,
            static::MODEL,
            static::ORDER,
            static::TALK,
            static::THIEF,
            static::TRE,

            static::BADUM_TISH,
            static::FIND_THE_MATCH,
            static::HIT_THE_ROAD,
            static::IDENTICAL_PICTURE,
            static::IS_IT_THERE,
            static::LETS_GO_FISHING,
            static::NAVIGATE_ME,
            static::STACK_THE_BRICKS,
            static::TAP_TAP_TAP,
            static::TO_EAT,
            static::UP_DOWN,
            static::WHATS_THE_NUMBER
        ];
    }

    /**
     * 영문 게임 제목 리턴
     * ===
     * @param $game_code
     * @return string
     */
    public static function getGameName($game_code)
    {
        return static::ENG_NAMES[$game_code - 1];
    }

    /**
     * 소문자로 된 영문 게임 제목 리턴
     * ===
     * @param $game_code
     * @return string
     */
    public static function getGameLowerCaseName($game_code)
    {
        return strtolower(static::getGameName($game_code));
    }

    /**
     * 해당 게임의 테이블 이름 리턴
     * ===
     * @param $game_code
     * @return string
     */
    public static function getGameTableName($game_code)
    {
        return 'yf_game_' . static::getGameLowerCaseName($game_code);
    }

    /**
     * 해당 게임의 리소스 경로 리턴
     * ===
     * @param $game_code
     * @return string
     */
    public static function getGameResourceFilePath($game_code)
    {
        return Configuration::$values['path']['resources'] . '/games/' . $game_code;
    }

    /**
     * 해당 게임의 결과 리소스를 저장할 경로 리턴
     * ===
     * @param $game_code
     * @return string
     */
    public static function getGameResultFilePath($game_code)
    {
        return Configuration::$values['path']['resources'] . '/results/game_' . $game_code;
    }

    /**
     * 해당 게임이 속한 앱 타입 리턴
     * ===
     * @param $game_code
     * @return string
     */
    public static function getGameAppType($game_code)
    {
        switch ($game_code) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
                return APP_TYPE_YFACE;

            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 24:
                return APP_TYPE_YCOG;

            default:
                return '';
        }
    }
}
