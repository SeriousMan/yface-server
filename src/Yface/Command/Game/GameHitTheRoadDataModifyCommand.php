<?php
namespace Yface\Command\Game;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Yface\Command\BaseCommand;
use Yface\Library\Database\ConnectionProvider;
use Yface\Service\Game\TodayGameService;

class GameHitTheRoadDataModifyCommand extends BaseCommand
{
    protected function configure()
    {
        $this->setName('game:ht:modify');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        static::report('ONCE - 탐방탐방 데이터 보정 시작');

        self::modify();

        static::report('ONCE - 탐방탐방 데이터 보정 종료');
    }

    protected function modify()
    {
        $db = ConnectionProvider::getConnection();
        // 전체 대상 다시 돌려야 함 (강제 업데이트 이후에)
        $results = $db->fetchAll('SELECT * FROM yf_game_hit_the_road WHERE accuracy IS NOT NULL');

        foreach ($results as $result) {
            var_dump($result['idx']);

            $answer_list = explode('|', $result['answer_list']);
            $user_answer_list = explode('|', $result['user_answer_list']);

            $answer_count = 0;
            $count_except_no_response = 0;
            foreach ($user_answer_list as $answer) {
                if (empty($answer)) {
                    continue;
                }

                $count_except_no_response++;
            }

            foreach ($user_answer_list as $key => $answer) {
                if (empty($answer)) {
                    continue;
                }

                $tempArr = [];
                if (strpos($answer, ',') !== false) {
                    $tempArr = explode(',', $answer);
                } else {
                    $tempArr[] = $answer;
                }

                $answerArr = [];
                if (strpos($answer_list[$key], ',') !== false) {
                    $answerArr = explode(',', $answer_list[$key]);
                } else {
                    $answerArr[] = $answer_list[$key];
                }

                if (count($tempArr) != count($answerArr)) {
                    continue;
                }

                $diff = array_diff($tempArr, $answerArr);
                if (count($diff) > 0) {
                    continue;
                }

                $answer_count++;
            }

            $db->update(
                'yf_game_hit_the_road',
                [
                    'accuracy_except_no_response' => ($answer_count / $count_except_no_response)
                ],
                [
                    'idx' => $result['idx']
                ]
            );
        }
    }
}
