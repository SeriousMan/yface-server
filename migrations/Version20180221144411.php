<?php declare(strict_types = 1);

namespace YFACE\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180221144411 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('
          ALTER TABLE `yf_admin`
          ADD `role` varchar(30) NOT NULL;
        ');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('
          ALTER TABLE `yf_admin`
          DROP COLUMN `role`;
        ');
    }
}
