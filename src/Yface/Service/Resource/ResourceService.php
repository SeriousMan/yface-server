<?php
namespace Yface\Service\Resource;

use Yface\DataStore\Resource\ResourceFileInfoRepository;
use Yface\DataStore\Resource\ResourceUpdateInfoRepository;
use Yface\Game\GameCode;
use Yface\Library\Configuration;
use Yface\Library\DateHelper;
use Yface\Model\Resource\ResourceFileInfo;

class ResourceService {
    /**
     * 리소스 업데이트 압축 파일 URL 리턴
     * ===
     * @param $files
     * @param $app_type
     * @param $timestamp
     * @return null|string
     * @throws \Exception
     */
    public static function getResourceUpdateUrl($files, $app_type, $timestamp) {
        $datetime = DateHelper::convertTimestampToDateTime($timestamp);
        if (!static::isUpdateNeeded($files, $app_type, $datetime)) {
            return null;
        }

        $update_files = ResourceFileInfoRepository::getRepository()->findByAppTypeAndDateTime(
            $app_type,
            $datetime,
            $files
        );

        if (count($update_files) == 0) {
            return null;
        }

        try {
            $zip_file_name = static::zipResourceFilesForUpdate($update_files);

            return Configuration::$values['url']['resources'] . '/updates/' . $zip_file_name;
        } catch (\Exception $e) {
            throw new \Exception('리소스 업데이트 실패');
        }
    }

    /**
     * 업데이트 리소스 파일들을 압축 후 파일 이름 리턴
     * ===
     * @param ResourceFileInfo[] $update_files
     * @return string
     */
    private static function zipResourceFilesForUpdate($update_files)
    {
        $path = Configuration::$values['path']['resources'] . '/updates/';
        $zip_file_name = time() . '.zip';

        $zip = new \ZipArchive();
        $zip->open($path . $zip_file_name, \ZipArchive::CREATE);

        foreach ($update_files as $update_file) {
            $game_code = $update_file->getGameCode();
            $file_name = $update_file->getFileName();

            $file_path = GameCode::getGameResourceFilePath($game_code) . '/' . $file_name;
            $zip_file_path = '/games/game_' . $game_code . '/' . $update_file->getFileName();

            $zip->addFile($file_path, $zip_file_path);
        }

        $zip->close();

        return $zip_file_name;
    }

    /**
     * 리소스가 갱신이 된 경우 최신 리소스로 압축 파일을 갱신
     */
    public static function zipAllFilesForFirstUpdateUser()
    {
        $update_files = ResourceFileInfoRepository::getRepository()->findAll();
        $update_files = array_slice($update_files, 0, 300);

        $path = Configuration::$values['path']['resources'] . '/updates/';
        $zip_file_name = 'latest_files.zip';

        $zip = new \ZipArchive();
        $zip->open($path . $zip_file_name, \ZipArchive::CREATE);

        /**
         * @var $update_file ResourceFileInfo
         */
        foreach ($update_files as $update_file) {
            $game_code = $update_file->getGameCode();
            $file_name = $update_file->getFileName();

            $file_path = GameCode::getGameResourceFilePath($game_code) . '/' . $file_name;
            $zip_file_path = '/games/game_' . $game_code . '/' . $update_file->getFileName();

            $zip->addFile($file_path, $zip_file_path);
        }

        $zip->close();
    }

    /**
     * 앱에서 존재하지 않은 파일들이 있거나 업데이트 파일이 존재하는지 확인하여
     * 업데이트가 실제로 필요한지 여부 리턴
     * ===
     * @param $files
     * @param $app_type
     * @param $datetime
     * @return bool
     */
    private static function isUpdateNeeded($files, $app_type, $datetime) {
        return count($files) > 0 || static::hasUpdateFiles($app_type, $datetime);
    }

    /**
     * 앱에서 업데이트 한 날짜와 앱 별로 리소스가 가장 최근에 업데이트 된 날짜를 비교하여 업데이트 파일 존재하는지 여부 리턴
     * ===
     * @param $app_type
     * @param $datetime
     * @return bool
     */
    private static function hasUpdateFiles($app_type, $datetime) {
        $update_info = ResourceUpdateInfoRepository::getRepository()->findByAppTypeAndDateTime($app_type, $datetime);

        return count($update_info) > 0;
    }

    /**
     * 리소스 폴더 내에 있는 파일 존재 여부
     * ===
     * @param $path
     * @return bool
     */
    public static function isResourceFileExists($path)
    {
        return file_exists(Configuration::$values['path']['resources'] . '/' . $path);
    }
}
