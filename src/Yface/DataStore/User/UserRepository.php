<?php
namespace Yface\DataStore\User;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\User\User;

class UserRepository extends EntityRepository
{
    /**
     * @param $id
     * @return null|object|User
     */
    public function findOneById($id) {
        return $this->findOneBy(array('id' => $id));
    }

    /**
     * @param $nickname
     * @return null|object|User
     */
    public function findOneByNickname($nickname) {
        return $this->findOneBy(array('nickname' => $nickname));
    }

    /**
     * @param $user_idx
     * @return null|object|User
     */
    public function findOneByUserIdx($user_idx) {
        return $this->findOneBy(array('idx' => $user_idx));
    }

    /**
     * @param $email
     * @param $name
     * @param $birth_date
     * @return null|object|User
     */
    public function findOneByEmailAndNameAndBirthDate($email, $name, $birth_date) {
        return $this->findOneBy(array('email' => $email, 'name' => $name, 'birth_date' => $birth_date));
    }

    /**
     * @param $app_type
     * @return User[]
     */
    public function findSurveyCompletedUserByAppType($app_type)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.' . strtolower($app_type) . '_survey_completed_date != ' . $qb->expr()->literal('NULL'));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $app_type
     * @return User[]
     */
    public function findPraiseStartedUserByAppType($app_type)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->where('u.' . strtolower($app_type) . '_praise_start_date != ' . $qb->expr()->literal('NULL'));

        return $qb->getQuery()->getResult();
    }

    /**
     * @return UserRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\User\User');
    }
}
