<?php
namespace Yface\Library\Template;

use Yface\Library\Configuration;
use Yface\Library\Template\Extension\JsonDecode;
use Yface\Library\Database\ConnectionProvider;
use Yface\Admin\User\UserSession;

/**
 * Twig Template Engine Render 클래스
 */
class TwigRenderer
{
    /**
     * @var $twig \Twig_Environment
     */
    protected $twig;
    protected $attributes = array();

    public function __construct()
    {
        $this->twig = new \Twig_Environment($this->getLoader(), array(
            'cache' => sys_get_temp_dir() . '/twig/cache/v1',
            'auto_reload' => true,
            'strict_variables' => false
        ));

        $this->setDefaultAttributes();
        $this->setDefaultExtensions();
    }

    protected function getLoader()
    {
        return new \Twig_Loader_Filesystem(Configuration::$values['path']['views']);
    }

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;

        return $this;
    }

    public function setAttributes(array $array)
    {
        if (is_array($array)) {
            $this->attributes = array_merge($this->attributes, $array);
        }

        return $this;
    }

    protected function setDefaultAttributes()
    {
        $this->attributes['ADMIN_URL'] = 'http://' . Configuration::$values['url']['base'] . '/admin';
        $this->attributes['STATIC_URL'] = 'http://' . Configuration::$values['url']['static'];
        if(UserSession::getLoginRole() == 'admin') {
            $this->attributes['research_tabs'] = ConnectionProvider::getConnection()->fetchAll('SELECT * FROM yf_research ORDER BY idx ASC');
        }
        else if(UserSession::getLoginRole() == 'researcher') {
            // FIXME: Vulnerable to SQL Injection
            $research_idxes = join("','", UserSession::getLoginRIdxes());
            $this->attributes['research_tabs'] = ConnectionProvider::getConnection()->fetchAll("SELECT * FROM yf_research WHERE idx IN ('$research_idxes') ORDER BY idx ASC");
        }
        $this->attributes['login_role'] = UserSession::getLoginRole();
    }

    protected function setDefaultExtensions()
    {
        $this->twig->addExtension(new JsonDecode());
    }

    /**
     * @param $template_name
     * @return string
     */
    public function render($template_name)
    {
        $this->attributes['BASE_TEMPLATE'] = 'base.twig';

        return $this->twig->loadTemplate($template_name . '.twig')->render($this->attributes);
    }

    /**
     * @return TwigRenderer
     */
    public static function getRenderer()
    {
        return new static();
    }
}
