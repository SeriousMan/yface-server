<?php
namespace Yface\DataStore\Resource;

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Resource\ResourceUpdateInfo;

class ResourceUpdateInfoRepository extends EntityRepository {
    /**
     * @param $app_type
     * @param $datetime
     * @return ResourceUpdateInfo[]
     */
    public function findByAppTypeAndDateTime($app_type, $datetime) {
        $qb = $this->createQueryBuilder('ru');
        $qb->where('ru.app_type = :app_type')
            ->setParameter('app_type', $app_type, Type::STRING)
            ->andWhere('ru.latest_mod_date > :datetime')
            ->setParameter('datetime', $datetime, Type::DATETIME);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return ResourceUpdateInfoRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Resource\ResourceUpdateInfo');
    }
}
