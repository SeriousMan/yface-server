<?php
namespace Yface\Library\Database;

use Yface\Library\Configuration as Config;
use Doctrine\Common\Cache\ApcCache;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\FilesystemCache;

class DatabaseCacheHelper
{
    /**
     * 현재 운영 환경에 맞는 캐시 오브젝트 리턴
     * 1. 운영 환경에서는 ApcCache -> FilesystemCache 순으로 적용
     * 2. 테스트 환경에서는 ArrayCache 적용
     * ======
     * @return ApcCache|ArrayCache|FilesystemCache
     */
    public static function getAdaptableCache()
    {
        if (Config::$values['debug'] || defined('PHPUNIT')) {
            return new ArrayCache();
        }

        if (extension_loaded('apc')) {
            return new ApcCache();
        }

        return new FilesystemCache(sys_get_temp_dir());
    }
}
