<?php
namespace Yface\Library\Localization;

class Lang
{
    private static $locale = 'en';

    /**
     * 해당 메시지에 맞는 지문을 리턴
     * ======
     * @param $message
     * @return string
     */
    public static function get($message)
    {
        return TranslatorProvider::getTranslator(static::$locale)->trans($message);
    }

    /**
     * 현재 설정된 Locale 값 리턴
     * ======
     * @return string
     */
    public static function getLocale()
    {
        return static::$locale;
    }

    /**
     * 현재 어플리케이션에서 사용할 Locale 을 설정
     * ======
     * @param $locale
     */
    public static function setLocale($locale)
    {
        static::$locale = $locale;
    }
}
