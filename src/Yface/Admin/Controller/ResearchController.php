<?php
namespace Yface\Admin\Controller;

use Kilte\Pagination\Pagination;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Yface\Admin\Library\UrlHelper;
use Yface\Admin\Statistics\User;
use Yface\Game\GameCode;
use Yface\Game\GameResult;
use Yface\Library\Configuration;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\DateHelper;
use Yface\Library\Template\TwigRenderer;

class ResearchController
{
    const USER_COUNT_PER_PAGE = 20;

    /**
     * 컨트롤러) 연구 목록
     * ===
     * @param $request Request
     * @return string
     */
    public function index(Request $request)
    {
        $db = ConnectionProvider::getConnection();
        $qb = $db->createQueryBuilder();
        $qb->select('*', 'r.idx', 'count(urs.idx)')
        ->from('yf_research', 'r')
        ->leftJoin('r', 'yf_user_research_status', 'urs', 'r.idx = urs.r_idx')
        ->groupBy('r.idx')
        ->orderBy('r.idx', 'DESC');
        $researchList = $qb->execute()->fetchAll();

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'research_list' => $researchList,
        ]);
        return $twig_renderer->render('admin/research/index');
    }

    public function manage(Request $request)
    {
        $db = ConnectionProvider::getConnection();
        $qb = $db->createQueryBuilder();
        $qb->select('*', 'r.idx', 'count(urs.idx)')
        ->from('yf_research', 'r')
        ->leftJoin('r', 'yf_user_research_status', 'urs', 'r.idx = urs.r_idx')
        ->groupBy('r.idx');
        $researchList = $qb->execute()->fetchAll();

        if(empty($request->get('research'))) {
          $research_idx = $researchList[0]['idx'];
        }
        else {
          $research_idx = $request->get('research');
        }

        $qb = $db->createQueryBuilder();
        $qb->select('*', 'u.idx')
        ->from('yf_user', 'u')
        ->orderBy('u.idx', 'DESC')
        ->innerJoin('u', 'yf_user_research_status', 'urs', 'u.idx = urs.u_idx')
        ->andWhere('urs.r_idx = :r_idx')
        ->setParameter('r_idx', $research_idx);
        $selected_group_users = $qb->execute()->fetchAll();

        if(!empty($selected_group_users)) {
            $qb = $db->createQueryBuilder();
            $qb->select('*')
            ->from('yf_user', 'u')
            ->where($qb->expr()->notIn('u.idx', array_map(function ($u) { return $u['idx']; }, $selected_group_users)));
            $not_selected_group_users = $qb->execute()->fetchAll();
        }
        else {
            $qb = $db->createQueryBuilder();
            $qb->select('*')
            ->from('yf_user');
            $not_selected_group_users = $qb->execute()->fetchAll();
        }

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'research_list' => $researchList,
            'research_idx' => $research_idx,
            'selected_group_users' => $selected_group_users,
            'not_selected_group_users' => $not_selected_group_users,
        ]);
        return $twig_renderer->render('admin/research/manage');
    }
}
