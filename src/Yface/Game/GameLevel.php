<?php
namespace Yface\Game;

final class GameLevel
{
    const LevelTitleMap = [
        GameCode::CARD => [
            'blurLevel' => '가우시안 블러 강도',
            'pairCount' => '쌍 개수',
            'availableFailCount' => '실패가능 횟수',
            'cardVisibleTime' => '카드제시시간'
        ],
        GameCode::EYE => [
            'showImageTime' => '사진 제시 시간',
            'directionCount' => '방향'
        ],
        GameCode::EYE_FIGHT => [
            'showExampleFaceShowTime' => '사진 제시 시간(양쪽눈 또는 한쪽눈 감은 사진)',
            'showOriginFaceShowTIme' => '사진 제시 시간',
            'bothPercent' => '양쪽눈 (제시비율 10으로 했을 때)',
            'throwPercent' => '한쪽눈 (제시비율 10으로 했을 때)'
        ],
        GameCode::FACE => [
            'exampleCount' => '보기의 수',
            'problemShowingTime' => '타겟 제시 시간'
        ],
        GameCode::FRIEND => [
            'showingTime' => '타겟 제시 시간',
            'answerCount' => '정답개수(최소)',
            'repeatCount' => '반복 횟수',
            'exampleCount' => '타겟 개수(늘 원본만)'
        ],
        GameCode::LOOK => [
            'maxIdentityLevel' => '등장하는 최대 identity 강도',
            'showingTime' => '얼굴 제시시간',
            'exampleCount' => '보기 개수'
        ],
        GameCode::MATCH => [
            'showingTime' => '타겟 제시 시간',
            'exampleCount' => '보기 개수'
        ],
        GameCode::MODEL => [
            'exampleCount' => '보기의 수',
            'problemShowingTime' => '타겟 제시 시간'
        ],
        GameCode::ORDER => [
            'imageCount' => '이미지 컷수'
        ],
        GameCode::TALK => [],
        GameCode::THIEF => [
            'moveLevel' => '이동 단계',
            'showingTime' => '타겟 제시시간',
            'exampleCount' => '보기개수'
        ],
        GameCode::TRE => [
            'treasureSize' => '보석크기',
            'treasureCountOnce' => '보석개수',
            'treasureShowingTime' => '제시시간',
            'alpha' => '투명도'
        ],

        GameCode::BADUM_TISH => [
            'beatCount' => '연주 수',
            'distanceBetweenBeats' => '연주 사이의 간격'
        ],
        GameCode::FIND_THE_MATCH => [
            'pairCount' => '쌍 개수',
            'availableFailCount' => '실패가능 횟수',
            'cardVisibleTime' => '카드제시시간'
        ],
        GameCode::HIT_THE_ROAD => [
            'exampleCount' => '객관식 보기의 수',
            'maxAnswerCount' => '정답 수(최대)'
        ],
        GameCode::IDENTICAL_PICTURE => [
            'userInputTime' => '유효반응 시간'
        ],
        GameCode::IS_IT_THERE => [
            'showWordCountOnce' => '한번에 제시되는 단어 개수 (= 보기개수)',
            'acceptAnswerCount' => 'O를 채우기 위해 맞춰야 하는 단어의 수',
            'showWordTime' => '단어 제시 시간'
        ],
        GameCode::LETS_GO_FISHING => [
            'fishCount' => '물고기 수',
            'showFishTime' => '물고기 제시 시간',
            'userReactTime' => '유효반응시간',
            'directionCount' => '방향 수'
        ],
        GameCode::NAVIGATE_ME => [
            'showBackgroundTime' => '배경색 제시 시간(초)',
            'userReactTime' => '유효 반응시간',
            'directionCount' => '화살표 방향'
        ],
        GameCode::STACK_THE_BRICKS => [
            'moveCount' => '움직임의 수',
            'brickCount' => '벽돌수'
        ],
        GameCode::TAP_TAP_TAP => [
            'numberCount' => '숫자 수',
            'dayCount' => '요일 수',
            'maxNumber' => '숫자 범위'
        ],
        GameCode::TO_EAT => [
            'showSushiTime' => '초밥 제시시간 (= 유효반응시간)',
            'eatPercent' => '먹기 비율',
            'throwPercent' => '버리기 비율'
        ],
        GameCode::UP_DOWN => [
            'flagCount' => '깃발 개수',
            'fakeCount' => 'fake 지시 개수',
        ],
        GameCode::WHATS_THE_NUMBER => [
            'numberCount' => '숫자개수',
            'showNumberTime' => '숫자 제시 시간',
            'showSignalTime' => '기호 제시 시간',
            'showPromptTime' => 'prompt 팝업 띄우는 시간(시행당)'
        ]
    ];

    /**
     * 게임별 레벨 한글 제목 리턴
     * ===
     * @param $app_code
     * @return array
     */
    public static function getKoreanTitleMap($app_code)
    {
        $common_array = [
            'level' => '레벨',
            'earnStarCount' => '별획득가능',
            'playCount' => '시행 수',
            'gameTime' => '제한시간',
            'promptTime' => '프롬트 제시시간 시작 (+2초)'
        ];

        return array_merge($common_array, static::LevelTitleMap[$app_code]);
    }
}
