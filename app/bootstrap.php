<?php
use Yface\Library\Configuration as Config;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/constants.php';

/**
 * Globally Configuration Load
 */
$config_path = __DIR__ . '/../configs/sites';
Config::load($config_path . '/default.json');

if (file_exists($config_path . '/prod.json')) {
    Config::load($config_path . '/prod.json');
} elseif (file_exists($config_path . '/local.dev.json')) {
    Config::load($config_path . '/local.dev.json');
}

/**
 * Install Issue Tracker (Rollbar)
 * ! 운영 환경에서 에러 핸들러에서의 처리는 그냥 알림이 가도록 놔둔다.
 */
if (isset(Config::$values['rollbar']['accessToken'])) {
    Rollbar::init(
        [
            'access_token' => Config::$values['rollbar']['accessToken'],
            'environment' => 'production'
        ],
        false,
        true
    );
}

/**
 * (Only Debug) Globally Error & Exception Handling
 * 디버그 모드일 경우 아래 로직이 상단에 있는 Error Reporting Handling 로직을 overwrite
 */
if (Config::$values['debug']) {
    ErrorHandler::register();
    ExceptionHandler::register();
}

/**
 * Application Folder Path Setting
 */
Config::$values['path']['resources'] = __DIR__ . '/../resources';
Config::$values['path']['views'] = __DIR__ . '/../views';
Config::$values['path']['lang'] = __DIR__ . '/../lang';

// PHP Setting
{
    mb_internal_encoding('UTF-8');
    mb_http_output('UTF-8');

    error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);

    date_default_timezone_set("Asia/Seoul");
}
