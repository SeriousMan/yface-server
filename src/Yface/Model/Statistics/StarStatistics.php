<?php
namespace Yface\Model\Statistics;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="Yface\DataStore\Statistics\StarStatisticsRepository")
 * @ORM\Table(name="yf_stat_stars")
 * @JMS\ExclusionPolicy("none")
 */
class StarStatistics
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", options={"unsigned":true})
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Exclude
     *
     * @var $idx integer
     */
    private $idx;
    /**
     * @ORM\Column(type="integer", length=3, options={"unsigned":true})
     *
     * @var $days integer
     */
    private $days;
    /**
     * @ORM\Column(type="float")
     * @JMS\SerializedName("avgValue")
     *
     * @var $avg_value float
     */
    private $avg_value;
    /**
     * @ORM\Column(type="string", columnDefinition="EUM('YFACE', 'YCOG')")
     * @JMS\Exclude
     *
     * @var $app_type string
     */
    private $app_type;

    /**
     * @return int
     */
    public function getIdx(): int
    {
        return $this->idx;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

    /**
     * @param int $days
     */
    public function setDays(int $days)
    {
        $this->days = $days;
    }

    /**
     * @return float
     */
    public function getAvgValue(): float
    {
        return $this->avg_value;
    }

    /**
     * @param float $avg_value
     */
    public function setAvgValue(float $avg_value)
    {
        $this->avg_value = $avg_value;
    }

    /**
     * @return string
     */
    public function getAppType(): string
    {
        return $this->app_type;
    }

    /**
     * @param string $app_type
     */
    public function setAppType(string $app_type)
    {
        $this->app_type = $app_type;
    }
}
