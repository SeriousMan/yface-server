<?php
namespace Yface\Service\User;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use Yface\DataStore\User\UserRepository;
use Yface\Exception\RegistrationException;
use Yface\Library\Validation\ParametersNotBlankValidator;
use Yface\Library\Validation\Validator;

class RegistrationValidationService
{
    /**
     * 회원 가입 전 입력값 유효 검사
     * ===
     * @param $id
     * @param $password
     * @param $email
     * @param $name
     * @param $nickname
     * @param $phone
     * @param $gender
     * @param $birth_date
     * @param $diagnosis
     * @param $guardian_password
     * @throws RegistrationException
     */
    public static function validate(
        $id,
        $password,
        $email,
        $name,
        $nickname,
        $phone,
        $gender,
        $birth_date,
        $diagnosis,
        $guardian_password
    )
    {
        static::validateAllParameters([
            $id,
            $password,
            $name,
            $email,
            $nickname,
            $phone,
            $gender,
            $birth_date,
            $diagnosis,
            $guardian_password
        ]);

        static::validateId($id);
        static::validatePassword($password);
        static::validateEmail($email);
        static::validateNickname($nickname);
        static::validatePhoneNumber($phone);
        static::validateGender($gender);
        static::validateBirthDate($birth_date);
        static::validateGuardianPassword($guardian_password);
    }

    /**
     * 빈 파라미터인지 유효 검사
     * ===
     * @param array $parameters
     * @throws RegistrationException
     */
    private static function validateAllParameters(array $parameters = [])
    {
        // 빈 파리미터 유효 검증
        $is_valid = ParametersNotBlankValidator::validate($parameters);
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.all.incorrect');
        }
    }

    /**
     * 아이디 유효 검사
     * ===
     * @param $id
     * @throws RegistrationException
     */
    public static function validateId($id)
    {
        static::assertIdLength($id);
        static::assertIdFormat($id);
        static::assertDuplicatedId($id);
    }

    /**
     * 아이디 최소, 최대 길이 검증
     * ===
     * @param $id
     * @throws RegistrationException
     */
    private static function assertIdLength($id)
    {
        $is_valid = Validator::validate($id, new Assert\Length(['min' => 4, 'max' => 15]));
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.id.length');
        }
    }

    /**
     * 영문 + 숫자, 영문으로 된 아이디인지 아이디 형태 검증
     * ===
     * @param $id
     * @throws RegistrationException
     */
    private static function assertIdFormat($id)
    {
        $is_valid = Validator::validate($id, new Assert\Regex([
            'pattern' => '/^[a-zA-Z]|[0-9_]$/i',
            'match' => true
        ]));
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.id.format.incorrect');
        }
    }

    /**
     * 아이디 중복 검증
     * ===
     * @param $id
     * @throws RegistrationException
     */
    private static function assertDuplicatedId($id)
    {
        $user = UserRepository::getRepository()->findOneById($id);
        if (!empty($user)) {
            throw new RegistrationException('user.sign.up.id.already.in.use');
        }
    }

    /**
     * 패스워드 유효 검사
     * ===
     * @param $password
     * @throws RegistrationException
     */
    public static function validatePassword($password)
    {
        // 패스워드 길이 검증
        $is_valid = Validator::validate($password, new Assert\Length(['min' => 4, 'max' => 20]));
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.password.length');
        }
    }

    /**
     * 이메일 유효 검사
     * ===
     * @param $email
     * @throws RegistrationException
     */
    public static function validateEmail($email)
    {
        // 이메일 형식 검증
        $invalidation_list = Validation::createValidator()->validate($email, new Assert\Email());
        if (count($invalidation_list) !== 0) {
            throw new RegistrationException('user.sign.up.email.incorrect');
        }
    }

    /**
     * 닉네임 유효 검사
     * ===
     * @param $nickname
     * @throws RegistrationException
     */
    public static function validateNickname($nickname)
    {
        static::assertNicknameLength($nickname);
        static::assertDuplicatedNickname($nickname);
    }

    /**
     * 닉네임 길이 유효 검증
     * ===
     * @param $nickname
     * @throws RegistrationException
     */
    private static function assertNicknameLength($nickname)
    {
        $is_valid = Validator::validate($nickname, new Assert\Length(['min' => 1, 'max' => 10]));
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.nickname.length');
        }
    }

    /**
     * 닉네임 중복 검증
     * ===
     * @param $nickname
     * @throws RegistrationException
     */
    private static function assertDuplicatedNickname($nickname)
    {
        $user = UserRepository::getRepository()->findOneByNickname($nickname);
        if (!empty($user)) {
            throw new RegistrationException('user.sign.up.nickname.already.in.use');
        }
    }

    /**
     * 휴대폰 번호 유효 검사
     * ===
     * @param $phone
     * @throws RegistrationException
     */
    private static function validatePhoneNumber($phone)
    {
        // 휴대폰 번호 형식 검증
        $is_valid = Validator::validate(
            $phone,
            new Assert\Regex([
                'pattern' => '/^[0]{1}\\d{8,10}$/i',
                'match' => true
            ])
        );
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.phone.incorrect');
        }
    }

    /**
     * 성별 유효 검사
     * ===
     * @param $gender
     * @throws RegistrationException
     */
    private static function validateGender($gender)
    {
        if ($gender != 'M' && $gender != 'F') {
            throw new RegistrationException('user.sign.up.sex.incorrect');
        }
    }

    /**
     * 생년월일 유효 검사
     * ===
     * @param $birth_date
     * @throws RegistrationException
     */
    private static function validateBirthDate($birth_date)
    {
        static::assertBirthDateFormat($birth_date);
        static::assertValidBirthDate($birth_date);
    }

    /**
     * 생년월일 입력 형식 검증
     * ===
     * @param $birth_date
     * @throws RegistrationException
     */
    private static function assertBirthDateFormat($birth_date)
    {
        $is_valid = Validator::validate(
            $birth_date,
            new Assert\Regex([
                'pattern' => '/^[0-9]{4}(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])$/i',
                'match' => true
            ])
        );
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.birthdate.incorrect');
        }
    }

    /**
     * 미래의 생년월일인지 검증
     * ===
     * @param $birth_date
     * @throws RegistrationException
     */
    private static function assertValidBirthDate($birth_date)
    {
        // 일자 계산을 위해 0시 0분 0초로 설정
        $now = (new \DateTime('now'))->setTime(0, 0, 0);
        $birth_date = (new \DateTime($birth_date))->setTime(0, 0, 0);

        if ($now <= $birth_date) {
            throw new RegistrationException('user.sign.up.birthdate.future');
        }
    }

    /**
     * 보호자 비밀번호 유효 검사
     * ===
     * @param $guardian_password
     * @throws RegistrationException
     */
    public static function validateGuardianPassword($guardian_password)
    {
        static::assertGuardianPasswordLength($guardian_password);
        static::assertGuardianPasswordNumberFormat($guardian_password);
    }

    /**
     * 보호자 비밀번호 길이 검증
     * ===
     * @param $guardian_password
     * @throws RegistrationException
     */
    private static function assertGuardianPasswordLength($guardian_password)
    {
        $is_valid = Validator::validate($guardian_password, new Assert\Length(['min' => 4, 'max' => 4]));
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.guardian.password.four.digit');
        }
    }

    /**
     * 보호자 비밀번호 입력 형식 검증
     * ===
     * @param $guardian_password
     * @throws RegistrationException
     */
    private static function assertGuardianPasswordNumberFormat($guardian_password)
    {
        $is_valid = Validator::validate(
            $guardian_password,
            new Assert\Regex([
                'pattern' => '/^[0-9]*$/',
                'match' => true
            ])
        );
        if (!$is_valid) {
            throw new RegistrationException('user.sign.up.guardian.password.should.number');
        }
    }
}
