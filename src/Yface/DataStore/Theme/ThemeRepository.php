<?php
namespace Yface\DataStore\Theme;

use Doctrine\ORM\EntityRepository;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Theme\Theme;

class ThemeRepository extends EntityRepository
{
    /**
     * @param $code
     * @return null|object|Theme
     */
    public function findOneByThemeCode($code) {
        return $this->findOneBy(array('code' => $code));
    }

    /**
     * @return ThemeRepository
     */
    public static function getRepository()
    {
        $em = EntityManagerProvider::getEntityManager();
        return $em->getRepository('Yface\Model\Theme\Theme');
    }
}
