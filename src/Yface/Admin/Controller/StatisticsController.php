<?php
namespace Yface\Admin\Controller;

use Kilte\Pagination\Pagination;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Yface\Admin\Library\UrlHelper;
use Yface\Admin\Statistics\Application;
use Yface\Admin\Statistics\Game;
use Yface\Admin\Statistics\Praise;
use Yface\Game\GameCode;
use Yface\Library\Database\ConnectionProvider;
use Yface\Library\Template\TwigRenderer;

class StatisticsController
{
    public function app(Request $request)
    {
        $research_idx = trim($request->get('research'));
        $app_type = trim($request->get('type', APP_TYPE_YFACE));

        if(!empty($research_idx)) {
            $db = ConnectionProvider::getConnection();
            $qb = $db->createQueryBuilder();
            $qb->select('*')
               ->from('yf_research')
               ->andWhere('idx = :idx')
               ->setParameter('idx', $research_idx);
            $research = $qb->execute()->fetch();
        }
        else {
            $research = array('title' => '전체');
        }

        $stats = Application::buildStat($app_type, $request->get('research'));
        $stats = array_merge($stats, [
            'app_type' => $app_type,
            'research' => $research
        ]);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes($stats);

        return $twig_renderer->render('admin/statistics/app');
    }

    public function appToExcel(Request $request)
    {
        $app_type = trim($request->get('type', APP_TYPE_YFACE));

        $stats = Application::buildStat($app_type, $request->get('research'));

        /** 엑셀로 추출하기 위한 작업 */
        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle(strtoupper($app_type) . ' Game Statistics Sheet');

        $row_offset = 1;
        $php_excel->setActiveSheetIndex(0)
            ->setTitle(strtoupper($app_type))
            ->fromArray([
                '전체 참가자 수 (비활성 유저 제외)',
                '설문 작성까지 완료한 참가자 수 (비활성 유저 제외)',
                '칭찬스티커를 사용하는 참가자 수 (비활성 유저 제외)',
                '전체 게임 평균 소요 시간',
                '공룡시대 선택 횟수',
                '루루공주 선택 횟수',
                '세계여행 선택 횟수',
                '로봇세상 선택 횟수',
                '베테랑 달성 평균 소요일수',
                '전문가 달성 평균 소요일수',
                '마스터 달성 평균 소요일수',
                '그랜드 마스터 달성 평균 소요일수',
                '하루 평균 게임 실행 횟수 (10일 이상 사용하지 않은 사용자 제외)',
                '평균 지속 일수 (10일 이상 사용하지 않은 사용자 제외)'
            ], null, 'A' . $row_offset++);

        $php_excel->getActiveSheet()->fromArray([
            $stats['total_user_count'],
            $stats['survey_completed_count'],
            $stats['praise_start_count'],
            $stats['user_themes'][0],
            $stats['user_themes'][1],
            $stats['user_themes'][2],
            $stats['user_themes'][3],
            $stats['avg_user_titles'][0],
            $stats['avg_user_titles'][1],
            $stats['avg_user_titles'][2],
            $stats['avg_user_titles'][3],
            $stats['avg_play_time'],
            $stats['avg_game_play_per_day'],
            $stats['avg_consecutive_game_play_day']
        ], null, 'A' . $row_offset);

        $file_name = strtolower($app_type) . '_app_' . date('Y-m-d');
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        header('Content-type: application/x-msexcel;charset=utf-8');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($php_excel, 'Excel5');
        $objWriter->save('php://output');

        return new RedirectResponse('/admin/statistics/app?type=' . $app_type);
    }

    public function game(Request $request)
    {
        $research_idx = trim($request->get('research'));
        $app_type = trim($request->get('app', APP_TYPE_YFACE));
        if (empty($app_type)) {
            return UrlHelper::returnWithMessage('잘못된 경로입니다.');
        }

        $sql = 'SELECT * FROM yf_game WHERE app_type = ? ORDER BY code ASC';

        $db = ConnectionProvider::getConnection();
        $games = $db->fetchAll($sql, [$app_type]);

        if(!empty($research_idx)) {
            $db = ConnectionProvider::getConnection();
            $qb = $db->createQueryBuilder();
            $qb->select('*')
               ->from('yf_research')
               ->andWhere('idx = :idx')
               ->setParameter('idx', $research_idx);
            $research = $qb->execute()->fetch();
        }
        else {
            $research = array('title' => '전체');
        }

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'app_type' => $app_type,
            'games' => $games,
            'research' => $research
        ]);

        return $twig_renderer->render('admin/statistics/game.list');
    }

    public function detailGame(Request $request)
    {
        $stats = Game::buildGame($request->get('game_code'), $request->get('research'));

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes(array_merge([
            'app_type' => GameCode::getGameAppType($game_code),
            'game_code' => $game_code,
            'games' => GameCode::ENG_NAMES
        ], $stats));

        return $twig_renderer->render('admin/statistics/game.detail');
    }

    public function detailGamesToExcel(Request $request)
    {
        $app_type = trim($request->get('app_type'));
        $research_idx = trim($request->get('research'));

        if (strtoupper($app_type) == APP_TYPE_YFACE) {
            $start = GameCode::CARD;
            $end = GameCode::TRE;
        } else {
            $start = GameCode::BADUM_TISH;
            $end = GameCode::WHATS_THE_NUMBER;
        }

        $titles = [];
        for ($i = $start; $i <= $end; $i++) {
            $titles[] = GameCode::ACRONYM[GameCode::ENG_NAMES[$i - 1]] . '정확도';
            if ($i == 3 || ($i >= 13 && $i <= 24)) {
                $titles[] = GameCode::ACRONYM[GameCode::ENG_NAMES[$i - 1]] . '무반응 제외 정답률';
            }
            $titles[] = GameCode::ACRONYM[GameCode::ENG_NAMES[$i - 1]] . '평균반응시간';
        }

        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle(strtoupper($app_type) . ' Game Statistics Sheet');

        $row_offset = 1;
        $php_excel->getActiveSheet()
            ->fromArray(array_merge([
                '아이디',
                '이름',
                '생년월일',
                '성별',
                '진단명',
                '훈련시작일',
            ], $titles), null, 'A' . $row_offset++);

        if(empty($research_idx)) {
            $sql = "SELECT * FROM yf_user WHERE {$app_type}_survey_completed_date IS NOT NULL ORDER BY idx DESC";
        }
        else {
            $sql = "SELECT * FROM yf_user u JOIN yf_user_research_status urs ON u.idx = urs.u_idx WHERE {$app_type}_survey_completed_date IS NOT NULL AND urs.r_idx = " . $research_idx . " ORDER BY u.idx DESC";
        }

        $db = ConnectionProvider::getConnection();
        $users = $db->fetchAll($sql);

        foreach ($users as $user) {
            $user_cell_values = [];
            for ($i = $start; $i <= $end; $i++) {
                $game_table_name = GameCode::getGameTableName($i);

                $sql = "SELECT avg(accuracy) as a_0, avg(avg_response_time) as a_1 FROM {$game_table_name} WHERE u_idx = ? AND is_disabled = 0";
                $result = $db->fetchAssoc($sql, [$user['idx']]);

                $user_cell_values[] = $result['a_0'];
                if ($i == 3) {
                    $sql = "SELECT avg(active_accuracy) FROM {$game_table_name} WHERE u_idx = ? AND is_disabled = 0";
                    $accuracy = $db->fetchColumn($sql, [$user['idx']]);

                    $user_cell_values[] = $accuracy;
                }

                if ($i >= 13 && $i <= 24) {
                    $sql = "SELECT avg(accuracy_except_no_response) FROM {$game_table_name} WHERE u_idx = ? AND is_disabled = 0";
                    $accuracy = $db->fetchColumn($sql, [$user['idx']]);

                    $user_cell_values[] = $accuracy;
                }

                $user_cell_values[] = $result['a_1'];
            }

            $start_date = new \DateTime($user["{$app_type}_survey_completed_date"]);

            $php_excel->getActiveSheet()
                ->fromArray(array_merge([
                    $user['id'],
                    $user['name'],
                    $user['birth_date'],
                    $user['gender'],
                    $user['diagnosis'],
                    $start_date->format('Y-m-d'),
                ], $user_cell_values), null, 'A' . $row_offset++);
        }

        $file_name = strtolower($app_type) . '_app_additional_stats_' . date('Y-m-d');
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        header('Content-type: application/x-msexcel;charset=utf-8');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($php_excel, 'Excel5');
        $objWriter->save('php://output');

        return new RedirectResponse('/admin/statistics/games?app=' . $app_type);
    }

    public function listUsersUsingPraise(Request $request)
    {
        $research_idx = $request->get('research');
        $app_type = trim($request->get('type', APP_TYPE_YFACE));
        $page = intval(trim($request->get('page', 1)));
        $offset = ($page - 1) * 20;

        $db = ConnectionProvider::getConnection();
        $qb = $db->createQueryBuilder();
        if(empty($research_idx)) {
            $qb->select('*')
                ->from('yf_user', 'u')
                ->orderBy('idx', 'DESC')
                ->setMaxResults(20)
                ->setFirstResult($offset);
        }
        else {
            $qb->select('*')
                ->from('yf_user', 'u')
                ->innerJoin('u', 'yf_user_research_status', 'urs', 'u.idx = urs.u_idx')
                ->andWhere('urs.r_idx = :r_idx')
                ->setParameter('r_idx', $research_idx)
                ->orderBy('u.idx', 'DESC')
                ->setMaxResults(20)
                ->setFirstResult($offset);
        }

        $users = $qb->execute()->fetchAll();

        $qb->select('count(u.idx)')
            ->setFirstResult(null)
            ->setMaxResults(null);

        $user_count = $qb->execute()->fetchColumn();

        if(!empty($research_idx)) {
            $db = ConnectionProvider::getConnection();
            $qb = $db->createQueryBuilder();
            $qb->select('*')
               ->from('yf_research')
               ->andWhere('idx = :idx')
               ->setParameter('idx', $research_idx);
            $research = $qb->execute()->fetch();
        }
        else {
            $research = array('title' => '전체');
        }

        $pagination = new Pagination($user_count, $page, 20);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes([
            'app_type' => $app_type,
            'users' => $users,
            'user_count' => $user_count,
            'page' => $page,
            'pagination' => $pagination->build(),
            'research' => $research
        ]);

        return $twig_renderer->render('admin/statistics/praise.index');
    }

    public function detailPraise($app_type, $user_idx)
    {
        $stats = Praise::buildUserPraise($user_idx, $app_type);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes(array_merge([
            'app_type' => $app_type
        ], $stats));

        return $twig_renderer->render('admin/statistics/praise.detail');
    }

    public function viewPraiseGraph($app_type, $user_idx)
    {
        $stats = Praise::buildUserPraise($user_idx, $app_type);

        $twig_renderer = TwigRenderer::getRenderer();
        $twig_renderer->setAttributes(array_merge([
            'app_type' => $app_type
        ], $stats));

        return $twig_renderer->render('admin/statistics/praise.graph');
    }

    /**
     * TODO: 요청 엑셀 데이터에 있는 유저 기본정보 추가해야하는 건가?
     *
     * @param $app_type
     * @param $user_idx
     *
     * @return RedirectResponse
     */
    public function exportPraise($app_type, $user_idx)
    {
        $stats = Praise::buildUserPraise($user_idx, $app_type);

        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle($stats['user']['name'] . " {$app_type} Praise Statistics Sheet");

        $php_excel->getActiveSheet()
            ->setTitle('칭찬 스티커');

        $row_offset = 1;
        $php_excel->getActiveSheet()
            ->fromArray([
                '아이디',
                '이름',
                '생년월일',
                '성별',
                '진단명',
                '훈련시작일',
                '시작일',
                '주차',
                '일자',
                '행동1',
                '행동2',
                '행동3',
                '행동1_점수',
                '행동2_점수',
                '행동3_점수',
                '행동총점',
                '게임총점',
                '하루총점',
                '주총점',
                '목표점수',
                '보상내용',
                '보상여부',
            ], null, 'A' . $row_offset++);

        $praise_start_date = new \DateTime($stats['user'][$app_type . '_praise_start_date']);
        $start_date = new \DateTime($stats['user'][$app_type . '_survey_completed_date']);

        $php_excel->getActiveSheet()
            ->fromArray([
                $stats['user']['id'],
                $stats['user']['name'],
                $stats['user']['birth_date'],
                $stats['user']['gender'],
                $stats['user']['diagnosis'],
                $start_date->format('Y-m-d'),
                $praise_start_date->format('Y-m-d')
            ], null, 'A' . $row_offset);

        foreach ($stats['praises'] as $praise) {
            $temp_offset = $row_offset;

            $start = $temp_offset;
            $end = $temp_offset + 6;

            $php_excel->getActiveSheet()
                ->mergeCells("H{$start}:H{$end}");
            $php_excel->getActiveSheet()
                ->setCellValue("H{$start}", $praise['weeks'] . '주차')
                ->getStyle("H{$start}")
                ->getAlignment()
                ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $week_point = 0;
            for ($i = $start; $i <= $end; $i++) {
                $modified_day = (($praise['weeks'] - 1) * 7) + ($i - $start);
                $start_date = new \DateTime($stats['user'][$app_type . '_praise_start_date']);
                $start_date->modify("+{$modified_day} days");

                $promise_total_point = intval($stats['promises'][$praise['weeks']][0]['day_' . ($i - $start + 1)]);
                $promise_total_point += intval($stats['promises'][$praise['weeks']][1]['day_' . ($i - $start + 1)]);
                $promise_total_point += intval($stats['promises'][$praise['weeks']][2]['day_' . ($i - $start + 1)]);

                $game_total_point = intval($stats['game_scores_by_week'][$praise['weeks']][$i - $start]);
                $day_total_point = intval($promise_total_point) + $game_total_point;
                $week_point += intval($day_total_point);

                $php_excel->getActiveSheet()
                    ->setCellValue("I{$i}", $start_date->format('Y-m-d'))
                    ->setCellValue("J{$i}", $stats['promises'][$praise['weeks']][0]['title'])
                    ->setCellValue("K{$i}", $stats['promises'][$praise['weeks']][1]['title'])
                    ->setCellValue("L{$i}", $stats['promises'][$praise['weeks']][2]['title'])
                    ->setCellValue("M{$i}", $stats['promises'][$praise['weeks']][0]['day_' . ($i - $start + 1)])
                    ->setCellValue("N{$i}", $stats['promises'][$praise['weeks']][1]['day_' . ($i - $start + 1)])
                    ->setCellValue("O{$i}", $stats['promises'][$praise['weeks']][2]['day_' . ($i - $start + 1)])
                    ->setCellValue("P{$i}", $promise_total_point)
                    ->setCellValue("Q{$i}", $game_total_point)
                    ->setCellValue("R{$i}", $day_total_point)
                    ->setCellValue("S{$i}", $week_point);
            }

            // 목표 점수
            $php_excel->getActiveSheet()
                ->mergeCells("T{$start}:T{$end}");
            $php_excel->getActiveSheet()
                ->setCellValue("T{$start}", $praise['goal_score'])
                ->getStyle("T{$start}")
                ->getAlignment()
                ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            // 보상 내용
            $php_excel->getActiveSheet()
                ->mergeCells("U{$start}:U{$end}");
            $php_excel->getActiveSheet()
                ->setCellValue("U{$start}", $praise['present'])
                ->getStyle("U{$start}")
                ->getAlignment()
                ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            // 보상 여부
            $is_achieved = $praise['goal_score'] <= $stats['total_scores_by_week'][$praise['weeks']];

            $php_excel->getActiveSheet()
                ->mergeCells("V{$start}:V{$end}");
            $php_excel->getActiveSheet()
                ->setCellValue("V{$start}", ($is_achieved) ? 'Y' : 'N')
                ->getStyle("V{$start}")
                ->getAlignment()
                ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $row_offset = $end + 1;
        }

        $file_name = $user_idx . '_' . strtolower($app_type) . '_praise_stats_' . date('Y-m-d');
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        header('Content-type: application/x-msexcel;charset=utf-8');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($php_excel, 'Excel5');
        $objWriter->save('php://output');

        return new RedirectResponse('/admin/statistics/praises/' . $app_type . '/' . $user_idx);
    }

    /**
     * @param $app_type
     *
     * @return RedirectResponse
     */
    public function exportPraiseSummary(Request $request)
    {
        $app_type = trim($request->get('app_type'));
        $research_idx = trim($request->get('research'));

        $stats = Praise::buildPraiseSummary($app_type, $research_idx);

        $php_excel = new \PHPExcel();
        $php_excel->getProperties()->setCreator('Yonsei Univ. Behavioral Psychology Lab')
            ->setTitle("{$app_type} Praise Statistics Sheet");

        $titles = [];
        for ($i = 1; $i <= $stats['maximum_weeks']; $i++) {
            $titles[] = "목표점수_{$i}주차";
            $titles[] = "총점수_{$i}주차";
        }

        $row_offset = 1;
        $php_excel->getActiveSheet()
            ->fromArray(array_merge([
                '아이디',
                '이름',
                '생년월일',
                '성별',
                '진단명',
                '훈련시작일',
                '시작일',
                '목표달성율(누적)',
            ], $titles), null, 'A' . $row_offset++);

        foreach ($stats['users'] as $user) {
            $user_cell_values = [];

            foreach($stats['praise_summary_by_users'][$user['idx']]['total_scores_by_week'] as $week => $value) {
                $user_cell_values[] = $stats['praise_summary_by_users'][$user['idx']]['goal_scores_by_week'][$week];
                $user_cell_values[] = $value;
            }

            $start_date = new \DateTime($user["{$app_type}_survey_completed_date"]);
            $praise_start_date = new \DateTime($user["{$app_type}_praise_start_date"]);

            $php_excel->getActiveSheet()
                ->fromArray(array_merge([
                    $user['id'],
                    $user['name'],
                    $user['birth_date'],
                    $user['gender'],
                    $user['diagnosis'],
                    $start_date->format('Y-m-d'),
                    $praise_start_date->format('Y-m-d'),
                    $stats['praise_summary_by_users'][$user['idx']]['rate_of_achieved']
                ], $user_cell_values), null, 'A' . $row_offset++);
        }

        $file_name = strtolower($app_type) . '_praise_stats_' . date('Y-m-d');
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        header('Content-type: application/x-msexcel;charset=utf-8');
        header('Content-Disposition: attachment;filename="' . $file_name . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = \PHPExcel_IOFactory::createWriter($php_excel, 'Excel5');
        $objWriter->save('php://output');

        return new RedirectResponse('/admin/statistics/praises/' . $app_type);
    }
}
