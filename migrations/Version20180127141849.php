<?php declare(strict_types = 1);

namespace YFACE\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180127141849 extends AbstractMigration
{
    // 연구 테이블을 생성하는 마이그레이션입니다.
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('
          CREATE TABLE `yf_research` (
          `idx` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `title` varchar(30) NOT NULL,
          `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`idx`),
          `mod_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
          ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
        ');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('
          DROP TABLE `yf_research`;
        ');
    }
}
