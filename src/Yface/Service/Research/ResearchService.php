<?php
namespace Yface\Service\Research;

use Doctrine\ORM\EntityManager;
use Yface\DataStore\Research\ResearchRepository;
use Yface\Library\Configuration;
use Yface\Library\Database\EntityManagerProvider;
use Yface\Model\Research\Research;
use Doctrine\Common\Collections\ArrayCollection;

class ResearchService {
    /**
     * 연구 그룹 등록
     * ===
     * @param $title
     * @throws RegistrationException
     *
     * @return Research
     */
    public static function create($title) {
        // TODO: validation
        // RegistrationValidationService::validate(
        //     $name,
        // );

        $research = new Research();
        $research->setTitle($title);

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($research) {
                $em->persist($research);
                $em->flush($research);
            }
        );

        return $research;
    }

    /**
     * 연구 그룹 수정
     * ===
     * @param $research_idx
     * @param $research_title
     * @throws RegistrationException
     *
     * @return Research
     */
    public static function update($research_idx, $research_title) {
        $research = ResearchRepository::getRepository()->findOneByResearchIdx($research_idx);
        if (empty($research)) {
            //TODO
            // throw new UserException('user.please.enter.correctly');
        }
        $research->setTitle($research_title);

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($research) {
                $em->persist($research);
                $em->flush($research);
            }
        );

        return $research;
    }

    /**
     * 연구 그룹 삭제
     * ===
     * @param $research_idx
     * @throws RegistrationException
     *
     * @return Research
     */
    public static function delete($research_idx) {
        $research = ResearchRepository::getRepository()->findOneByResearchIdx($research_idx);
        if (empty($research)) {
            //TODO
            // throw new UserException('user.please.enter.correctly');
        }

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($research) {
                $em->remove($research);
                $em->flush($research);
            }
        );

        return $research;
    }

    /**
     * 연구 그룹 사용자 수정
     * ===
     * @param $body
     * @throws RegistrationException
     *
     * @return Research
     */
    public static function updateUsers($body) {
        $research = ResearchRepository::getRepository()->findOneByResearchIdx($body['research_idx']);
        if (empty($research)) {
            //TODO
            // throw new UserException('user.please.enter.correctly');
        }
        foreach($body['user_idxes_added'] as $user_idx) {
            $research->addUserResearchStatus($user_idx);
        }
        foreach($body['user_idxes_removed'] as $user_idx) {
            $research->removeUserResearchStatus($user_idx);
        }

        $em = EntityManagerProvider::getEntityManager();
        $em->transactional(
            function (EntityManager $em) use ($research) {
                $em->persist($research);
                $em->flush($research);
            }
        );
        // FIXME: 테스트 코드
        return $research;
    }
}
