<?php
namespace Yface\Game\ResultData\Ycog;

use Yface\Game\GameCode;
use Yface\Game\ResultData;

/**
 * CODE [20] - STACK_THE_BRICKS_벽돌을 착착착
 */
class GameStackTheBricks extends ResultData
{
    protected $answer_move_count;
    protected $user_move_count_list;
    protected $average_move_count;
    protected $accuracy_except_no_response;

    public function __construct($results)
    {
        parent::__construct(GameCode::STACK_THE_BRICKS, $results);

        /** 게임 정보 저장 */
        $this->answer_move_count = $results['answerMoveCount'];
        $this->user_move_count_list = implode('|', $results['userMoveCountList']);
        $this->average_move_count = $results['averageMoveCount'];
        $this->accuracy_except_no_response = $results['accuracyExceptNoResponse'];
    }

    /**
     * @return array
     */
    public function export()
    {
        $arr = parent::export();

        return array_merge($arr, [
            'answer_move_count' => $this->answer_move_count,
            'user_move_count_list' => $this->user_move_count_list,
            'average_move_count' => $this->average_move_count,
            'accuracy_except_no_response' => $this->accuracy_except_no_response
        ]);
    }
}
