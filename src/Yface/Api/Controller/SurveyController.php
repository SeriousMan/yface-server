<?php
namespace Yface\Api\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Yface\Exception\SurveyException;
use Yface\Library\Localization\Lang;
use Yface\Library\Serializer;
use Yface\Service\Survey\SurveyService;
use Yface\Service\User\UserService;

class SurveyController
{
    public function index(Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));

        $survey_list = SurveyService::getUserSurveyList($user_idx, $app_type);

        $json_string = (new Serializer())->serializeWithJson($survey_list);

        return new Response($json_string, 200, ['Content-Type' => 'application/json']);
    }

    public function save(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $survey_idx = intval(trim($request->request->get('surveyIdx')));
        $answers = $request->request->get('answers');
        $app_type = trim($request->headers->get('app'));

        try {
            SurveyService::saveSurveyAnswers($user_idx, $survey_idx, $answers, $app_type);
            $user = UserService::getUserWithUserIdx($user_idx, $app_type);

            $is_pre_survey_completed = $user->getPreSurveyCompleted();
            $survey_completed_date = $user->getSurveyCompletedDate();
            if (!empty($survey_completed_date)) {
                $survey_completed_date = $survey_completed_date->format('Y-m-d');
            }

            return $app->json([
                'isPreSurveyCompleted' => $is_pre_survey_completed,
                'surveyCompletedDate' => $survey_completed_date
            ]);
        } catch (SurveyException $se) {
            return $app->json(['message' => Lang::get($se->getMessage())], 400);
        }
    }

    public function getResults(Application $app, Request $request)
    {
        $user_idx = intval(trim($request->headers->get('Y-Face-User-Idx')));
        $app_type = trim($request->headers->get('app'));

        try {
            $survey_result = SurveyService::getUserSurveyResults($user_idx, $app_type);

            return $app->json($survey_result);
        } catch (SurveyException $se) {
            return $app->json(['message' => Lang::get($se->getMessage())], 400);
        }
    }
}
